import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";

const firebaseConfig = {
  // apiKey: "AIzaSyCPb5kDOrk0WrPUfEB6PcXom7-E3hY78JY",
  // authDomain: "eventeer-ae9ec.firebaseapp.com",
  // databaseURL: "https://eventeer-ae9ec-default-rtdb.firebaseio.com",
  // projectId: "eventeer-ae9ec",
  // storageBucket: "eventeer-ae9ec.appspot.com",
  // messagingSenderId: "983377049571",
  // appId: "1:983377049571:web:153ebb29c01e5c3f1892d6",
  // measurementId: "G-QRYDKPBV4Y",
};

const app = initializeApp(firebaseConfig);

export const analytics = () => {
  if (typeof window !== "undefined") {
    return getAnalytics(app);
  } else {
    return null
  }
}
