import axios from "axios";
import env from "./env";
import { getAuth, getToken } from "./helper";

const auth = getAuth();

const defaultHeaders = {
  // Authorization: `Bearer ${token}`,
  // Authorization: `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjkwNzcxIiwiZGV2aWNlIjpudWxsLCJ0aW1lc3RhbXAiOjE2NDg3ODMzNzV9.SYTbDq15Y9v7nsA_k4DXBrizuZAWua2OYecfEZ11h3s`,
  // Authorization: `Bearer ${getToken()}`,
  Authorization: env.apiAuth,
  "X-API-KEY": env.apiKey,
  // "Content-Type": "application/x-www-form-urlencoded"
};

const clientHttp = axios.create({
  baseURL: env.apiUrl,
  timeout: 15000,
  headers: defaultHeaders,
});

export { defaultHeaders, clientHttp };
