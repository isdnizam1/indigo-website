import { getAuth, isDev } from "./helper";

const { token } = getAuth();

const api = {
  url: {
    development: "https://apidev.indigoconnect.id",
    production: "https://api.indigoconnect.id",
  },
  webUrl: {
    development: "https://dev.indigoconnect.id",
    production: "https://indigoconnect.id",
  },
  auth: "Bearer " + token,
  key: "bebf128669be148fb1a536656f4de8faeb8bffa0",
};

const ENV = {
  dev: {
    apiUrl: api.url.development,
    apiAuth: api.auth,
    apiKey: api.key,
    webUrl: api.webUrl.development,
  },
  prod: {
    apiUrl: api.url.production,
    apiAuth: api.auth,
    apiKey: api.key,
    webUrl: api.webUrl.production,
  },
};

export default ENV[isDev() ? "dev" : "prod"];
