import Cookies from "universal-cookie";
import CryptoAES from "crypto-js/aes";
import CryptoENC from "crypto-js/enc-utf8";
import { parse, stringify, toJSON, fromJSON } from "flatted";
import { isNil, isEmpty } from "lodash-es";
import Router from "next/router";
import ReactHtmlParser from "react-html-parser";
import swal from "sweetalert";

const cookies = new Cookies();

export const isEmailValid = (email) => {
  // eslint-disable-next-line no-useless-escape
  const re =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

export const isDev = () => {
  const environmentMode = process.env.NODE_ENV;
  if (environmentMode === "development") return true;
  return false;
};

export const encrypt = (params) => {
  let encrypted = CryptoAES.encrypt(JSON.stringify(params), "uh0zo9DjSWtMjF9oU6i5");
  return encrypted;
};

export const decrypt = (params) => {
  let decrypted = JSON.parse(
    CryptoAES.decrypt(params, "uh0zo9DjSWtMjF9oU6i5").toString(CryptoENC)
  );
  return decrypted;
};

export const setAuth = (params) => {
  return new Promise((resolve) => {
    const encryptedData = stringify(encrypt(params));
    cookies.set("indigo_user", encryptedData, { path: "/" });
    resolve(getAuth());
  });
};

export const getAuth = () => {
  const encryptedData = cookies.get("indigo_user");
  if (!isNil(encryptedData)) {
    const parsedData = fromJSON(encryptedData);
    return decrypt(parsedData);
  } else return {};
};

export const removeAuth = () => {
  cookies.remove("indigo_user", { path: "/" });
  // cookies.remove("eventeer_token", { path: "/" });
  localStorage.clear();
};

// export const setToken = (token) => {
//   cookies.set("eventeer_token", token, { path: "/" });
// };

// export const getToken = () => {
//   return cookies.get("eventeer_token");
// };

export const isLogin = () => {
  const property = [
    "id_user",
    "email",
    "registration_step",
    "event_title",
    "token",
  ];

  if (!isEmpty(getAuth())) {
    for (let i = 0; i < property.length; i++) {
      if (!getAuth().hasOwnProperty(property[i])) {
        removeAuth();
        return false;
      }
    }
    return true;
  } else return false;
};

export const convertToBase64 = (file) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });
};

export const dateFormatter = (dateParam, isFullNameOfMonth) => {
  const monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sept",
    "Oct",
    "Nov",
    "Dec",
  ];
  const fullMonthNames = [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember",
  ];
  const date = new Date(
    dateParam?.length > 12 ? dateParam.slice(0, 10) : dateParam
  );
  const day = date.getDate();
  const month = !isFullNameOfMonth ? monthNames[date.getMonth()] : fullMonthNames[date.getMonth()];
  const year = date.getFullYear();
  const formattedDate = `${day} ${month} ${year}`;
  return formattedDate;
};

export const capitalizeFirstLetter = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

export const getWindowDimensions = () => {
  // const { innerWidth: width, innerHeight: height } = window;
  const width = 1440;
  const height = 800;

  return { width, height };
};

export const authVerifier = (props) => {
  return new Promise((resolve) => {

    if (!isEmpty(getAuth())) {
      const { id_user, email, registration_step, event_title, token } =
        getAuth();

      if (id_user && email && registration_step && token) {
        if (registration_step !== "finish") {
          /* logged in & registration not yet completed */
          const authNavigator = {
            1: "/register/profile",
            3: "/register/profession",
            4: "/register/interest",
          };
          Router.push(authNavigator[registration_step]);
        } else {
          /* logged in, do something */
          if (isEmpty(event_title)) {
            Router.push("/event-selection");
          }
        }
      } else {
        /* cookie data fail, reset cookie & logout */
        removeAuth();
        Router.push("/");
      }
    }
  });
};

export const logout = () => {
  swal({
    title: "Apakah anda yakin ingin keluar?",
    text: "Anda dapat login kembali untuk masuk",
    icon: "warning",
    buttons: ["Batal", "Keluar"],
    dangerMode: true,
  }).then((isLoggedOut) => {
    if (isLoggedOut) {
      swal("Anda berhasil keluar", {
        icon: "success",
      }).then(() => {
        removeAuth();
        Router.push("/login");
      });
    }
  });
};

export const formatDate = (createdAt) => {
  if (!createdAt) {
    return null;
  }

  const date = typeof dateParam === "object" ? createdAt : new Date(createdAt);
  const DAY_IN_MS = 86400000; // 24 * 60 * 60 * 1000
  const today = new Date();
  const seconds = Math.round((today - date) / 1000);
  const minutes = Math.round(seconds / 60);
  const hours = Math.round(minutes / 60);
  const days = Math.round(hours / 24);
  const months = Math.round(days / 30);
  const years = Math.round(months / 12);

  if (seconds < 5) {
    return "now";
  } else if (seconds < 60) {
    return `${seconds} seconds ago`;
  } else if (seconds < 90) {
    return "about a minute ago";
  } else if (minutes < 60) {
    return `${minutes} minutes ago`;
  } else if (hours < 24) {
    return `${hours} hours ago`;
  } else if (days < 30) {
    return `${days} days ago`;
  } else if (months < 12) {
    return `${months} months ago`;
  }
  return `${years} years ago`;
};

export const dayFormatter = (dateParam) => {
  const monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  const date = new Date(dateParam.slice(1, 11));
  const dayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  const day = dayNames[date.getDay()];

  const tanggal = dateParam.slice(9, 11);
  const month = monthNames[date.getMonth()];
  const year = date.getFullYear();
  const hour = dateParam.slice(12, 17);

  const formatDay = `${day}, ${tanggal} ${month} ${year} | ${hour} WIB`;
  return formatDay;
};

export const webinarFormatter = (dateParam) => {
  const monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  const date = new Date(dateParam.slice(0, 10));
  const dayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  const day = dayNames[date.getDay()];

  const tanggal = dateParam.slice(8, 10);
  const month = monthNames[date.getMonth()];
  const year = date.getFullYear();
  const hour = dateParam.slice(11, 16);
  const formatDay = `${day}, ${tanggal} ${month} ${year} | ${hour} WIB`;
  return formatDay;
};

export const monthFormatter = (dateParam) => {
  const monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sept",
    "Oct",
    "Nov",
    "Dec",
  ];
  const date = new Date(dateParam.slice(1, 11));
  const tanggal = dateParam.slice(9, 11);
  const month = monthNames[date.getMonth()];
  const formatDay = `${month} ${tanggal}`;
  return formatDay;
};

export const convertToPrice = (price) => {
  let numberString = price.toString();
  let split = numberString.split(",");
  let remainder = split[0].length % 3;
  let rupiah = split[0].substr(0, remainder);
  let thousand = split[0].substr(remainder).match(/\d{3}/gi);

  // add a dot if the input is already a thousand numbers
  if (thousand) {
    let separator = remainder ? "." : "";
    rupiah += separator + thousand.join(".");
  }

  rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;

  return rupiah;
};

export const messageFormatter = (createdAt) => {
  const date = typeof dateParam === "object" ? createdAt : new Date(createdAt);
  const today = new Date();
  const seconds = Math.round((today - date) / 1000);
  const minutes = Math.round(seconds / 60);
  const hours = Math.round(minutes / 60);
  const monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  const tanggal = createdAt.slice(0, 10);
  const tanggalaja = createdAt.slice(8, 10);
  const month = monthNames[date.getMonth()];
  const year = date.getFullYear();

  if (hours < 24) {
    return `Today`;
  } else if (hours > 24 && hours < 48) {
    return `Yesterday`;
  } else {
    return `${tanggalaja} ${month} ${year}`;
  }
};

export const detGroupFormatter = (createdAt) => {
  const date = typeof dateParam === "object" ? createdAt : new Date(createdAt);
  const today = new Date();
  const seconds = Math.round((today - date) / 1000);
  const minutes = Math.round(seconds / 60);
  const hours = Math.round(minutes / 60);
  const monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  const tanggalaja = createdAt.slice(8, 10);
  const month = monthNames[date.getMonth()];
  const year = date.getFullYear();
  const groupcreate = `${tanggalaja} ${month} ${year}`;

  return groupcreate;
};

export const dateTimeFormatter = (dateTime) => {
  let yyyy = dateTime.getFullYear();
  let mm = dateTime.getMonth() + 1; // getMonth() is zero-based
  let dd = dateTime.getDate();
  let h = dateTime.getHours();
  let m = dateTime.getMinutes();
  let s = dateTime.getSeconds();

  const date = [yyyy, (mm > 9 ? "" : "0") + mm, (dd > 9 ? "" : "0") + dd].join(
    "-"
  );

  const hour = [
    (h > 9 ? "" : "0") + h,
    (m > 9 ? "" : "0") + m,
    (s > 9 ? "" : "0") + s,
  ].join(":");

  return [date, hour].join(" ");
};

export const dispatcher = (action, params = {}, allowError = false) => {
  return new Promise((resolve, reject) => {
    action(params)
      .then(({ data }) => {
        const errorCode = Number(data.code);

        if (allowError) resolve(data);
        else if (errorCode < 400 && errorCode >= 200) {
          resolve(data.result);
        } else {
          // show error
        }
      })
      .catch((err) => {
        if (err.response) {
          // client received an error response (5xx, 4xx)
        } else if (err.request) {
          // client never received a response, or request never left
        } else {
          // anything else
        }
      });
  });
};

export const apiDispatch = (
  action,
  data = {},
  allowError = false,
  beforeLoaded = async (dataResponse) => {},
  isLoading = true
) => {
  return new Promise((resolve, reject) => {
    dispatcher(action, data, allowError)
      .then(async (dataResponse) => {
        resolve(dataResponse);
      })
      .catch((e) => {
        if (e.response) {
          // do something for error response cause
        } else {
          // do something for else error
        }
        resolve({ result: [] });
      });
  });
};

export const normalizePhoneNumber = (phoneNumberString) => {
  let cleaned = ('' + phoneNumberString).replace(/\D/g, '');
  let match = cleaned.match(/(?:\+62)?8\d{2}(\d{6,9})/);
  if(match) {
    let intlCode = '+62'
    return [intlCode, match[0]].join('');
  }
  return null;
}

export const stringHtmlMentionParser = (stringHtml, highlightColor, pathname, push) => {
  const html = stringHtml?.split("\n").join("<br>");

  return ReactHtmlParser(html, {
    transform: (node, idx) => {
      const { name, type, attribs, children } = node;

      if (name === "mention" && type === "tag") {
        const userName = children[0].data;
        const { id } = attribs;

        return (
          <span
            key={idx}
            className={`color-${highlightColor} cursor-pointer`}
            onClick={(e) => {
              e.preventDefault();

              push({
                pathname,
                query: { id },
              });
            }}
          >
            {`@${userName}`}
          </span>
        );
      }
    },
  });
};

export const random = () => Math.random().toString(16).slice(2, 12);
