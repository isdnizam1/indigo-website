/* @flow */

import moment from "moment/min/moment-with-locales";
import { compact, isEmpty } from "lodash-es";
import * as yup from "yup";

// API_DATA to COLLABORATION_DATA
export const MAPPER_FROM_API = {
  id: "id_ads",
  title: "title",
  description: "description",
  professions: ["additional_data.profession", { name: "name", notes: "notes" }],
  locationName: "additional_data.location.name",
  bannerPictureUri: "image",
  bannerPictureb64: "bannerPictureb64",
  specification: "additional_data.specification",
  postDuration: "additional_data.post_duration",
  collaborationReference: "additional_data.collaboration_reference",
  daysRemaining: "day_left",
  createdBy: "created_by",
  createdAt: (source) =>
    moment(source.created_at).locale("id").fromNow(false).replace("yang ", ""),
  updatedBy: "updated_by",
  updatedAt: "updated_at",
  isNew: (source) => moment().diff(moment(source.update_at), "days") <= 3,
  // Author Profile
  authorPicture: "profile_picture",
  authorId: "id_user",
  authorJobTitle: "job_title",
  authorCompany: "company",
  authorCityName: "location.city_name",
  authorIsPremium: "is_premium",
  comment: "comment",
  totalComment: (source) => parseInt(source.total_comment),
  joined: "joined",
  members: "members.data",
  shortlist: "shortlist",
  memberQuota: "additional_data.memberQuota",
  idGroupMessage: "id_groupmessage",
  start: "additional_data.date.start",
  end: "additional_data.date.end",
};

// COLLABORATION_DATA to API_DATA
export const MAPPER_FOR_API = {
  title: "title",
  description: "description",
  profession: "professions",
  location_name: "locationName",
  banner_picture: "bannerPictureb64",
  spesification: "specification",
  post_duration: "postDuration",
  // startDate: "startDate",
  // endDate: "endDate",
  startDate: "start",
  endDate: "end",
  memberQuota: "memberQuota",
  collaboration_reference: "collaborationReference",
};

// COLLABORATION_DATA to API_DATA
export const MAPPER_FOR_FEEDS = {
  name: "name",
};

// export type Collaboration = {
//   id: string | number,
//   title: string,
//   description: string,
//   professions: Array,
//   comment: Array,
//   total_comment: string | number,
//   locationName: string,
//   bannerPictureUri: string,
//   bannerPictureb64: string,
//   specification: string,
//   postDuration: string,
//   collaborationReference: string,
//   daysRemaining: string,
//   createdBy: string,
//   createdAt: string,
//   updatedBy: string,
//   updatedAt: string,
//   isNew: boolean,
//   authorPicture: string,
//   authorId: string | number,
//   authorJobTitle: string,
//   authorCompany: string,
//   authorCityName: string,
//   authorIsPremium: boolean,
// };

// export type PropsType = {
//   initialLoaded?: boolean,
//   id?: number | string,
//   previewData?: Collaboration,
// };

// export type StateType = {
//   isReady?: boolean,
//   collaboration?: Collaboration,
//   hasPreviewData?: boolean,
// };
