import * as Yup from "yup";

export const EditProfileSchema = Yup.object().shape({
  full_name: Yup.string()
    // .matches(/^[a-zA-Z., ]*$/, "Only alphabets are allowed for this field")
    .max(25, "Must be 25 characters or less")
    .required("Required"),
  about_me: Yup.string().max(160, "Must be 160 characters or less"),
  job_title: Yup.string()
    .max(20, "Must be 20 characters or less")
    .matches(/^[a-zA-Z., ]*$/, "Only alphabets are allowed for this field"),
  social_media: Yup.object().shape({
    instagram: Yup.string().matches(
      /^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/,
      "URL invalid"
    ),
    github: Yup.string().matches(
      /^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/,
      "URL invalid"
    ),
    email: Yup.string().email("Invalid email address").required("Required"),
    youtube: Yup.string().matches(
      /^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/,
      "URL invalid"
    ),
    website: Yup.string().matches(
      /^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/,
      "URL invalid"
    ),
    other: Yup.string().matches(
      /^https?:\/\/(?:www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b(?:[-a-zA-Z0-9()@:%_\+.~#?&\/=]*)$/,
      "URL invalid"
    ),
  }),
});

export const evaluateFormCertificateSchema = Yup.object().shape({
  startup_level: Yup.string().required("Required"),
  topik_acara: Yup.string().required("Required"),
  pengisi_acara: Yup.string().required("Required"),
  fasilitas_acara: Yup.string().required("Required"),
  topik_berikutnya: Yup.string().required("Required"),
  // ikut_program_indigo: Yup.array().required("Required"),
  saran: Yup.string().required("Required"),
});

export const adminAskSchema = Yup.object().shape({
  admin: Yup.string().required("Required"),
  message: Yup.string().required("Required"),
});
