import { isDev } from "/services/utils/helper";

export const REGISTRATION_ROUTES = {
  waiting: "/register/email-verification",
  1: "/register/profile",
  2: "/register/profile",
  3: "/register/profession",
  4: "/register/interest",
  finish: "/register/success",
};

export const MAPPER_FEEDS_EVENT_DATA = {
  collaboration: "Collaboration",
  sc_session: "Agenda",
};

export const MQTT_SUBSCRIPTION = isDev() ? "dev-indigo-message" : "indigo-message";
