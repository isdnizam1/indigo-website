import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { getWindowDimensions } from "/services/utils/helper";

const dispatcher = (action, params = {}, allowError = false) => new Promise((resolve, reject) => {
  action(params).then(({ data }) => {
    const errorCode = Number(data.code);

    if (allowError) resolve(data);
    else if (errorCode < 400 && errorCode >= 200) {
      resolve(data.result);
    }
    else {
      // show error
    }
  }).catch((err) => {
    if (err.response) {
      // client received an error response (5xx, 4xx)
    } else if (err.request) {
      // client never received a response, or request never left
    } else {
      // anything else
    }
  });
});

const useWindowDimensions = () => {
  const [windowDimensions, setWindowDimensions] = useState(
    getWindowDimensions()
  );

  useEffect(() => {
    function handleResize() {
      setWindowDimensions(getWindowDimensions());
    }

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);


  // const isMobile = windowDimensions.width < 576;
  // const isTablet = windowDimensions.width < 992;

  const isMobile = windowDimensions.width < 576;
  const isTablet = {
    portrait: windowDimensions.width < 992,
    landscape: windowDimensions.width < 1200,
  };
  const isDesktop = windowDimensions.width < 1400;

  const width = windowDimensions.width;
  const height = windowDimensions.height;

  return { width, height, isMobile, isTablet, isDesktop };
};

const Enhance = (Component) => {
  const responsiveDimension = (Component) => {
    return function ComposedComponent(props) {
      const dimension = useWindowDimensions();
      return <Component {...props} dimension={dimension} />;
    };
  }

  class Enhancer extends React.Component {
    apiDispatch = (
      action,
      data = {},
      allowError = false,
      beforeLoaded = async (dataResponse) => {},
      isLoading = true
    ) => {
      return new Promise((resolve, reject) => {
        dispatcher(action, data, allowError).then(async (dataResponse) => {
          resolve(dataResponse);
        })
        .catch((e) => {
          if (e.response) {
            // do something for error response cause
          } else {
            // do something for else error
          }
          resolve({ result: [] });
        });
      });
    };

    render() {
      return (
        <Component
          {...this.props}
          apiDispatch={this.apiDispatch}
          // dimension={dimension}
        />
      )
    }
  }

  Enhancer.navigationOptions = Component.navigationOptions || undefined;
  return responsiveDimension(connect(null, {})(Enhancer));
}

export {
  Enhance,
  useWindowDimensions,
  dispatcher
};
