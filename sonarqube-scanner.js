const scanner = require("sonarqube-scanner");

scanner(
  {
    serverUrl: "http://localhost:9000",
    login: "admin",
    password: "Lala040403",
    token: "sqp_5ccdb4f7772507ec2384031a31c8693015cbf90c",
    options: {
      "sonar.projectName": "indigo-website",
      "sonar.projectKey": "indigo-website",
      "sonar.exclusions":".github/**, .vscode/**, android/**, assets/**, build/**, ios/**, node_modules/**, sonarqube-scanner.js, scripts/**",
      // "sonar.projectDescription": 'Description for "My App" project...',
      "sonar.sources": "./",
      // "sonar.tests": "specs",
    },
  },
  () => process.exit()
);
