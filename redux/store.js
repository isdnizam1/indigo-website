import { combineReducers, configureStore } from "@reduxjs/toolkit";
import { persistReducer, persistStore } from "redux-persist";
import { createWrapper } from "next-redux-wrapper";
import registerReducer from "./slices/registerSlice";
import chatReducer from "./slices/chatSlice";
import feedsReducer from "./slices/feedsSlice";
import createWebStorage from "redux-persist/lib/storage/createWebStorage";
import thunk from "redux-thunk";
import collabsReducer from "./slices/collabsSlice";

const createNoopStorage = () => {
  return {
    getItem(_key) {
      return Promise.resolve(null);
    },
    setItem(_key, value) {
      return Promise.resolve(value);
    },
    removeItem(_key) {
      return Promise.resolve();
    },
  };
};

const combinedReducer = combineReducers({
  registerReducer,
  chatReducer,
  feedsReducer,
  collabsReducer,
});

const storage =
  typeof window !== "undefined"
    ? createWebStorage("local")
    : createNoopStorage();

const persistConfig = {
  key: "root",
  storage,
  whitelist: ["chat", "feeds", "collabs"],
};

const persistedReducer = persistReducer(persistConfig, combinedReducer);

export const store = configureStore({
  reducer: persistedReducer,
  devTools: process.env.NODE_ENV !== "production",
  middleware: [thunk],
});

export const makeStore = () => store;

export const persistor = persistStore(store);

export const wrapper = createWrapper(makeStore);
