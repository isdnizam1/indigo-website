import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  data: {
    selectedPost: {}
  }
};

export const feedsSlice = createSlice({
  name: "feeds",
  initialState,
  reducers: {
    setFeedsData: (state, action) => {
      state.data = {
        ...state.data,
        ...action.payload,
      };
    },
    resetFeedsData: (state) => {
      state.data = {
        ...initialState.data,
      };
    },
  },
});

export const {
  setFeedsData,
  resetFeedsData
} = feedsSlice.actions;

export default feedsSlice.reducer;
