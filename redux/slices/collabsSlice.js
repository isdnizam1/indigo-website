import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  data: {
    filterInterest: [],
    filterCity: [],
    filterProf: [],
    filterRecProf: [],
    filterRecCity: [],
    filterPopProf: [],
    filterPopCity: []
  },
};

export const collabsSlice = createSlice({
  name: "collabs",
  initialState,
  reducers: {
    setCollabsData: (state, action) => {
      state.data = {
        ...state.data,
        ...action.payload,
      };
    },
    resetCollabsData: (state) => {
      state.data = {
        ...initialState.data,
      };
    },
  },
});

export const { setCollabsData, resetCollabsData } = collabsSlice.actions;

export default collabsSlice.reducer;
