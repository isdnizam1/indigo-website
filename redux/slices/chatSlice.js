import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  data: {
    selectedMembers: [],
    existingMembers: [],
    selectedMessage: {},
    listMessages: [],
    id_ads: "",
    client: null,
    topic: "",
    message: {},
    allFeatureUpdateCount: 0,
    listMessageUpdateCount: 0,
    detailMessageUpdateCount: 0,
    detailGroupUpdateCount: 0,
  },
};

export const chatSlice = createSlice({
  name: "chat",
  initialState,
  reducers: {
    setChatData: (state, action) => {
      state.data = {
        ...state.data,
        ...action.payload,
      };
    },
    resetChatData: (state) => {
      state.data = {
        ...initialState.data,
      };
    },
  },
});

export const { setChatData, resetChatData } = chatSlice.actions;

export default chatSlice.reducer;
