import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  behaviour: {
    step: 0,
    totalStep: 4,
  },
  data: {
    id_user: null,
    email: null,
    password: null,
    full_name: "",
    id_city: null,
    city_name: "",
    city_keyword: "",
    id_space: "",
    profile_type: null,
    profile_picture: null,
    referral_code: "",
    job_group_name: null,
    job_title: "",
    job_title_keyword: "",
    company_name: null,
    interest: [],
    registered_via: "",
    gender: null,
  },
};

export const registerSlice = createSlice({
  name: "register",
  initialState,
  reducers: {
    setRegisterBehaviour: (state, action) => {
      state.behaviour = {
        ...state.behaviour,
        ...action.payload,
      };
    },
    setRegisterData: (state, action) => {
      state.data = {
        ...state.data,
        ...action.payload,
      };
    },
    resetRegisterBehaviour: (state) => {
      state.behaviour = {
        ...initialState.behaviour,
      };
    },
    resetRegisterData: (state) => {
      state.data = {
        ...initialState.data,
      };
    },
  },
});

export const {
  setRegisterBehaviour,
  setRegisterData,
  resetRegisterBehaviour,
  resetRegisterData,
} = registerSlice.actions;

export default registerSlice.reducer;
