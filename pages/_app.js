// import { useEffect } from "react";
// import { analytics } from "../services/utils/firebase";

// bootstrap
import "bootstrap/dist/css/bootstrap.min.css";

import "/public/styles/Styles.scss";

import "react-h5-audio-player/lib/styles.css";

import { Provider } from "react-redux";
// import { store } from "/src/store";
import { wrapper, store, persistor } from "/redux/store";

import SSRProvider from "react-bootstrap/SSRProvider";

import { PersistGate } from "redux-persist/integration/react";

// import App from 'next/app'

const MyApp = ({ Component, pageProps }) => {
  // useEffect(() => process.env.NODE_ENV === "production" && analytics(), []);

  return (
    <SSRProvider>
      <Provider store={store}>
        <PersistGate persistor={persistor} loading={null}>
          <Component {...pageProps} />
        </PersistGate>
      </Provider>
    </SSRProvider>
  );
};

// Only uncomment this method if you have blocking data requirements for
// every single page in your application. This disables the ability to
// perform automatic static optimization, causing every page in your app to
// be server-side rendered.
//
// MyApp.getInitialProps = async (appContext) => {
//   // calls page's `getInitialProps` and fills `appProps.pageProps`
//   const appProps = await App.getInitialProps(appContext);
//
//   return { ...appProps }
// }

export default wrapper.withRedux(MyApp);
