import React, { Fragment, useEffect, useState } from "react";
import { Button, Col, Row } from "react-bootstrap";
import { getWebinarParticipantDetail } from "/services/actions/api";
import {
  apiDispatch,
  capitalizeFirstLetter,
  dateFormatter,
} from "/services/utils/helper";
import HeaderLv1 from "/components/header/HeaderLv1";
import { useRouter } from "next/router";
import Layout from "/components/common_layout/Layout";
import MaterialIcon from "/components/utils/MaterialIcon";
import { useRef } from "react";
import indigoIcon from "/public/assets/icons/icon-logo-indigo-white.svg";
import leapIcon from "/public/assets/icons/icon-logo-leap-white.svg";
import jsPDF from "jspdf";
import { toPng } from "html-to-image";
import classnames from "classnames";
import { isEmpty } from "lodash-es";

const ECertificate = () => {
  const [data, setData] = useState({
    certificate_name: "",
    email: "",
    full_name: "",
    id_ads: "",
    id_event: "",
    id_sc_user: "",
    id_user: null,
    image: "",
    speaker: [],
    status: "",
    title: "",
    whatsapp: "",
  });

  const { isReady, query, push } = useRouter();
  const { id: id_sc_user } = query;

  useEffect(() => isReady && getData(), [isReady]);

  const getData = () => {
    apiDispatch(getWebinarParticipantDetail, { id_sc_user }, true).then(
      (response) => {
        response.code === 200 ? setData(response.result) : push("/login");
      }
    );
  };

  const imageRef = useRef();

  const {
    additional_data_arr,
    certificate_name,
    email,
    full_name,
    id_ads,
    id_event,
    id_user,
    image,
    speaker,
    status,
    title,
    whatsapp,
  } = data;

  const handleCreatePDF = () => {
    toPng(imageRef.current).then(dataUrl => {
      const pdf = new jsPDF("l", "pt", "b3");
      const imgProperties = pdf.getImageProperties(dataUrl);
      const pdfWidth = pdf.internal.pageSize.getWidth();
      const pdfHeight = (imgProperties.height * pdfWidth) / imgProperties.width;

      pdf.addImage(dataUrl, "PNG", 0, -50, pdfWidth, pdfHeight);
      pdf.save(`Certificate_${certificate_name}.pdf`);
    })
    .catch(error => {
      console.error("oops, something went wrong!", error);
    });
  };

  return (
    <Layout title="Webinar Detail - Indigo">
      <div className="x-ecertificate-page">
        <HeaderLv1
          HideMenuTopBar
          customCta={
            <Button
              className="w-auto d-flex justify-content-center align-items-center gap-2"
              onClick={() => handleCreatePDF()}
            >
              <MaterialIcon action="download" />
              Download
            </Button>
          }
        />

        <div className="x-ecertificate-wrapper">
          <div
            className="d-flex justify-content-center align-items-center"
            ref={imageRef}
          >
            <div className="x-ecertificate-content">
              <div className="d-flex justify-content-between mx-4 mt-4 mb-2">
                <div className="d-flex justify-content-between gap-4">
                  <img src={indigoIcon.src} width="86" height="44" alt="" />
                  {/* <img src={leapIcon.src} width="68" height="36" alt="" /> */}
                </div>

                <div className="d-flex justify-content-between gap-4">
                  <img src={leapIcon.src} width="68" height="36" alt="" />
                  {/* <img src={indigoIcon.src} width="86" height="44" alt="" /> */}
                </div>
              </div>

              <div className="d-flex align-items-center flex-column w-100">
                <h1 className="x-heading-certificate">SERTIFIKAT</h1>

                <h2 className="x-heading-participate">PARTISIPASI</h2>

                <p className="text-lg-light color-red-lust mt-4 mb-3">
                  diberikan kepada :
                </p>

                <h1 className="x-participant-name">
                  {capitalizeFirstLetter(full_name)}
                </h1>

                <p className="text-lg-light color-red-lust mt-2 mb-0">
                  Atas partisipasinya sebagai Peserta di acara talkshow
                </p>

                <p className="text-lg-bold color-red-lust m-0">{title}</p>

                <p className="text-lg-light color-red-lust m-0">
                  {`Pada tanggal ${dateFormatter(additional_data_arr?.date.start, true)}.`}
                </p>

                <p className="text-lg-bold color-red-lust mt-4 mb-4">
                  {`Jakarta, ${dateFormatter(additional_data_arr?.date.end, true)}`}
                </p>

                <p className="text-lg-bold color-red-lust mt-4 mb-0">Speaker</p>

                <div className="d-flex">
                  {speaker.map((item, idx) => (
                    <div className="d-flex align-items-center flex-column mx-5" key={idx}>
                      {item.speaker_sign && <img src={item.speaker_sign} className="x-digitalsign-image" alt="" />}
                      <div className={classnames("x-separator", isEmpty(item.speaker_sign) && "x-margin-top")} />
                      <p className="text-lg-bold color-red-lust mt-2 mb-0">{item.title}</p>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default ECertificate;
