import { useEffect, useState, Fragment } from "react";
import { Button } from "react-bootstrap";
import MainLayout from "/components/layout/MainLayout";
import defaultCover from "/public/assets/images/img-default-cover-profile.png";
import {
  getCity,
  getGenreInterest,
  getProfileDetailV2,
  postEditProfile,
} from "/services/actions/api";
import { convertToBase64, getAuth, apiDispatch } from "/services/utils/helper";
import InputText from "/components/input/InputText";
import { useFormik } from "formik";
import InputSelect from "/components/input/InputSelect";
import { isEmpty } from "lodash-es";
import InputToggleGroup from "/components/input/InputToggleGroup";
import Layout from "/components/common_layout/Layout";
import appRoute from "../../components/routes/appRoute";
import { useRouter } from "next/router";
import { LoaderContent } from "/components/loaders/Loaders";
import { EditProfileSchema } from "/services/constants/Schemas";

const ProfileEdit = () => {
  const { id_user } = getAuth();
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(true);

  const genderData = [
    { name: "Pria", value: "male" },
    { name: "Wanita", value: "female" },
  ];

  const [profileData, setProfileData] = useState({
    id_user,
    full_name: "",
    id_city: null,
    about_me: "",
    company_name: "",
    job_title: "",
    profile_picture: "",
    cover_image: "",
    social_media: {
      instagram: "",
      youtube: "",
      email: "",
      github: "",
      website: "",
      other: "",
    },
    interest_name: [],
    gender: "",
  });

  //for city
  const [allCities, setAllCities] = useState([]);
  const [suggestionCities, setSuggestionCities] = useState([]);
  const [cityName, setCityName] = useState("");
  const [cityKeyword, setCityKeyword] = useState("");

  //for interest
  const [allInterests, setAllInterests] = useState([]);
  const [suggestionInterests, setSuggestionInterests] = useState([]);
  const [interestKeyword, setInterestKeyword] = useState("");
  const [selectedInterest, setSelectedInterest] = useState([]);
  const [currentInterest, setCurrentInterset] = useState([]);

  //for gender
  const [genderSelect, setGenderSelect] = useState();

  //for social social media
  const [isExpandLink, setIsExpandLink] = useState(false);

  //for date
  const [dateSelect, setdateSelect] = useState();

  //for profile picture
  const [profilePic, setProfilePic] = useState();

  const [coverPic, setCoverPic] = useState();

  const formik = useFormik({
    initialValues: profileData,
    enableReinitialize: true,
    validationSchema: EditProfileSchema,
    onSubmit: (values) => postUpdateProfile(values),
  });

  useEffect(() => {
    getProfileDetail();
    getCities();
    getInterests();
  }, []);

  const getProfileDetail = () => {
    apiDispatch(getProfileDetailV2, { id_user }, true).then((response) => {
      setIsLoading(false);
      if (response.code === 200) {
        const {
          about_me,
          birthdate,
          company,
          contact,
          cover_image,
          full_name,
          gender,
          id_city,
          interest,
          job_title,
          location,
          profile_picture,
        } = response.result;

        setGenderSelect(gender);
        setProfilePic(profile_picture);
        setCoverPic(cover_image);

        setProfileData({
          ...profileData,
          about_me,
          birthdate,
          company_name: company,
          social_media: { ...contact },
          cover_image,
          full_name,
          gender,
          id_city,
          interest_name: interest,
          job_title,
          location,
        });
      }
    });
  };

  const getCities = () => {
    apiDispatch(getCity, { start: 0 }, true).then((response) => {
      response.code === 200 && setAllCities(response.result);
    });
  };

  const getInterests = () => {
    apiDispatch(getGenreInterest, { start: 0 }, true).then((response) => {
      response.code === 200 && setAllInterests(response.result);
    });
  };

  const handleCityChange = (keyword) => {
    let datas = allCities.filter((data) =>
      data.city_name.toLowerCase().includes(keyword.toLowerCase())
    );
    setCityKeyword(keyword);
    setSuggestionCities(!isEmpty(keyword) ? datas.slice(0, 6) : []);
  };

  const handleInterestChange = (keyword) => {
    let datas = allInterests.filter((data) => {
      return data.interest_name.toLowerCase().includes(keyword.toLowerCase());
    });

    setInterestKeyword(keyword);
    setSuggestionInterests(!isEmpty(keyword) ? datas.slice(0, 10) : []);
  };

  const handleImageChange = (e) => {
    if (e.target.files && e.target.files[0]) {
      let img = e.target.files[0];
      convertToBase64(img).then((res) => {
        setProfilePic(res);
        formik.setValues({ ...formik.values, profile_picture: res });
      });
    }
  };

  const handleCoverChange = (e) => {
    if (e.target.files && e.target.files[0]) {
      let img = e.target.files[0];
      convertToBase64(img).then((res) => {
        setCoverPic(res);
        formik.setValues({ ...formik.values, cover_image: res });
      });
    }
  };

  const postUpdateProfile = (params) => {
    apiDispatch(postEditProfile, { ...params }, true).then((response) => {
      router.push("/profile");
    });
  };

  if (!profileData && !allCities && !allInterests) return <MainLayout />;

  return (
    <Layout title="Profile Edit - Eventeer">
      <div className="x-editprofile-page">
        <MainLayout size="md">
          <div className="my-4">
            <form onSubmit={formik.handleSubmit}>
              <div className="x-image-cover">
                <div className="x-image-wrapper">
                  <LoaderContent
                    height={258}
                    width="100%"
                    isLoading={isLoading}
                  >
                    <img src={coverPic ? coverPic : defaultCover.src} alt="" />
                  </LoaderContent>

                  <label htmlFor="cover-upload" className="x-icon-add-images">
                    <span className="material-icons">add_a_photo</span>
                    <input
                      id="cover-upload"
                      style={{ display: "none" }}
                      accept="image/png, image/jpg, image/jpeg"
                      type="file"
                      name="myImage"
                      onChange={handleCoverChange}
                    />
                  </label>
                </div>
              </div>
              <div className="x-profile-photo">
                <div className="x-upload-wrapper">
                  <LoaderContent
                    width={160}
                    height={160}
                    radius="100%"
                    isLoading={isLoading}
                  >
                    {profilePic ? (
                      <img src={profilePic} alt="profile" />
                    ) : (
                      <div className="x-empty-image">
                        <p className="text-lg-normal color-base-10 m-0">
                          {profileData.full_name.slice(0, 1).toUpperCase()}
                        </p>
                      </div>
                    )}
                    <label htmlFor="image-upload" className="x-icon-add-images">
                      <span className="material-icons">add_a_photo</span>
                      <input
                        id="image-upload"
                        accept="image/png, image/jpg, image/jpeg"
                        type="file"
                        name="myImage"
                        onChange={handleImageChange}
                      />
                    </label>
                  </LoaderContent>
                </div>
              </div>
              <LoaderContent
                width="25%"
                height={24}
                Column
                className="mt-4 mb-1"
                isLoading={isLoading}
              />
              <LoaderContent
                width="100%"
                height={40}
                count={3}
                Column
                className="my-4"
                isLoading={isLoading}
              >
                <Fragment>
                  <p className="text-lg-bold color-neutral-80 my-3">
                    BASIC INFORMATION
                  </p>
                  <InputText
                    title="Name"
                    name="full_name"
                    placeholder={"Name"}
                    onChangeText={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.full_name}
                    isError={!isEmpty(formik.errors.full_name)}
                    errorSign={formik.errors.full_name}
                    autoComplete={"off"}
                  />
                  <InputSelect
                    title="City / Location"
                    type="text"
                    className="mb-3"
                    disableError
                    value={cityKeyword}
                    placeholder="Choose location"
                    data={suggestionCities}
                    selectedValue={cityName}
                    onFocus={() =>
                      !isEmpty(cityName)
                        ? handleCityChange(cityName)
                        : setSuggestionCities(allCities.slice(0, 6))
                    }
                    onRemoveSelected={() => {
                      setCityName("");
                      setCityKeyword("");
                      setSuggestionCities([]);
                    }}
                    onBlur={() => {
                      setCityKeyword(cityName);
                      setTimeout(() => setSuggestionCities([]), 300);
                    }}
                    onChangeText={(e) => handleCityChange(e.target.value)}
                    suggestionText={"city_name"}
                    suggestionOnClick={(city) => {
                      setCityName(city.city_name);
                      setCityKeyword(city.city_name);
                      setSuggestionCities([]);
                      formik.setValues({
                        ...formik.values,
                        id_city: city.id_city,
                      });
                    }}
                  />
                  <InputText
                    title="Bio"
                    as="textarea"
                    disableError
                    placeholder={"Write a short description about yourself"}
                    name="about_me"
                    onChangeText={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.about_me}
                    customFooter={
                      <p className="text-sm-bold color-neutral-70 m-0">
                        {formik.values.about_me.length}/160
                      </p>
                    }
                  />
                </Fragment>

                <hr className="my-4" />

                <Fragment>
                  <p className="text-lg-bold color-neutral-80 my-3">
                    PRIMARY PROFESSION
                  </p>
                  <InputText
                    title="Profession"
                    name="job_title"
                    placeholder={"Name"}
                    onChangeText={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.job_title}
                    isError={!isEmpty(formik.errors.job_title)}
                    errorSign={formik.errors.job_title}
                  />
                </Fragment>

                <hr className="mt-2 mb-4" />

                <Fragment>
                  <p className="text-lg-bold color-neutral-80 my-3">INTEREST</p>
                  <InputSelect
                    title="Interest"
                    extraTitle="(Example : front end, back end, UI/UX designer)"
                    type="text"
                    value={interestKeyword}
                    placeholder="Tambahkan interest"
                    data={suggestionInterests}
                    selectedValue={""}
                    disableError={true}
                    icon={"add"}
                    onFocus={() => allInterests.slice(0, 6)}
                    onKeyPress={(e) => {
                      if (e.key === "Enter") {
                        setInterestKeyword("");
                        setSuggestionInterests([]);
                        setSelectedInterest([
                          ...selectedInterest,
                          e.target.value,
                        ]);
                        setCurrentInterset([
                          ...selectedInterest,
                          e.target.value,
                        ]);
                        formik.setValues({
                          ...formik.values,
                          interest_name: [...selectedInterest, e.target.value],
                        });
                      }
                    }}
                    onBlur={() => {
                      setTimeout(() => {
                        setInterestKeyword("");
                        setSuggestionInterests([]);
                      }, 300);
                    }}
                    onChangeText={(e) => handleInterestChange(e.target.value)}
                    suggestionText={"interest_name"}
                    suggestionOnClick={(interest) => {
                      setInterestKeyword("");
                      setSuggestionInterests([]);
                      setSelectedInterest([
                        ...selectedInterest,
                        interest.interest_name,
                      ]);
                      setCurrentInterset([
                        ...selectedInterest,
                        interest.interest_name,
                      ]);
                      formik.setValues({
                        ...formik.values,
                        interest_name: [
                          ...selectedInterest,
                          interest.interest_name,
                        ],
                      });
                    }}
                  />
                  <div className="d-flex align-items-center flex-wrap w-100 pt-1">
                    {currentInterest.map((name, i) => (
                      <div className="x-tag-interest" key={i}>
                        <p className="text-sm-normal m-0 line-height-sm color-primary-main">
                          {name}
                        </p>
                        <span className="material-icons-round">close</span>
                      </div>
                    ))}
                  </div>
                </Fragment>

                <hr className="my-4" />

                <Fragment>
                  <p className="text-lg-bold color-neutral-80 my-3">
                    {"CONTACT & SOCIAL MEDIA"}
                  </p>
                  <InputText
                    title="Instagram"
                    name="social_media.instagram"
                    placeholder={"Example : www.instagram.com/eventeer/"}
                    onChangeText={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.social_media.instagram}
                    isError={!isEmpty(formik.errors.social_media?.instagram)}
                    errorSign={formik.errors.social_media?.instagram}
                  />
                  <InputText
                    title="Youtube"
                    placeholder={"Insert the youtube channel link/url"}
                    name="social_media.youtube"
                    onChangeText={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.social_media.youtube}
                    isError={!isEmpty(formik.errors.social_media?.youtube)}
                    errorSign={formik.errors.social_media?.youtube}
                  />
                  <InputText
                    title="Email"
                    placeholder={"Example : info@indigoconnect.com"}
                    name="social_media.email"
                    onChangeText={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.social_media.email}
                    isError={!isEmpty(formik.errors.social_media?.email)}
                    errorSign={formik.errors.social_media?.email}
                  />
                  {isExpandLink && (
                    <>
                      <InputText
                        title="Github"
                        placeholder={"Example : www.github.com/username"}
                        name="social_media.github"
                        onChangeText={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.social_media.github}
                        isError={!isEmpty(formik.errors.social_media?.github)}
                        errorSign={formik.errors.social_media?.github}
                      />
                      <InputText
                        title="Website"
                        placeholder={"Example : www.site.com"}
                        name="social_media.website"
                        onChangeText={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.social_media.website}
                        isError={!isEmpty(formik.errors.social_media?.website)}
                        errorSign={formik.errors.social_media?.website}
                      />
                      <InputText
                        title="Other Link"
                        placeholder={"Set any URL"}
                        name="social_media.other"
                        onChangeText={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.social_media.other}
                      />
                    </>
                  )}
                  <p
                    className="text-lg-normal color-semantic-main cursor-pointer mb-0"
                    onClick={() => setIsExpandLink(!isExpandLink)}
                  >
                    {isExpandLink ? `Hide` : `Show All`}
                  </p>
                </Fragment>

                <hr className="my-4" />

                <Fragment>
                  <p className="text-lg-bold color-neutral-80 my-3">
                    PRIVATE INFORMATION
                  </p>
                  {genderSelect && (
                    <InputToggleGroup
                      title="Jenis kelamin"
                      data={genderData}
                      onClick={(e) => {
                        setGenderSelect(e.target.value);
                        formik.setValues({
                          ...formik.values,
                          gender: e.target.value,
                        });
                      }}
                      toggleValue={genderSelect}
                      disableError={true}
                    />
                  )}
                  <div className="x-input-date-container">
                    <InputSelect
                      title="Date of birth"
                      type="text"
                      disableError
                      placeholder="Date"
                    />
                    <InputSelect title=" " placeholder="Month" disableError />
                    <InputSelect title=" " placeholder="Year" disableError />
                  </div>
                </Fragment>

                <hr className="my-4" />

                <Button type="submit" className="w-25">
                  Save Profile
                </Button>
              </LoaderContent>
            </form>
          </div>
        </MainLayout>
      </div>
    </Layout>
  );
};

export default appRoute(ProfileEdit);
