import React, { Fragment } from "react";
import {
  getProfileDetailV2,
  getProfileFollowStatus,
  postProfileFollow,
  postProfileUnfollow,
  postProfileAddPortofolio,
  postProfileDelPortofolio,
  postProfileEditPortofolio,
} from "/services/actions/api";
import { useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
import { getAuth } from "/services/utils/helper";
import MainLayout from "/components/layout/MainLayout";
import defaultCover from "/public/assets/images/img-default-cover-profile.png";
import { Button } from "react-bootstrap";
import { useRouter } from "next/router";
import { isEmpty } from "lodash-es";
import EmptyOverview from "/components/empty_state/EmptyOverview";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";
import { apiDispatch } from "/services/utils/helper";
import ModalConfirmation from "/components/modal/ModalConfirmation";
import InputText from "/components/input/InputText";
import classnames from "classnames";
import MaterialIcon from "components/utils/MaterialIcon";
import SkeletonElement from "/components/skeleton/SkeletonElement";

const Profile = ({ type }) => {
  const [activeTab, setActiveTab] = useState("overview");
  const [isFollowed, setIsFollowed] = useState(false);
  const [modal, setModal] = useState(false);
  const [modalDelete, setModalDelete] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [state, setState] = useState({
    title: "",
    url: "",
    id: null,
  });
  const [data, setData] = useState({
    about_me: "",
    cover_image: "",
    full_name: "",
    id_user: null,
    id_city: null,
    interest: [],
    portofolio: null,
    job_title: "",
    location: "",
    profile_picture: "",
    total_followers: 0,
    total_following: 0,
  });
  const [isOwnProfile, setIsOwnProfile] = useState(false);
  const { id_user } = getAuth();
  const { query, isReady, push } = useRouter();
  const { id: id_related_user } = query;
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => isReady && getData(), [isReady]);

  const getData = () => {
    apiDispatch(
      getProfileDetailV2,
      {
        id_user: id_related_user ? id_related_user : id_user,
      },
      true
    ).then((response) => {
      setIsLoading(false);
      if (response.code === 200) {
        const { result } = response;

        setData(result);
        setIsOwnProfile(
          id_related_user && id_related_user !== id_user ? false : true
        );

        id_related_user &&
          apiDispatch(
            getProfileFollowStatus,
            {
              // id_user: id_related_user ? id_related_user : id_user,
              followed_by: id_user,
              id_user: id_related_user,
            },
            true
          ).then((response) => {
            if (response.code === 200) {
              const { status } = response.result;

              setIsFollowed(status === "followed" ? true : false);
            }
          });
      } else return push("/home");
    });
  };

  const handleModalPortfolio = (item = {}) => {
    const isEdit = !isEmpty(item);
    const { id, title, link_portofolio: url } = item;

    setState({
      id: isEdit ? id : null,
      title: isEdit ? title : "",
      url: isEdit ? url : "",
    });
    setIsEdit(isEdit);
    setModal(true);
  };

  const handleFollow = () => {
    apiDispatch(
      postProfileFollow,
      {
        followed_by: id_user,
        id_user: id_related_user,
      },
      true
    ).then((response) => {
      response.code === 200 && setIsFollowed(true);
    });
  };

  const handleUnfollow = () => {
    apiDispatch(
      postProfileUnfollow,
      {
        id_user,
        id_user_following: id_related_user,
      },
      true
    ).then((response) => {
      response.code === 200 && setIsFollowed(false);
    });
  };

  const onSubmitPortfolio = (action) => {
    const { id, title, url } = state;
    const api =
      action === "delete"
        ? postProfileDelPortofolio
        : action === "edit"
        ? postProfileEditPortofolio
        : postProfileAddPortofolio;
    const params =
      action === "delete"
        ? { id, id_user }
        : action === "edit"
        ? { id, id_user, title, link_portofolio: url }
        : { id_user, title, link_portofolio: url };

    apiDispatch(api, params, true).then((response) => {
      if (response.code === 200) {
        getData();
        setModal(false);
      }
    });
  };

  const {
    about_me,
    cover_image,
    full_name,
    id_city,
    interest,
    job_title,
    location,
    profile_picture,
    total_followers,
    total_following,
    portofolio,
  } = data;

  return (
    <Layout title="Profile - Indigo">
      <MainLayout size="md" Breadcrumb={!isEmpty(type)}>
        <div className={classnames("x-profile-page", !type && "mt-4")}>
          <div className="x-cover-img">
            {!isLoading ? (
              <img src={defaultCover.src} alt="" />
            ) : (
              <SkeletonElement width="100%" height={258} radius={12} />
            )}
          </div>
          <div className="x-profile-top">
            <div className="x-profile-photo">
              {!isLoading ? (
                <Fragment>
                  {profile_picture ? (
                    <img src={profile_picture} alt="profile" />
                  ) : (
                    <div className="empty-image">
                      <p className="heading-lg-normal color-base-10 m-0">
                        {full_name.slice(0, 1).toUpperCase()}
                      </p>
                    </div>
                  )}
                </Fragment>
              ) : (
                <SkeletonElement width={160} height={160} radius="100%" />
              )}
            </div>
            <div className="x-profile-top-detail">
              <div className="x-detail-name">
                {!isLoading ? (
                  <Fragment>
                    <h3 className="heading-md-bolder color-neutral-80 m-0">
                      {full_name}
                    </h3>
                    <p className="text-lg-light color-neutral-80 m-0">
                      {job_title}
                    </p>
                  </Fragment>
                ) : (
                  <Fragment>
                    <SkeletonElement width="50%" height={24} className="mb-2" />
                    <SkeletonElement width="50%" height={16} />
                  </Fragment>
                )}
              </div>
              <div className="d-flex justify-content-between align-items-center">
                <div className="d-flex gap-3">
                  <div className="d-flex gap-1">
                    {!isLoading ? (
                      <Fragment>
                        <p className="text-lg-bolder color-neutral-80 m-0">
                          {total_followers}
                        </p>
                        <p className="text-lg-normal color-neutral-400 m-0">
                          Followers
                        </p>
                      </Fragment>
                    ) : (
                      <SkeletonElement width={96} height={24} />
                    )}
                  </div>
                  <div className="d-flex gap-1">
                    {!isLoading ? (
                      <Fragment>
                        <p className="text-lg-bolder color-neutral-80 m-0">
                          {total_following}
                        </p>
                        <p className="text-lg-normal color-neutral-400 m-0">
                          Following
                        </p>
                      </Fragment>
                    ) : (
                      <SkeletonElement width={96} height={24} />
                    )}
                  </div>
                </div>

                <div className="d-flex gap-2">
                  {!isLoading ? (
                    <Fragment>
                      {isOwnProfile ? (
                        <Button
                          onClick={() => push(`/profile/edit`)}
                          className="btn-cancel w-auto"
                        >
                          Edit Profile
                        </Button>
                      ) : (
                        <div className="d-flex gap-2">
                          <Button
                            className="btn-disable d-flex justify-content-center align-items-center"
                            onClick={() => {
                              // dispatch(
                              //   setChatData({
                              //     selectedMessage: {
                              //       id_user: data.id_user,
                              //       full_name: data.full_name,
                              //       image: data.profile_picture,
                              //       // id_groupmessage: item.id_groupmessage, // tidak ada
                              //       type: "personal",
                              //     },
                              //   })
                              // );
                              // push({
                              //   pathname: "/chat",
                              //   query: {
                              //     id_message: !item.id_groupmessage
                              //       ? false
                              //       : item.id_groupmessage,
                              //     message_type: "message",
                              //     type: "personal",
                              //   },
                              // });
                            }}
                          >
                            {/* <span
                              className="material-icons-round pe-1"
                              style={{ fontSize: "20px" }}
                            >
                              chat
                            </span> */}
                            <MaterialIcon
                              size={20}
                              className="pe-1"
                              action="chat"
                            />
                            Message
                          </Button>
                          <Button
                            onClick={isFollowed ? handleUnfollow : handleFollow}
                            className={classnames(
                              "w-auto",
                              isFollowed && "btn-disable"
                            )}
                          >
                            {isFollowed ? "Following" : "Follow"}
                          </Button>
                        </div>
                      )}
                    </Fragment>
                  ) : (
                    <SkeletonElement width={150} height={44} count={1} />
                  )}
                </div>
              </div>

              {/* <div className="d-flex mt-2 gap-3">
                <Button className="btn-disable d-flex justify-content-center align-items-center">
                  <div className="color-semantic-success-main d-flex align-items-center">
                    <MaterialIcon action="check" size={18} weight={600} className="pe-2" />
                    Accept
                  </div>
                </Button>
                <Button className="btn-disable d-flex justify-content-center align-items-center">
                  <div className="color-semantic-danger-main d-flex align-items-center">
                    <MaterialIcon action="clear" size={18} weight={600} className="pe-2" />
                    Deny
                  </div>
                </Button>
              </div> */}
            </div>
          </div>
          <hr />
          <div className="x-profile-bottom">
            <div className="x-badges">
              {!isLoading ? (
                !isEmpty(interest) &&
                interest.map(({ interest_name }, i) => (
                  <span key={i}>{interest_name}</span>
                ))
              ) : (
                <SkeletonElement width={72} height={24} count={3} />
              )}
            </div>
            <p>{about_me}</p>
            <div className="x-loc-warp">
              <div className="x-icon-loc" />
              {!isLoading ? (
                <span>
                  {location.city_name}, {location.country_name}
                </span>
              ) : (
                <SkeletonElement width={164} height={24} count={1} />
              )}
            </div>
          </div>
          <div className="x-profile-overview">
            <div className="x-tags">
              <span
                className={classnames(activeTab === "overview" && "active")}
                onClick={() => setActiveTab("overview")}
              >
                overview
              </span>
              <span
                className={classnames(activeTab === "activity" && "active")}
                onClick={() => setActiveTab("activity")}
              >
                activities
              </span>
            </div>
            <hr />
            <div className="x-details-container">
              {activeTab === "overview" ? (
                <>
                  {!isEmpty(data.portofolio) ||
                  !isEmpty(data.portofolio?.value) ? (
                    <Fragment>
                      <div className="d-flex justify-content-between align-items-center">
                        <h3 className="x-overview-title">Portfolio</h3>
                        {isOwnProfile && (
                          <div
                            className="x-overview-add"
                            onClick={() => handleModalPortfolio()}
                          >
                            <span className="material-icons-round">add</span>
                            <p>Add Portfolio</p>
                          </div>
                        )}
                      </div>
                      {JSON.parse(portofolio.value).map((item, idx) => (
                        <div className="x-card-attachment" key={idx}>
                          <Row className="m-0">
                            <Col
                              xs="1"
                              className="d-flex justify-content-center align-items-center"
                              onClick={() =>
                                window.open(item.link_portofolio, "_blank")
                              }
                            >
                              <div className="x-attachment-icon" />
                            </Col>
                            <Col
                              xs="10"
                              className="d-flex justify-content-center flex-column"
                            >
                              <div className="my-3">
                                <h4
                                  onClick={() =>
                                    window.open(item.link_portofolio, "_blank")
                                  }
                                >
                                  {item.title}
                                </h4>
                                {isOwnProfile && (
                                  <p onClick={() => handleModalPortfolio(item)}>
                                    Edit Portfolio
                                  </p>
                                )}
                              </div>
                            </Col>
                            <Col
                              xs="1"
                              className="d-flex align-items-center cursor-pointer"
                              onClick={() =>
                                window.open(item.link_portofolio, "_blank")
                              }
                            >
                              <span className="material-icons-outlined">
                                chevron_right
                              </span>
                            </Col>
                          </Row>
                        </div>
                      ))}
                    </Fragment>
                  ) : (
                    <EmptyOverview
                      head="Portfolio"
                      title="Upload Your Portfolio"
                      subtitle="Share your portfolio and connect with new person!"
                      icon={<div className="x-icon-portfolio" />}
                      buttonText="Add Portfolio"
                      onClick={() => handleModalPortfolio()}
                    />
                  )}
                </>
              ) : (
                <EmptyOverview
                  head="Activities"
                  title="There are no posts yet"
                  subtitle="All posts will appear on this page"
                  icon={<div className="x-icon-activity" />}
                  buttonText="Make Posts"
                />
              )}
            </div>
          </div>
          <div className="x-modal-portfolio">
            <ModalConfirmation
              show={modal}
              size="lg"
              onHide={() => setModal(false)}
              customTitle={
                <div className="x-modalportfolio-title">
                  <div className="d-flex justify-content-between">
                    <h3>{isEdit ? "Edit Portfolio" : "Add Portfolio"}</h3>

                    <span
                      className="material-icons-round cursor-pointer font-weight-bolder color-neutral-70"
                      onClick={() => setModal(false)}
                      style={{
                        fontSize: "28px",
                      }}
                    >
                      clear
                    </span>
                  </div>
                  <div className="x-divider" />
                </div>
              }
              customBody={
                <div className="x-modalportfolio-content">
                  <InputText
                    title="Title"
                    type="text"
                    value={state.title}
                    onChangeText={(e) =>
                      setState({ ...state, title: e.target.value })
                    }
                    placeholder="Input Title"
                    // isError={emailIsError}
                  />

                  <InputText
                    title="Portfolio URL"
                    type="text"
                    value={state.url}
                    className="pt-2"
                    onChangeText={(e) =>
                      setState({ ...state, url: e.target.value })
                    }
                    placeholder="Input Portfolio URL"
                    // isError={emailIsError}
                  />

                  <div
                    className={classnames(
                      "d-flex pt-4",
                      isEdit ? "justify-content-between" : "justify-content-end"
                    )}
                  >
                    {isEdit && (
                      <Button
                        className="btn-danger"
                        onClick={() => {
                          setModal(false);
                          setModalDelete(true);
                        }}
                      >
                        Delete Portfolio
                      </Button>
                    )}
                    <Button
                      className="btn-primary"
                      onClick={() =>
                        onSubmitPortfolio(!isEdit ? "add" : "edit")
                      }
                    >
                      Save
                    </Button>
                  </div>
                </div>
              }
            />

            <ModalConfirmation
              show={modalDelete}
              onHide={() => {
                setModalDelete(false);
                setModal(true);
              }}
              customExitIcon={
                <span
                  className="material-icons-round font-weight-bolder color-neutral-70"
                  style={{
                    fontSize: "28px",
                  }}
                >
                  clear
                </span>
              }
              customTitle={
                <>
                  <h1 className="heading-lg-bold mt-2">Delete Portfolio</h1>
                  <p className="pt-1 text-lg-normal color-neutral-400">
                    Are you sure want to delete this portfolio?
                  </p>
                  <div className="justify-content-center w-100">
                    <Button
                      className="btn-delete mb-2"
                      onClick={() => {
                        onSubmitPortfolio("delete");
                        setModalDelete(false);
                      }}
                      style={{ height: "54px" }}
                    >
                      Delete Portfolio
                    </Button>
                    <Button
                      className="btn-primary mt-2"
                      style={{ height: "54px" }}
                      onClick={() => {
                        setModalDelete(false);
                        setModal(true);
                      }}
                    >
                      Cancel
                    </Button>
                  </div>
                </>
              }
            />
          </div>
        </div>
      </MainLayout>
    </Layout>
  );
};

export default appRoute(Profile);
