import React from "react";
import MainLayout from "/components/layout/MainLayout";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";

const FAQ = () => {
  return (
    <Layout title="FAQ - Indigo">
      <MainLayout>
        <div className="x-faq-page">
          {/* your code here, you can delete below & start code entire this pages */}
          <h1 className="heading-lg-bold m-0 p-4">FAQ Pages</h1>
        </div>
      </MainLayout>
    </Layout>
  );
};

export default appRoute(FAQ);
