import { useState } from "react";
import { Container, Button } from "react-bootstrap";
import InputText from "/components/input/InputText";
import { isEmailValid, apiDispatch } from "/services/utils/helper";
import { postForgotPassword } from "/services/actions/api";
import AuthLayout from "/components/layout/AuthLayout";
import { useRouter } from "next/router";
import Layout from "/components/common_layout/Layout";
import publicRoute from "/components/routes/publicRoute";

const ForgotPassword = () => {
  const [state, setState] = useState({
    email: "",
    emailErrorSign: "",
    emailIsError: false,
  });

  const router = useRouter();

  const onSubmit = () => {
    const { email } = state;

    apiDispatch(postForgotPassword, { email }, true).then((response) => {
      if (response.status === "success") {
        router.push(
          {
            pathname: "/forgot-password/recovery-code",
            query: { email },
          },
          "/forgot-password/recovery-code"
        );
      } else {
        setState({
          ...state,
          emailIsError: true,
          emailErrorSign: "Email tidak ditemukan",
        });
      }
    });
  };

  const { email, emailErrorSign, emailIsError } = state;

  return (
    <Layout title="Forgot Password - Indigo">
      <AuthLayout
        headerDisableRoute={true}
        Breadcrumb
        breadcrumbOnBackhandler={() => router.push("/login")}
      >
        <div className="x-forgotpassword-page">
          <Container className="d-flex justify-content-center align-items-center w-100 h-100">
            <div className="x-card-form">
              <h1 className="pb-3">Lupa password</h1>

              <InputText
                title="Email"
                type="email"
                value={email}
                onChangeText={(event) =>
                  setState({
                    ...state,
                    email: event.target.value,
                    emailIsError: false,
                    emailErrorSign: "",
                  })
                }
                placeholder="Masukkan email"
                errorSign={emailErrorSign}
                isError={emailIsError}
              />

              <Button
                className="btn-primary mt-2"
                onClick={onSubmit}
                disabled={!isEmailValid(email)}
              >
                Kirim recovery email
              </Button>
            </div>
          </Container>
        </div>
      </AuthLayout>
    </Layout>
  );
};

export default publicRoute(ForgotPassword);
