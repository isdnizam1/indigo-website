import React, { useEffect, useState } from "react";
import { Container, Button } from "react-bootstrap";
import { postInputForgotPassword } from "/services/actions/api";
import InputOtp from "/components/input/InputOtp";
import AuthLayout from "/components/layout/AuthLayout";
import { useRouter } from "next/router";
import Layout from "/components/common_layout/Layout";
import publicRoute from "../../components/routes/publicRoute";
import { apiDispatch } from "/services/utils/helper";

const RecoveryCode = () => {
  const [state, setState] = useState({
    otp: "",
    otpIsError: false,
  });

  const router = useRouter();

  useEffect(() => !router.query.email && router.push("/forgot-password"), []);

  const onSubmit = () => {
    const { otp } = state;

    apiDispatch(
      postInputForgotPassword,
      { email: router.query.email, forgotten_password_code: otp },
      true
    ).then((response) => {
      if (response.status === "success") {
        router.push(
          {
            pathname: "/forgot-password/password-recovery",
            query: { email: router.query.email, otp },
          },
          "/forgot-password/password-recovery"
        );
      } else {
        setState({ ...state, otpIsError: true });
      }
    });
  };

  const { otp, otpIsError } = state;

  return (
    <Layout title="Recovery Code - Indigo">
      <AuthLayout
        headerDisableRoute={true}
        Breadcrumb
        breadcrumbOnBackhandler={() => router.push("/forgot-password")}
      >
        <div className="x-forgotpassword-page">
          <Container className="d-flex justify-content-center align-items-center w-100 h-100">
            <div className="x-card-form">
              <h1
                className="pb-3 heading-xl-bolder"
                style={{ color: "var(--grey-5)" }}
              >
                Lupa password
              </h1>

              <p className="text-lg-normal" style={{ color: "var(--grey-3)" }}>
                {"Masukkan recovery code yang dikirim melalui email "}
                <span className="color-semantic-main">
                  {router.query.email}
                </span>
              </p>

              <div className="mt-2 mb-4">
                <InputOtp
                  numInputs={6}
                  onChangeValue={(e) =>
                    setState({
                      ...state,
                      otp: e.target.otpValue,
                      otpIsError: false,
                    })
                  }
                  errorSign={
                    "Kode yang kamu masukkan tidak sesuai, silahkan cek kembali"
                  }
                  isError={otpIsError}
                />

                <Button
                  className="btn-primary mt-4"
                  onClick={onSubmit}
                  disabled={otp.length !== 6 || otpIsError}
                >
                  Lanjut
                </Button>
              </div>
            </div>
          </Container>
        </div>
      </AuthLayout>
    </Layout>
  );
};

export default publicRoute(RecoveryCode);
