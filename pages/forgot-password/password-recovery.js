import { useEffect, useState } from "react";
import { Container, Button } from "react-bootstrap";
import { postForgotNewPassword } from "/services/actions/api";
import InputPassword from "/components/input/InputPassword";
import ModalConfirmation from "/components/modal/ModalConfirmation";
import AuthLayout from "/components/layout/AuthLayout";
import { useRouter } from "next/router";
import Layout from "/components/common_layout/Layout";
import publicRoute from "../../components/routes/publicRoute";
import { apiDispatch } from "/services/utils/helper";

const PasswordRecovery = () => {
  const [state, setState] = useState({
    password: "",
    retypePassword: "",
    modalFinish: false
  });

  const router = useRouter();

  const [submitDisabled, setSubmitDisabled] = useState(true);


  useEffect(() => {
    if (state.password && state.retypePassword) {
      if (state.password.length >= 8 && state.retypePassword.length >= 8) {
        if (state.password === state.retypePassword) setSubmitDisabled(false);
        else setSubmitDisabled(true);
      } else setSubmitDisabled(true);
    } else setSubmitDisabled(true);
  }, [state.password, state.retypePassword]);

  useEffect(() => {
    if (!router.query.email && !router.query.otp) router.push("/forgot-password");
  }, []);

  const passwordOnChangeValue = (event) => {
    const password = event.target.value;
    setState({
      ...state,
      password,
      passwordIsError: password.length && password.length < 8 ? true : false,
      retypePasswordIsError:
        state.retypePassword.length &&
        state.retypePassword !== password
          ? true
          : false,
    });
  };

  const retypePasswordOnChangeValue = (event) => {
    const retypePassword = event.target.value;
    setState({
      ...state,
      retypePassword,
      passwordIsError: state.password.length && state.password.length < 8
        ? true
        : false,
      retypePasswordIsError: retypePassword !== state.password ? true : false,
    });
  };

  const onSubmit = () => {
    apiDispatch(
      postForgotNewPassword,
      {
        email: router.query.email,
        password,
        confirm_password: retypePassword,
        forgotten_password_code: router.query.otp,
      },
      true
    ).then((response) => {
      if (response.status === "success") {
        setState({ ...state, modalFinish: true });
      }
    });
  };

  const {
    password,
    retypePassword,
    passwordIsError,
    retypePasswordIsError,
    modalFinish
  } = state;

  return (
    <Layout title="Password Recovery - Indigo">
      <AuthLayout
        headerDisableRoute={true}
        Breadcrumb
        breadcrumbOnBackhandler={() => router.push("/forgot-password")}
      >
        <div className="x-forgotpassword-page">
          <Container className="d-flex justify-content-center align-items-center w-100 h-100">
            <div className="x-card-form">
              <h1
                className="pb-1 heading-xl-bolder"
                style={{ color: "var(--grey-5)" }}
              >
                Buat password baru
              </h1>
              <p className="text-lg-normal" style={{ color: "var(--grey-3)" }}>
                Tuliskan password kamu minimal 8 karakter
              </p>

              <div className="pt-3">
                <InputPassword
                  title="Password baru"
                  extraTitle="(min. 8 karakter)"
                  value={password}
                  onChangeText={passwordOnChangeValue.bind(this)}
                  placeholder="Masukkan password baru"
                  errorSign={
                    "Password kurang sesuai, silahkan gunakan karakter lain"
                  }
                  isError={passwordIsError}
                />

                <InputPassword
                  title="Konfirmasi password"
                  onChangeText={retypePasswordOnChangeValue.bind(this)}
                  value={retypePassword}
                  placeholder="Masukkan kembali password baru"
                  errorSign="Password tidak sama, silahkan cek kembali"
                  isError={retypePasswordIsError}
                />
              </div>

              <Button
                className="btn-primary mt-3"
                onClick={onSubmit}
                disabled={submitDisabled}
              >
                Ubah password
              </Button>
            </div>
          </Container>

          <ModalConfirmation
            backdrop={"static"}
            onEscapeKeyDown={() => router.push("/login")}
            show={modalFinish}
            onHide={() => setState({ ...state, modalFinish: false })}
            customTitle={
              <div className="x-changepassword-success">
                <div className="x-icon" />
                <h3
                  className="heading-md-bold mt-4"
                  style={{ color: "var(--neutral-100)" }}
                >
                  Password berhasil diubah
                </h3>
                <p
                  className="text-lg-light"
                  style={{ color: "var(--neutral-60)" }}
                >
                  Password baru sudah aktif, silahkan login kembali
                </p>
                <Button
                  className="btn-primary"
                  onClick={() => router.push("/login")}
                >
                  Kembali ke halaman login
                </Button>
              </div>
            }
          />
        </div>
      </AuthLayout>
    </Layout>
  );
};

export default publicRoute(PasswordRecovery);
