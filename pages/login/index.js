import React, { useEffect, useState } from "react";
import { postCheckEmail, postLogin } from "/services/actions/api";
import { isNil } from "lodash-es";
import { Container, Button } from "react-bootstrap";
import { isEmailValid, setAuth } from "/services/utils/helper";
import InputPassword from "/components/input/InputPassword";
import InputText from "/components/input/InputText";
import AuthLayout from "/components/layout/AuthLayout";
import { useRouter } from "next/router";

import { useDispatch } from "react-redux";
import { setRegisterBehaviour, setRegisterData } from "/redux/slices/registerSlice";
import Layout from "/components/common_layout/Layout";
import publicRoute from "/components/routes/publicRoute";
import LoginForm from "/components/authentication/LoginForm";

const Login = () => {
  const [state, setState] = useState({
    passwordIsHide: true,
    email: "",
    password: "",
    emailIsError: false,
    passwordIsError: false
  });

  const dispatch = useDispatch();

  const [submitDisabled, setSubmitDisabled] = useState(true);

  const router = useRouter();

  useEffect(() => {
    if(isEmailValid(state.email) && state.password) {
      state.password.length > 6 && setSubmitDisabled(false);
    }
    else setSubmitDisabled(true)
  }, [state.email, state.password]);

  const emailOnChangeValue = (event) => {
    const email = event.target.value;
    setState({...state, email, emailIsError: false, passwordIsError: false });
  };

  const passwordOnChangeValue = (event) => {
    const password = event.target.value;
    setState({...state, password, passwordIsError: false, emailIsError: false });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    const { email, password } = state;

    if (!isEmailValid(email)) setState({ ...state, emailIsError: true });
    else {
      postLogin({ email, password }).then(async (response) => {
        const { status, result } = response.data;
        if (status === "failed") {
          postCheckEmail({ email }).then(({ data: { status } }) => {
            if (status === "success") {
              setState({ ...state, passwordIsError: true });
            } else {
              setState({ ...state, emailIsError: true });
            }
          });
        } else {
          const {
            email,
            company,
            full_name,
            gender,
            id_city,
            id_user,
            job_title,
            profile_picture,
            interest,
            location,
            event_title,
            profile_type,
            referral_code,
            registered_via,
            registration_step,
            token,
          } = result;

          setAuth({
            id_user,
            email,
            registration_step,
            event_title,
            token,
            picture_user: profile_picture,
            name_user: full_name,
          }).then((res) => {
            if (registration_step !== "finish") {
              dispatch(
                setRegisterData({
                  company,
                  email: !isNil(email) ? email : "",
                  gender,
                  id_city,
                  id_user,
                  city_name: !isNil(location) ? location.city_name : "",
                  city_keyword: !isNil(location) ? location.city_name : "",
                  interest: !isNil(interest) ? interest : [],
                  job_title,
                  job_title_keyword: job_title,
                  full_name: !isNil(full_name) ? full_name : "",
                  password,
                  profile_picture,
                  profile_type,
                  referral_code: !isNil(referral_code) ? referral_code : "",
                  registered_via,
                })
              );

              dispatch(setRegisterBehaviour({ step: registration_step }));
            }
            router.reload();
          });
        }
      });
    }
  };

  const { emailIsError, passwordIsError } = state;

  return (
    <Layout title="Login - Indigo">
      <AuthLayout>
        <div className="x-login-page">
          <LoginForm
            onSubmit={onSubmit}
            onEmailInput={emailOnChangeValue.bind(this)}
            onPasswordInput={passwordOnChangeValue.bind(this)}
            isButtonDisable={submitDisabled}
            emailIsError={emailIsError}
            passwordIsError={passwordIsError}
          />
        </div>
      </AuthLayout>
    </Layout>
  );
}

export default publicRoute(Login);
