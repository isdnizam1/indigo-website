import React from 'react';
import AgendaDetail from "/pages/home/agenda/detail";

const SessionDetail = () => {
  return (
    <AgendaDetail type="direct-link" />
  );
}

export default SessionDetail;
