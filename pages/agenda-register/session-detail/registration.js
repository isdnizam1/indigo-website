import React from 'react';
import AgendaRegistration from "/pages/home/agenda/detail/registration";

const Registration = () => {
  return (
    <AgendaRegistration type="direct-link" />
  );
}

export default Registration;
