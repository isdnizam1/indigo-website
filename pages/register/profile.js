import { useEffect, useState } from "react";
import { Container, Button } from "react-bootstrap";
import InputText from "/components/input/InputText";
import InputSelect from "/components/input/InputSelect";
import DefaultPicture from "/public/assets/images/img-default-photo.svg";
import AddPhoto from "/components/input/AddPhoto";
import InputToggleGroup from "/components/input/InputToggleGroup";
import {
  getCity,
  postRegisterV3SetupProfile,
  getNearestSpaces,
} from "/services/actions/api";
import { isEmpty, isNil } from "lodash-es";
import { useDispatch, useSelector } from "react-redux";
import { convertToBase64, setAuth, getAuth, apiDispatch } from "/services/utils/helper";
import AuthLayout from "/components/layout/AuthLayout";

import { setRegisterBehaviour, setRegisterData } from "/redux/slices/registerSlice";
import { useRouter } from "next/router";
import Layout from "/components/common_layout/Layout";
import authRoute from "../../components/routes/authRoute";
import InputSelectV2 from "/components/input/InputSelectV2";

const RegisterProfile = () => {
  const [allCities, setAllCities] = useState([]);
  const [allNearests, setAllNearests] = useState([]);
  const [suggestionCities, setSuggestionCities] = useState([]);
  const router = useRouter();

  const genderData = [
    { name: "Pria", value: "male" },
    { name: "Wanita", value: "female" }
  ];

  const { id_user } = getAuth();

  const dispatch = useDispatch();
  const registerData = useSelector((state) => state.registerReducer);
  const {
    full_name,
    profile_type,
    id_city,
    city_name,
    city_keyword,
    id_space,
    id_company,
    profile_picture,
    gender,
  } = registerData.data;

  useEffect(() => getData(), []);

  const getData = () => {
    apiDispatch(getCity, { start: 0 }, true).then(response => {
      response.code === 200 && setAllCities(response.result);
    }).catch(err => console.log(err));

    apiDispatch(getNearestSpaces, { setting_name: "indigospace", key_name: "location", show: "array" }, true).then(response => {
      response.code === 200 && setAllNearests(response.result);
    }).catch(err => console.log(err));
  };

  const onImageChange = (event) => {
    if (event.target.files && event.target.files[0]) {
      let img = event.target.files[0];
      convertToBase64(img).then((result) =>
        dispatch(setRegisterData({ profile_picture: result }))
      );
    }
  };

  const onSubmit = () => {
    const picture = !isNil(profile_picture) && profile_picture.indexOf("base64") >= 0
      ? profile_picture.split("base64,")[1]
      : null

    apiDispatch(postRegisterV3SetupProfile, {
      name: full_name,
      id_city,
      id_user,
      id_space,
      profile_type,
      id_company,
      profile_picture: picture,
      gender,
    }, true).then(response => {
      console.log(response)
      if(response.code === 200) {
        setAuth({ ...getAuth(), name_user: full_name });
        dispatch(setRegisterBehaviour({ step: 3 }));
        router.push("/register/profession");
      }
    }).catch(err => console.log(err))
  };

  const handleCityChange = (keyword) => {
    let datas = allCities.filter((data) => {
      return data.city_name.toLowerCase().includes(keyword.toLowerCase());
    });

    dispatch(setRegisterData({city_keyword: keyword}));

    setSuggestionCities(!isEmpty(keyword) ? datas.slice(0, 6) : []);
  };

  return (
    <Layout title="Register Profile - Indigo">
      <AuthLayout
        Breadcrumb
        breadcrumbOnBackhandler={"register"}
      >
        <div className="x-registerprofile-page">
          <Container className="d-flex justify-content-center align-items-center w-100 h-100">
            <div className="x-card-form">
              <AddPhoto
                image={
                  !isNil(profile_picture) ? profile_picture : DefaultPicture.src
                }
                onClick={(e) => onImageChange(e)}
              />
              <div className="x-separator" />
              <h4 className="heading-sm-bold">PROFILE PERSONAL</h4>

              <InputText
                title="Nama"
                type="text"
                value={full_name}
                onChangeText={(event) =>
                  dispatch(setRegisterData({ full_name: event.target.value }))
                }
                placeholder="Tuliskan nama lengkap kamu"
              />

              <InputSelect
                title="Kota"
                type="text"
                value={city_keyword}
                placeholder="Pilih kota domisili"
                data={suggestionCities}
                selectedValue={city_name}
                onFocus={() =>
                  isEmpty(city_name)
                    ? setSuggestionCities(allCities.slice(0, 6))
                    : handleCityChange(city_name)
                }
                onRemoveSelected={() => {
                  dispatch(
                    setRegisterData({
                      id_city: null,
                      city_name: "",
                      city_keyword: "",
                    })
                  );
                  setSuggestionCities([]);
                }}
                onBlur={() => {
                  dispatch(setRegisterData({ city_keyword: city_name }));
                  setTimeout(() => setSuggestionCities([]), 300);
                }}
                onChangeText={(event) => handleCityChange(event.target.value)}
                suggestionText={"city_name"}
                suggestionOnClick={(city) => {
                  dispatch(
                    setRegisterData({
                      id_city: city.id_city,
                      city_name: city.city_name,
                      city_keyword: city.city_name,
                    })
                  );
                  setSuggestionCities([]);
                }}
              />

              <InputToggleGroup
                title="Jenis kelamin"
                data={genderData}
                onClick={(e) =>
                  dispatch(setRegisterData({ gender: e.target.value }))
                }
                toggleValue={gender}
              />

              <InputSelectV2
                value={id_space}
                name="selectedSuggest"
                title={"Nearest IndigoSpace"}
                placeholder="Select your nearest IndigoSpace from your city"
                onChange={(e) => dispatch(setRegisterData({id_space: e.target.value}))}
                customOptions={allNearests?.map((item, idx) => (
                  <option key={idx} value={item.id} selected={id_space}>
                    {item.space}
                  </option>
                ))}
              />

              <Button
                className="btn-primary mt-4"
                onClick={() => onSubmit()}
                disabled={
                  isEmpty(full_name) ||
                  isEmpty(id_city) ||
                  isEmpty(gender) ||
                  isEmpty(city_name) ||
                  isEmpty(id_space)
                }
              >
                Buat profile
              </Button>
            </div>
          </Container>
        </div>
      </AuthLayout>
    </Layout>
  );
};

export default authRoute(RegisterProfile);
