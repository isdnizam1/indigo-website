import React, { Fragment } from "react";
import { Container, Button } from "react-bootstrap";
import AuthLayout from "/components/layout/AuthLayout";
import { useRouter } from "next/router";
import Layout from "/components/common_layout/Layout";
import BadgeAppstore from "public/assets/images/img-badge-appstore.svg";
import BadgePlaystore from "public/assets/images/img-badge-playstore.svg";

const RegisterSuccess = ({ isMobileView }) => {
  const router = useRouter();

  return (
    <Layout title="Register Success - Indigo">
      <AuthLayout>
        <div className="x-registersuccess-page">
          <Container className="d-flex justify-content-center align-items-center w-100 h-100">
            <div className="x-card-form">
              <div className="x-icon-success" />

              <h2
                className="text-center pb-3 pt-5 heading-lg-bolder"
                style={{ color: "var(--body-50)" }}
              >
                Your profile created successfully
              </h2>

              <p
                className="text-center text-lg-normal m-0"
                style={{ color: "var(--grey-3)" }}
              >
                Congrats! Start participating in a series of events and
                activities right now anywhere.
                {isMobileView && (
                  <Fragment>
                    <br />
                    You can access Indigo Connect from your Mobile Device via{" "}
                    <span className="color-semantic-main">
                      Indigo Connect App
                    </span>
                  </Fragment>
                )}
              </p>

              {isMobileView && (
                <Fragment>
                  <div className="d-flex justify-content-between w-100 gap-2 mb-4">
                    <img src={BadgeAppstore.src} alt="" width="100%" />
                    <img
                      src={BadgePlaystore.src}
                      alt=""
                      width="100%"
                      onClick={() =>
                        window.open(
                          "https://play.google.com/store/apps/details?id=com.eventeer.indigo",
                          "_blank"
                        )
                      }
                    />
                  </div>
                  <p
                    className="text-center text-lg-normal m-0"
                    style={{ color: "var(--grey-3)" }}
                  >
                    Or you can access through Desktop/Laptop via our website{" "}
                    <span className="color-semantic-main">
                      www.connect.indigo.id
                    </span>
                  </p>
                </Fragment>
              )}

              {!isMobileView && (
                <Button
                  className="btn-primary mt-4"
                  onClick={() => router.push("/home")}
                >
                  Start Now
                </Button>
              )}
            </div>
          </Container>
        </div>
      </AuthLayout>
    </Layout>
  );
};

RegisterSuccess.getInitialProps = async (ctx) => {
  let isMobileView = (
    ctx.req ? ctx.req.headers["user-agent"] : navigator.userAgent
  ).match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile|WPDesktop/i);

  //Returning the isMobileView as a prop to the component for further use.
  return {
    isMobileView: Boolean(isMobileView),
  };
};

export default RegisterSuccess;
