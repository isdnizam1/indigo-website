import { useEffect, useState } from "react";
import { Container, Button } from "react-bootstrap";
import InputSelect from "/components/input/InputSelect";
import DefaultPicture from "/public/assets/images/img-default-photo.svg";
import AddPhoto from "/components/input/AddPhoto";
import { convertToBase64, getAuth, setAuth, apiDispatch } from "/services/utils/helper";
import { isEmpty, isNil } from "lodash-es";
import { getGenreInterest, postRegisterV3SelectGenre } from "/services/actions/api";
import AuthLayout from "/components/layout/AuthLayout";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import {
  setRegisterData,
  resetRegisterBehaviour,
  resetRegisterData,
} from "/redux/slices/registerSlice";
import Layout from "/components/common_layout/Layout";
import authRoute from "../../components/routes/authRoute";

const RegisterInterest = () => {
  const [allInterests, setAllInterests] = useState([]);
  const [suggestionInterests, setSuggestionInterests] = useState([]);
  const [interestKeyword, setInterestKeyword] = useState("");
  const [selectedInterest, setSelectedInterest] = useState([]);

  useEffect(() => getData(), []);

  const router = useRouter();
  const dispatch = useDispatch();
  const registerData = useSelector((state) => state.registerReducer);
  const { id_user } = getAuth();
  const { email, profile_picture, interest } = registerData.data;

  const getData = () => {
    apiDispatch(getGenreInterest, { start: 0 }, true).then((response) => {
      if (response.code === 200) {
        const data = response.result;
        setAllInterests(data);
      }
    });
  };

  const onImageChange = (event) => {
    if (event.target.files && event.target.files[0]) {
      let img = event.target.files[0];
      convertToBase64(img).then((result) =>
        setRegisterData({ profile_picture: result })
      );
    }
  };

  const handleInterestChange = (keyword) => {
    let datas = allInterests.filter((data) => {
      return data.interest_name.toLowerCase().includes(keyword.toLowerCase());
    });

    setInterestKeyword(keyword);
    setSuggestionInterests(!isEmpty(keyword) ? datas.slice(0, 10) : []);
  };

  const onSubmit = () => {
    const picture =
      !isNil(profile_picture) && profile_picture.indexOf("base64") >= 0
        ? profile_picture.split("base64,")[1]
        : null;

    apiDispatch(
      postRegisterV3SelectGenre,
      { id_user, interest_name: interest, profile_picture: picture },
      true
    )
      .then((response) => {
        if (response.code === 200) {
          const { picture_user, name_user, token } = getAuth();
          if (response.code === 200) {
            setAuth({
              id_user,
              email,
              registration_step: "finish",
              event_title: "Indigo Alpha",
              picture_user,
              name_user,
              token,
            }).then((result) => {
              dispatch(resetRegisterData());
              dispatch(resetRegisterBehaviour());
              localStorage.clear();
              router.push("/register/success");
            });
          }
        }
      })
      .catch((err) => console.log(err));
  };

  return (
    <Layout title="Register Interest - Indigo">
      <AuthLayout
        Breadcrumb
        breadcrumbOnBackhandler={() => router.push("/register/profession")}
      >
        <div className="x-registerinterest-page">
          <Container className="d-flex justify-content-center align-items-center w-100 h-100">
            <div className="x-card-form">
              <AddPhoto
                image={
                  !isNil(profile_picture) ? profile_picture : DefaultPicture.src
                }
                onClick={(event) => onImageChange(event)}
              />
              <div className="x-separator" />
              <h4 className="heading-sm-bold">CHOOSE INTEREST</h4>

              <InputSelect
                title="Interest"
                extraTitle="(contoh: JavaScript, e-commerce, edukasi, dll)"
                type="text"
                value={interestKeyword}
                placeholder="Tambahkan interest"
                data={suggestionInterests}
                // disableInput={selectedInterest.length > 2}
                selectedValue={""}
                disableError={true}
                icon={"add"}
                onFocus={() => setSuggestionInterests(allInterests.slice(0, 6))}
                onKeyPress={(event) => {
                  if (event.key === "Enter") {
                    setInterestKeyword("");
                    setSuggestionInterests([]);
                    setSelectedInterest([
                      ...selectedInterest,
                      event.target.value,
                    ]);
                    dispatch(setRegisterData({ interest: selectedInterest }));
                  }
                }}
                onBlur={() => {
                  setTimeout(() => {
                    setInterestKeyword("");
                    setSuggestionInterests([]);
                  }, 300);
                }}
                onChangeText={(event) =>
                  handleInterestChange(event.target.value)
                }
                suggestionText={"interest_name"}
                suggestionOnClick={(interest) => {
                  setInterestKeyword("");
                  setSuggestionInterests([]);
                  dispatch(setRegisterData({ interest: [...selectedInterest, interest.interest_name] }));
                  setSelectedInterest([...selectedInterest, interest.interest_name]);
                }}
              />

              <div className="d-flex align-items-center flex-wrap w-100 pt-1">
                {interest.map((item, idx) => (
                  <div className="x-tag-interest" key={idx}>
                    <p
                      className="text-sm-normal m-0 line-height-sm p-0"
                      style={{ color: "var(--primary-main)" }}
                    >
                      {item}
                    </p>
                    <span
                      className="material-icons-round"
                      onClick={() => {
                        setSelectedInterest(selectedInterest.filter(data => item !== data));
                        dispatch(setRegisterData({ interest: selectedInterest.filter(data => item !== data) }));
                      }}
                    >
                      close
                    </span>
                  </div>
                ))}
              </div>

              <Button
                className="btn-primary mt-4"
                onClick={() => onSubmit()}
                disabled={isEmpty(interest)}
              >
                Pilih Interest
              </Button>
            </div>
          </Container>
        </div>
      </AuthLayout>
    </Layout>
  );
};

export default authRoute(RegisterInterest);
