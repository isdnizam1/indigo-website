import React, { useEffect, useState } from "react";
import { Container, Button } from "react-bootstrap";
import InputPassword from "/components/input/InputPassword";
import InputText from "/components/input/InputText";
import ModalConfirmation from "/components/modal/ModalConfirmation";
import { postCheckEmail, postRegisterV3 } from "/services/actions/api";
import { isEmpty } from "lodash-es";
import { isEmailValid, setAuth, apiDispatch } from "/services/utils/helper";
import { useDispatch } from "react-redux";
import AuthLayout from "/components/layout/AuthLayout";
import { useRouter } from "next/router";
import { setRegisterBehaviour, setRegisterData } from "/redux/slices/registerSlice";
import Layout from "/components/common_layout/Layout";
import publicRoute from "/components/routes/publicRoute";

const Register = () => {
  const [state, setState] = useState({
    passwordIsHide: true,
    email: "",
    password: "",
    retypePassword: "",
    registerDataIsValid: false,
    modalIsOpened: false,
    isEmailSent: false,
    OTPTimerValue: "0:00",
    OTPTimerIsActive: false,
    emailErrorSign: "",
    emailIsError: false,
    passwordIsError: false,
    retypePasswordIsError: false,
  });

  const [submitDisabled, setSubmitDisabled] = useState(true);

  const router = useRouter();

  const dispatch = useDispatch();

  useEffect(() => setOTPTimer(30), [state.OTPTimerIsActive]);

  useEffect(() => {
    if (isEmailValid(state.email) && state.password && state.retypePassword) {
      if (state.password.length >= 8 && state.retypePassword.length >= 8) {
        if (state.password === state.retypePassword) setSubmitDisabled(false);
        else setSubmitDisabled(true);
      } else setSubmitDisabled(true);
    } else setSubmitDisabled(true);
  }, [state.email, state.password, state.retypePassword]);

  const initialState = () => {
    setState({
      passwordIsHide: true,
      email: "",
      password: "",
      retypePassword: "",
      registerDataIsValid: false,
      modalIsOpened: false,
      isEmailSent: false,
      OTPTimerValue: "0:00",
      OTPTimerIsActive: false,
      emailErrorSign: "",
      emailIsError: false,
      passwordIsError: false,
      retypePasswordIsError: false,
    });
  };

  const emailOnChangeValue = (event) => {
    const email = event.target.value;
    setState({ ...state, email, emailIsError: false });
  };

  const passwordOnChangeValue = (event) => {
    const password = event.target.value;

    setState({
      ...state,
      password,
      passwordIsError: password?.length && password.length < 8 ? true : false,
      retypePasswordIsError:
        state.retypePassword?.length && state.retypePassword !== password
          ? true
          : false,
    });
  };

  const retypePasswordOnChangeValue = (event) => {
    const retypePassword = event.target.value;

    setState({
      ...state,
      retypePassword,
      passwordIsError:
        state.password?.length && state.password.length < 8 ? true : false,
      retypePasswordIsError: retypePassword !== state.password ? true : false,
    });
  };

  const checkEmail = (email) => {
    if (!isEmpty(email)) {
      if (!isEmailValid(email)) {
        setState({
          ...state,
          emailIsError: true,
          emailErrorSign: "Email tidak sesuai, mohon periksa kembali.",
        });
      } else {
        postCheckEmail({ email }).then(({ data: { status } }) => {
          if (status === "success")
            setState({
              ...state,
              emailIsError: true,
              emailErrorSign:
                "Email ini sudah digunakan, silahkan coba email lain",
            });
          else setState({ ...state, emailIsError: false });
        });
      }
    }
  };

  const setOTPTimer = (remaining) => {
    let m = Math.floor(remaining / 60);
    let s = remaining % 60;

    s = s < 10 ? "0" + s : s;
    setState({ ...state, OTPTimerValue: m + ":" + s });
    remaining -= 1;

    if (remaining >= 0 && state.OTPTimerIsActive) {
      setTimeout(() => setOTPTimer(remaining), 1000);
      return;
    }

    if (!state.OTPTimerIsActive) {
      // Do validate stuff here
      return;
    }

    // Do timeout stuff here
    setState({ ...state, OTPTimerIsActive: false });
  };

  const validateRegisterData = () => {
    const {
      email,
      password,
      retypePassword,
      emailIsError,
      passwordIsError,
      retypePasswordIsError,
    } = state;
    if (
      !emailIsError &&
      !passwordIsError &&
      !retypePasswordIsError &&
      email.length &&
      password.length &&
      retypePassword.length
    ) {
      setState({ ...state, registerDataIsValid: true });
    }
  };

  const onSubmit = () => {
    const { email, password } = state;
    setState({ ...state, OTPTimerIsActive: true, isEmailSent: true });

    apiDispatch(postRegisterV3, { email, password }, true).then((response) => {
      if(response.code === 200) {
        const { id_user, token } = response.result;

        dispatch(setRegisterData({
          email, password, id_user
        }));
        dispatch(setRegisterBehaviour({step: 1}));

        setAuth({
          id_user,
          email,
          registration_step: "1",
          event_title: "",
          token,
          name_user: "",
        });

        setTimeout(() => router.reload(), 5000);
      }
    }).catch(err => console.log(err))
  };

  return (
    <Layout title="Register - Indigo">
      <AuthLayout>
        <Container fluid className="x-register-page">
          {!state.registerDataIsValid ? (
            <form
              onSubmit={validateRegisterData}
              className="x-card-form"
              disabled={submitDisabled}
            >
              <h1>Create account</h1>
              <p>Bergabung dan kolaborasi dengan community kamu !</p>

              <InputText
                title="Email"
                type="email"
                value={state.email}
                onChangeText={emailOnChangeValue.bind(this)}
                onBlur={(e) => checkEmail(e.target.value)}
                placeholder="Masukkan email"
                errorSign={state.emailErrorSign}
                isError={state.emailIsError}
              />

              <InputPassword
                title="Password"
                extraTitle="(min. 8 karakter)"
                value={state.password}
                onChangeText={passwordOnChangeValue.bind(this)}
                placeholder="Masukkan password"
                errorSign={
                  "Password kurang sesuai, silahkan gunakan karakter lain"
                }
                isError={state.passwordIsError}
              />

              <InputPassword
                title="Password"
                onChangeText={retypePasswordOnChangeValue.bind(this)}
                value={state.retypePassword}
                placeholder="Ketik ulang password"
                errorSign="Password tidak sama, silahkan cek kembali"
                isError={state.retypePasswordIsError}
              />

              <Button
                className="btn-primary mt-2 btn-mobile"
                type="submit"
                disabled={
                  state.passwordIsError ||
                  state.emailIsError ||
                  state.retypePasswordIsError ||
                  !state.email ||
                  !state.password ||
                  !state.retypePassword
                }
              >
                Buat akun
              </Button>
            </form>
          ) : (
            <div className="x-card-form pt-4">
              <div className="d-flex justify-content-center align-items-center flex-column text-center">
                <div className="x-eventeer-icon" />

                <div className="x-verification-icon" />

                <h3
                  className="heading-md-bolder pt-4 pb-3"
                  style={{ color: "var(--grey-5)" }}
                >
                  Verifikasi Email
                </h3>

                <p
                  className="ps-2 pe-2 pb-4 text-lg-normal"
                  style={{ color: "var(--grey-4)" }}
                >{`Tekan tombol berikut untuk mendapatkan verifikasi email ke ${state.email}`}</p>
              </div>

              <Button
                className="btn-primary mt-2"
                onClick={() => setState({ ...state, modalIsOpened: true })}
              >
                Kirim verifikasi
              </Button>
            </div>
          )}
        </Container>

        <ModalConfirmation
          backdrop={"static"}
          show={state.modalIsOpened}
          onHide={() => setState({ ...state, modalIsOpened: false })}
          customTitle={
            <>
              {!state.isEmailSent ? (
                <>
                  <h2
                    className={"heading-md-bold"}
                  >{`Lanjutkan buat akun dengan\n${state.email}?`}</h2>

                  <p
                    className="pt-1 text-lg-normal"
                    style={{ color: "var(--neutral-60)" }}
                  >
                    Belum ada akun yang menggunakan email ini
                  </p>

                  <Button
                    className="btn-primary w-100 mt-4"
                    onClick={() => onSubmit()}
                  >
                    Ya, lanjutkan
                  </Button>

                  <Button
                    className="btn-cancel w-100 mt-3"
                    onClick={() =>
                      setState({
                        ...state,
                        modalIsOpened: false,
                        registerDataIsValid: false,
                      })
                    }
                  >
                    Batalkan
                  </Button>
                </>
              ) : (
                <>
                  <h2 className="heading-md-bold">{`Email verifikasi telah dikirim`}</h2>

                  <p
                    className="pt-1 text-lg-normal"
                    style={{ color: "var(--neutral-60)" }}
                  >
                    {"Kami telah mengirimkan kode verifikasi ke "}
                    <span style={{ color: "var(--primary-main)" }}>
                      {state.email}
                    </span>
                    {", silahkan cek inbox atau spam kamu"}
                  </p>

                  <Button
                    className={
                      !state.OTPTimerIsActive ? "btn-cancel" : "btn-disable"
                    }
                    disabled={!state.OTPTimerIsActive ? false : true}
                  >
                    Kirim ulang verifikasi {state.OTPTimerValue}
                  </Button>
                </>
              )}
            </>
          }
          customFooter={
            state.isEmailSent && (
              <p
                className="text-center w-100 text-lg-normal"
                style={{ color: "var(--neutral-60)" }}
              >
                Terjadi kesalahan?
                <span
                  className="cursor-pointer"
                  style={{ color: "var(--neutral-100)" }}
                  onClick={() => initialState()}
                >
                  {" "}
                  Ulangi pendaftaran
                </span>
              </p>
            )
          }
        />
      </AuthLayout>
    </Layout>
  );
}

export default publicRoute(Register);
