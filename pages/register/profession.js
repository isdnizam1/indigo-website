import { useEffect, useState } from "react";
import { Container, Button } from "react-bootstrap";
import InputSelect from "/components/input/InputSelect";
import DefaultPicture from "/public/assets/images/img-default-photo.svg";
import AddPhoto from "/components/input/AddPhoto";
import { useDispatch, useSelector } from "react-redux";
import { isEmpty, isNil } from "lodash-es";
import { convertToBase64, getAuth, apiDispatch } from "/services/utils/helper";
import { getJob, postRegisterV3CreateCompanyJob } from "/services/actions/api";
import AuthLayout from "/components/layout/AuthLayout";
import { useRouter } from "next/router";
import { setRegisterBehaviour, setRegisterData } from "/redux/slices/registerSlice";
import Layout from "/components/common_layout/Layout";
import authRoute from "../../components/routes/authRoute";

const RegisterProfession = () => {
  const [allJobs, setAllJobs] = useState([]);
  const [suggestionJobs, setSuggestionJobs] = useState([]);

  const dispatch = useDispatch();
  const registerData = useSelector(state => state.registerReducer);
  const router = useRouter();

  const { id_user } = getAuth();

  const { job_title, profile_picture, job_title_keyword } = registerData.data;

  useEffect(() => getData(), []);

  const getData = () => {
    apiDispatch(getJob, { start: 0 }, true).then(response => {
      if(response.code === 200) {
        const result = response.result;
        setAllJobs(result);
      }
    })
  };

  const handleJobChange = (keyword) => {
    let datas = allJobs.filter((data) => {
      return data.job_title.toLowerCase().includes(keyword.toLowerCase());
    });

    dispatch(setRegisterData({ job_title_keyword: keyword }))

    setSuggestionJobs(!isEmpty(keyword) ? datas.slice(0, 10) : []);
  };

  const onImageChange = (event) => {
    if (event.target.files && event.target.files[0]) {
      let img = event.target.files[0];
      convertToBase64(img).then((result) =>
        dispatch(setRegisterData({ profile_picture: result }))
      );
    }
  };

  const onSubmit = () => {
    const picture = !isNil(profile_picture) && profile_picture.indexOf("base64") >= 0
      ? profile_picture.split("base64,")[1]
      : null

    apiDispatch(postRegisterV3CreateCompanyJob, { id_user, job_title, profile_picture: picture }, true).then((response) => {
      if(response.code === 200) {
        dispatch(setRegisterBehaviour({ step: 4 }));
        router.push("/register/interest");
      }
    }).catch(err => console.log(err));
  }

  return (
    <Layout title="Register Profession - Indigo">
      <AuthLayout
        Breadcrumb
        breadcrumbOnBackhandler={() => router.push("/register/profile")}
      >
        <div className="x-registerprofession-page">
          <Container className="d-flex justify-content-center align-items-center w-100 h-100">
            <div className="x-card-form">
              <AddPhoto
                image={
                  !isNil(profile_picture) ? profile_picture : DefaultPicture.src
                }
                onClick={(event) => onImageChange(event)}
              />
              <div className="x-separator" />
              <h4 className="heading-sm-bold">PROFESSION INFORMATIONS</h4>

              <InputSelect
                title="Profesi"
                type="text"
                value={job_title_keyword}
                placeholder="Pilih profesi kamu"
                data={suggestionJobs}
                disableError={true}
                selectedValue={job_title}
                onFocus={() =>
                  isEmpty(job_title)
                    ? setSuggestionJobs(allJobs.slice(0, 10))
                    : handleJobChange(job_title)
                }
                onRemoveSelected={() => {
                  dispatch(setRegisterData({ job_title: "", job_title_keyword: "" }));
                  setSuggestionJobs([]);
                }}
                onKeyPress={(event) => {
                  if (event.key === "Enter") {
                    dispatch(
                      setRegisterData({
                        job_title: event.target.value,
                        job_title_keyword: event.target.value,
                      })
                    );
                    setSuggestionJobs([]);
                  }
                }}
                onBlur={(event) => {
                  dispatch(
                    setRegisterData({
                      job_title: event.target.value,
                      job_title_keyword: event.target.value,
                    })
                  );
                  setTimeout(() => setSuggestionJobs([]), 300);
                }}
                onChangeText={(event) => handleJobChange(event.target.value)}
                suggestionText={"job_title"}
                suggestionOnClick={(job) => {
                  dispatch(
                    setRegisterData({
                      job_title: job.job_title,
                      job_title_keyword: job.job_title,
                    })
                  );
                  setSuggestionJobs([]);
                }}
              />

              <Button
                className="btn-primary mt-4"
                onClick={() => onSubmit()}
                disabled={isEmpty(job_title)}
              >
                Lanjutkan
              </Button>
            </div>
          </Container>
        </div>
      </AuthLayout>
    </Layout>
  );
};

export default authRoute(RegisterProfession);
