/* eslint-disable no-useless-escape */
import React, { Fragment } from "react";
import {
  getAdsDetail,
  getCheckVoucher,
  postJoinSession,
} from "/services/actions/api";
import { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { useRouter } from "next/router";
import { apiDispatch, getAuth, webinarFormatter } from "/services/utils/helper";
import InputText from "/components/input/InputText";
import MainLayout from "/components/layout/MainLayout";
import Layout from "/components/common_layout/Layout";
// import appRoute from "/components/routes/appRoute";
import { isEmpty } from "lodash-es";
import CardLv2 from "/components/home/CardLv2";
import classnames from "classnames";
import ContentBox from "/components/home/ContentBox";
import MaterialIcon from "/components/utils/MaterialIcon";
import ModalLogin from "/components/modal/ModalLogin";
import ModalConfirmation from "/components/modal/ModalConfirmation";
import ModalOnSubmit from "/components/modal/ModalOnSubmit";
import ModalDetailSpeaker from "/components/modal/ModalDetailSpeaker";

const AgendaRegistration = ({ type }) => {
  const [modalLogin, setModalLogin] = useState(false);
  const [modalRegisterConfirmation, setModalRegisterConfirmation] = useState(false);
  const [modalPostSubmit, setModalPostSubmit] = useState(false);
  const [agenda, setAgenda] = useState({
    thumb: "",
    title: "",
    vendor: "",
    date: {},
    speaker: [],
    price: {},
  });
  const [data, setData] = useState({
    name: "",
    nameCount: 0,
    phone: "",
    email: "",
    voucher: "",
  });
  const [error, setError] = useState({
    name: true,
    phone: true,
    email: true,
    voucher: false,
  });
  const [isLoadingSubmit, setIsLoadingSubmit] = useState(false);
  const [price, setPrice] = useState({});
  const [voucherCode, setVoucherCode] = useState();

  const { thumb, title, vendor, date, speaker } = agenda;
  const { name, phone, email, voucher, nameCount } = data;

  // const initialData = {
  //   name: "",
  //   nameCount: 0,
  //   phone: "",
  //   email: "",
  //   voucher: "",
  // };
  // const initialAgenda = {
  //   thumb: "",
  //   title: "",
  //   vendor: "",
  //   date: {},
  //   speaker: [],
  //   price: {},
  // };
  // const initialError = {
  //   name: true,
  //   phone: true,
  //   email: true,
  //   voucher: false,
  // };

  const { isReady, query, push, pathname } = useRouter();
  const { id_user } = getAuth();
  const { id: id_ads } = query;

  useEffect(() => isReady && getData(), [isReady]);

  useEffect(() => {
    nameCount > 0
      ? setError({ ...error, name: false })
      : setError({ ...error, name: true });

    phone.match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im)
      ? setError({ ...error, phone: false })
      : setError({ ...error, phone: true });

    email.match(
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
    )
      ? setError({ ...error, email: false })
      : setError({ ...error, email: true });
  }, [data]);


  const getData = () => {
    if (!id_ads) return push("/home/agenda");
    else if (
      isEmpty(getAuth()) &&
      pathname === "/home/agenda/detail/registration"
    )
      return push({
        pathname: "/agenda-register/session-detail/registration",
        query: { id: id_ads },
      });
    else if (
      !isEmpty(getAuth()) &&
      pathname === "/agenda-register/session-detail/registration"
    )
      return push({
        pathname: "/home/agenda/detail/registration",
        query: { id: id_ads },
      });

    apiDispatch(getAdsDetail, { id_ads, id_user }, true)
      .then((response) => {
        if (response.code === 200 && !isEmpty(response.result)) {
          const result = response.result;

          const { available_seats, status } = result;

          if (available_seats < 1 || status !== "active")
            return push({
              pathname: "/agenda-register/session-detail",
              query: { id: id_ads },
            });

          console.log({ result });

          setAgenda({
            thumb: result.image,
            title: result.title,
            vendor: result.vendor_name,
            date: result.additional_data_arr.date,
            speaker: result.speaker,
          });

          setPrice(result.additional_data_arr.price);
          return result.available_seats;
        } else push(isEmpty(getAuth()) ? "/login" : "/home/agenda");
      })
      .catch((err) => console.log(err));
  };

  const handleVoucherCheck = () => {
    apiDispatch(getCheckVoucher, { voucher_code: voucher, id_user }, true).then(
      (response) => {
        if (response.code === 200) {
          const result = response.result;
          setError({ ...error, voucher: false });
          if (result.status === "active") {
            const x = price.discount.replace("%", "") + result.amount_discount;
            if (x > 100) {
              setPrice({
                discount: "100%",
                normal_price: price.normal_price,
                discount_price: "0",
              });
              setVoucherCode(result.token_voucher);
            } else {
              setPrice({
                discount: x + "%",
                normal_price: price.normal_price,
                discount_price: (
                  price.normal_price -
                  (price.normal_price * result.amount_discount) / 100
                ).toString(),
              });
              setVoucherCode(result.token_voucher);
            }
          }
        } else setError({ ...error, voucher: true });
      }
    );
  };

  const handleSubmit = (e) => {
    // e.preventDefault();
    setIsLoadingSubmit(true);

    apiDispatch(
      postJoinSession,
      {
        id_user,
        id_ads,
        whatsapp: phone,
        full_name: name,
        email,
        payment_channel: "echannel",
        token_voucher: voucherCode ? voucherCode : "",
      },
      true
    ).then((response) => {
      setIsLoadingSubmit(false);

      if (response.code === 200) {
        const { result, payment_midtrans, redirect_payment } = response;

        payment_midtrans && window.location.replace(redirect_payment);
        // else
        //   push({
        //     pathname: "/home/agenda/detail",
        //     query: {
        //       id: id_ads,
        //     },
        //   });
      }
    });
  };

  const renderModalLogin = () => {
    return <ModalLogin show={modalLogin} onHide={() => setModalLogin(false)} />;
  }

  const renderModalRegisterConfirmation = () => {
    return (
      <ModalConfirmation
        show={modalRegisterConfirmation}
        onHide={() => setModalRegisterConfirmation(false)}
        HasExitIcon
        title="Ticket will be issued."
        subTitle={`Are your booking information is correct? You will receive email after submit this agenda.`}
        onSubmit={() => {
          setModalRegisterConfirmation(false);
          setModalPostSubmit(true);
          handleSubmit();
        }}
        onSubmitText="Yes, submit"
      />
    );
  }


	const renderModalPostOnSubmit = () => {
		return (
      <ModalOnSubmit
        className="x-agendaregister-modal"
        backdrop={isLoadingSubmit ? "static" : true}
        size="md"
        show={modalPostSubmit}
        onHide={() => {
          isLoadingSubmit && setModalPostSubmit(false);
          push({
            pathname: !isEmpty(getAuth())
              ? "/home/agenda/detail"
              : "/agenda-register/session-detail",
            query: { id: id_ads },
          });
        }}
        title="You’ve registered this agenda successfully"
        subTitle="Congrats! Start participating in a series of events and activities right now anywhere"
        isLoading={isLoadingSubmit}
        ctaText="Back to Session Detail"
        action="success"
        cta={() => {
          setModalPostSubmit(false);
          push({
            pathname: !isEmpty(getAuth())
              ? "/home/agenda/detail"
              : "/agenda-register/session-detail",
            query: { id: id_ads },
          });
        }}
      />
    );
	};

  return (
    <Layout title="Agenda Registration - Indigo">
      <MainLayout
        Breadcrumb
        HideSideNavigationBar={(type === "direct-link" || isEmpty(getAuth())) ? true : false}
        HasMarginSideNavigationBar={(type === "direct-link" || isEmpty(getAuth())) ? true : false}
      >
        <div className="x-agendaregistration-page">
          <CardLv2
            className="x-bannercard"
            image={thumb}
            title={title}
            customSubTitle={
              <Fragment>
                <p className="text-lg-light color-neutral-old-40 m-0 mt-3">
                  Promote by <span className="text-lg-bold">{vendor}</span>
                </p>
                <div
                  className={classnames(
                    "d-flex align-items-center cursor-pointer mt-1"
                  )}
                >
                  <span className="material-icons -ms-4 color-secondary-main">
                    date_range
                  </span>
                  <p className="m-0 ms-2 text-lg-light color-neutral-old-40">
                    {!isEmpty(date.start) && webinarFormatter(date.start)}
                  </p>
                </div>
                <div
                  className={classnames(
                    "d-flex align-items-center cursor-pointer my-1"
                  )}
                >
                  <span className="material-icons color-neutral-old-40">
                    account_circle
                  </span>
                  {speaker.map((items, i) => (
                    <p
                      className="m-0 ms-2 text-lg-light color-neutral-old-40"
                      key={i}
                    >
                      {items.title}
                      {i !== speaker.length - 1 ? "," : ""}
                    </p>
                  ))}
                </div>
              </Fragment>
            }
          />

          <div className="p-3 mt-2">
            <div className="d-flex justify-content-between">
              <p className="text-lg-normal color-neutral-old-40 m-0">
                1 ticket{" "}
                {price.discount > "0" && `(discount ${price.discount})`}
              </p>
              <div className="d-flex gap-4">
                {price.discount > "0" && (
                  <p className="text-lg-light color-neutral-30 m-0 text-decoration-line-through">
                    {`Rp.${price.normal_price}`}
                  </p>
                )}
                <p className="text-lg-light color-neutral-70 m-0">{`Rp.${
                  price.discount > "0"
                    ? price.discount_price
                    : price.normal_price
                }`}</p>
              </div>
            </div>

            <hr className="w-100 my-3" />

            <div className="d-flex justify-content-between">
              <p className="text-lg-bold color-neutral-70 m-0">
                {"Total Payment : "}
              </p>
              <p className="text-lg-bold color-neutral-70 m-0">{`Rp.${price.discount_price}`}</p>
            </div>
            {isEmpty(getAuth()) && (
              <Fragment>
                <hr className="w-100 mt-2" />
                <ContentBox NoDivider className="d-flex justify-content-between align-items-center mt-2 p-3">
                  <div className="d-flex gap-3 align-items-center">
                    <MaterialIcon action="person" size={28} color="neutral-950" />
                    <p className="text-lg-normal color-base-900 m-0">
                      Log in to your Indigo Connect account for faster booking
                    </p>
                  </div>
                  <Button className="w-auto" onClick={() => setModalLogin(true)}>
                    <span className="px-4">Login</span>
                  </Button>
                </ContentBox>
              </Fragment>
            )}
          </div>

          <hr className="w-100 mb-3 mt-0" />

          <div className="px-3">
            {/* <form onSubmit={handleSubmit}> */}
              <div className="pb-2">
                <InputText
                  title={"Full Name "}
                  extraTitle="(for certificate)"
                  placeholder="Write your name"
                  value={name}
                  maxLength="22"
                  disableError={true}
                  onChangeText={(e) => {
                    setData({
                      ...data,
                      name: e.target.value,
                      nameCount: e.target.value.length,
                    });
                  }}
                />
                <div className="d-flex justify-content-end">
                  <p className="text-md-light color-neutral-old-40 m-0">
                    {nameCount}/22
                  </p>
                </div>
              </div>

              <InputText
                title="WhatsApp Number"
                placeholder="Write your whatsapp number"
                value={phone}
                type="tel"
                onChangeText={(e) => {
                  setData({ ...data, phone: e.target.value });
                }}
              />

              <InputText
                title="Email"
                placeholder="Write your email"
                type="email"
                disableError
                value={email}
                onChangeText={(e) => {
                  setData({
                    ...data,
                    email: e.target.value.toLowerCase(),
                  });
                }}
              />

              {!isEmpty(getAuth()) && (
                <Fragment>
                  <hr className="w-100 my-3" />

                  <div className="d-flex justify-content-between align-items-center gap-3">
                    <InputText
                      title="Voucher Code "
                      extraTitle="(optional)"
                      className={error.voucher && "x-input-error"}
                      placeholder="Write your voucher code"
                      value={voucher}
                      // disableError={true}
                      errorSign="Voucher tidak sesuai"
                      isError={error.voucher}
                      onChangeText={(e) => {
                        setData({ ...data, voucher: e.target.value });
                      }}
                    />
                    <Button
                      onClick={handleVoucherCheck}
                      className="btn-cancel w-25 py-2"
                    >
                      Use
                    </Button>
                  </div>
                </Fragment>
              )}

              <div className={classnames("d-flex justify-content-center flex-column align-items-center", isEmpty(getAuth()) && "mt-4")}>
                <Button
                  className="w-75 mt-4 p-3"
                  disabled={
                    (error.email || error.name || error.phone) &&
                    (nameCount === 0 ||
                      phone.length === 0 ||
                      email.length === 0)
                  }
                  // type="submit"
                  onClick={() => setModalRegisterConfirmation(true)}
                >
                  Register
                </Button>
                {price.discount > "0" && (
                  <p className="text-md-light color-grey-3 mt-3 mb-5">
                    You save <strong>{`Rp.${price.discount_price}`}</strong> (
                    {price.discount}) from this purchase
                  </p>
                )}
              </div>
            {/* </form> */}
          </div>
        </div>
      </MainLayout>
      {renderModalLogin()}
      {renderModalRegisterConfirmation()}
      {renderModalPostOnSubmit()}
    </Layout>
  );
};

// export default appRoute(AgendaRegistration);
export default AgendaRegistration;
