import React, { Fragment, useEffect, useState } from "react";
import { Button, Col, Container, Modal, Row } from "react-bootstrap";
import { getAdsDetail, postLogRequestSession } from "/services/actions/api";
import { getAuth, webinarFormatter, apiDispatch } from "/services/utils/helper";
import DefaultImage from "/public/assets/images/img-default-image.jpg";
import WebCover from "/public/assets/images/img-detailweb-cover.png";
import MainLayout from "/components/layout/MainLayout";
import { isEmpty, isString } from "lodash-es";
import ModalConfirmation from "/components/modal/ModalConfirmation";
import classnames from "classnames";
import InputText from "/components/input/InputText";
import { useRouter } from "next/router";
import Layout from "/components/common_layout/Layout";
// import appRoute from "/components/routes/appRoute";
import RightBarBox from "/components/home/RightBarBox";
import IdentityCard from "/components/home/IdentityCard";
import SkeletonElement from "/components/skeleton/SkeletonElement";
import MaterialIcon from "/components/utils/MaterialIcon";
import bannerSpeaker from "/public/assets/images/img-banner-speakerdetail.svg";
import CardLv2 from "/components/home/CardLv2";

const AgendaDetail = ({ type }) => {
  const [data, setData] = useState({
    title: "",
    image: DefaultImage,
    vendorName: "",
    dateSession: "",
    ticket: "",
    description: "",
    status: "",
    userJoined: true,
    link: "",
    isSessionReq: false,
    normalPrice: "",
    discountPrice: "",
    discount: "",
    speaker: [],
    copySuccess: "",
  });
  const [isLoadingData, setIsLoadingData] = useState(true);
  const [modalRequestSession, setModalRequestSession] = useState(false);
  const [modalDetailSpeaker, setModalDetailSpeaker] = useState(false);
  const [speakerDetail, setSpeakerDetail] = useState({
    profilePicture: null,
    jobTitle: "",
    name: ""
  });
  const { query, isReady, push, pathname } = useRouter();

  const { id_user } = getAuth();
  const { id: id_ads } = query;

  useEffect(() => isReady && getAgendaDetail(), [isReady]);

  const getAgendaDetail = () => {
    if (!id_ads) return push("/home/agenda");
    else if (isEmpty(getAuth()) && pathname === "/home/agenda/detail") return push({
      pathname: "/agenda-register/session-detail",
      query: { id: id_ads },
    });
    else if(!isEmpty(getAuth()) && pathname === "/agenda-register/session-detail") return push({
      pathname: "/home/agenda/detail",
      query: { id: id_ads },
    });

     apiDispatch(getAdsDetail, { id_user, id_ads }, true).then((response) => {
      setIsLoadingData(false);
      if (response.code === 200 && !isEmpty(response.result)) {
        const { result } = response;
        const { additional_data_arr } = result;
        const additionalData = isString(additional_data_arr) ? JSON.parse(additional_data_arr) : additional_data_arr;

        setData((prev) => {
          return {
            ...prev,
            title: result.title,
            image: result.image,
            vendorName: result.vendor_name,
            dateSession: additionalData?.date?.start,
            ticket: result.available_seats,
            description: result.description,
            status: result.status,
            userJoined: result.user_joined,
            link: additionalData?.invitation_link,
            isSessionReq: result.is_session_request,
            normalPrice: additionalData?.price?.normal_price,
            discountPrice: additionalData?.price?.discount_price,
            discount: additionalData?.price?.discount,
            speaker: result.speaker,
          };
        });
      }
      else push(isEmpty(getAuth()) ? "/login" : "/home/agenda");
    });
  };

  const onSubmitRequest = () => {
    apiDispatch(postLogRequestSession, { id_user, id_ads }, true).then(
      (response) => {
        response.code === 200 && setData({ ...data, isSessionReq: true });
      }
    );
  };

  const renderRightBarSpeakers = (data) => {
    // let dynamicData = isLoadingData ? [1, 2, 3] : data;
    // if (isEmpty(data) && !isLoadingData) {
    //   return null;
    // }
    return (
      <Container>
        <RightBarBox title="Speaker">
          {(isLoadingData ? [1,2,3] : data)?.map((item, idx) => (
            <Fragment key={idx}>
              <IdentityCard
                onClick={() => {
                  console.log({item});
                  setSpeakerDetail({
                    ...speakerDetail,
                    profilePicture: item.image,
                    name: item.title,
                    jobTitle: item.job_title
                  })
                  setModalDetailSpeaker(true);
                }}
                image={item.image}
                title={item.title}
                subTitle={item.job_title}
                isLoading={isLoadingData}
              />
            </Fragment>
          ))}
        </RightBarBox>
      </Container>
    );
  };

  const {
    title,
    image,
    vendorName,
    dateSession,
    ticket,
    description,
    status,
    userJoined,
    link,
    isSessionReq,
    normalPrice,
    discountPrice,
    discount,
  } = data;

  const bannerLv3 = {
    banner: WebCover.src,
    image,
    title,
    customTitle: !isLoadingData ? (
      <p className="m-0 heading-sm-light text-center color-neutral-old-40 pt-1">
        Promote by <span className="heading-sm-bold">{vendorName}</span>
      </p>
    ) : (
      <SkeletonElement width={250} height={16} className="my-2" />
    ),
    customSubTitle: (
      <Fragment>
        <div className="d-flex align-items-center justify-content-center">
          <h3 className="heading-sm-light d-flex align-items-center color-neutral-old-40 m-0">
            <span className="material-icons color-secondary-main me-2">
              date_range
            </span>
            {!isLoadingData ? (
              !isEmpty(dateSession) && webinarFormatter(dateSession)
            ) : (
              <SkeletonElement width={200} height={24} />
            )}
          </h3>
        </div>

        {/* notif request session */}
        {status !== "active" && userJoined === false && (
          <div className="w-100 mt-3 align-items-center d-flex justify-content-center">
            <div
              className={classnames(
                isSessionReq ? "x-request-complete" : "x-request-status btn"
              )}
              onClick={() => {
                if (!isSessionReq) {
                  setModalRequestSession(true);
                  onSubmitRequest();
                }
              }}
            >
              <div className="align-items-center d-flex justify-content-center">
                <span className="material-icons color-neutral-old-80">
                  {isSessionReq ? "check" : "access_time_filled"}
                </span>
                <p className="m-0 ms-2 heading-sm-light color-neutral-old-80">
                  Request This Session
                </p>
              </div>
            </div>
          </div>
        )}
      </Fragment>
    ),
  };

  const renderModalRequestSession = () => {
    return (
      <ModalConfirmation
        show={modalRequestSession}
        onHide={() => setModalRequestSession(false)}
        customExitIcon={
          <span className="material-icons-round font-weight-bolder color-neutral-70">
            clear
          </span>
        }
        title={"Request Sent"}
        subTitle={
          "Thankyou for your enthusiasm for this session. You can check other session again."
        }
        customBody={
          <Button className="mt-4" onClick={() => push("/home/agenda")}>
            <p className="d-flex justify-content-center align-items-center m-0 p-2">
              Find More Sessions
            </p>
          </Button>
        }
      />
    );
  }

  const renderModalDetailSpeaker = () => {
    return (
      <Modal
        className="x-speakerdetail-modal"
        size="lg"
        show={modalDetailSpeaker}
        onHide={() => setModalDetailSpeaker(false)}
        centered
      >
        <Row className="p-3">
          <Col xs={1} />
          <Col
            xs={10}
            className="d-flex justify-content-center align-items-center"
          >
            <h3 className="heading-md-bold color-neutral-700 m-0">
              Speaker Profile
            </h3>
          </Col>
          <Col xs={1} className="d-flex align-items-center justify-content-end">
            <MaterialIcon
              action="clear"
              className="cursor-pointer"
              size={28}
              color="neutral-700"
              onClick={() => setModalDetailSpeaker(false)}
            />
          </Col>
        </Row>
        <div className="x-picture-wrapper">
          <img className="x-banner" src={bannerSpeaker.src} alt="" />
          <img
            className="x-profile-picture"
            src={speakerDetail.profilePicture}
            alt=""
          />
        </div>

        <div className="d-flex justify-content-center align-items-center flex-column mt-4">
          <h3 className="heading-md-bold color-neutral-700 m-0">
            {speakerDetail.name}
          </h3>
          <p className="text-lg-light color-neutral-500 m-0 mt-2">
            {speakerDetail.jobTitle}
          </p>
        </div>

        <hr className="my-3 mx-5" />
        <div className="d-flex flex-column mx-5 mb-4">
          <p className="text-lg-normal color-neutral-700 m-0">
            Sesi bersama {speakerDetail.name}:
          </p>
          <CardLv2
            className="x-card-speaker"
            isLoading={false}
            image={image}
            title={title}
            subTitle={
              <div className="d-flex">
                {data.speaker.map((item, idx) => (
                  <p
                    className="text-md-light color-neutral-700"
                    key={idx}
                  >
                    {item.title}
                    {idx !== data.speaker.length - 1 ? <>,&nbsp;</> : ""}
                  </p>
                ))}
              </div>
            }
            customSubTitle={
              <Fragment>
                {status === "active" &&
                  (isEmpty(getAuth()) || userJoined === false) &&
                  ticket > 0 && (
                    <Button
                      className="w-25 mt-3"
                      onClick={() => {
                        push({
                          pathname:
                            type === "direct-link"
                              ? "/agenda-register/session-detail/registration"
                              : "/home/agenda/detail/registration",
                          query: { id: id_ads },
                        });
                      }}
                    >
                      Join
                    </Button>
                  )}
              </Fragment>
            }
          />

          {/* <p className="text-lg-normal color-neutral-700 mb-2">
            Tentang {speakerDetail.name}:
          </p>

          <p className="text-md-light color-neutral-500 m-0">
            Ganjar Ariel Santosa adalah seorang Product Manager di salah satu
            Startup yang bergerak di bidang Financial Technology khususnya untuk
            pendanaan para petani untuk meningkatkan kapasitas produksi mereka
            melalui crowd-funding.
          </p> */}
        </div>
      </Modal>
    );
  }

  return (
    <Layout title="Webinar Detail - Indigo">
      <div className="x-agendadetail-page">
        <MainLayout
          size="sm"
          Breadcrumb
          rightBarContent={renderRightBarSpeakers(data.speaker)}
          rightBarClassName="x-rightbar-speakers"
          bannerLv3={bannerLv3}
          isLoading={isLoadingData}
          HideSideNavigationBar={(type === "direct-link" || isEmpty(getAuth())) ? true : false}
          HasMarginSideNavigationBar={(type === "direct-link" || isEmpty(getAuth())) ? true : false}
        >
          <div className="w-100 mb-3">
            {/* link agenda session */}
            {status === "active" && userJoined && (
              <div
                className={classnames(
                  !isEmpty(link) ? "x-tersedia" : "x-belumtersedia"
                )}
              >
                <div
                  className={classnames(
                    "x-emailcheck",
                    !isEmpty(link)
                      ? "x-emailcheck-success"
                      : "x-emailcheck-pending"
                  )}
                >
                  <div className="align-items-center d-flex justify-content-center">
                    <p
                      className={classnames(
                        "heading-sm-light m-0",
                        !isEmpty(link)
                          ? "color-semantic-success-pressed"
                          : "color-primary-pressed"
                      )}
                    >
                      Check your email for complete information about this
                      session
                    </p>
                    <span
                      className={classnames(
                        "material-icons ms-2",
                        !isEmpty(link)
                          ? "color-semantic-success-pressed"
                          : "color-primary-pressed"
                      )}
                    >
                      email
                    </span>
                  </div>
                </div>

                <div className="mx-3">
                  <p className="m-0 mt-2 heading-sm-light color-neutral-70">
                    Link Webinar Session
                  </p>
                  <div className="align-items-center d-flex justify-content-center">
                    <div className="comment-input me-3 color-neutral-30">
                      <InputText
                        onChangeText={() => {}}
                        disabled={isEmpty(link)}
                        placeholder={"Belum tersedia"}
                        disableError={true}
                        value={link}
                      />
                    </div>
                    <div className="x-btn-copy">
                      <Button
                        disabled={isEmpty(link)}
                        onClick={() => navigator.clipboard.writeText(link)}
                      >
                        Copy
                      </Button>
                    </div>
                  </div>
                  <p className="heading-sm-light color-grey-4 m-0">
                    Available 1 hour before the session start
                  </p>
                </div>
              </div>
            )}

            <div className="mx-3">
              {/* Ticket availability */}
              {(isLoadingData || status === "active") && (
                <Fragment>
                  <p className="heading-sm-light color-neutral-70 m-0">
                    Ticket :
                  </p>
                  {!isLoadingData ? (
                    <div
                      className={classnames(
                        "x-ticket",
                        ticket > 0
                          ? "x-ticket-available"
                          : "x-ticket-unavailable"
                      )}
                    >
                      <span>
                        {ticket > 0
                          ? `${ticket} Ticket Available`
                          : "Ticket Unavailable"}
                      </span>
                    </div>
                  ) : (
                    <SkeletonElement width="25%" height={24} className="my-2" />
                  )}
                </Fragment>
              )}

              {/* Description */}
              {(isLoadingData || !isEmpty(description)) && (
                <Fragment>
                  <p className="heading-sm-light color-neutral-70 m-0 mt-4 mb-2">
                    Materi yang dipelajari :
                  </p>
                  {!isLoadingData ? (
                    <div
                      className="heading-sm-light color-neutral-old-40 m-0"
                      dangerouslySetInnerHTML={{ __html: description }}
                    />
                  ) : (
                    <SkeletonElement
                      width="100%"
                      height={16}
                      className="my-2"
                      count={3}
                    />
                  )}
                </Fragment>
              )}
            </div>

            {status === "active" && (isEmpty(getAuth()) || userJoined === false) && ticket > 0 && (
              <div className="x-card-ticket">
                <Row className="d-flex justify-content-center align-items-center w-100 ms-1 me-1">
                  <div className="col-xl-4 col-12 p-0">
                    <h4 className="heading-sm-normal color-neutral-old-80 m-0">
                      1 ticket (discount {discount})
                    </h4>
                  </div>
                  <div className="ms-auto col-xl-7 col-12 p-0">
                    <div className="ms-auto align-items-center d-flex justify-content-center">
                      <div className="x-price ms-auto">
                        <p className="x-normal-price m-0 heading-sm-light color-neutral-old-40">
                          Rp{normalPrice}
                        </p>
                        <p className="x-discount-price m-0 heading-sm-bold color-neutral-70">
                          Rp{discountPrice}
                        </p>
                      </div>
                      <div className="x-button me-2">
                        <Button
                          onClick={() => {
                            push({
                              pathname: type === "direct-link" ? "/agenda-register/session-detail/registration" : "/home/agenda/detail/registration",
                              query: { id: id_ads },
                            })
                          }}
                        >
                          Register
                        </Button>
                      </div>
                    </div>
                  </div>
                </Row>
              </div>
            )}
          </div>
          {renderModalRequestSession()}
          {renderModalDetailSpeaker()}
        </MainLayout>
      </div>
    </Layout>
  );
};

// export default appRoute(AgendaDetail);
export default AgendaDetail;
