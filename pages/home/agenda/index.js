import React, { Fragment, useEffect, useState } from "react";
import { Button, Container } from "react-bootstrap";
import ImageWebinarHeader from "/public/assets/images/img-webinar-header.png";
import { getAds } from "/services/actions/api";
import classnames from "classnames";
import { getAuth, dayFormatter } from "/services/utils/helper";
import MainLayout from "/components/layout/MainLayout";
import { isEmpty } from "lodash-es";
import { useRouter } from "next/router";
import { apiDispatch } from "/services/utils/helper";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";
import TabBar from "/components/home/TabBar";
import RightBarBox from "/components/home/RightBarBox";
import IdentityCard from "/components/home/IdentityCard";
import CardLv2 from "/components/home/CardLv2";
import SkeletonElement from "/components/skeleton/SkeletonElement";

const Agenda = () => {
  const [loading, setLoading] = useState(true);
  const [sessions, setSessions] = useState([]);
  const [speakers, setSpeakers] = useState([]);
  const [isActiveTab, setIsActiveTab] = useState("active");
  const router = useRouter();
  const { id_user } = getAuth();
  const [isLoadingSpeakers, setIsLoadingSpeakers] = useState(true);
  const [isLoadingSessions, setIsLoadingSession] = useState(true);
  let dynamicSession = isLoadingSessions ? [1, 2, 3] : sessions;

  const tabItems = [
    {
      name: "SESSION AVAILABLE",
      type: "active",
    },
    {
      name: "PREVIOUS SESSION",
      type: "expired",
    },
  ];

  useEffect(() => {
    const { id_user } = getAuth();
    getWebinar("sc_session", "active", id_user, 0, 10);
    getSpeaker("active", "sc_speaker", id_user, 0, 10);
  }, []);

  const handleScroll = (e) => {
    const { id_user } = getAuth();
    const status = isActiveTab;
    let size = sessions.length;
    // const bottom = e.target.scrollHeight - Math.floor(e.target.scrollTop) === e.target.clientHeight;
    const bottom =
      Math.abs(
        e.target.scrollHeight - (e.target.scrollTop + e.target.clientHeight)
      ) <= 1;
    if (bottom) {
      loadMore("sc_session", status, id_user, size, 5);
    }
  };

  const loadMore = async (category, status, id_viewer, start, limit) => {
    let size = sessions.length;

    let newData;
    await apiDispatch(
      getAds,
      {
        category: "sc_session",
        status: status,
        id_viewer: id_viewer,
        start: start,
        limit: limit,
      },
      true
    ).then((response) => {
      if (response.code === 200) {
        const data = response.result;
        newData = data;
      }
    });

    let newArray = sessions;
    let newDatas = newArray.concat(newData);
    setSessions(newDatas);

  };

  const getWebinar = async (category, status, id_viewer, start, limit) => {
    apiDispatch(
      getAds,
      {
        category: "sc_session",
        status: status,
        id_viewer: id_viewer,
        start: start,
        limit: limit,
      },
      true
    ).then((response) => {
      setIsLoadingSession(false);
      if (response.code === 200) {
        const data = response.result;
        setSessions(data);
      }
    });
  };

  const getSpeaker = async (status, category) => {
    apiDispatch(
      getAds,
      { status: "active", category: "sc_speaker" },
      true
    ).then((response) => {
      setIsLoadingSpeakers(false);
      if (response.code === 200) {
        const datas = response.result;
        setSpeakers(datas);
      }
    });
  };
  const renderRightBarSpeakers = (data) => {
    let dynamicData = isLoadingSpeakers ? [1, 2, 3] : data;
    if (isEmpty(data) && !isLoadingSpeakers){
      return null;
    }
    return (
      <Container>
          <RightBarBox title="Speaker">
            {dynamicData?.map((item, idx) => (
              <Fragment key={idx}>
                <IdentityCard
                  image={item.image}
                  title={item.title}
                  subTitle={item.profession}
                  isLoading={isLoadingSpeakers}
                />
              </Fragment>
            ))}
          </RightBarBox>
      </Container>
    );
  };

  return (
    <Layout title="Agenda - Indigo">
      <div
        style={{ width: "100%", height: "100vh", overflowY: "scroll" }}
        onScroll={handleScroll}
      >
        <div className="x-agenda-page">
          <MainLayout
            size="sm"
            Breadcrumb
            banner={ImageWebinarHeader.src}
            rightBarContent={renderRightBarSpeakers(speakers)}
            rightBarClassName="x-rightbar-speakers"
            StickyRightBar
          >
            <div className="mb-3">
              <TabBar
                tabItems={tabItems}
                isActiveTab={isActiveTab}
                onClick={(e) => {
                  const type = e.target.getAttribute("type");
                  setIsActiveTab(type);
                  getWebinar("sc_session", type, id_user, 0, 10);
                }}
              />

              {dynamicSession.map((item, idx) => (
                <div className="py-2" key={idx}>
                  <CardLv2
                    isLoading={isLoadingSessions}
                    image={item.image}
                    title={item.title}
                    customSubTitle={
                      <Fragment>
                        {!isLoadingSessions ? isActiveTab === "active" ? (
                          <Fragment>
                            {item.available_seats > 5 ? (
                              <p className="x-ticket x-ticket-available">
                                {item.available_seats} ticket available
                              </p>
                            ) : item.available_seats > 0 &&
                              item.available_seats < 6 ? (
                              <p className="x-ticket x-ticket-sold">
                                {item.available_seats} ticket available
                              </p>
                            ) : (
                              <p className="x-ticket x-ticket-unavailable">
                                Ticket Unavailable
                              </p>
                            )}
                          </Fragment>
                        ) : (
                          <p className="x-ticket x-ticket-unavailable">
                            Ticket Unavailable
                          </p>
                        ) : <SkeletonElement width="35%" height={24} className="my-2" />}
                        <div
                          className={classnames(
                            "d-flex align-items-center cursor-pointer mt-1"
                          )}
                        >
                          <span className="material-icons -ms-4 color-secondary-main">
                            date_range
                          </span>
                          {!isLoadingSessions ? <p className="m-0 ms-2 heading-sm-normal color-neutral-old-40">
                            {dayFormatter(item.date_session)}
                          </p> : <SkeletonElement width={65} height={16} className="ms-2" />}
                        </div>
                        <div
                          className={classnames(
                            "d-flex align-items-center cursor-pointer mt-1"
                          )}
                        >
                          <span className="material-icons -ms-4 color-neutral-old-40">
                            account_circle
                          </span>
                          {!isLoadingSessions ? item.speaker.map((items, i) => (
                            <p
                              className="m-0 ms-2 heading-sm-normal color-neutral-old-40"
                              key={i}
                            >
                              {items.title}
                              {i !== item.speaker.length - 1 ? "," : ""}
                            </p>
                          )) : <SkeletonElement width={120} height={16} count={2} className="mx-2" />}
                        </div>
                      </Fragment>
                    }
                    customFooter={
                      <div className="d-flex justify-content-between align-items-center">
                        {!isLoadingSessions ? <p className="m-0 heading-sm-normal color-neutral-old-40">
                          {`Promote by ${item.vendor_name}`}
                        </p> : <SkeletonElement width={150} height={16} />}
                        {isActiveTab === "active" &&
                          !item.user_joined &&
                          item.available_seats > 0 && (
                            <div className="ms-auto">
                              <Button
                                className="py-2 px-4"
                                onClick={() =>
                                  router.push({
                                    pathname:
                                      "/home/agenda/detail/registration",
                                    query: { id: item.id_ads },
                                  })
                                }
                              >
                                Join
                              </Button>
                            </div>
                          )}
                      </div>
                    }
                    onClick={() =>
                      router.push({
                        pathname: "/home/agenda/detail",
                        query: { id: item.id_ads },
                      })
                    }
                  />
                </div>
              ))}
            </div>
          </MainLayout>
        </div>
      </div>
    </Layout>
  );
};

export default appRoute(Agenda);
