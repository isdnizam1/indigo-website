import React from 'react';
import DetailCollaborationProject from "/pages/home/collaboration/detail";

const CollaborationDetailProject = () => <DetailCollaborationProject />;

export default CollaborationDetailProject;
