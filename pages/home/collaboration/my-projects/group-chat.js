import { useState, useEffect, Fragment } from "react";
import MainLayout from "/components/layout/MainLayout";
import { Button, Col } from "react-bootstrap";
import {
  getDetailProject,
  postProfileFollow,
  postProfileUnfollow,
} from "/services/actions/api";
import classnames from "classnames";
import { apiDispatch, getAuth } from "/services/utils/helper";
import { useDispatch } from "react-redux";
import { setChatData } from "/redux/slices/chatSlice";
import { useRouter } from "next/router";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";
import CardUser from "/components/home/CardUser";
import ContentBox from "/components/home/ContentBox";

const CollaborationGroupChat = () => {
  const [listMember, setListMember] = useState([]);
  const { id_user } = getAuth();
  const [isFollowed, setIsFollowed] = useState(false);
  const [isDisableButton, setIsDisableButton] = useState(true);
  const dispatch = useDispatch();
  const { push, query, isReady } = useRouter();
  const { id: id_ads } = query;
  // const chatData = useSelector((state) => state.chatReducer.data);
  // const { selectedMembers, id_ads } = chatData;

  useEffect(() => isReady && getData(), [isFollowed, isReady]);

  const getData = () => {
    apiDispatch(
      getDetailProject,
      { id_ads, limit: 10, id_user, filter: "joined" },
      true
    ).then((response) => {
      if (response.code === 200) {
        const data = response.result;
        const members = response.result?.data;

        setListMember(data);
        setIsFollowed(response.result.followed_by);

        setIsDisableButton(false); // define default value

        for (let i = 0; i < members.length; i++) {
          if (!members[i].followed_by && members[i].id_user !== id_user)
            return setIsDisableButton(true);
        }
      }
    });
  };
  const handleAction = (item, type) => {
    const action = {
      follow: {
        api: postProfileFollow,
        params: {
          id_user: item.id_user,
          followed_by: id_user,
        },
      },
      unfollow: {
        api: postProfileUnfollow,
        params: {
          id_user: id_user,
          id_user_following: item.id_user,
        },
      },
    };

    apiDispatch(action[type].api, action[type].params, true).then(
      (response) => {
        response.code === 200 && setIsFollowed(true);
      }
    );
  };

  const createGroup = () => {
    dispatch(
      setChatData({
        selectedMembers: listMember.data,
        id_ads: listMember?.detail?.id_ads,
      })
    );
    push({
      pathname: "/chat",
      query: query?.id_message
        ? {
            id_message: query.id_message,
            message_type: "group_chat",
          }
        : { message_type: "group_chat" },
    });
  };

  return (
    <Layout title="Collaboration Group - Indigo">
      <div className="x-collaborationgroupchat-page">
        <MainLayout size="md" Breadcrumb>
          <div className="d-flex align-items-center mt-3">
            <p className="heading-md-bold color-primary-pressed m-0">
              Follow All Member
            </p>
          </div>
          <p className="m-0 heading-sm-normal color-neutral-60">
            You must follow all members of your collaboration project before
            create group chat
          </p>
          <ContentBox NoDivider className="d-flex flex-column justify-content-center gap-3 p-3 mt-4">
            {listMember?.data?.map(
              (item, idx) =>
                item.id_user !== id_user && (
                  <div className="x-member-card" key={idx}>
                    <Col xs={7}>
                      <CardUser
                        image={item.profile_picture}
                        jobTitle={item.job_title}
                        name={item.full_name}
                        city={item.city_name}
                        customInformation={
                          <p
                            className={classnames(
                              "x-memberstatus",
                              item.shortlist === "3"
                                ? "x-memberstatus-taken"
                                : item.shortlist === "1" &&
                                    "x-memberstatus-untaken"
                            )}
                          >
                            {item.shortlist === "3"
                              ? "Taken and Joined Project"
                              : item.shortlist === "1" &&
                                "Not Taken and Haven’t Joined Project"}
                          </p>
                        }
                      />
                    </Col>

                    <Col
                      xs={5}
                      className="p-0 d-flex justify-content-end align-items-center gap-3"
                    >
                      {item.id_user !== id_user && (
                        <>
                          <div className="x-btn-wrapper">
                            <Button
                              className={classnames(
                                item.followed_by === false
                                  ? "btn-primary"
                                  : "btn-delete"
                              )}
                              onClick={() =>
                                handleAction(
                                  item,
                                  item.followed_by === false
                                    ? "follow"
                                    : "unfollow"
                                )
                              }
                            >
                              {item.followed_by === false
                                ? "Follow"
                                : "Following"}
                            </Button>
                          </div>
                          <div className="x-btn-wrapper">
                            <Button
                              className="btn-delete"
                              onClick={() => {
                                dispatch(
                                  setChatData({
                                    selectedMessage: {
                                      id_user: item.id_user,
                                      full_name: item.full_name,
                                      image: item.profile_picture,
                                      id_groupmessage: item.id_groupmessage,
                                      type: "personal",
                                    },
                                  })
                                );
                                push({
                                  pathname: "/chat",
                                  query: {
                                    id_message: !item.id_groupmessage
                                      ? false
                                      : item.id_groupmessage,
                                    message_type: "message",
                                    type: "personal",
                                  },
                                });
                              }}
                            >
                              Message
                            </Button>
                          </div>
                        </>
                      )}
                    </Col>
                  </div>
                )
            )}
          </ContentBox>
          <div className="d-flex justify-content-center">
            <Button
              className="w-50 mt-5 mb-5"
              disabled={isDisableButton}
              onClick={createGroup}
            >
              Create Group Chat
            </Button>
          </div>
        </MainLayout>
      </div>
    </Layout>
  );
};

export default appRoute(CollaborationGroupChat);
