import React from "react";
import {
  getProfileDetailV2,
  getProfileFollowStatus,
  postProfileFollow,
  postProfileUnfollow,
} from "/services/actions/api";
import { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import { getAuth } from "/services/utils/helper";
import BreadcrumbLv1 from "/components/header/BreadcrumbLv1";
import MainLayout from "/components/layout/MainLayout";
import defaultCover from "/public/assets/images/img-default-cover-profile.png";
import { Button } from "react-bootstrap";
import { useRouter } from "next/router";
import { isEmpty } from "lodash-es";
import EmptyOverview from "/components/empty_state/EmptyOverview";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";
import Profile from "/pages/profile";

const ProfileUser = () => {
  const { id_user } = getAuth();
  const router = useRouter();
  const { id: id_ads_user } = router.query;
  const [profileDetail, setProfileDetail] = useState({
    about_me: "",
    cover_image: "",
    full_name: "",
    id_city: null,
    interest: [],
    job_title: "",
    location: "",
    profile_picture: "",
    total_followers: 0,
    total_following: 0,
    isMyProfile: true,
  });
  const [isOverviewTab, setIsOverviewTab] = useState(true);

  useEffect(() => {
    if (router.isReady) {
      console.log(id_ads_user);
      getProfileDetailV2({ id_user: id_ads_user }).then((res) => {
        if (isEmpty(res.data.result))
          return router.push("/home/collaboration/my-projects");
        setProfileDetail(res.data.result);
      });
    }
  }, [router.query]);

  const {
    about_me,
    cover_image,
    full_name,
    id_city,
    interest,
    job_title,
    location,
    profile_picture,
    total_followers,
    total_following,
  } = profileDetail;

  return (
    <Layout title="Profile User - Indigo">
      <MainLayout>
        <div className="x-profileuser">
          <Container>
            <div className="x-profileuser-page">
              <div className="x-breadscrumb-wrapper">
                <BreadcrumbLv1 />
              </div>
              <div className="x-cover-img">
                <img src={defaultCover.src} alt="" />
              </div>
              <div className="x-profile-top">
                <div className="x-profile-photo">
                  {profile_picture ? (
                    <img src={profile_picture} alt="profile" />
                  ) : (
                    <div className="empty-image">
                      <p style={{ color: "var(--base-10)" }}>
                        {full_name.slice(0, 1).toUpperCase()}
                      </p>
                    </div>
                  )}
                </div>
                <div className="x-profile-top-detail">
                  <div className="x-detail-name">
                    <h3>{full_name}</h3>
                    <p className="m-0">{job_title}</p>
                  </div>
                  <div className="x-detail-follow">
                    <span>
                      <strong>{total_followers}</strong> Followers
                    </span>
                    <span className="ms-2">
                      <strong>{total_following}</strong> Following
                    </span>
                  </div>
                  <div className="d-flex x-detail-acc">
                    <div className="d-flex x-accept justify-content-center align-items-center cursor-pointer" >
                      <span
                        className="p-0 material-icons"
                        style={{ fontSize: 20, color: "var(--semantic-success-main)" }}
                      >
                        check
                      </span>
                      <p className="m-0" style={{ color: "var(--semantic-success-main)" }}>Accept</p>
                    </div>
                    <div className="d-flex x-decline ms-3 justify-content-center align-items-center cursor-pointer">
                      <span
                        className="p-0 material-icons"
                        style={{ fontSize: 20, color: "var(--semantic-danger-main)" }}
                      >
                        close
                      </span>
                      <p className="m-0" style={{ color: "var(--semantic-danger-main)" }}>Decline</p>
                    </div>
                  </div>
                </div>
              </div>
              <hr />
              <div className="x-profile-bottom">
                <div className="x-badges">
                  {interest.map(({ interest_name }, i) => (
                    <span key={i}>{interest_name}</span>
                  ))}
                </div>
                <p>{about_me}</p>
                <div className="x-loc-warp">
                  <div className="x-icon-loc" />
                  <span>
                    {location.city_name}, {location.country_name}
                  </span>
                </div>
              </div>
              <div className="x-profile-overview">
                <div className="x-tags">
                  <span
                    className={isOverviewTab && `active`}
                    onClick={() => setIsOverviewTab(true)}
                  >
                    overview
                  </span>
                  <span
                    className={!isOverviewTab && `active`}
                    onClick={() => setIsOverviewTab(false)}
                  >
                    activities
                  </span>
                </div>
                <hr />
                <div className="x-details-container">
                  {isOverviewTab ? (
                    <EmptyOverview
                      head="Portfolio"
                      title="Upload Your Portfolio"
                      subtitle="Share your portfolio and connect with new person!"
                      icon={<div className="x-icon-portfolio" />}
                      buttonText="Add Portfolio"
                    />
                  ) : (
                    <EmptyOverview
                      head="Activities"
                      title="There are no posts yet"
                      subtitle="All posts will appear on this page"
                      icon={<div className="x-icon-activity" />}
                      buttonText="Make Posts"
                    />
                  )}
                </div>
              </div>
            </div>
          </Container>
        </div>
      </MainLayout>
    </Layout>
  );
};

const DetailProfileUser = () => <Profile type="my-collaboration-projects" />;

// export default appRoute(ProfileUser);
export default appRoute(DetailProfileUser);
