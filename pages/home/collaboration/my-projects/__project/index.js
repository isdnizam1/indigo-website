/*eslint-disable*/
import React, { useState, useEffect } from "react";
import { Button } from "react-bootstrap";
import classnames from "classnames";
import { getAuth, apiDispatch } from "/services/utils/helper";
import MainLayout from "/components/layout/MainLayout";
import { isEmpty } from "lodash-es";
import defaultThumbnail from "/public/assets/images/img-myproject-thumbnail.png";
import { useRouter } from "next/router";
import {
  getDetailProject,
  postEditMemberProject,
  postProfileFollow,
  postProfileUnfollow,
  postAddMember,
} from "/services/actions/api";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";

const ProjectDetail = (props) => {
  const [isActiveTab, setIsActiveTab] = useState("My Project");
  const [detailProject, setDetailProject] = useState([]);
  const [updateMem, setUpdateMem] = useState(0);
  const [isFollowed, setIsFollowed] = useState(false);
  const [isAccepted, setIsAccepted] = useState(false);
  const { isReady, query, push } = useRouter();
  const { id_user } = getAuth();
  const { detail, data } = detailProject;
  const { id: id_ads } = query;

  useEffect(() => isReady && getProject(), [isFollowed, isReady]);

  const getProject = () => {
    apiDispatch(
      getDetailProject,
      {
        id_ads,
        limit: 10,
        id_user,
      },
      true
    ).then((response) => {
      console.log(response);
      if (response.code === 200) {
        const data = response.result;
        setDetailProject(data);
        setIsFollowed(response.result.followed_by);
        if (data.shortlist === "1") {
          setIsAccepted(true);
        }
      }
    });
  };
  console.log(data);

  const handleAction = (items, type) => {
    const action = {
      follow: {
        api: postProfileFollow,
        params: {
          id_user: items.id_user,
          followed_by: id_user,
        },
      },
      unfollow: {
        api: postProfileUnfollow,
        params: {
          id_user: id_user,
          id_user_following: items.id_user,
        },
      },
    };

    apiDispatch(action[type].api, action[type].params, true).then(
      (response) => {
        response.code === 200 && setIsFollowed(true);
      }
    );
  };

  const handleTakeGroup = (items) => {
    apiDispatch(
      postAddMember,
      {
        id_user: detail.id_user,
        id_groupmessage: detail.id_groupmessage?.id_groupmessage,
        id_user_insert: [items.id_user],
      },
      true
    ).then((response) => {
      // if (response.code === 200) {
      push({
        pathname: "/chat",
        query: {
          id_message: !detail.id_groupmessage?.id_groupmessage
            ? false
            : detail.id_groupmessage?.id_groupmessage,
          message_type: "message",
          type: "group",
        },
      });
    });
  };

  const postEditMember = (id_user, id_user_target, shortlist) => {
    apiDispatch(
      postEditMemberProject,
      {
        id_ads,
        id_user: id_user,
        id_user_target: id_user_target,
        shortlist: shortlist,
      },
      true
    ).then((res) => {
      const status = res.code;
      if (status === "200") {
        console.log("sukses", status);
        updateMem > 0 ? setUpdateMem(0) : setUpdateMem(1);
        console.log("updatemem", updateMem);
      }
    });
  };

  return (
    <Layout title="Detail Project - Indigo">
      <div className="x-mycollaborationproject-page">
        <MainLayout size="md" Breadcrumb>
          <div className="x-mycollabproject">
            <div className="x-border">
              <div className="x-project-navbar">
                <nav className="navbar navbar-expand-lg navbar-light">
                  <ul className="navbar-nav me-auto">
                    <li className="nav-item">
                      <p
                        className={classnames(
                          "nav-link m-0 cursor-pointer heading-sm-bold",
                          isActiveTab === "My Project" ? "active" : "inactive"
                        )}
                      >
                        My Project
                      </p>
                    </li>
                  </ul>
                </nav>
              </div>
              <div className="x-separator" />

              <div className="x-myproject">
                <div className="x-border-in">
                  <div className="x-thumbnail">
                    {!isEmpty(detail?.image) ? (
                      <>
                        <img src={detail?.image} alt="" />
                        <div className="x-icon-eventeer" />
                      </>
                    ) : (
                      <>
                        <img src={defaultThumbnail.src} alt="" />
                        <div className="x-icon-eventeer" />
                      </>
                    )}
                  </div>
                  <p
                    className="m-0 mt-3 heading-md-bolder"
                    style={{ color: "var(--neutral-80)" }}
                  >
                    {detail?.description}
                  </p>
                  <div className="d-flex x-btn-row mt-3 mb-3">
                    <Button
                      className="btn-cancel ms-1 me-1 d-flex justify-content-center align-items-center x-show-detail"
                      onClick={() => {
                        push({
                          pathname:
                            "/home/collaboration/my-projects/project/detail-project",
                          query: {
                            id: detail?.id_ads,
                          },
                        });
                      }}
                    >
                      <span
                        className="p-0 me-2 material-icons"
                        style={{
                          fontSize: 18,
                        }}
                      >
                        text_snippet
                      </span>
                      <p className="m-0">View Collaboration Details</p>
                    </Button>
                    <Button
                      className="btn-primary ms-1 me-1 d-flex justify-content-center align-items-center x-show-detail"
                      onClick={() => {
                        detail.id_groupmessage === null
                          ? push(
                              "/home/collaboration/my-projects/project/group"
                            )
                          : push({
                              pathname: "/chat",
                              query: {
                                id_message: !detail.id_groupmessage
                                  .id_groupmessage
                                  ? false
                                  : detail.id_groupmessage.id_groupmessage,
                                message_type: "message",
                                type: "group",
                              },
                            });
                      }}
                    >
                      <span
                        className="p-0 me-2 material-icons"
                        style={{
                          fontSize: 18,
                        }}
                      >
                        forum
                      </span>
                      <p className="m-0">Group Chat</p>
                    </Button>
                  </div>
                  <div className="x-separator-2" />
                  {isActiveTab === "My Project" && (
                    <div className="x-myproject">
                      <p
                        className="m-0 text-lg-normal mt-3 mb-3"
                        style={{ color: "var(--grey-3)" }}
                      >
                        People who wants to join your collaborative project
                      </p>
                      <div className="x-people-border">
                        {data?.map((items, i) => (
                          <div className="x-data-member" key={i}>
                            <div className="x-people d-flex align-items-center mt-2 mb-2">
                              <div
                                className="x-bb d-flex align-items-center cursor-pointer"
                                onClick={() =>
                                  push({
                                    pathname: `/home/collaboration/my-projects/profile`,
                                    query: { id: items.id_user },
                                  })
                                }
                              >
                                {!isEmpty(items.profile_picture) ? (
                                  <img
                                    src={items.profile_picture}
                                    alt=""
                                    className="x-image-account"
                                  />
                                ) : (
                                  <div className="x-empty-image">
                                    <p
                                      className="text-xl-normal m-0"
                                      style={{
                                        color: "var(--base-10)",
                                      }}
                                    >
                                      {items.full_name
                                        .slice(0, 1)
                                        .toUpperCase()}
                                    </p>
                                  </div>
                                )}
                                <div className="x-desc ms-2">
                                  <p
                                    className="m-0 text-lg-normal"
                                    style={{ color: "var(--body-60)" }}
                                  >
                                    {items.full_name}
                                  </p>
                                  <p
                                    className="m-0 text-sm-light line-height-sm"
                                    style={{ color: "var(--grey-4)" }}
                                  >
                                    {items.job_title}
                                  </p>
                                  <p
                                    className="m-0 text-sm-light line-height-sm"
                                    style={{ color: "var(--grey-4)" }}
                                  >
                                    {items.city_name}
                                  </p>
                                </div>
                              </div>
                              {items.shortlist === "3" && (
                                <div className="x-respon ms-auto d-flex flex-column gap-2">
                                  <div className="x-respon ms-auto d-flex flex-column gap-2">
                                    <Button
                                      className={classnames(
                                        !items.followed_by === false
                                          ? "btn-delete"
                                          : "btn-primary"
                                      )}
                                      onClick={() => {
                                        handleAction(
                                          items,
                                          items.followed_by === false
                                            ? "follow"
                                            : "unfollow"
                                        );
                                        items.followed_by === false &&
                                          handleTakeGroup(items);
                                      }}
                                    >
                                      {items.followed_by === false
                                        ? "Follow"
                                        : "Following"}
                                    </Button>
                                    <Button className="btn-cancel" disabled>
                                      cancel
                                    </Button>
                                  </div>
                                </div>
                              )}
                              {items.shortlist === "1" && (
                                <div className="x-respon ms-auto d-flex flex-column gap-2">
                                  <Button
                                    className={classnames(
                                      !items.followed_by === false
                                        ? "btn-delete"
                                        : "btn-primary"
                                    )}
                                    onClick={() => {
                                      handleAction(
                                        items,
                                        items.followed_by === false
                                          ? "follow"
                                          : "unfollow"
                                      );
                                      items.followed_by === false &&
                                        handleTakeGroup(items);
                                    }}
                                  >
                                    {items.followed_by === false
                                      ? "Follow"
                                      : "Following"}
                                  </Button>
                                  <Button
                                    className="btn-cancel"
                                    onClick={() => {
                                      postEditMember(
                                        items.id_ads,
                                        id_user,
                                        items.id_user,
                                        "0"
                                      );
                                    }}
                                  >
                                    cancel
                                  </Button>
                                </div>
                              )}
                              {items.shortlist === "0" && (
                                <div className="x-respon ms-auto d-flex">
                                  <span
                                    className="p-0 material-icons x-decline cursor-pointer"
                                    style={{
                                      fontSize: 20,
                                      color: "var(--semantic-danger-main)",
                                    }}
                                    onClick={() => {
                                      postEditMember(
                                        items.id_ads,
                                        id_user,
                                        items.id_user,
                                        "2"
                                      );
                                      getProject(id_user, "request", 10);
                                      setIsAccepted(true);
                                    }}
                                  >
                                    close
                                  </span>
                                  <span
                                    className="p-0 material-icons x-accept align-items-center cursor-pointer"
                                    style={{
                                      fontSize: 20,
                                      color: "var(--semantic-success-main)",
                                    }}
                                    onClick={() => {
                                      postEditMember(
                                        items.id_ads,
                                        id_user,
                                        items.id_user,
                                        "1"
                                      );
                                      getProject(id_user, "request", 10);
                                    }}
                                  >
                                    check
                                  </span>
                                </div>
                              )}
                            </div>
                          </div>
                        ))}
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </MainLayout>
      </div>
    </Layout>
  );
};

export default appRoute(ProjectDetail);
