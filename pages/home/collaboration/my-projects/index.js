import React, { Fragment, useState, useEffect } from "react";
import { Row, Button } from "react-bootstrap";
import classnames from "classnames";
import { getAuth, apiDispatch, formatDate } from "/services/utils/helper";
import MainLayout from "/components/layout/MainLayout";
import { isEmpty } from "lodash-es";
import { useRouter } from "next/router";
import {
  getMyProject,
  postAddMember,
  getDetailProject,
  postEditMemberProject,
  postProfileFollow,
  postProfileUnfollow,
} from "/services/actions/api";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";
import ContentBox from "/components/home/ContentBox";
import PostCard from "/components/cards/PostCard";
import EmptyStateLv1 from "/components/empty_state/EmptyStateLv1";
import EmptyIcon from "/public/assets/icons/icon-empty-collaborationproject.svg";
import DefaultPicture from "/public/assets/images/img-default-image.jpg";
import CardUser from "/components/home/CardUser";
import MaterialIcon from "/components/utils/MaterialIcon";
import SkeletonElement from "/components/skeleton/SkeletonElement";

const MyCollaborationProjects = () => {
  const { push, query, isReady } = useRouter();
  const { id_user } = getAuth();
  const { id: id_ads_collaboration } = query;

  const [toggleActive, setToggleActive] = useState("My Project");
  const [projectRequests, setProjectRequests] = useState([{}]);
  const [projectJoined, setProjectJoined] = useState([{}]);
  const [projectDetails, setProjectDetails] = useState({
    users: [],
    usersJoined: [],
    detail: {},
  });
  const [isLoadingData, setIsLoadingData] = useState(true);
  const [isLoadingDetails, setIsLoadingDetails] = useState(true);

  const { detail, users } = projectDetails;

  useEffect(() => {
    getData();
    isReady && query && getProject(id_ads_collaboration);
  }, [isReady]);

  const getData = () => {
    apiDispatch(getMyProject, { id_user, shortlist: "request" }, true).then(
      (response) => {
        setIsLoadingData(false);
        if (response.code === 200) {
          !isEmpty(response.result) && setProjectRequests(response.result);

          apiDispatch(
            getMyProject,
            { id_user, shortlist: "joined" },
            true
          ).then((response) => {
            response.code === 200 &&
              !isEmpty(response.result) &&
              setProjectJoined(response.result);
          });
        }
      }
    );
  };

  const getProject = (id_ads) => {
    apiDispatch(
      getDetailProject,
      {
        id_ads,
        limit: "10",
        id_user,
      },
      true
    ).then((response) => {
      setIsLoadingDetails(false);
      if (response.code === 200) {
        const { data, data_joined, detail } = response.result;

        setProjectDetails({
          users: data,
          usersJoined: data_joined,
          detail,
        });
        // setIsFollowed(response.result.followed_by);
        // if (data.shortlist === "1") {
        //   setIsAccepted(true);
        // }
      }
    });
  };

  const postFollowUnfollowAction = (item) => {
    const type = !item.followed_by ? "follow" : "unfollow";

    const action = {
      follow: {
        api: postProfileFollow,
        params: {
          id_user: item.id_user,
          followed_by: id_user,
        },
      },
      unfollow: {
        api: postProfileUnfollow,
        params: {
          id_user: id_user,
          id_user_following: item.id_user,
        },
      },
    };

    apiDispatch(action[type].api, action[type].params, true).then(
      (response) => {
        response.code === 200 && getProject(detail.id_ads);
      }
    );
  };

  const handleTakeGroup = (items) => {
    apiDispatch(
      postAddMember,
      {
        id_user: detail.id_user,
        id_groupmessage: detail.id_groupmessage?.id_groupmessage,
        id_user_insert: [items.id_user],
      },
      true
    ).then((response) => {
      // if (response.code === 200) {
      push({
        pathname: "/chat",
        query: {
          id_message: !detail.id_groupmessage?.id_groupmessage
            ? false
            : detail.id_groupmessage?.id_groupmessage,
          message_type: "message",
          type: "group",
        },
      });
    });
  };

  const postEditMemberByShortlist = (id_user_target, shortlist) => {
    const { id_ads } = projectDetails.detail;

    apiDispatch(
      postEditMemberProject,
      {
        id_ads,
        id_user,
        id_user_target,
        shortlist,
      },
      true
    ).then((response) => {
      response.code === 200 && getProject(id_ads);
    });
  };

  const renderEmptyState = () => {
    return (
      <div className="d-flex justify-content-center pt-5 pb-5">
        <div className="w-50 pt-5 pb-5">
          <EmptyStateLv1
            customIcon={EmptyIcon}
            title={`No collaborations you ${toggleActive === "My Project" ? "created" : "joined"
              }`}
            subTitle="Start collaborating with other participants right now!"
          />
        </div>
      </div>
    );
  };

  return (
    <Layout title="My Collaboration Project - Indigo">
      <div className="x-mycollaborationproject-page">
        <MainLayout size="md" Breadcrumb>
          <ContentBox
            className="mb-4"
            toggles={["My Project", "Joined"]}
            toggleActive={toggleActive}
            setToggleActive={(toggle) => {
              setToggleActive(toggle);
              push({ query: {} });
              setProjectDetails({
                users: [],
                usersJoined: [],
                detail: {},
              });
            }}
          >
            {(isEmpty(projectDetails?.detail)) ? (
              <Row className="py-1">
                {(!isLoadingData ? (toggleActive === "My Project"
                  ? projectRequests
                  : projectJoined
                ) : [1, 2, 3]).map((item, idx) => (
                  <Fragment key={idx}>
                    {!isLoadingData ? !isEmpty(item) ? (
                      <PostCard
                        image={item.profile_picture}
                        HasHr
                        HideFooter
                        name={item.full_name}
                        jobTitle={item.job_title}
                        city={item.city_name}
                        postTimeAgo={formatDate(item.created_at)}
                        title={
                          <span
                            className="color-grey-3"
                            dangerouslySetInnerHTML={{
                              __html: item.description,
                            }}
                          />
                        }
                        totalView={item.total_view}
                        totalComment={item.total_comment}
                        profession={JSON.parse(item.profession)}
                        onCtaCustom={
                          <p
                            className={classnames(
                              "x-projectstatus x-projectstatus-active"
                            )}
                          >
                            {item.status}
                          </p>
                        }
                        onCtaCard={() => {
                          getProject(item.id_ads);
                          push({
                            // pathname: "/home/collaboration/my-projects/project",
                            query: {
                              id: item.id_ads,
                            },
                          });
                        }}
                      />
                    ) : (
                      renderEmptyState()
                    ) : <PostCard
                      isLoading={true}
                      HasHr
                    />
                    }
                  </Fragment>
                ))}
              </Row>
            ) : (
              <ContentBox NoDivider className="mt-3">
                <div className="x-image-thumbnail">
                  <div className="x-icon-eventeer" />
                  {!isLoadingDetails ? <img
                    src={
                      !isEmpty(detail?.image)
                        ? detail?.image
                        : DefaultPicture.src
                    }
                    alt=""
                  /> : <SkeletonElement width="100%" height={350} className="my-2" />}
                </div>
                {!isLoadingDetails ? <p className="m-0 mt-3 heading-md-bolder color-neutral-80">
                  {detail?.description}
                </p> : <SkeletonElement width="40%" height={24} className="mt-3" />}
                <div className="d-flex my-3 gap-3">
                  <Button
                    className="btn-cancel d-flex justify-content-center align-items-center"
                    onClick={() => {
                      push({
                        pathname:
                          "/home/collaboration/my-projects/collaboration-detail",
                        query: {
                          id: detail?.id_ads,
                        },
                      });
                    }}
                  >
                    <MaterialIcon
                      action="text_snippet"
                      size={18}
                      className="pe-2"
                    />
                    View Collaboration Details
                  </Button>
                  <Button
                    className="d-flex justify-content-center align-items-center"
                    disabled={isEmpty(users) || users.some(item => item.shortlist === "0")}
                    onClick={() => {
                      detail.id_groupmessage === null
                        ? push({pathname: "/home/collaboration/my-projects/group-chat", query: {id: detail?.id_ads}})
                        : push({
                          pathname: "/chat",
                          query: {
                            id_message: !detail.id_groupmessage
                              .id_groupmessage
                              ? false
                              : detail.id_groupmessage.id_groupmessage,
                            message_type: "message",
                            type: "group",
                          },
                        });
                    }}
                  >
                    <MaterialIcon action="forum" size={18} className="pe-2" />
                    Group Chat
                  </Button>
                </div>

                {!isEmpty(users) && (
                  <Fragment>
                    <hr />

                    <p className="text-lg-normal color-grey-3 my-3">
                      {`People who ${detail.id_user === id_user
                          ? "wants to join your collaborative project"
                          : "have joined"
                        }`}
                    </p>

                    <div className="d-flex flex-column gap-2">
                      {users?.map((item, idx) => (
                        <Fragment key={idx}>
                          {((detail.id_user !== id_user &&
                            item.shortlist === "1") ||
                            detail.id_user === id_user) && (
                              <ContentBox NoDivider className="x-user-lists">
                                <div className="d-flex justify-content-between align-items-center">
                                  <CardUser
                                    className="w-100"
                                    image={item.profile_picture}
                                    name={item.full_name}
                                    jobTitle={item.job_title}
                                    city={item.city_name}
                                    onClick={() =>
                                      push({
                                        pathname:
                                          "/home/collaboration/my-projects/profile",
                                        query: { id: item.id_user },
                                      })
                                    }
                                  />

                                  {detail.id_user === id_user && (
                                    <Fragment>
                                      {item.shortlist === "0" ? (
                                        <div className="d-flex align-items-center gap-2 py-3">
                                          {/* <Button disabled className="btn-disable x-btn-confirmation"> */}
                                          <Button
                                            className="btn-emergency x-btn-confirmation"
                                            onClick={() =>
                                              postEditMemberByShortlist(
                                                item.id_user,
                                                "2"
                                              )
                                            }
                                          >
                                            <MaterialIcon
                                              action="clear"
                                              weight={500}
                                            />
                                          </Button>

                                          <Button
                                            className="btn-success x-btn-confirmation"
                                            onClick={() =>
                                              postEditMemberByShortlist(
                                                item.id_user,
                                                "1"
                                              )
                                            }
                                          >
                                            <MaterialIcon
                                              action="done"
                                              weight={500}
                                            />
                                          </Button>
                                        </div>
                                      ) : (
                                        <div className="d-flex justify-content-center flex-column gap-2">
                                          <Button
                                            className={classnames(
                                              "w-auto py-1 px-3",
                                              item.followed_by && "btn-delete"
                                            )}
                                            onClick={() =>
                                              postFollowUnfollowAction(item)
                                            }
                                          >
                                            {item.followed_by
                                              ? "Following"
                                              : "Follow"}
                                          </Button>
                                          <Button
                                            className="btn-disable w-auto py-1 px-3"
                                            disabled={item.shortlist === "3"}
                                            onClick={() =>
                                              postEditMemberByShortlist(
                                                item.id_user,
                                                "0"
                                              )
                                            }
                                          >
                                            Cancel
                                          </Button>
                                        </div>
                                      )}
                                    </Fragment>
                                  )}
                                </div>
                              </ContentBox>
                            )}
                        </Fragment>
                      ))}
                    </div>
                  </Fragment>
                )}
              </ContentBox>
            )}
          </ContentBox>
        </MainLayout>
      </div>
    </Layout>
  );
};

export default appRoute(MyCollaborationProjects);
