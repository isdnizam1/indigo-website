import { Button, Col, Row, Form } from "react-bootstrap";
import React, { useState, useEffect } from "react";
import { getAuth, apiDispatch } from "/services/utils/helper";
import MainLayout from "/components/layout/MainLayout";
import { isEmpty } from "lodash-es";
import {
  getListCollaboration,
  getJob,
  getTopFilter,
} from "/services/actions/api";
import { useRouter } from "next/router";
import { setCollabsData } from "/redux/slices/collabsSlice";
import { useDispatch, useSelector } from "react-redux";
import ModalConfirmation from "/components/modal/ModalConfirmation";
import InputText from "/components/input/InputText";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";
import ModalShare from "/components/modal/ModalShare";
import ContentBox from "/components/home/ContentBox";
import CardExploreCollaboration from "/components/collaboration/CardExploreCollaboration";
import SkeletonElement from "/components/skeleton/SkeletonElement";

const ExploreCollaboration = () => {
  const router = useRouter();
  const { id_user } = getAuth();
  const [listData, setListData] = useState([]);
  const [listInterest, setListInterest] = useState([]);
  const [topJobs, setTopJobs] = useState([]);
  const [newInterest, setNewInterest] = useState([]);
  const [showModalInterest, setShowModalInterest] = useState(false);
  const [showModalTopProf, setShowModalTopProf] = useState(false);
  const collabsData = useSelector((state) => state.collabsReducer.data);
  const { filterInterest } = collabsData;
  const [getApiCount, setGetApiCount] = useState(0);
  const [dataShare, setDataShare] = useState(null);
  const [modalShareIsOpened, setModalShareIsOpened] = useState(false);
  const [filterInterestById, setFilterInterestById] = useState([]);
  const dispatch = useDispatch();
  const [isLoadingListData, setIsLoadingListData] = useState(true);
  let dynamicListData = isLoadingListData ? [1, 2, 3, 4, 5, 6] : listData;

  useEffect(() => {
    apiDispatch(
      getListCollaboration,
      { id_user, proffesion: filterInterestById },
      true
    ).then((response) => {
      setIsLoadingListData(false);
      if (response.code === 200) {
        const data = response.result;
        setListData(data);
      }
    });
  }, [getApiCount]);

  useEffect(() => getData(), []);

  const getData = () => {
    apiDispatch(getJob, {}, true).then((response) => {
      if (response.code === 200) {
        const data = response.result;
        setListInterest(data);
        setNewInterest(data);
      }
    });

    apiDispatch(getTopFilter, { filter: "profession", limit: "10" }, true).then(
      (response) => {
        if (response.code === 200) {
          const result = response.result;
          setTopJobs(result);
        }
      }
    );
  };

  const onFiltering = () => {
    let TempfilterInterestById = [];
    filterInterest.forEach((item) =>
      TempfilterInterestById.push(item.job_title)
    );
    setFilterInterestById(TempfilterInterestById);
    setGetApiCount(1 + getApiCount);
  };

  const onAddProfession = (e) => {
    const profession = JSON.parse(e.target.value);
    const isChecked = e.target.checked;

    const selected = isChecked
      ? [...filterInterest, profession]
      : filterInterest.filter((person) => person.id_job !== profession.id_job);
    dispatch(setCollabsData({ filterInterest: selected }));
  };

  const onSearchByKeyword = (event) => {
    const keyword = event.target.value.toLowerCase();
    const filteredInterest = newInterest.filter((item) =>
      item.job_title.toLowerCase().includes(keyword)
    );
    setListInterest(filteredInterest);
  };

  const renderModalInterest = () => {
    return (
      <>
        <ModalConfirmation
          modalClass="x-modal-city"
          backdrop={"static"}
          show={showModalInterest}
          size={"lg"}
          onHide={() => setShowModalInterest(false)}
          customTitle={
            <div className="x-modal modal-lg">
              <div className="d-flex align-items-center">
                <h1 className="heading-md-bold line-height-lg me-auto m-0 color-neutral-80">
                  Profession
                </h1>
                <InputText
                  disableError={true}
                  placeholder={"Find Profession"}
                  inlineIconSmall={"search"}
                  style={{ width: "550px", height: "40px", margin: "8px" }}
                  onChangeText={(event) => onSearchByKeyword(event)}
                />
                <span
                  className="material-icons-round cursor-pointer font-weight-bolder color-neutral-old-80"
                  onClick={() => {
                    setShowModalInterest(false);
                    onFiltering();
                  }}
                >
                  clear
                </span>
              </div>
              <div className="x-separator mt-1 mb-2 ps-0 pe-0 ms-0 me-0" />
              <div className="x-list-city text-sm-light align-items-center">
                <div className="x-city-word d-flex mt-1">
                  <p className="m-0 text-md-normal">Popular Profession</p>
                </div>
                <Row>
                  {topJobs.map((item, idx) => {
                    return (
                      <Col md={4} className="align-items-center" key={idx}>
                        <Form.Check
                          onChange={onAddProfession}
                          name="group1"
                          type={"checkbox"}
                          id={"UI/UX"}
                          label={item.job_title}
                          value={JSON.stringify(item)}
                          checked={filterInterest.some((user) =>
                            user.id_job === item.id_job ? true : false
                          )}
                        />
                      </Col>
                    );
                  })}
                  {newInterest.map((item, idx) => {
                    return (
                      <>
                        {newInterest[-1 + idx]?.job_title.slice(0, 1) !==
                          newInterest[idx]?.job_title.slice(0, 1) && (
                            <div className="x-city-word d-flex mt-1">
                              <p className="m-0 text-md-normal">
                                {newInterest[idx].job_title.slice(0, 1)}
                              </p>
                            </div>
                          )}

                        <Col md={4} className="align-items-center" key={idx}>
                          <Form.Check
                            name="group1"
                            type={"checkbox"}
                            onChange={onAddProfession}
                            id={"UI/UX"}
                            checked={filterInterest.some((user) =>
                              user.id_job === item.id_job ? true : false
                            )}
                            label={item.job_title}
                            value={JSON.stringify(item)}
                          />
                        </Col>
                      </>
                    );
                  })}
                </Row>
              </div>
            </div>
          }
          customFooter={
            <div className="d-flex x-footer2 align-items-center">
              <div className="ms-auto">
                <div className="d-flex align-items-center">
                  <Button
                    className="btn-cancel m-2 mb-0"
                    onClick={() => {
                      // handleDeleteProf();
                    }}
                  >
                    Reset
                  </Button>
                  <Button
                    className="btn-primary m-2 mb-0"
                    onClick={() => {
                      setShowModalInterest(false);
                      onFiltering();
                    }}
                  >
                    Apply
                  </Button>
                </div>
              </div>
            </div>
          }
        />
      </>
    );
  };

  const renderModalTopProf = () => {
    return (
      <>
        <ModalConfirmation
          modalClass="x-modal-profession"
          backdrop={"static"}
          show={showModalTopProf}
          size={"lg"}
          onHide={() => setShowModalTopProf(false)}
          customTitle={
            <div className="x-modal w-100">
              <div
                className="d-flex align-items-center"
                style={{ width: "700px" }}
              >
                <h1 className="heading-md-bold line-height-lg me-auto color-neutral-80 m-0 ps-0 pe-0 me-5">
                  Filter
                </h1>
                <span
                  className="material-icons-round cursor-pointer font-weight-bolder color-neutral-old-80"
                  onClick={() => setShowModalTopProf(false)}
                >
                  clear
                </span>
              </div>
              <div className="x-separator mt-1 mb-2 ps-0 pe-0 ms-0 me-0" />
              <div className="d-flex align-items-center ms-3 me-3">
                <h1 className="heading-sm-normal color-neutral-70 m-0">
                  Profession
                </h1>
                <h1
                  className="heading-sm-normal m-0 ms-auto cursor-pointer color-neutral-old-80"
                  onClick={() => {
                    setShowModalInterest(true);
                    setShowModalTopProf(false);
                  }}
                >
                  See All
                </h1>
              </div>
              <div className="d-flex align-items-center flex-wrap mt-2 ms-3 me-3">
                <div className="x-prof d-flex align-items-center">
                  {filterInterest.map((items, idx) => {
                    return (
                      <p
                        className="x-top-prof text-sm-light color-base-10 m-0 ms-2 me-2"
                        key={idx}
                        style={{
                          backgroundColor: "var(--primary-main)",
                        }}
                      >
                        {items.job_title}
                      </p>
                    );
                  })}
                </div>
              </div>
            </div>
          }
          customFooter={
            <div className="d-flex x-footer2 align-items-center">
              <div className="ms-auto">
                <div className="d-flex align-items-center">
                  <Button
                    className="btn-cancel m-2 mb-0"
                    onClick={() =>
                      dispatch(setCollabsData({ filterInterest: [] }))
                    }
                  >
                    Reset
                  </Button>
                  <Button
                    className="btn-primary m-2 mb-0"
                    onClick={() => {
                      setShowModalTopProf(false);
                      onFiltering();
                    }}
                  >
                    Apply
                  </Button>
                </div>
              </div>
            </div>
          }
        />
      </>
    );
  };

  return (
    <Layout title="Detail Collaboration Project - Indigo">
      <div className="x-explorecollaboration-page">
        <MainLayout size="xl" Breadcrumb>
          <ContentBox
            title="Explore Collaboration"
            subTitle="Collaborate your skills and create your dream team now"
          >
            {(isLoadingListData || !isEmpty(listData)) && (
              <Row className="m-0">
                <Col lg={12} className="mt-4">
                  {!isLoadingListData ? <Button
                    className="x-button-filter"
                    onClick={() => setShowModalTopProf(true)}
                  >
                    <span className="material-icons-round pe-1">tune</span>
                    <span className="pe-1">
                      Filter
                      {!isEmpty(filterInterest) && (
                        <span>
                          {" ("}
                          {filterInterest.length}
                          {")"}
                        </span>
                      )}
                    </span>
                  </Button> : <SkeletonElement width={85} height={30} />}
                </Col>
                {dynamicListData?.map((item, idx) => (
                  <Col lg={4} key={idx} className="x-cardexplore-wrapper">
                    <CardExploreCollaboration
                      isLoading={isLoadingListData}
                      status={item.status}
                      image={item.image}
                      title={item.title}
                      professions={item.profession}
                      profilePicture={item.profile_picture}
                      name={item.full_name}
                      jobTitle={item.job_title}
                      city={item.city_name}
                      totalView={item.total_view}
                      timeAgo={item.time_ago}
                      // onComment={() => }
                      onShare={() => {
                        setModalShareIsOpened(
                          modalShareIsOpened ? false : true
                        );
                        setDataShare(item);
                      }}
                      onClick={() =>
                        router.push({
                          pathname: "/home/collaboration/explore/detail",
                          query: { id: item.id_ads },
                        })
                      }
                    />
                  </Col>
                ))}
              </Row>
            )}
          </ContentBox>
        </MainLayout>
      </div>
      {renderModalInterest()}
      {renderModalTopProf()}
      <ModalShare
        show={modalShareIsOpened}
        onHide={() => setModalShareIsOpened(false)}
        dataShare={dataShare}
      />
    </Layout>
  );
};

export default appRoute(ExploreCollaboration);
