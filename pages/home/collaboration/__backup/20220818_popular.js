import { Container, Button, Form, Row, Col } from "react-bootstrap";
import React, { useState, useEffect } from "react";
import { isEmpty } from "lodash-es";
import { useRouter } from "next/router";
import { setCollabsData } from "/redux/slices/collabsSlice";
import { useDispatch, useSelector } from "react-redux";
import {
  getListCollaboration,
  getJob,
  getCity,
  getTopFilter,
} from "/services/actions/api";
import { getAuth, apiDispatch } from "/services/utils/helper";
import ModalConfirmation from "/components/modal/ModalConfirmation";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";
import InputText from "/components/input/InputText";
import CardPostCollab from "/components/collaboration/CardPostCollab";
import MainLayout from "/components/layout/MainLayout";
import BreadcrumbLv1 from "/components/header/BreadcrumbLv1";

import HeaderBackground from "/public/assets/images/img-collaboration-header-card.png";

const PopularCollab = (props) => {
  const router = useRouter();
  const [showModalPopCol, setShowModalPopCol] = useState(false);
  const [showModalCity, setShowModalCity] = useState(false);
  const [showModalProf, setShowModalProf] = useState(false);
  const [allCity, setAllCity] = useState([]);
  const [topCity, setTopCity] = useState([]);
  const [newAllCity, setNewAllCity] = useState([]);
  const [allJobs, setAllJobs] = useState([]);
  const [topJobs, setTopJobs] = useState([]);
  const [newAllJobs, setNewAllJobs] = useState([]);
  const [popularPosts, setPopularPosts] = useState([]);
  const [filterPopCityById, setFilterPopCityById] = useState([]);
  const [filterPopProfById, setFilterPopProfById] = useState([]);
  const [getApiCount, setGetApiCount] = useState(0);
  const collabsData = useSelector((state) => state.collabsReducer.data);
  const { filterPopCity, filterPopProf } = collabsData;
  const dispatch = useDispatch();
  const { id_user } = getAuth();

  useEffect(() => {
    getListCol(id_user);
  }, [getApiCount]);

  useEffect(() => {
    apiDispatch(getTopFilter, { filter: "profession", limit: "10" }, true).then(
      (response) => {
        if (response.code === 200) {
          const result = response.result;
          setTopJobs(result);
        }
      }
    );
  }, []);

  useEffect(() => {
    apiDispatch(getTopFilter, { filter: "city", limit: "10" }, true).then(
      (response) => {
        if (response.code === 200) {
          const result = response.result;
          setTopCity(result);
        }
      }
    );
  }, []);

  useEffect(() => {
    apiDispatch(getJob, {}, true).then((response) => {
      if (response.code === 200) {
        const result = response.result;
        setAllJobs(result);
        setNewAllJobs(result);
      }
    });
  }, []);

  useEffect(() => {
    apiDispatch(getCity, {}, true).then((response) => {
      if (response.code === 200) {
        const result = response.result;
        setAllCity(result);
        setNewAllCity(result);
      }
    });
  }, []);

  const getListCol = (id_user, order, proffesion, city_name) => {
    apiDispatch(
      getListCollaboration,
      {
        id_user: id_user,
        order: "popular",
        proffesion: filterPopProfById,
        city_name: filterPopCityById,
      },
      true
    ).then((response) => {
      if (response.code === 200) {
        setPopularPosts(response.result);
      }
    });
    console.log(filterPopProf);
  };

  const filtering = () => {
    let TempfilterPopCityById = [];
    let TempfilterPopProfById = [];
    filterPopCity.forEach((item) => TempfilterPopCityById.push(item.city_name));
    filterPopProf.forEach((item) => TempfilterPopProfById.push(item.job_title));
    setFilterPopCityById(TempfilterPopCityById);
    setFilterPopProfById(TempfilterPopProfById);
    setGetApiCount(1 + getApiCount);
  };

  const addCity = (e) => {
    const city = JSON.parse(e.target.value);
    // console.log({ city });
    const isChecked = e.target.checked;
    const selected = isChecked
      ? [...filterPopCity, city]
      : filterPopCity.filter((person) => person.id_city !== city.id_city);
    dispatch(setCollabsData({ filterPopCity: selected }));
  };

  const addProf = (e) => {
    const profession = JSON.parse(e.target.value);
    const isChecked = e.target.checked;

    const selected = isChecked
      ? [...filterPopProf, profession]
      : filterPopProf.filter((person) => person.id_job !== profession.id_job);
    dispatch(setCollabsData({ filterPopProf: selected }));
    console.log(selected);
  };

  const searchByKeyword = (event) => {
    const keyword = event.target.value.toLowerCase();
    const filteredPopCity = allCity.filter((item) =>
      item.city_name.toLowerCase().includes(keyword)
    );
    setNewAllCity(filteredPopCity);
    const filteredPopProf = allJobs.filter((item) =>
      item.job_title.toLowerCase().includes(keyword)
    );
    setNewAllJobs(filteredPopProf);
  };

  const handleDeleteCity = () => {
    dispatch(setCollabsData({ filterPopCity: [] }));
  };

  const handleDeleteProf = () => {
    dispatch(setCollabsData({ filterPopProf: [] }));
  };

  const renderPopularCollab = () => {
    return (
      <>
        <ModalConfirmation
          modalClass="x-modal-popcol"
          backdrop={"static"}
          show={showModalPopCol}
          size={"lg"}
          onHide={() => setShowModalPopCol(false)}
          customTitle={
            <div className="x-modal w-100">
              <div
                className="d-flex align-items-center"
                style={{ width: "700px" }}
              >
                <h1
                  className="heading-md-bold line-height-lg me-auto m-0 ps-0 pe-0 me-5"
                  style={{ color: "var(--neutral-80)" }}
                >
                  Filter
                </h1>
                <span
                  className="material-icons-round cursor-pointer"
                  style={{ fontWeight: "bolder", color: "var(--primary-main)" }}
                  onClick={() => {
                    setShowModalPopCol(false);
                    filtering();
                  }}
                >
                  clear
                </span>
              </div>
              <div className="x-separator mt-2 mb-2 ps-0 pe-0 ms-0 me-0" />
              <div className="d-flex align-items-center">
                <h1 className="text-md-normal m-0">City</h1>
                <h1
                  className="text-md-normal m-0 ms-auto cursor-pointer"
                  style={{ color: "var(--primary-main)" }}
                  onClick={() => {
                    setShowModalCity(true);
                    setShowModalPopCol(false);
                  }}
                >
                  See All
                </h1>
              </div>
              <div className="d-flex align-items-center flex-wrap mt-2">
                <div className="x-prof d-flex align-items-center">
                  {filterPopCity.map((item, idx) => {
                    return (
                      <p
                        className="x-top-prof text-sm-light m-0 me-2"
                        key={idx}
                        style={{
                          backgroundColor: "var(--primary-main)",
                          color: "var(--base-10)",
                        }}
                      >
                        {item.city_name}
                      </p>
                    );
                  })}
                </div>
              </div>
              <div className="x-separator mt-2 mb-2 ps-0 pe-0 ms-0 me-0" />
              <div className="d-flex align-items-center">
                <h1 className="text-md-normal m-0">Profession</h1>
                <h1
                  className="text-md-normal m-0 ms-auto cursor-pointer"
                  style={{ color: "var(--primary-main)" }}
                  onClick={() => {
                    setShowModalProf(true);
                    setShowModalPopCol(false);
                  }}
                >
                  See All
                </h1>
              </div>
              <div className="d-flex align-items-center flex-wrap mt-2">
                <div className="x-prof d-flex align-items-center">
                  {filterPopProf.map((item, idx) => {
                    return (
                      <p
                        className="x-top-prof text-sm-light m-0 me-2"
                        key={idx}
                        style={{
                          backgroundColor: "var(--primary-main)",
                          color: "var(--base-10)",
                        }}
                      >
                        {item.job_title}
                      </p>
                    );
                  })}
                </div>
              </div>
            </div>
          }
          customFooter={
            <div className="d-flex x-footer2 align-items-center">
              <div className="ms-auto">
                <div className="d-flex align-items-center">
                  <Button
                    className="btn-cancel m-2 mb-0"
                    onClick={() => {
                      handleDeleteCity();
                      handleDeleteProf();
                    }}
                  >
                    Reset
                  </Button>
                  <Button
                    className="btn-primary m-2 mb-0"
                    onClick={() => {
                      setShowModalPopCol(false);
                      filtering();
                    }}
                  >
                    Apply
                  </Button>
                </div>
              </div>
            </div>
          }
        />
      </>
    );
  };

  const renderModalCity = () => {
    return (
      <>
        <ModalConfirmation
          modalClass="x-modal-city"
          backdrop={"static"}
          show={showModalCity}
          size={"lg"}
          onHide={() => setShowModalCity(false)}
          customTitle={
            <div className="x-modal modal-lg">
              <div className="d-flex align-items-center">
                <h1
                  className="heading-md-bold line-height-lg me-auto m-0"
                  style={{ color: "var(--neutral-80)" }}
                >
                  City
                </h1>
                <InputText
                  disableError={true}
                  placeholder={"Find city"}
                  inlineIconSmall={"search"}
                  style={{ width: "679px", height: "40px", margin: "8px" }}
                  onChangeText={(event) => searchByKeyword(event)}
                />
                <span
                  className="material-icons-round cursor-pointer"
                  style={{ fontWeight: "bolder", color: "var(--primary-main)" }}
                  onClick={() => {
                    setShowModalCity(false);
                    filtering();
                  }}
                >
                  clear
                </span>
              </div>
              <div className="x-separator mt-1 mb-2 ps-0 pe-0 ms-0 me-0" />
              <div className="x-list-city ps-4 pe-4 text-sm-light align-items-center">
                <div className="x-city-word d-flex mt-1">
                  <p className="m-0 text-md-normal">Popular City</p>
                </div>
                <Row>
                  {topCity.map((item, idx) => {
                    return (
                      <Col md={4} className="align-items-center" key={idx}>
                        <Form.Check
                          onChange={addCity}
                          name="group1"
                          type={"checkbox"}
                          id={"UI/UX"}
                          label={item.city_name}
                          value={JSON.stringify(item)}
                          checked={filterPopCity.some((user) =>
                            user.id_city === item.id_city ? true : false
                          )}
                        />
                      </Col>
                    );
                  })}
                  {newAllCity.map((item, idx) => {
                    return (
                      <>
                        {newAllCity[-1 + idx]?.city_name.slice(0, 1) !==
                          newAllCity[idx]?.city_name.slice(0, 1) && (
                          <div className="x-city-word d-flex mt-1">
                            <p className="m-0 text-md-normal">
                              {newAllCity[idx].city_name.slice(0, 1)}
                            </p>
                          </div>
                        )}

                        <Col md={4} className="align-items-center" key={idx}>
                          <Form.Check
                            name="group1"
                            type={"checkbox"}
                            onChange={addCity}
                            id={"UI/UX"}
                            checked={filterPopCity.some((user) =>
                              user.id_city === item.id_city ? true : false
                            )}
                            label={item.city_name}
                            value={JSON.stringify(item)}
                          />
                        </Col>
                      </>
                    );
                  })}
                </Row>
              </div>
            </div>
          }
          customFooter={
            <div className="d-flex x-footer2 align-items-center">
              <div className="ms-auto">
                <div className="d-flex align-items-center">
                  <Button
                    className="btn-cancel m-2 mb-0"
                    onClick={() => {
                      handleDeleteCity();
                    }}
                  >
                    Reset
                  </Button>
                  <Button
                    className="btn-primary m-2 mb-0"
                    onClick={() => {
                      setShowModalCity(false);
                      filtering();
                    }}
                  >
                    Apply
                  </Button>
                </div>
              </div>
            </div>
          }
        />
      </>
    );
  };

  const renderModalProf = () => {
    return (
      <>
        <ModalConfirmation
          modalClass="x-modal-city"
          backdrop={"static"}
          show={showModalProf}
          size={"lg"}
          onHide={() => setShowModalProf(false)}
          customTitle={
            <div className="x-modal modal-lg">
              <div className="d-flex align-items-center">
                <h1
                  className="heading-md-bold line-height-lg me-auto m-0"
                  style={{ color: "var(--neutral-80)" }}
                >
                  Profession
                </h1>
                <InputText
                  disableError={true}
                  placeholder={"Find Profession"}
                  inlineIconSmall={"search"}
                  style={{ width: "550px", height: "40px", margin: "8px" }}
                  onChangeText={(event) => searchByKeyword(event)}
                />
                <span
                  className="material-icons-round cursor-pointer"
                  style={{ fontWeight: "bolder", color: "var(--primary-main)" }}
                  onClick={() => {
                    setShowModalProf(false);
                    filtering();
                  }}
                >
                  clear
                </span>
              </div>
              <div className="x-separator mt-1 mb-2 ps-0 pe-0 ms-0 me-0" />
              <div className="x-list-city text-sm-light align-items-center">
                <div className="x-city-word d-flex mt-1">
                  <p className="m-0 text-md-normal">Popular Profession</p>
                </div>
                <Row>
                  {topJobs.map((item, idx) => {
                    return (
                      <Col md={4} className="align-items-center" key={idx}>
                        <Form.Check
                          onChange={addProf}
                          name="group1"
                          type={"checkbox"}
                          id={"UI/UX"}
                          label={item.job_title}
                          value={JSON.stringify(item)}
                          checked={filterPopProf.some((user) =>
                            user.id_job === item.id_job ? true : false
                          )}
                        />
                      </Col>
                    );
                  })}
                  {newAllJobs.map((item, idx) => {
                    return (
                      <>
                        {newAllJobs[-1 + idx]?.job_title.slice(0, 1) !==
                          newAllJobs[idx]?.job_title.slice(0, 1) && (
                          <div className="x-city-word d-flex mt-1">
                            <p className="m-0 text-md-normal">
                              {newAllJobs[idx].job_title.slice(0, 1)}
                            </p>
                          </div>
                        )}

                        <Col md={4} className="align-items-center" key={idx}>
                          <Form.Check
                            name="group1"
                            type={"checkbox"}
                            onChange={addProf}
                            id={"UI/UX"}
                            checked={filterPopProf.some((user) =>
                              user.id_job === item.id_job ? true : false
                            )}
                            label={item.job_title}
                            value={JSON.stringify(item)}
                          />
                        </Col>
                      </>
                    );
                  })}
                </Row>
              </div>
            </div>
          }
          customFooter={
            <div className="d-flex x-footer2 align-items-center">
              <div className="ms-auto">
                <div className="d-flex align-items-center">
                  <Button
                    className="btn-cancel m-2 mb-0"
                    onClick={() => {
                      handleDeleteProf();
                    }}
                  >
                    Reset
                  </Button>
                  <Button
                    className="btn-primary m-2 mb-0"
                    onClick={() => {
                      setShowModalProf(false);
                      filtering();
                    }}
                  >
                    Apply
                  </Button>
                </div>
              </div>
            </div>
          }
        />
      </>
    );
  };

  return (
    <Layout title="Popular Collaborations - Indigo">
      <MainLayout>
        <div className="x-popularcollaborations-page">
          <Container>
            <div className="col-12 p-0 w-100">
              <div className="x-popularcollab w-100">
                <div className="x-breadcrumb-wrapper">
                  <BreadcrumbLv1 />
                </div>
                <img
                  src={HeaderBackground.src}
                  className="x-collab-img"
                  alt=""
                />
                <div className="x-content-border">
                  <p
                    className="m-0 heading-md-bolder"
                    style={{ color: "var(--neutral-80)" }}
                  >
                    Popular Collaborations
                  </p>
                  <div
                    className="x-btn-filter d-flex align-items-center cursor-pointer justify-content-center mt-2"
                    onClick={() => {
                      setShowModalPopCol(true);
                    }}
                  >
                    <div className="x-icon-mi-filter" />
                    <p
                      className="m-0 ms-1 text-sm-normal line-height-sm"
                      style={{ color: "var(--neutral-70)" }}
                    >
                      Filter
                    </p>
                    {filterPopProf.length + filterPopCity.length > 0 ? (
                      <>
                        <p
                          className="text-sm-normal line-height-sm m-0 me-1"
                          style={{ color: "var(--neutral-70)" }}
                        >
                          {" "}
                          ({filterPopProf.length + filterPopCity.length})
                        </p>
                      </>
                    ) : (
                      <p></p>
                    )}
                  </div>
                  <div className="x-separate" />
                  {popularPosts.length > 0 && (
                    <div className="x-posts-box">
                      {popularPosts.map((post, i) => {
                        return (
                          <div
                            className="cursor-pointer"
                            key={i}
                            onClick={() =>
                              router.push({
                                pathname: "/home/collaboration/detail",
                                query: { id: post.id_ads },
                              })
                            }
                          >
                            <CardPostCollab data={post} />
                          </div>
                        );
                      })}
                    </div>
                  )}
                </div>
              </div>
            </div>
          </Container>
        </div>
        {renderPopularCollab()}
        {renderModalCity()}
        {renderModalProf()}
      </MainLayout>
    </Layout>
  );
};

export default appRoute(PopularCollab);
