import React, { Fragment, useEffect, useState } from "react";
import { Row } from "react-bootstrap";
import PostCard from "/components/cards/PostCard";
import { isEmpty } from "lodash-es";
import { useRouter } from "next/router";
import ContentBox from "/components/home/ContentBox";
import MainLayout from "/components/layout/MainLayout";
import { formatDate, getAuth, apiDispatch } from "/services/utils/helper";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";
import { getListCollaborationLv1 } from "/services/actions/api";
import SkeletonElement from "/components/skeleton/SkeletonElement";

//svgs
import HeaderBackground from "/public/assets/images/img-collaboration-header-card.png";
import CardCreate from "/public/assets/images/img-collaboration-card_create.svg";
import CardExplore from "/public/assets/images/img-collaboration-card_explore.svg";
import CardNetwork from "/public/assets/images/img-collaboration-card_network.svg";
import CardProject from "/public/assets/images/img-collaboration-card_project.svg";

const Collaboration = () => {
  const router = useRouter();
  const { id_user } = getAuth();
  const [isLoading, setIsLoading] = useState(true);

  const [recentPosts, setRecentPosts] = useState([]);
  const [popularPosts, setPopularPosts] = useState([]);
  const activityMenu = [
    {
      image: CardCreate.src,
      url: "/home/collaboration/create-collaboration",
    },
    {
      image: CardExplore.src,
      url: "/home/collaboration/explore",
    },
    {
      image: CardNetwork.src,
      url: "/home/collaboration/my-network",
    },
    {
      image: CardProject.src,
      url: "/home/collaboration/my-projects",
    },
  ];

  useEffect(() => {
    apiDispatch(getListCollaborationLv1, { id_user }, true).then((response) => {
      setIsLoading(false);
      if (response.code === 200) {
        const { recent, popular } = response.result;
        setRecentPosts(recent);
        setPopularPosts(popular);
      }
    });
  }, []);

  const NoCollab = () => (
    <div className="x-emptypost-box">
      <div className="x-icon-webinar" />
      <p className="x-collab-subtitle-bold">No collaborations at this time</p>
      <div style={{ width: "30%" }}>
        <p className="x-collab-subtitle text-center">
          Start collaborating with other participants right now!
        </p>
      </div>
    </div>
  );

  const onRouteDetailProject = (id) => {
    router.push({
      pathname: "/home/collaboration/detail",
      query: { id },
    });
  };

  return (
    <Layout title="Collaboration - Indigo">
      <MainLayout Breadcrumb size="md" banner={HeaderBackground.src}>
        <div className="x-collaboration-page">
          <div className="mb-3">
            <ContentBox
              title={"Activity"}
              subTitle={"Cari aktivitas sesuai dengan kebutuhan kamu!"}
            >
              <div className="x-cards-box">
                {!isLoading ? activityMenu.map((item, idx) => (
                  <img
                    key={idx}
                    src={item.image}
                    alt=""
                    onClick={() => router.push(item.url)}
                  />
                )) : <SkeletonElement width={168.375} height={163.812} count={4} />
                }
              </div>
            </ContentBox>
          </div>

          <div className="mb-3">
            <ContentBox
              title={"Recently Updates"}
              cta={
                !isEmpty(recentPosts)
                  ? () => router.push("/home/collaboration/recent")
                  : null
              }
            >
              <Row className="py-1">
                {!isLoading ? !isEmpty(recentPosts) ? (
                  recentPosts.map((item, idx) => {
                    const data = JSON.parse(item.additional_data);
                    return (
                      <Fragment key={idx}>
                        <PostCard
                          isLoading={isLoading}
                          image={item.profile_picture}
                          name={item.created_by}
                          HasHr
                          HideReshare
                          jobTitle={item.job_title}
                          city={item.city_name}
                          postTimeAgo={formatDate(item.created_at)}
                          title={item.title}
                          totalView={item.total_view}
                          totalComment={item.total_comment}
                          profession={data.profession}
                          onCta={() => onRouteDetailProject(item.id_ads)}
                          onCtaCard={() => onRouteDetailProject(item.id_ads)}
                          onCtaFooter={() => onRouteDetailProject(item.id_ads)}
                        />
                      </Fragment>
                    );
                  })
                ) : (
                  <NoCollab />
                ) :
                  <PostCard
                    HasHr
                    isLoading={true}
                  />}
              </Row>
            </ContentBox>
          </div>

          <div className="mb-3">
            <ContentBox
              title={"Popular Collaborations"}
              cta={
                !isEmpty(popularPosts)
                  ? () => router.push("/home/collaboration/popular")
                  : null
              }
            >
              <Row className="py-1">
                {!isLoading ? !isEmpty(popularPosts) ? (
                  popularPosts.map((item, idx) => {
                    const data = JSON.parse(item.additional_data);
                    return (
                      <Fragment key={idx}>
                        <PostCard
                          image={item.profile_picture}
                          name={item.created_by}
                          HasHr
                          HideReshare
                          jobTitle={item.job_title}
                          city={item.city_name}
                          postTimeAgo={formatDate(item.created_at)}
                          title={item.title}
                          totalView={item.total_view}
                          totalComment={item.total_comment}
                          profession={data.profession}
                          onCta={() => onRouteDetailProject(item.id_ads)}
                          onCtaCard={() => onRouteDetailProject(item.id_ads)}
                          onCtaFooter={() => onRouteDetailProject(item.id_ads)}
                        />
                      </Fragment>
                    );
                  })
                ) : (
                  <NoCollab />
                ) :
                  <PostCard
                    HasHr
                    isLoading={true}
                  />}
              </Row>
            </ContentBox>
          </div>
        </div>
      </MainLayout>
    </Layout>
  );
};

export default appRoute(Collaboration);
