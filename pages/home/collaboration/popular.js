import { Container, Row } from "react-bootstrap";
import React, { useState, useEffect, Fragment } from "react";
import { isEmpty } from "lodash-es";
import { useRouter } from "next/router";
import { setCollabsData } from "/redux/slices/collabsSlice";
import { useDispatch, useSelector } from "react-redux";
import {
  getListCollaboration,
  getJob,
  getCity,
  getTopFilter,
} from "/services/actions/api";
import { getAuth, apiDispatch } from "/services/utils/helper";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";
import MainLayout from "/components/layout/MainLayout";
import BreadcrumbLv1 from "/components/header/BreadcrumbLv1";
import PostCard from "/components/cards/PostCard";
import ContentBox from "/components/home/ContentBox";
import ModalFilter from "/components/collaboration/modal/ModalFilter";
import ModalFilterDetail from "/components/collaboration/modal/ModalFilterDetail";
import { LoaderComponent } from "/components/loaders/Loaders";
import HeaderBackground from "/public/assets/images/img-collaboration-header-card.png";

const PopularCollaborations = () => {
  const router = useRouter();
  const [modalFilter, setModalFilter] = useState(false);
  const [modalFilterCities, setModalFilterCities] = useState(false);
  const [modalFilterProfessions, setModalFilterProfessions] = useState(false);
  const [allCity, setAllCity] = useState([]);
  const [topCity, setTopCity] = useState([]);
  const [newAllCity, setNewAllCity] = useState([]);
  const [allJobs, setAllJobs] = useState([]);
  const [topJobs, setTopJobs] = useState([]);
  const [newAllJobs, setNewAllJobs] = useState([]);
  const [posts, setPosts] = useState([]);
  const [filterCityById, setFilterCityById] = useState([]);
  const [filterProfessionById, setFilterProfessionById] = useState([]);
  const [apiCount, setApiCount] = useState(0);
  const [isLoading, setIsLoading] = useState(true);

  const collabsData = useSelector((state) => state.collabsReducer.data);
  const { filterPopCity, filterPopProf } = collabsData;
  const dispatch = useDispatch();
  const { id_user } = getAuth();

  useEffect(() => getData(), []);

  useEffect(() => getListCollaborationData("popular"), [apiCount]);

  const getData = () => {
    apiDispatch(getTopFilter, { filter: "profession", limit: "10" }, true).then(
      (response) => {
        if (response.code === 200) {
          const result = response.result;
          setTopJobs(result);
        }
      }
    );

    apiDispatch(getTopFilter, { filter: "city", limit: "10" }, true).then(
      (response) => {
        if (response.code === 200) {
          const result = response.result;
          setTopCity(result);
        }
      }
    );

    apiDispatch(getJob, {}, true).then((response) => {
      if (response.code === 200) {
        const result = response.result;
        setAllJobs(result);
        setNewAllJobs(result);
      }
    });

    apiDispatch(getCity, {}, true).then((response) => {
      if (response.code === 200) {
        const result = response.result;
        setAllCity(result);
        setNewAllCity(result);
      }
    });
  };

  const getListCollaborationData = (orderBy) => {
    apiDispatch(
      getListCollaboration,
      {
        id_user,
        order: orderBy,
        proffesion: filterProfessionById,
        city_name: filterCityById,
      },
      true
    ).then((response) => {
      setIsLoading(false);
      response.code === 200 && setPosts(response.result);
    });
  };

  const filtering = () => {
    let TempFilterCityById = [];
    let TempFilterProfessionById = [];

    filterPopCity.forEach((item) => TempFilterCityById.push(item.city_name));
    filterPopProf.forEach((item) =>
      TempFilterProfessionById.push(item.job_title)
    );

    setFilterCityById(TempFilterCityById);
    setFilterProfessionById(TempFilterProfessionById);
    setApiCount(1 + apiCount);
  };

  const addCityOnChange = (e) => {
    const city = JSON.parse(e.target.value);
    const isChecked = e.target.checked;
    const selected = isChecked
      ? [...filterPopCity, city]
      : filterPopCity.filter((person) => person.id_city !== city.id_city);
    dispatch(setCollabsData({ filterPopCity: selected }));
  };

  const addProfessionOnChange = (e) => {
    const profession = JSON.parse(e.target.value);
    const isChecked = e.target.checked;

    const selected = isChecked
      ? [...filterPopProf, profession]
      : filterPopProf.filter((person) => person.id_job !== profession.id_job);
    dispatch(setCollabsData({ filterPopProf: selected }));
  };

  const filterSearchByKeyword = (event) => {
    const keyword = event.target.value.toLowerCase();

    const filteredRecCity = allCity.filter((item) =>
      item.city_name.toLowerCase().includes(keyword)
    );
    setNewAllCity(filteredRecCity);

    const filteredRecProf = allJobs.filter((item) =>
      item.job_title.toLowerCase().includes(keyword)
    );
    setNewAllJobs(filteredRecProf);
  };

  return (
    <Layout title="Popular Collaborations - Indigo">
      <MainLayout>
        <div className="x-popularcollaborations-page">
          <Container>
            <div className="col-12 p-0 w-100">
              <div className="w-100">
                <div className="x-breadcrumb-wrapper">
                  <BreadcrumbLv1 />
                </div>

                <img src={HeaderBackground.src} className="x-banner" alt="" />

                <ContentBox
                  title={"Popular Collaborations"}
                  filterOnClick={() => setModalFilter(true)}
                  filterCount={filterPopProf.length + filterPopCity.length}
                  isLoading={isLoading}
                >
                  <Row className="py-1">
                    <LoaderComponent
                      component={PostCard}
                      count={3}
                      isLoading={isLoading}
                    >
                      {posts?.map((item, idx) => {
                        const data = JSON.parse(item.additional_data);
                        return (
                          <Fragment key={idx}>
                            <PostCard
                              isLoading={isLoading}
                              image={item.profile_picture}
                              name={item.created_by}
                              banner={item.image}
                              jobTitle={item.job_title}
                              city={item.city_name}
                              postTimeAgo={item.time_ago}
                              title={item.title}
                              totalView={item.total_view}
                              totalComment={item.total_comment}
                              profession={data.profession}
                              onCta={() => {
                                router.push({
                                  pathname: "/home/collaboration/detail",
                                  query: {
                                    id: item.id_ads,
                                  },
                                });
                              }}
                              onCtaCard={() => {
                                router.push({
                                  pathname: "/home/collaboration/detail",
                                  query: {
                                    id: item.id_ads,
                                  },
                                });
                              }}
                            />
                          </Fragment>
                        );
                      })}
                    </LoaderComponent>
                  </Row>
                </ContentBox>
              </div>
            </div>
          </Container>
        </div>

        <ModalFilter
          show={modalFilter}
          cities={filterPopCity}
          professions={filterPopProf}
          onHide={() => {
            setModalFilter(false);
            filtering();
          }}
          onSeeAllCities={() => {
            setModalFilterCities(true);
            setModalFilter(false);
          }}
          onSeeAllProfessions={() => {
            setModalFilterProfessions(true);
            setModalFilter(false);
          }}
          onReset={() =>
            dispatch(setCollabsData({ filterPopCity: [], filterPopProf: [] }))
          }
          onSubmit={() => {
            setModalFilter(false);
            filtering();
          }}
        />

        <ModalFilterDetail
          show={modalFilterCities}
          title="City"
          data={newAllCity}
          dataTitleReferred="city_name"
          dataIdReferred="id_city"
          popularData={topCity}
          popularTitle="Popular City"
          filteredData={filterPopCity}
          onFilterSearch={filterSearchByKeyword}
          onHide={() => {
            setModalFilterCities(false);
            filtering();
          }}
          onAddData={addCityOnChange}
          onReset={() => dispatch(setCollabsData({ filterPopCity: [] }))}
          onSubmit={() => {
            setModalFilterCities(false);
            filtering();
          }}
        />

        <ModalFilterDetail
          show={modalFilterProfessions}
          title="Profession"
          data={newAllJobs}
          dataTitleReferred="job_title"
          dataIdReferred="id_job"
          popularData={topJobs}
          popularTitle="Popular Profession"
          filteredData={filterPopProf}
          onFilterSearch={filterSearchByKeyword}
          onHide={() => {
            setModalFilterProfessions(false);
            filtering();
          }}
          onAddData={addProfessionOnChange}
          onReset={() => dispatch(setCollabsData({ filterPopProf: [] }))}
          onSubmit={() => {
            setModalFilterProfessions(false);
            filtering();
          }}
        />
      </MainLayout>
    </Layout>
  );
};

export default appRoute(PopularCollaborations);
