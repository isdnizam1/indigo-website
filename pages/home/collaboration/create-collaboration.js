import React, { Fragment, useEffect, useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { isEmpty, toNumber } from "lodash-es";
import { Button, Form } from "react-bootstrap";
import { useRouter } from "next/router";
import { getCity, getDetailCollaboration } from "/services/actions/api";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";
import InputText from "/components/input/InputText";
import InputSelect from "/components/input/InputSelect";
import MainLayout from "/components/layout/MainLayout";
import { apiDispatch, convertToBase64, getAuth } from "/services/utils/helper";
import {
  getGenreInterest,
  getProfileDetail,
  postCreateCollaboration,
} from "/services/actions/api";
import ContentBox from "/components/home/ContentBox";
import DefaultBanner from "/public/assets/images/img-collaboration-banner.svg";
import classnames from "classnames";
import MaterialIcon from "/components/utils/MaterialIcon";
import ModalOnSubmit from "/components/modal/ModalOnSubmit";
import { postEditCollaboration } from "../../../services/actions/api";

const CreateCollaboration = () => {
  const { id_user } = getAuth();
  const { query, push, reload, isReady, back, pathname } = useRouter();
  const isEdit = pathname.includes("edit-collaboration");
  const { id: id_ads } = query;
  console.log({ isEdit });

  const [isBannerChanged, setBannerChanged] = useState(false);

  const formik = useFormik({
    initialValues: {
      title: "",
      profession: [],
      notes: "",
      city: "",
      quota: "",
      date_start: "",
      date_end: "",
      about: "",
      reference: "",
      id_city: null,
    },
    validationSchema: Yup.object({
      title: Yup.string().max(70).required(),
      profession: Yup.array(),
      notes: Yup.string().max(70).required(),
      id_city: Yup.string().required(),
      quota: Yup.number().required(),
      date_start: Yup.string().required(),
      date_end: Yup.string().required(),
      about: Yup.string().max(250).required(),
      reference: Yup.string(),
    }),
    onSubmit: (values) => {},
  });

  useEffect(() => {
    if (isReady && isEdit) {
      id_ads &&
        getDetailCollaboration({
          id_user,
          id_ads,
          limit: 1,
        }).then((res) => {
          if (res.status === 200) {
            const { title, description, additional_data_arr, location, image } =
              res.data.result;
            const {
              collaboration_reference,
              date,
              member_quota,
              profession,
              specification,
            } = additional_data_arr;

            setCityName(additional_data_arr?.location?.name);
            setCityKeyword(additional_data_arr?.location?.name);
            setBanner64(image);

            formik.setValues({
              ...formik.values,
              title,
              notes: description,
              id_city: toNumber(location.id_city),
              profession: profession.map(({ name }) => name),
              about: specification,
              date_start: date.start,
              date_end: date.end,
              quota: toNumber(member_quota),
              reference: collaboration_reference,
            });
          } else back();
        });
    }
  }, [isReady]);

  const {
    title,
    notes,
    id_city,
    quota,
    date_start,
    date_end,
    about,
    profession,
    reference,
  } = formik.values;

  //city
  const [allCities, setAllCities] = useState([]);
  const [suggestionCities, setSuggestionCities] = useState([]);
  const [cityName, setCityName] = useState("");
  const [cityKeyword, setCityKeyword] = useState("");
  const [isLoading, setIsLoading] = useState(true);
  const [modal, setModal] = useState(false);

  //Profession
  const [allProfessions, setAllProfessions] = useState([]);
  const [suggestionProfessions, setSuggestionProfessions] = useState([]);
  const [professionKeyword, setProfessionKeyword] = useState("");

  //banner img
  const [banner64, setBanner64] = useState();

  //step validation
  const [user, setUser] = useState({});
  const [step, setStep] = useState(1);
  const [stepValidate, setStepValidate] = useState(1);

  useEffect(() => {
    getCities();
    getUserDetail();
    getProfessions();
  }, []);

  useEffect(() => {
    if(title && notes && id_city && quota > 0 && date_start && date_end) {
      about ? setStepValidate(3) : setStepValidate(2)
    }
    else setStepValidate(1);
  }, [formik.values]);

  const getProfessions = () => {
    apiDispatch(getGenreInterest, { start: 0 }, true).then((response) => {
      response.code === 200 && setAllProfessions(response.result);
    });
  }

  const getUserDetail = () => {
    apiDispatch(getProfileDetail, { id_user }, true).then((response) => {
      response.code === 200 && setUser(response.result);
    });
  }

  const getCities = () => {
    apiDispatch(getCity, {start: 0}, true).then(response => {
      response.code === 200 && setAllCities(response.result);
    })
  }

  const handleCityChange = (keyword) => {
    let datas = allCities.filter((data) =>
      data.city_name.toLowerCase().includes(keyword.toLowerCase())
    );
    setCityKeyword(keyword);
    setSuggestionCities(!isEmpty(keyword) ? datas.slice(0, 6) : []);
  }

  const handleProfessionChange = (keyword) => {
    let datas = allProfessions.filter((data) => {
      return data.interest_name.toLowerCase().includes(keyword.toLowerCase());
    });
    setProfessionKeyword(keyword);
    setSuggestionProfessions(!isEmpty(keyword) ? datas.slice(0, 10) : []);
  };

  const onImageChange = (event) => {
    setBannerChanged(true);
    if (event.target.files && event.target.files[0]) {
      let img = event.target.files[0];
      convertToBase64(img).then((result) => setBanner64(result));
    }
  };

  const handleSubmit = () => {
    formik.setSubmitting(true);
    setIsLoading(true);
    setModal(true);

    const diffInMs = new Date(date_end) - new Date(date_start);
    const diffInDays = diffInMs / (1000 * 60 * 60 * 24);

    let professionParams = [];
    profession.map(name => professionParams.push({name}));

    const params = {
      id_user,
      title,
      description: notes,
      profession: professionParams,
      location_name: cityName,
      banner_picture: banner64,
      spesification: about,
      collaboration_reference: reference,
      memberQuota: quota,
      startDate: date_start,
      endDate: date_end,
    };

    const idAds = isEdit ? {id_ads} : null;
    const postApi = isEdit ? postEditCollaboration : postCreateCollaboration;
    const banner = {banner_picture: isBannerChanged ? banner64 : ""};

    apiDispatch(postApi, { ...params, ...idAds, ...banner }, true).then((response) => {
      setIsLoading(false);
      formik.setSubmitting(false);

      if (response.code === 200) {
        formik.setStatus("success");
        setTimeout(() => push("/home/collaboration/"), 5000);
      }
    });
  }

	const renderModalOnSubmit = () => {
		return (
			<ModalOnSubmit
        isLoading={isLoading}
				backdrop={isLoading ? 'static' : true}
				size="md"
				show={modal}
				onHide={() => {
          if(!isLoading) {
            setModal(false);
            setTimeout(() => push("/home/collaboration"), 500);
          }
        }}
        loadingTitle="Processing Collaboration Project"
        loadingSubTitle="Please wait a minute until the process is complete"
				title="Success"
				subTitle="Your collaboration project was uploaded"
			/>
		);
	};

  return (
    <Layout title="Create Collaboration - Indigo">
      <div className="x-createcollaboration-page">
        <MainLayout size="md" Breadcrumb>
          <ContentBox
            NoDivider
            NoBorder={step === 3 && true}
            className="x-content-borderbox"
          >
            <h3 className="heading-md-bold color-neutral-80 m-0">
              Create Collaboration
            </h3>
            <div className="x-progressbar">
              <div
                className={`${step === 1 ? "x-progressbar-active" : ""} ${
                  step > 1 ? "x-progressbar-behind" : ""
                }`}
                onClick={() => setStep(1)}
              >
                <span>1</span>
              </div>
              <hr
                className={classnames("x-progressline", step > 1 && "active")}
              />
              <div
                className={`${step === 2 ? "x-progressbar-active" : ""} ${
                  step > 2 ? "x-progressbar-behind" : ""
                } ${step < 2 ? "x-step" : ""} `}
                onClick={() => {
                  stepValidate > 1 && setStep(2);
                }}
              >
                <span>2</span>
              </div>{" "}
              <hr
                className={classnames("x-progressline", step > 2 && "active")}
              />
              <div
                className={`${step === 3 ? "x-progressbar-active" : ""} ${
                  step > 3 ? "x-progressbar-behind" : ""
                } ${step < 3 ? "x-step" : ""} `}
                onClick={() => stepValidate > 2 && setStep(3)}
              >
                <span>3</span>
              </div>
            </div>

            {step === 1 && (
              <Fragment>
                <div className="d-flex w-100 gap-3">
                  <div className="profile-pic">
                    {user.profile_picture ? (
                      <img src={user.profile_picture} alt="profile" />
                    ) : (
                      <div className="x-empty-image">
                        <p
                          className="text-xl-normal m-0 color-base-10"
                        >
                          {user.full_name?.slice(0, 1).toUpperCase()}
                        </p>
                      </div>
                    )}
                  </div>
                  <div className="w-100 position-relative">
                    <InputText
                      id="title"
                      title=""
                      name="title"
                      placeholder="Cari UIUX Designer untuk design aplikasi pendanaan agroteknologi."
                      onChangeText={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values.title}
                      disableError
                      maxLength={70}
                    />
                    <span className="x-input-count">
                      {formik.values.title.length}/70
                    </span>
                  </div>
                </div>

                <hr className="w-100 my-4" />

                <div className="w-100 d-flex flex-column gap-2">
                  <div className="x-input-profession">
                    <div className="d-flex gap-2 pb-1">
                      <p className="text-lg-normal color-neutral-80 m-0">
                        Search Profession:
                      </p>
                      <p className="text-lg-light color-neutral-old-40 m-0">
                        (Example : front end, back end, UI/UX designer)
                      </p>
                    </div>
                    <div className="d-flex align-items-center">
                      <MaterialIcon
                        action="add"
                        className="color-neutral-old-80 pe-1"
                      />
                      <InputSelect
                        type="text"
                        value={professionKeyword}
                        placeholder="Add Profession"
                        data={suggestionProfessions}
                        selectedValue={""}
                        disableError={true}
                        hasIcon={false}
                        onFocus={() =>
                          setSuggestionProfessions(allProfessions.slice(0, 5))
                        }
                        onBlur={() => {
                          setTimeout(() => {
                            setProfessionKeyword("");
                            setSuggestionProfessions([]);
                          }, 300);
                        }}
                        onChangeText={(event) =>
                          handleProfessionChange(event.target.value)
                        }
                        suggestionText={"interest_name"}
                        suggestionOnClick={(interest) => {
                          setProfessionKeyword("");
                          setSuggestionProfessions([]);
                          formik.setValues({
                            ...formik.values,
                            profession: [...profession, interest.interest_name],
                          });
                        }}
                      />
                    </div>
                    <div className="d-flex align-items-center flex-wrap w-100 pt-1">
                      {profession.length > 0 &&
                        profession.map((item, idx) => (
                          <div className="x-tag-interest" key={idx}>
                            <p className="text-sm-normal m-0 line-height-sm p-0">
                              {item}
                            </p>
                            <span
                              className="material-icons-round"
                              onClick={() => {
                                formik.setValues({
                                  ...formik.values,
                                  profession: profession.filter(
                                    (data) => item !== data
                                  ),
                                });
                              }}
                            >
                              close
                            </span>
                          </div>
                        ))}
                    </div>
                  </div>

                  <div className="w-100 position-relative">
                    <InputText
                      id="notes"
                      title="Notes"
                      name="notes"
                      placeholder="Example: UI Design for Fintech"
                      onChangeText={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values.notes}
                      disableError
                      maxLength={70}
                    />
                    <span className="x-input-count">
                      {formik.values.notes.length}/70
                    </span>
                  </div>

                  <div className="w-100">
                    <InputSelect
                      title="City / Location"
                      type="text"
                      disableError
                      value={cityKeyword}
                      placeholder="Choose location"
                      data={suggestionCities}
                      selectedValue={cityName}
                      onFocus={() =>
                        isEmpty(cityName)
                          ? setSuggestionCities(allCities.slice(0, 6))
                          : handleCityChange(cityName)
                      }
                      onRemoveSelected={() => {
                        setCityName("");
                        setCityKeyword("");
                        setSuggestionCities([]);
                      }}
                      onBlur={() => {
                        setCityKeyword(cityName);
                        setTimeout(() => setSuggestionCities([]), 300);
                      }}
                      onChangeText={(e) => handleCityChange(e.target.value)}
                      suggestionText={"city_name"}
                      suggestionOnClick={(city) => {
                        setCityName(city.city_name);
                        setCityKeyword(city.city_name);
                        setSuggestionCities([]);
                        formik.setValues({
                          ...formik.values,
                          id_city: city.id_city,
                        });
                      }}
                    />
                  </div>

                  <div className="w-100 position-relative">
                    <InputText
                      id="quota"
                      title="Member quota"
                      name="quota"
                      type="number"
                      placeholder="Enter the number of members"
                      onChangeText={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values.quota}
                      disableError
                    />
                  </div>

                  <div className="x-input-text mb-1">
                    <Form.Label column sm={12} className="p-0 text-lg-normal">
                      Duration project
                    </Form.Label>

                    <div className="d-flex gap-1 align-items-center">
                      <InputText
                        type="date"
                        id="date_start"
                        title=""
                        name="date_start"
                        placeholder="Start date"
                        onChangeText={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.date_start}
                        disableError
                        min={new Date().toISOString().split("T")[0]}
                      />
                      <span className="material-icons-round opacity-50">
                        remove
                      </span>
                      <InputText
                        type="date"
                        id="date_end"
                        title=""
                        name="date_end"
                        placeholder="End date"
                        onChangeText={formik.handleChange}
                        onBlur={formik.handleBlur}
                        value={formik.values.date_end}
                        disableError
                        min={new Date().toISOString().split("T")[0]}
                      />
                    </div>
                  </div>
                </div>
              </Fragment>
            )}

            {step === 2 && (
              <>
                <div className="w-100">
                  <div className="x-banner-container">
                    <img src={banner64 ?? DefaultBanner.src} alt="" />
                  </div>
                  <label
                    className="x-image-selector x-input-banner"
                    htmlFor="banner-upload"
                  >
                    <input
                      id="banner-upload"
                      accept="image/png, image/jpg, image/jpeg"
                      type="file"
                      onChange={onImageChange}
                    />
                    <MaterialIcon action="photo_camera" />
                    <span>Edit Banner</span>
                  </label>
                </div>
                <hr className="w-100 my-4" />

                <div className="d-flex flex-column gap-3 w-100">
                  <div className="w-100 position-relative">
                    <InputText
                      id="about"
                      title="About Collaboration"
                      name="about"
                      placeholder="Write a description about collaboration"
                      onChangeText={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values.about}
                      disableError
                      maxLength={250}
                    />
                    <span className="x-input-count">
                      {formik.values.about.length}/250
                    </span>
                  </div>

                  <div className="w-100 position-relative">
                    <InputText
                      id="reference"
                      title="Collaboration reference (optional)"
                      name="reference"
                      type="text"
                      placeholder="Enter your collaboration reference link/URL"
                      onChangeText={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values.reference}
                      disableError
                    />
                  </div>

                  <label
                    className="x-image-selector position-relative"
                    onClick={() => {
                      navigator.clipboard.readText().then((text) => {
                        formik.setValues({ ...formik.values, reference: text });
                      });
                    }}
                  >
                    <span className="material-icons-round">link</span>
                    <span>Paste Link</span>
                  </label>
                </div>
              </>
            )}

            {step === 3 && (
              <div className="x-display-collab">
                <div className="d-flex w-100 gap-4">
                  <div className="profile-pic">
                    {user.profile_picture ? (
                      <img src={user.profile_picture} alt="profile" />
                    ) : (
                      <div className="x-empty-image">
                        <p
                          className="text-xl-normal m-0 color-base-10"
                        >
                          {user.full_name?.slice(0, 1).toUpperCase()}
                        </p>
                      </div>
                    )}
                  </div>
                  <h3>{title}</h3>
                </div>
                <div>
                  <h5 className="x-display-collab-title">
                    Searched position :{" "}
                  </h5>
                  <div className="d-flex align-items-center flex-wrap w-100 pt-1">
                    {profession.length > 0 &&
                      profession.map((item, idx) => (
                        <div className="x-tag-interest" key={idx}>
                          <p className="text-sm-normal m-0 line-height-sm p-0">
                            {item}
                          </p>
                        </div>
                      ))}
                  </div>
                </div>
                <div>
                  <h5 className="x-display-collab-title">Notes :</h5>
                  <p className="x-display-collab-text">{notes}</p>
                </div>
                <div>
                  <h5 className="x-display-collab-title">City :</h5>
                  <p className="x-display-collab-text">{cityName}</p>
                </div>
                <div className="d-flex justify-content-end">
                  <label
                    className="x-image-selector position-relative"
                    onClick={() => {
                      setStep(1);
                    }}
                  >
                    <MaterialIcon action="edit" size={16} />
                    <span style={{ fontSize: 14 }}>Edit</span>
                  </label>
                </div>

                <hr className="w-100 my-4" />

                <div className="x-banner-container">
                  <img src={banner64 ?? DefaultBanner.src} alt="" />
                </div>

                <hr className="w-100 my-4" />

                <div>
                  <h5 className="x-display-collab-title">
                    About Collaboration :{" "}
                  </h5>
                  <p className="x-display-collab-text">{about}</p>
                </div>

                <div>
                  <h5 className="x-display-collab-title">
                    Collaboration reference :{" "}
                  </h5>
                  <p
                    className="x-display-collab-text w-50 cursor-pointer color-neutral-old-80"
                    onClick={() => {
                      window.open(reference, "_blank");
                    }}
                  >
                    {reference ?? "-"}
                  </p>
                </div>
              </div>
            )}
          </ContentBox>

          <div
            className={`d-flex justify-content-${
              step > 1 ? "between" : "end"
            } w-100 pb-5 pt-3`}
          >
            {step > 1 && (
              <Button
                className="btn-topic w-auto d-flex gap-1 align-items-center"
                onClick={() => setStep(step - 1)}
              >
                <MaterialIcon action="arrow_back_ios" size={16} />
                Previous
              </Button>
            )}
            <div>
              <Button
                className="d-flex gap-1 align-items-center"
                disabled={stepValidate === 3 ? false : stepValidate <= step}
                onClick={step === 3 ? handleSubmit : () => setStep(step + 1)}
              >
                {step === 3 ? (
                  "Publish"
                ) : (
                  <Fragment>
                    Next
                    <MaterialIcon action="arrow_forward_ios" size={16} />
                  </Fragment>
                )}
              </Button>
            </div>
          </div>
        </MainLayout>
        {renderModalOnSubmit()}
      </div>
    </Layout>
  );
};

export default appRoute(CreateCollaboration);
