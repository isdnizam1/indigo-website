import React, { useState, useEffect, Fragment } from "react";
import { Container, Button } from "react-bootstrap";
import { getAuth, dateFormatter, apiDispatch } from "/services/utils/helper";

import classnames from "classnames";
import MainLayout from "/components/layout/MainLayout";
import { isEmpty } from "lodash-es";
import { useRouter } from "next/router";
import {
  getDetailCollaboration,
  postCloseCollaboration,
  postProfileFollow,
  postProfileUnfollow,
  postCreateComment,
  postAddMember,
  postFeedCommentLike,
  postJoinCollaboration,
  postEditMemberProject,
} from "/services/actions/api";
import CommentPost from "/components/comment/CommentPost";
import CommentCard from "/components/comment/CommentCard";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";
import ModalShare from "/components/modal/ModalShare";
import ModalConfirmation from "/components/modal/ModalConfirmation";
import { useDispatch } from "react-redux";
import { setChatData } from "/redux/slices/chatSlice";
import RightBarBox from "/components/home/RightBarBox";
import SkeletonElement from "/components/skeleton/SkeletonElement";
import MaterialIcon from "/components/utils/MaterialIcon";
import ModalOnSubmit from "/components/modal/ModalOnSubmit";

const DetailCollaborationProject = () => {
  // const [isActiveTab, setIsActiveTab] = useState("My Project");
  const [data, setData] = useState({});
  const [isLoadingData, setIsLoadingData] = useState(true);
  const [modalJoinSuccess, setModalJoinSuccess] = useState(false);
  const [isFollowed, setIsFollowed] = useState(false);
  const [dataShare, setDataShare] = useState(null);
  const [modalShareIsOpened, setModalShareIsOpened] = useState(false);
  const [commentValue, setCommentValue] = useState("");
  const [updateCount, setUpdateCount] = useState(0);
  const [modalConfirmation, setModalConfirmation] = useState(false);
  const [modalProjectConfirmation, setModalProjectConfirmation] = useState(false);
  const dispatch = useDispatch();

  const { push, query, isReady } = useRouter();
  const { id: id_ads } = query;
  const { id_user } = getAuth();

  const {
    title,
    image,
    description,
    city_name,
    time_ago,
    comment: comments,
    total_comment,
    created_by,
    job_title,
    profile_picture,
    additional_data_arr: detail,
    joined,
    id_user: id_owner,
    follow_by_owner,
    id_groupmessage,
    shortlist,
  } = data;
  let dummyData = [1, 2, 3];
  let dynamicComments = isLoadingData ? dummyData : comments;
  let dynamicJoined = isLoadingData ? dummyData : joined;

  useEffect(() => isReady && getData(), [updateCount, isReady]);

  const getData = () => {
    apiDispatch(
      getDetailCollaboration,
      { id_ads, id_user, limit: 3 },
      true
    ).then((response) => {
      setIsLoadingData(false);
      if (response.code === 200) {
        const result = response.result;
        setIsFollowed(result.followed_by);
        setData(
          !isEmpty(data)
            ? {
                ...data,
                total_comment: result.total_comment,
                comment: result.comment,
              }
            : result
        );
      }
    });
  };

  const postJoinCollab = () => {
    apiDispatch(postJoinCollaboration, { id_ads, id_user }, true).then(
      (response) => {
        if(response.code === 200) {
          setModalJoinSuccess(true);
          setData({...data, shortlist: "0"});
        }
      }
    );
  };

  const postTakeJoin = () => {
    apiDispatch(
      postEditMemberProject,
      { id_ads, id_user, id_user_target: id_user, shortlist: "3" },
      true
    ).then((response) => {
      if (response.code === 200) {
        id_groupmessage !== null && follow_by_owner === true && postTakeGroup();
        setData({ ...data, shortlist: "3" });
      }
    });
  };
  const postTakeGroup = () => {
    apiDispatch(
      postAddMember,
      {
        id_user: id_owner,
        id_groupmessage: id_groupmessage?.id_groupmessage,
        id_user_insert: [id_user],
      },
      true
    ).then((response) => {
      // if (response.code === 200) {
      push({
        pathname: "/chat",
        query: {
          id_message: !id_groupmessage?.id_groupmessage
            ? false
            : id_groupmessage?.id_groupmessage,
          message_type: "message",
          type: "group",
        },
      });
    });
  };

  const postCloseProject = () => {
    apiDispatch(postCloseCollaboration, { id_ads, id_user }, true).then(
      (response) => {
        if (response.code === 200) {
          push({
            pathname: "/home/collaboration",
          });
        }
      }
    );
  };

  const handleAction = (type) => {
    const action = {
      follow: {
        api: postProfileFollow,
        params: {
          id_user: id_owner,
          followed_by: id_user,
        },
      },
      unfollow: {
        api: postProfileUnfollow,
        params: {
          id_user: id_user,
          id_user_following: id_owner,
        },
      },
    };

    apiDispatch(action[type].api, action[type].params, true).then(
      (response) => {
        response.code === 200 && setUpdateCount(1 + updateCount);
      }
    );
  };

  const handleCommentSubmit = (e) => {
    apiDispatch(
      postCreateComment,
      {
        related_to: "id_ads",
        id_related_to: id_ads,
        comment: commentValue,
        id_user,
      },
      true
    ).then((response) => {
      if (response.code === 200) {
        setUpdateCount(1 + updateCount);
        setCommentValue("");
      }
    });

    e.preventDefault();
  };

  const renderModalJoinProjectOnSuccess = () => {
    return (
      <ModalOnSubmit
        backdrop={true}
        size="md"
        show={modalJoinSuccess}
        onHide={() => setModalJoinSuccess(false)}
        title="Success"
        subTitle="Your collaboration request has been sent."
        isLoading={false}
        customBody={
          <div className="d-flex flex-column gap-3">
            <Button
              className="btn-primary p-3"
              onClick={
                () => setModalJoinSuccess(false)
                // push({
                //   pathname: "/home",
                // })
              }
            >
              View my collaboration status
            </Button>
            <Button
              className="btn-cancel p-3"
              onClick={() => push("/home/collaboration")}
            >
              See more collaboration
            </Button>
          </div>
        }
      />
    );
  };
  const expandShare = () => {
    setModalShareIsOpened(modalShareIsOpened ? false : true);
  };

  const renderModalConfirmation = () => {
    return (
      <>
        <ModalConfirmation
          show={modalConfirmation}
          onHide={() => setModalConfirmation(false)}
          HasExitIcon
          customTitle={
            <>
              <h1 className="heading-lg-bold mt-2">Offer confirmation</h1>
              <p className="pt-1 text-lg-normal color-neutral-old-40">
                Are you sure join this project? After confirmation, the
                application process joins on other projects will be terminated
                and cannot be canceled
              </p>
              <div className="justify-content-center">
                <Button
                  className="btn-primary m-2"
                  onClick={() => {
                    setModalConfirmation(false);
                    postTakeJoin();
                  }}
                >
                  Yes
                </Button>
                <Button
                  className="btn-cancel m-2"
                  onClick={() => setModalConfirmation(false)}
                >
                  No
                </Button>
              </div>
            </>
          }
        />
      </>
    );
  };

  const renderModalProjectConfirmation = () => {
    return (
      <ModalConfirmation
        show={modalProjectConfirmation}
        onHide={() => setModalProjectConfirmation(false)}
        HasExitIcon
        title={
          id_owner === id_user
            ? "Are you sure to close this project?"
            : shortlist === "1" && "Offer confirmation"
        }
        subTitle={
          id_owner === id_user
            ? "After confirmation, the collaboration project status will end and cannot be canceled"
            : shortlist === "1" &&
              "Are you sure join this project? After confirmation, the application process joins on other projects will be terminated and cannot be canceled"
        }
        customBody={
          <>
            <div className="d-flex justify-content-center gap-3 mx-4">
              <Button
                className="btn-cancel"
                onClick={() => setModalProjectConfirmation(false)}
              >
                {id_owner === id_user ? "Cancel" : shortlist === "1" && "No"}
              </Button>
              <Button
                className="btn-primary"
                onClick={() => {
                  setModalProjectConfirmation(false);
                  id_owner === id_user ? postCloseProject() : shortlist === "1" && postTakeJoin();
                }}
              >
                Yes
              </Button>
            </div>
          </>
        }
      />
    );
  };

  const rightBarPostedBy = () => {
    return (
      <Container>
        <RightBarBox title="Posted By">
          <div className="x-user-content">
            <div className="d-flex flex-column align-items-center justify-content-center">
              {!isLoadingData ? (
                <Fragment>
                  {profile_picture ? (
                    <img
                      src={profile_picture}
                      className="x-image-user"
                      alt=""
                    />
                  ) : (
                    <div className="x-emptyimage">
                      <h1 className="heading-xl-normal color-base-10 m-0">
                        {created_by?.slice(0, 1).toUpperCase()}
                      </h1>
                    </div>
                  )}
                  <p className="text-center heading-sm-normal color-body-60 pt-2 m-0">
                    {created_by}
                  </p>
                  <p className="text-md-normal text-center color-grey-4 p-0 mb-3">
                    {job_title}
                  </p>
                </Fragment>
              ) : (
                <Fragment>
                  <SkeletonElement
                    width={104}
                    height={104}
                    type="rounded-circle"
                  />
                  <SkeletonElement width="100%" height={16} className="my-2" />
                  <SkeletonElement
                    width="100%"
                    height={14}
                    className="mb-3 mx-4"
                  />
                </Fragment>
              )}
            </div>
            <div className="x-btn-user">
              {/* user POV */}
              {!isLoadingData ? (
                id_owner === id_user ? (
                  <div className="d-flex flex-column gap-3">
                    <Button
                      className="btn-cancel d-flex justify-content-center align-items-center d-flex gap-2"
                      onClick={() =>
                        push({
                          pathname: "/home/collaboration/edit-collaboration",
                          query: {
                            id: id_ads,
                          },
                        })
                      }
                    >
                      <MaterialIcon action="edit" size={18} />
                      Edit Project
                    </Button>
                    <Button
                      className="d-flex justify-content-center align-items-center d-flex gap-2"
                      onClick={() => setModalProjectConfirmation(true)}
                    >
                      Close Project
                    </Button>
                  </div>
                ) : (
                  <>
                    <div className="x-btn-wrapper mb-2">
                      <Button
                        className={classnames(
                          !isFollowed ? "btn-primary" : "btn-delete"
                        )}
                        onClick={() =>
                          handleAction(!isFollowed ? "follow" : "unfollow")
                        }
                      >
                        <span
                          className="p-0 me-2 material-icons"
                          style={{
                            fontSize: "18px",
                          }}
                        >
                          person_add
                        </span>
                        {!isFollowed ? "Follow" : "Following"}
                      </Button>
                    </div>
                    <Button
                      className="btn-delete"
                      onClick={() => {
                        dispatch(
                          setChatData({
                            selectedMessage: {
                              id_user: id_owner,
                              full_name: created_by,
                              image: image,
                              id_groupmessage: id_groupmessage?.id_groupmessage,
                              type: "personal",
                            },
                          })
                        );
                        push({
                          pathname: "/chat",
                          query: {
                            id_message: !id_groupmessage?.id_groupmessage
                              ? false
                              : id_groupmessage?.id_groupmessage,
                            message_type: "message",
                            type: "personal",
                          },
                        });
                      }}
                    >
                      Message
                    </Button>
                  </>
                )
              ) : (
                <SkeletonElement
                  width={96}
                  height={38}
                  className="my-2"
                  count={2}
                />
              )}
            </div>
          </div>
        </RightBarBox>
      </Container>
    );
  };

  return (
    <Layout title="Detail Collaboration Project - Indigo">
      <div className="x-detailcollaborationproject-page">
        <MainLayout
          size="sm"
          Breadcrumb
          rightBarContent={rightBarPostedBy()}
          rightBarClassName="x-rightbar-postedby"
          StickyRightBar
          CenteredRightBar
        >
          {/* info joined */}
          {(shortlist === "3" || shortlist === "1") && (
            <div
              className={classnames(
                "x-information-wrapper",
                shortlist === "3"
                  ? "x-information-progress"
                  : shortlist === "1" && "x-information-success"
              )}
            >
              <span className="material-icons">
                {shortlist === "3" ? "info" : shortlist === "1" && "how_to_reg"}
              </span>
              <div className="d-flex flex-column">
                <p
                  className={classnames(
                    "heading-md-normal p-0 m-0",
                    shortlist === "3"
                      ? "color-primary-pressed"
                      : shortlist === "1" && "color-semantic-success-pressed"
                  )}
                >
                  {shortlist === "3"
                    ? "Currently you are joining this project"
                    : shortlist === "1" &&
                      "Congrats! You are accepted for this project!"}
                </p>
                {shortlist === "1" && (
                  <p className="heading-sm-normal p-0 m-0 color-semantic-success-pressed">
                    You can take this offer and join project right now
                  </p>
                )}
              </div>
            </div>
          )}

          <div className="x-detail-wrapper">
            {/* normal case */}
            {!isLoadingData ? (
              <Fragment>
                <div className="x-thumbnail">
                  <img src={image} alt="" />
                  <div className="x-icon-eventeer" />
                </div>
                <div className="d-flex justify-content-between">
                  <div className="d-flex flex-column">
                    <p className="m-0 mt-3 heading-md-bolder color-neutral-80">
                      {title}
                    </p>
                    <div className="d-flex mt-4 mb-3">
                      <p className="m-0 text-md-normal line-height-sm color-neutral-70">
                        {city_name}
                      </p>
                      <span
                        className="material-icons color-neutral-old-40 ps-1 pe-1 mt-1"
                        style={{
                          fontSize: "6px",
                        }}
                      >
                        fiber_manual_record
                      </span>

                      <p className="m-0 text-md-normal line-height-sm color-neutral-70">
                        {time_ago}
                      </p>
                    </div>
                  </div>

                  {shortlist === null && id_owner !== id_user && (
                    <div className="d-flex align-items-center">
                      <Button
                        className="d-flex justify-content-center align-items-center"
                        onClick={postJoinCollab}
                      >
                        <span className="px-3">Join</span>
                      </Button>
                    </div>
                  )}
                </div>
              </Fragment>
            ) : (
              <Fragment>
                <SkeletonElement width="100%" height={350} className="mb-3" />
                <SkeletonElement width="40%" height={24} className="mt-3" />
                <SkeletonElement
                  width="25%"
                  height={14}
                  className="mt-4 mb-3"
                />
              </Fragment>
            )}

            {shortlist === "1" && (
              <div className="d-flex justify-content-center">
                <Button
                  className="w-50"
                  onClick={() => setModalProjectConfirmation(true)}
                >
                  Take and Join Project
                </Button>
              </div>
            )}

            <hr />

            <div className="x-content-info">
              {/* project status */}
              {(isLoadingData || data.status) && (
                <Fragment>
                  <p className="m-0 mt-3 heading-sm-normal color-neutral-80">
                    Project Status :
                  </p>
                  <div className="d-flex gap-3 ">
                    {!isLoadingData ? (
                      <p className="x-badge x-badge-status m-0">
                        {data.status}
                      </p>
                    ) : (
                      <SkeletonElement width={75} height={24} />
                    )}
                  </div>
                </Fragment>
              )}

              {/* searched position */}
              {(isLoadingData || !isEmpty(detail?.profession)) && (
                <Fragment>
                  <p className="m-0 mt-3 heading-sm-normal color-neutral-80">
                    Searched Position :
                  </p>
                  <div className="d-flex gap-3 ">
                    {!isLoadingData ? (
                      detail.profession.map((item, idx) => (
                        <p className="x-badge x-badge-interest" key={idx}>
                          {item.name}
                        </p>
                      ))
                    ) : (
                      <SkeletonElement width={85} height={24} count={3} />
                    )}
                  </div>
                </Fragment>
              )}

              {/* member quota */}
              {(isLoadingData || !isEmpty(detail?.memberQuota)) && (
                <Fragment>
                  <p className="m-0 mt-3 heading-sm-normal color-neutral-80">
                    Member Kuota :
                  </p>
                  {!isLoadingData ? (
                    <p className="text-lg-light color-neutral-70">
                      {detail.memberQuota}
                    </p>
                  ) : (
                    <SkeletonElement width={45} height={18} />
                  )}
                </Fragment>
              )}

              {/* date start and end */}
              {(isLoadingData ||
                (!isEmpty(detail?.date?.start) &&
                  !isEmpty(detail?.date?.end))) && (
                <Fragment>
                  <p className="m-0 mt-3 heading-sm-normal color-neutral-80">
                    Duration Project :
                  </p>
                  {!isLoadingData ? (
                    <p className="text-lg-light color-neutral-70">
                      {dateFormatter(detail?.date?.start)}
                      {" - "}
                      {dateFormatter(detail?.date?.end)}
                    </p>
                  ) : (
                    <SkeletonElement width={200} height={16} />
                  )}
                </Fragment>
              )}

              {/* notes */}
              {(isLoadingData || !isEmpty(detail?.specification)) && (
                <Fragment>
                  <p className="m-0 mt-3 heading-sm-normal color-neutral-80">
                    Notes :
                  </p>
                  {!isLoadingData ? (
                    <p className="text-lg-light color-neutral-70">
                      {detail.specification}
                    </p>
                  ) : (
                    <SkeletonElement
                      width="100%"
                      height={16}
                      count={2}
                      className="mb-2"
                    />
                  )}
                </Fragment>
              )}

              <hr />

              {/* about */}
              {(isLoadingData || !isEmpty(description)) && (
                <Fragment>
                  <p className="m-0 mt-3 heading-sm-normal color-neutral-80">
                    About collaboration :
                  </p>
                  {!isLoadingData ? (
                    <p className="text-lg-light color-neutral-80">
                      {description}
                    </p>
                  ) : (
                    <SkeletonElement
                      width="100%"
                      height={16}
                      count={2}
                      className="mb-2"
                    />
                  )}
                </Fragment>
              )}

              {/* collaboration reference */}
              <Fragment>
                <p className="m-0 mt-3 heading-sm-normal color-neutral-80">
                  Collaboration Reference:
                </p>
                {!isLoadingData ? (
                  <p className="text-lg-light m-0 color-neutral-80">
                    {!detail?.collaboration_reference
                      ? "Data tidak ada"
                      : detail?.collaboration_reference}
                  </p>
                ) : (
                  <SkeletonElement width={200} height={16} />
                )}
              </Fragment>

              <hr />

              {/* status join */}
              <Fragment>
                <p className="m-0 mt-3 heading-sm-normal color-neutral-80">
                  Already Joined:
                </p>
                {isLoadingData || joined ? (
                  dynamicJoined?.map((item, idx) => (
                    <div
                      className="x-member d-flex align-items-center"
                      key={idx}
                    >
                      {!isLoadingData && item.profile_picture ? (
                        <img src={item.profile_picture} alt="profile" />
                      ) : !isLoadingData ? (
                        <div className="x-empty-image">
                          <p className="text-xl-normal color-base-10 m-0">
                            {item.full_name.slice(0, 1).toUpperCase()}
                          </p>
                        </div>
                      ) : (
                        <SkeletonElement
                          width={48}
                          height={48}
                          type="rounded-circle"
                        />
                      )}

                      <div className="x-desc ms-2">
                        {!isLoadingData ? (
                          <Fragment>
                            <p className="m-0 text-lg-normal color-body-60">
                              {item.full_name}
                            </p>
                            <div className="d-flex">
                              <p className="m-0 text-sm-light line-height-sm color-grey-4">
                                {item.job_title}
                              </p>
                              <span
                                className="ps-1 pe-1 mt-1 material-icons color-neutral-old-40"
                                style={{
                                  fontSize: "6px",
                                }}
                              >
                                fiber_manual_record
                              </span>

                              <p className="m-0 text-sm-light line-height-sm color-grey-4">
                                {item.city_name}
                              </p>
                            </div>
                          </Fragment>
                        ) : (
                          <Fragment>
                            <SkeletonElement width={85} height={16} />
                            <SkeletonElement
                              width={185}
                              height={12}
                              className="my-2"
                            />
                          </Fragment>
                        )}
                      </div>
                    </div>
                  ))
                ) : (
                  <p className="text-lg-light m-0 color-neutral-70">
                    belum ada member yang tergabung
                  </p>
                )}
              </Fragment>

              <hr />

              <div className="d-flex justify-content-between">
                {!isLoadingData ? (
                  <p className="text-lg-normal m-0 pb-2 color-neutral-80">
                    Comments ({total_comment})
                  </p>
                ) : (
                  <SkeletonElement width={105} height={18} />
                )}
                <div
                  className="d-flex align-items-center cursor-pointer gap-2"
                  onClick={() => {
                    expandShare();
                    setDataShare(data);
                  }}
                >
                  <MaterialIcon action="share" size={18} color="grey-4" />
                  <p className="text-md-normal color-neutral-60 m-0">Share</p>
                </div>
              </div>
              <CommentPost
                value={commentValue}
                onChangeText={(e) => setCommentValue(e.target.value)}
                placeholder="Leave a comment"
                onSubmit={handleCommentSubmit}
              />

              <hr />

              <div>
                {comments === "" ? (
                  <p className="text-lg-normal m-0 color-neutral-70">
                    tidak ada komen
                  </p>
                ) : (
                  dynamicComments?.map((comment, idx) => (
                    <Fragment key={idx}>
                      <CommentCard
                        isLoading={isLoadingData}
                        data={comment}
                        postApiLike={{
                          api: postFeedCommentLike,
                          params: {
                            related_to: "id_comment",
                            id_related_to: comment.id_comment,
                            id_user,
                          },
                        }}
                        postApiSubmit={{
                          api: postCreateComment,
                          params: {
                            related_to: "id_comment",
                            id_related_to: comment.id_comment,
                            id_user,
                          },
                        }}
                        onCompleteSubmit={() => setUpdateCount(1 + updateCount)}
                      />
                    </Fragment>
                  ))
                )}
              </div>
            </div>
          </div>
        </MainLayout>

        {renderModalJoinProjectOnSuccess()}
        {renderModalConfirmation()}
        {renderModalProjectConfirmation()}
        <ModalShare
          show={modalShareIsOpened}
          onHide={() => setModalShareIsOpened(false)}
          dataShare={dataShare}
        />
      </div>
    </Layout>
  );
};

export default appRoute(DetailCollaborationProject);
