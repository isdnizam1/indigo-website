import React from "react";
import appRoute from "/components/routes/appRoute";
import Profile from "/pages/profile";

const ProfileDetail = () => {
  return <Profile type="my-network" />
};

export default appRoute(ProfileDetail);
