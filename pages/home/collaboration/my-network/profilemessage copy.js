import {
  getProfileDetailV2,
  getProfileFollowStatus,
  postProfileFollow,
  postProfileUnfollow,
} from "/services/actions/api";
import { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import { getAuth, apiDispatch } from "/services/utils/helper";
import BreadcrumbLv1 from "/components/header/BreadcrumbLv1";
import MainLayout from "/components/layout/MainLayout";
import defaultCover from "/public/assets/images/img-default-cover-profile.png";
import { Button } from "react-bootstrap";
import { useRouter } from "next/router";
import { isEmpty } from "lodash-es";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";
import EmptyOverview from "/components/empty_state/EmptyOverview";

const ProfileMessage = (props) => {
  const { id_user } = getAuth();

  const router = useRouter();

  const { id: id_related_user } = router.query;

  const [profileDetail, setProfileDetail] = useState({
    about_me: "",
    cover_image: "",
    full_name: "",
    id_city: null,
    interest: [],
    job_title: "",
    location: "",
    profile_picture: "",
    total_followers: 0,
    total_following: 0,
    isMyProfile: true,
  });
  const [isOverviewTab, setIsOverviewTab] = useState(true);
  const [isFollowed, setIsFollowed] = useState(false);

  useEffect(() => {
    if (router.isReady) {
      getProfileDetailV2({
        id_user: id_related_user ? id_related_user : id_user,
      }).then((res) => {
        if (isEmpty(res.data.result)) return router.push("/home");

        setProfileDetail({
          ...res.data.result,
          isMyProfile: id_related_user ? false : true,
        });

        getProfileFollowStatus({
          id_user: id_related_user ? id_related_user : id_user,
          followed_by: id_user,
        }).then((res) => {
          console.log(res)
          res.data.result.status === "followed"
            ? setIsFollowed(true)
            : setIsFollowed(false);
          console.log("status follow:", res.data.result.status)
        });
      });
    }
  }, [router.query]);
  console.log({isFollowed})

  const handleFollow = () => {
    Promise.resolve(
      postProfileFollow({ id_user: id_related_user, followed_by: id_user })
    )
      .then((res) => console.log(res))
      .then(() => setIsFollowed(true));
  };

  const handleUnfollow = () => {
    Promise.resolve(
      postProfileUnfollow({
        id_user: id_user,
        id_user_following: id_related_user,
      })
    )
      .then((res) => console.log(res))
      .then(() => setIsFollowed(false));
  };

  const {
    about_me,
    cover_image,
    full_name,
    id_city,
    interest,
    job_title,
    location,
    profile_picture,
    total_followers,
    total_following,
  } = profileDetail;

  return (
    <Layout title="Profile User - Indigo">
      <MainLayout>
        <div className="x-profilemessage">
          <Container>
            <div className="x-profilemessage-page">
              <div className="x-breadscrumb-wrapper">
                <BreadcrumbLv1 />
              </div>
              <div className="x-cover-img">
                <img src={defaultCover.src} alt="" />
              </div>
              <div className="x-profile-top">
                <div className="x-profile-photo">
                  {profile_picture ? (
                    <img src={profile_picture} alt="profile" />
                  ) : (
                    <div className="empty-image">
                      <p style={{ color: "var(--base-10)" }}>
                        {full_name.slice(0, 1).toUpperCase()}
                      </p>
                    </div>
                  )}
                </div>
                <div className="x-profile-top-detail">
                  <div className="x-detail-name">
                    <h3>{full_name}</h3>
                    <p>{job_title}</p>
                  </div>
                  <div className="x-detail-follow">
                    <span>
                      <strong>{total_followers}</strong> Followers
                    </span>
                    <span>
                      <strong>{total_following}</strong> Following
                    </span>
                    <div className="x-edit-profile-button">
                      {profileDetail.isMyProfile ? (
                        <Button
                          onClick={() => router.push(`/profile/edit`)}
                          className="btn-cancel"
                        >
                          Edit Profile
                        </Button>
                      ) : isFollowed ? (
                        <div className="d-flex align-items-center">
                          <div className="x-button-message d-flex align-items-center">
                            <span
                              className="material-icons ps-1 pe-1 ms-2"
                              style={{
                                fontSize: "18px",
                                color: "var(--neutral-60)",
                              }}
                            >
                              message
                            </span>
                            <p
                              className="text-md-light m-0"
                              style={{ color: "var(--neutral-60)" }}
                            >
                              Message
                            </p>
                          </div>
                          <Button onClick={handleUnfollow} className="btn-cancel">
                            Following
                          </Button>
                        </div>
                      ) : (
                        <div className="d-flex align-items-center">
                          <div className="x-button-message d-flex align-items-center">
                            <span
                              className="material-icons ps-1 pe-1 pt-1"
                              style={{
                                fontSize: "18px",
                                color: "var(--neutral-60)",
                              }}
                            >
                              message
                            </span>
                            <p
                              className="text-md-light m-0"
                              style={{ color: "var(--neutral-60)" }}
                            >
                              Message
                            </p>
                          </div>
                          <Button onClick={handleFollow}>Follow</Button>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
              <hr />
              <div className="x-profile-bottom">
                <div className="x-badges">
                  {interest.map(({ interest_name }, i) => (
                    <span key={i}>{interest_name}</span>
                  ))}
                </div>
                <p>{about_me}</p>
                <div className="x-loc-warp">
                  <div className="x-icon-loc" />
                  <span>
                    {location.city_name}, {location.country_name}
                  </span>
                </div>
              </div>
              <div className="x-profile-overview">
                <div className="x-tags">
                  <span
                    className={isOverviewTab && `active`}
                    onClick={() => setIsOverviewTab(true)}
                  >
                    overview
                  </span>
                  <span
                    className={!isOverviewTab && `active`}
                    onClick={() => setIsOverviewTab(false)}
                  >
                    activities
                  </span>
                </div>
                <hr />
                <div className="x-details-container">
                  {isOverviewTab ? (
                    <EmptyOverview
                      head="Portfolio"
                      title="Upload Your Portfolio"
                      subtitle="Share your portfolio and connect with new person!"
                      icon={<div className="x-icon-portfolio" />}
                      buttonText="Add Portfolio"
                    />
                  ) : (
                    <EmptyOverview
                      head="Activities"
                      title="There are no posts yet"
                      subtitle="All posts will appear on this page"
                      icon={<div className="x-icon-activity" />}
                      buttonText="Make Posts"
                    />
                  )}
                </div>
              </div>
            </div>
          </Container>
        </div>
      </MainLayout>
    </Layout>
  );
};

export default appRoute(ProfileMessage);
