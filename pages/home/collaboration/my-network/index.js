import React, { Fragment, useState, useEffect } from "react";
import { Button, Form, Row, Col } from "react-bootstrap";
import classnames from "classnames";
import {
  getAuth,
  apiDispatch,
  capitalizeFirstLetter,
} from "/services/utils/helper";
import InputText from "/components/input/InputText";
import MainLayout from "/components/layout/MainLayout";
import { isEmpty } from "lodash-es";
import ContentBox from "/components/home/ContentBox";
import { useRouter } from "next/router";
import {
  getListCollaborator,
  postProfileFollow,
  postProfileUnfollow,
  getJob,
  getCity,
  getTopFilter,
} from "/services/actions/api";
import ModalConfirmation from "/components/modal/ModalConfirmation";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";
import CardUser from "/components/home/CardUser";
import MaterialIcon from "components/utils/MaterialIcon";
import EmptyStateLv1 from "/components/empty_state/EmptyStateLv1";
import EmptyIcon from "/public/assets/icons/icon-empty-collaborationproject.svg";
import SkeletonElement from "/components/skeleton/SkeletonElement";

const MyNetwork = () => {
  const [collaborators, setCollaborators] = useState([]);
  const [listCollaborators, setListCollaborators] = useState([]);
  const [isLoadingCollaborators, setIsLoadingCollaborators] = useState(true);
  let dynamicListCollaborators = isLoadingCollaborators ? [1, 2, 3, 4, 5, 6, 7, 8] : listCollaborators;

  const [professions, setProfessions] = useState([]);
  const [listProfessions, setListProfessions] = useState([]);
  const [topProfessions, setTopProfessions] = useState([]);

  const [cities, setCities] = useState([]);
  const [listCities, setListCities] = useState([]);
  const [topCities, setTopCities] = useState([]);

  const [modalFilter, setModalFilter] = useState(false);

  const [filteredCities, setFilteredCities] = useState([]);
  const [filteredProfessions, setFilteredProfessions] = useState([]);

  const [tempFilteredCities, setTempFilteredCities] = useState([]);
  const [tempFilteredprofessions, setTempFilteredProfessions] = useState([]);

  const [filteredCitiesById, setFilteredCitiesById] = useState([]);
  const [filteredProfessionsById, setFilteredProfessionsById] = useState([]);

  const [filterShown, setFilterShown] = useState("");

  const router = useRouter();
  const { id_user } = getAuth();

  useEffect(() => getData(), []);

  const getData = (
    proffesion = filteredProfessionsById,
    city_name = filteredCitiesById
  ) => {
    apiDispatch(
      getListCollaborator,
      {
        proffesion,
        city_name,
        id_user,
      },
      true
    ).then((response) => {
      setIsLoadingCollaborators(false);
      if (response.code === 200) {
        const { result } = response;

        isEmpty(collaborators) && setCollaborators(result);
        setListCollaborators(result);
      } else setListCollaborators([]);
    });
  };

  const getCities = () => {
    apiDispatch(getCity, {}, true).then((response) => {
      if (response.code === 200) {
        setCities(response.result);
        setListCities(response.result);
      }
    });
  };

  const getTopCities = () => {
    apiDispatch(getTopFilter, { filter: "city" }, true).then((response) => {
      response.code === 200 && setTopCities(response.result);
    });
  };

  const getTopProfessions = () => {
    apiDispatch(getTopFilter, { filter: "profession" }, true).then(
      (response) => {
        response.code === 200 && setTopProfessions(response.result);
      }
    );
  };

  const getProfessions = () => {
    apiDispatch(getJob, {}, true).then((response) => {
      if (response.code === 200) {
        setProfessions(response.result);
        setListProfessions(response.result);
      }
    });
  };

  const onFilterSubmit = () => {
    let filteredCitiesById = [];
    let filteredProfessionsById = [];

    filteredCities.forEach((item) => filteredCitiesById.push(item.city_name));
    filteredProfessions.forEach((item) =>
      filteredProfessionsById.push(item.job_title)
    );

    setFilteredCitiesById(filteredCitiesById);
    setFilteredProfessionsById(filteredProfessionsById);

    getData(filteredProfessionsById, filteredCitiesById);
  };

  const onFollowUnfollowAction = (item, type) => {
    const action = {
      follow: {
        api: postProfileFollow,
        params: {
          id_user: item.id_user,
          followed_by: id_user,
        },
      },
      unfollow: {
        api: postProfileUnfollow,
        params: {
          id_user: id_user,
          id_user_following: item.id_user,
        },
      },
    };

    apiDispatch(action[type].api, action[type].params, true).then(
      (response) => {
        if (response.code === 200) {
          getData();
        }
      }
    );
  };

  const onCheckedCity = (e) => {
    const city = JSON.parse(e.target.value);
    const isChecked = e.target.checked;
    const selected = isChecked
      ? [...filteredCities, city]
      : filteredCities.filter((person) => person.id_city !== city.id_city);
    setFilteredCities(selected);
  };

  const onCheckedProfession = (e) => {
    const profession = JSON.parse(e.target.value);
    const isChecked = e.target.checked;
    const selected = isChecked
      ? [...filteredProfessions, profession]
      : filteredProfessions.filter(
          (person) => person.id_job !== profession.id_job
        );
    setFilteredProfessions(selected);
  };

  const searchByKeyword = (event) => {
    const keyword = event.target.value.toLowerCase();
    const filteredCity = cities.filter((item) =>
      item.city_name.toLowerCase().includes(keyword)
    );
    setListCities(filteredCity);
    const filteredProf = professions.filter((item) =>
      item.job_title.toLowerCase().includes(keyword)
    );
    setListProfessions(filteredProf);
  };

  const renderModalFilter = () => {
    return (
      <ModalConfirmation
        modalClass="x-modalcollaborator-filter"
        backdrop={"static"}
        show={modalFilter}
        size={"lg"}
        onHide={() => {
          setModalFilter(false);
          setFilteredCities(tempFilteredCities);
          setFilteredProfessions(tempFilteredprofessions);
        }}
        customTitle={
          <div className="x-modal w-100">
            <div className="d-flex align-items-center justify-content-between">
              <h1 className="heading-md-bold line-height-lg color-neutral-80 m-0">
                {!isEmpty(filterShown)
                  ? capitalizeFirstLetter(filterShown)
                  : "Filter"}
              </h1>

              {!isEmpty(filterShown) && (
                <InputText
                  disableError={true}
                  placeholder={`Find ${capitalizeFirstLetter(filterShown)}`}
                  inlineIcon={"search"}
                  className="x-inputsearch"
                  onChangeText={(event) => searchByKeyword(event)}
                />
              )}

              <MaterialIcon
                action="clear"
                weight={600}
                className="cursor-pointer user-select-none"
                size={28}
                color="primary-main"
                onClick={() => {
                  setModalFilter(false);
                  setFilteredCities(tempFilteredCities);
                  setFilteredProfessions(tempFilteredprofessions);
                }}
              />
            </div>
          </div>
        }
        customBody={
          <Fragment>
            {isEmpty(filterShown) ? (
              <Fragment>
                <div className="d-flex align-items-center">
                  <h1 className="heading-sm-normal m-0 color-neutral-70">
                    Profession
                  </h1>
                  <h1
                    className="heading-sm-normal m-0 ms-auto cursor-pointer color-neutral-old-80"
                    onClick={() => {
                      isEmpty(professions) && getProfessions();
                      isEmpty(topProfessions) && getTopProfessions();

                      setFilterShown("profession");
                    }}
                  >
                    See All
                  </h1>
                </div>
                {!isEmpty(filteredProfessionsById) && (
                  <div className="x-profession-tags">
                    {filteredProfessions.map((item, idx) => {
                      return (
                        <p className="x-selected-profession" key={idx}>
                          {item.job_title}
                        </p>
                      );
                    })}
                  </div>
                )}
              </Fragment>
            ) : (
              <div className="x-list-city">
                <Row className="m-0 color-neutral-80">
                  <p
                    className="m-0 text-md-normal m-0 pb-2"
                    style={{ paddingLeft: "0.4rem" }}
                  >
                    {`Popular ${capitalizeFirstLetter(filterShown)}`}
                  </p>
                  {(filterShown === "profession"
                    ? topProfessions
                    : topCities
                  ).map((item, idx) => (
                    <Col md={4} className="align-items-center" key={idx}>
                      <Form.Check
                        onChange={
                          filterShown === "profession"
                            ? onCheckedProfession
                            : onCheckedCity
                        }
                        name={`filterCheckbox${idx}`}
                        type={"checkbox"}
                        label={
                          filterShown === "profession"
                            ? item.job_title
                            : item.city_name
                        }
                        value={JSON.stringify(item)}
                        checked={(filterShown === "profession"
                          ? filteredProfessions
                          : filteredCities
                        ).some((user) => {
                          if (filterShown === "profession")
                            return user.id_job === item.id_job ? true : false;
                          else
                            return user.id_city === item.id_city ? true : false;
                        })}
                      />
                    </Col>
                  ))}
                  {(filterShown === "profession"
                    ? listProfessions
                    : listCities
                  ).map((item, idx) => (
                    <Fragment key={idx}>
                      {filterShown === "profession"
                        ? listProfessions[-1 + idx]?.job_title.slice(0, 1) !==
                            listProfessions[idx]?.job_title.slice(0, 1) && (
                            <p
                              className="text-md-normal m-0 pb-2 mt-3"
                              style={{ paddingLeft: "0.4rem" }}
                            >
                              {listProfessions[idx].job_title.slice(0, 1)}
                            </p>
                          )
                        : listCities[-1 + idx]?.city_name.slice(0, 1) !==
                            listCities[idx]?.city_name.slice(0, 1) && (
                            <p
                              className="text-md-normal m-0 pb-2 mt-3"
                              style={{ paddingLeft: "0.4rem" }}
                            >
                              {listCities[idx].city_name.slice(0, 1)}
                            </p>
                          )}

                      <Col md={4} className="align-items-center" key={idx}>
                        <Form.Check
                          name="filter-collaborator"
                          type="checkbox"
                          onChange={
                            filterShown === "profession"
                              ? onCheckedProfession
                              : onCheckedCity
                          }
                          checked={(filterShown === "profession"
                            ? filteredProfessions
                            : filteredCities
                          ).some((user) => {
                            if (filterShown === "profession")
                              return user.id_job === item.id_job ? true : false;
                            else
                              return user.id_city === item.id_city
                                ? true
                                : false;
                          })}
                          label={
                            filterShown === "profession"
                              ? item.job_title
                              : item.city_name
                          }
                          value={JSON.stringify(item)}
                        />
                      </Col>
                    </Fragment>
                  ))}
                </Row>
              </div>
            )}
          </Fragment>
        }
        customFooter={
          <div className="d-flex x-footer2 align-items-center">
            <div className="ms-auto">
              <div className="d-flex align-items-center">
                <Button
                  className="btn-cancel m-2 mb-0"
                  onClick={() => {
                    if (filterShown === "profession" || isEmpty(filterShown)) {
                      setFilteredProfessions([]);
                      setFilteredProfessionsById([]);
                    } else {
                      setFilteredCities([]);
                      setFilteredCitiesById([]);
                    }
                  }}
                >
                  Reset
                </Button>
                <Button
                  className="btn-primary m-2 mb-0"
                  onClick={() => {
                    setModalFilter(false);
                    onFilterSubmit();
                  }}
                >
                  Apply
                </Button>
              </div>
            </div>
          </div>
        }
      />
    );
  };

  return (
    <Layout title="My Network - Indigo">
      <div className="x-mynetwork-page">
        <MainLayout size="md" Breadcrumb>
          <div className="x-mynetwork">
            <h3 className="heading-md-bold mb-1">Explore Oppurtunities</h3>
            <p className="text-lg-light mb-2 color-grey-3">
              Expand your network based on interest
            </p>
            <div className="d-flex mt-2">
              <span
                className="material-icons-round color-neutral-old-80"
                style={{ fontSize: "20px" }}
              >
                location_on
              </span>
              <div className="d-flex gap-1">
                <span className="m-0 ms-2 text-lg-light line-height-sm color-neutral-70">
                  {"Collaboration at :"}
                </span>
                {!isLoadingCollaborators ? <span className="text-lg-normal m-0 line-height-md color-neutral-old-80 pe-3">
                  {!isEmpty(filteredCities)
                    ? filteredCities.map((item, idx) => {
                        const separator =
                          idx !== filteredCities.length - 1 ? ", " : ""; // here
                        return item.city_name + separator;
                      })
                    : "All City"}
                </span>:<SkeletonElement width={64} height={16} className="" />}
              </div>
              <div className="ms-auto">
                <p
                  className="m-0 cursor-pointer heading-sm-normal color-neutral-old-80"
                  onClick={() => {
                    isEmpty(cities) && getCities();
                    isEmpty(topCities) && getTopCities();

                    setTempFilteredCities(filteredCities);
                    setTempFilteredProfessions(filteredProfessions);

                    setFilterShown("city");
                    setModalFilter(true);
                  }}
                >
                  Edit
                </p>
              </div>
            </div>
            {!isLoadingCollaborators ? (<Button
              className={classnames(
                "w-auto d-flex justify-content-center align-items-center py-1 px-2 my-3",
                isEmpty(filteredProfessionsById) ? "btn-disable" : "btn-cancel"
              )}
              onClick={() => {
                setTempFilteredCities(filteredCities);
                setTempFilteredProfessions(filteredProfessions);

                setFilterShown("");
                setModalFilter(true);
              }}
            >
              <MaterialIcon
                action="tune"
                size={14}
                weight={600}
                color={
                  isEmpty(filteredProfessionsById)
                    ? "neutral-40"
                    : "primary-main"
                }
              />

              <p
                className={classnames(
                  "text-sm-normal line-height-sm m-0 ps-1",
                  isEmpty(filteredProfessionsById)
                    ? "color-neutral-70"
                    : "color-neutral-old-80"
                )}
              >
                Filter
              </p>

              {!isEmpty(filteredProfessions) && (
                <p
                  className={classnames(
                    "text-sm-normal line-height-sm m-0 ps-1",
                    isEmpty(filteredProfessionsById)
                      ? "color-neutral-70"
                      : "color-neutral-old-80"
                  )}
                >
                  ({filteredProfessions.length})
                </p>
              )}
            </Button>) : <SkeletonElement width={64} height={24} className="my-2" />}

            <ContentBox
              title="Top Collaborator"
              NoDivider
              customHeaderContent={
                <Fragment>
                  {isLoadingCollaborators || !isEmpty(listCollaborators) ? (
                    <Row className="x-top-collaborator">
                      {dynamicListCollaborators?.map(
                        (item, idx) =>
                          idx < 6 && (
                            <Col
                              lg={2}
                              key={idx}
                              className="cursor-pointer"
                              onClick={() =>
                                router.push({
                                  pathname: `/home/collaboration/my-network/profile`,
                                  query: { id: item.id_user },
                                })
                              }
                            >
                              <div className="x-img-profile">
                                {!isEmpty(item.profile_picture && !isLoadingCollaborators) ? (
                                  <img
                                    src={item.profile_picture}
                                    alt=""
                                    className="x-image-account"
                                  />
                                ) : ( !isLoadingCollaborators ?
                                  <div className="x-empty-image">
                                    <h1 className="heading-lg-light m-0 color-base-10">
                                      {item.full_name.slice(0, 1).toUpperCase()}
                                    </h1>
                                  </div> : <SkeletonElement width={82} height={82} type="rounded-circle"/>
                                )}
                              </div>
                              {!isLoadingCollaborators ? <p className="text-md-normal text-center color-body-60">
                                {item.full_name.length > 12
                                  ? `${item.full_name.slice(0, 12)}...`
                                  : item.full_name}
                              </p> : <SkeletonElement width="100%" height={14} className="my-2" />}
                            </Col>
                          )
                      )}
                    </Row>
                  ) : (
                    <div className="d-flex justify-content-center pt-5 pb-5">
                      <div className="w-50 pt-5 pb-5">
                        <EmptyStateLv1
                          customIcon={EmptyIcon}
                          title="No collaborators found"
                          subTitle="Start collaborating with other participants right now!"
                        />
                      </div>
                    </div>
                  )}
                </Fragment>
              }
            >
              {(isLoadingCollaborators || listCollaborators.length > 5) && (
                <Fragment>
                  <hr />
                  <div className="mt-4">
                    {dynamicListCollaborators?.map(
                      (item, idx) =>
                        idx > 5 && (
                          <div className="x-cardmember-wrapper" key={idx}>
                            <div
                              className="cursor-pointer w-75"
                              onClick={() =>
                                router.push({
                                  pathname: `/home/collaboration/my-network/profile`,
                                  query: { id: item.id_user },
                                })
                              }
                            >
                              <CardUser
                                image={item.profile_picture}
                                name={item.full_name}
                                jobTitle={item.job_title}
                                city={item.city_name}
                                isLoading={isLoadingCollaborators}
                              />
                            </div>

                            <div className="p-0 d-flex align-items-center">
                              <Button
                                className={classnames(
                                  "w-auto",
                                  item.followed_by === false
                                    ? "btn-primary"
                                    : "btn-delete"
                                )}
                                onClick={() =>
                                  onFollowUnfollowAction(
                                    item,
                                    item.followed_by === false
                                      ? "follow"
                                      : "unfollow"
                                  )
                                }
                              >
                                {item.followed_by === false
                                  ? "Follow"
                                  : "Followed"}
                              </Button>
                            </div>
                          </div>
                        )
                    )}
                  </div>
                </Fragment>
              )}
            </ContentBox>
          </div>
          {renderModalFilter()}
        </MainLayout>
      </div>
    </Layout>
  );
};

export default appRoute(MyNetwork);


/*
import React, { Fragment, useState, useEffect } from "react";
import { Container, Button, Form, Row, Col } from "react-bootstrap";
import classnames from "classnames";
import { getAuth, apiDispatch } from "/services/utils/helper";
import InputText from "/components/input/InputText";
import MainLayout from "/components/layout/MainLayout";
import { isEmpty } from "lodash-es";
import BreadcrumbLv1 from "/components/header/BreadcrumbLv1";
import ContentBox from "/components/home/ContentBox";
import { useRouter } from "next/router";
import { setCollabsData } from "/redux/slices/collabsSlice";
import { useDispatch, useSelector } from "react-redux";
import {
  getListCollaborator,
  postProfileFollow,
  postProfileUnfollow,
  getJob,
  getCity,
  getTopFilter,
} from "/services/actions/api";
import ModalConfirmation from "/components/modal/ModalConfirmation";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";
import CardUser from "/components/home/CardUser";

const MyNetwork = () => {
  const [listCollab1, setListCollab1] = useState([]);
  const [listCollab2, setListCollab2] = useState([]);
  const [collaborationDatas, setCollaborationDatas] = useState([]);
  const [allJobs, setAllJobs] = useState([]);
  const [topJobs, setTopJobs] = useState([]);
  const [newAllJobs, setNewAllJobs] = useState([]);
  const [allCity, setAllCity] = useState([]);
  const [topCity, setTopCity] = useState([]);
  const [newAllCity, setNewAllCity] = useState([]);
  const [isFollowed, setIsFollowed] = useState(false);
  const [setFilterCityChecked] = useState([]);
  const [showModalCity, setShowModalCity] = useState(false);
  const [showModalTopProf, setShowModalTopProf] = useState(false);
  const [showModalProf, setShowModalProf] = useState(false);
  const [getApiCount, setGetApiCount] = useState(0);
  const [filterCityById, setFilterCityById] = useState([]);
  const [filterProfById, setFilterProfById] = useState([]);
  const router = useRouter();
  const { id_user } = getAuth();
  const collabsData = useSelector((state) => state.collabsReducer.data);
  const { filterCity, filterProf } = collabsData;
  const dispatch = useDispatch();

  useEffect(() => {
    getListCollab("", "");
  }, [isFollowed, getApiCount]);

  useEffect(() => {
    apiDispatch(getTopFilter, { filter: "profession", limit: "10" }, true).then(
      (response) => {
        if (response.code === 200) {
          const result = response.result;
          setTopJobs(result);
        }
      }
    );
    apiDispatch(getTopFilter, { filter: "city", limit: "10" }, true).then(
      (response) => {
        if (response.code === 200) {
          const result = response.result;
          setTopCity(result);
        }
      }
    );
    apiDispatch(getJob, {}, true).then((response) => {
      if (response.code === 200) {
        const result = response.result;
        setAllJobs(result);
        setNewAllJobs(result);
      }
    });
    apiDispatch(getCity, {}, true).then((response) => {
      if (response.code === 200) {
        const result = response.result;
        setAllCity(result);
        setNewAllCity(result);
      }
    });
  }, []);

  const filtering = () => {
    let TempfilterCityById = [];
    let TempfilterProfById = [];
    filterCity.forEach((item) => TempfilterCityById.push(item.city_name));
    filterProf.forEach((item) => TempfilterProfById.push(item.job_title));
    setFilterCityById(TempfilterCityById);
    setFilterProfById(TempfilterProfById);
    setGetApiCount(1 + getApiCount);
  };

  const getListCollab = (proffesion, city_name) => {
    apiDispatch(
      getListCollaborator,
      {
        proffesion: filterProfById,
        city_name: filterCityById,
        id_user,
      },
      true
    ).then((response) => {
      if (response.code === 200) {
        const { result } = response;
        // const dataLength = result.length;

        setCollaborationDatas(result);
        setIsFollowed(result.followed_by);

        // if (dataLength > 6) {
        //   setListCollab2(result.splice(6, dataLength));
        //   setListCollab1(result.splice(0, 6));
        // } else {
        //   setListCollab1(result);
        //   setListCollab2(null);
        // }
      }
    });
  };

  const handleAction = (item, type) => {
    const action = {
      follow: {
        api: postProfileFollow,
        params: {
          id_user: item.id_user,
          followed_by: id_user,
        },
      },
      unfollow: {
        api: postProfileUnfollow,
        params: {
          id_user: id_user,
          id_user_following: item.id_user,
        },
      },
    };

    apiDispatch(action[type].api, action[type].params, true).then(
      (response) => {
        response.code === 200 && setIsFollowed(true);
      }
    );
  };

  const handleClick = (e) => {
    if (e.target.checked) {
      setFilterCityChecked((prev) => [...prev, e.target.value]);
    } else {
      setFilterCityChecked((prev) => {
        let prevData = [...prev];
        prevData = prevData.filter((item) => item !== e.target.value);
        return prevData;
      });
    }
  };

  const onAddCity = (e) => {
    const city = JSON.parse(e.target.value);
    const isChecked = e.target.checked;
    const selected = isChecked
      ? [...filterCity, city]
      : filterCity.filter((person) => person.id_city !== city.id_city);
    dispatch(setCollabsData({ filterCity: selected }));
  };

  const onAddProfession = (e) => {
    const profession = JSON.parse(e.target.value);
    const isChecked = e.target.checked;

    const selected = isChecked
      ? [...filterProf, profession]
      : filterProf.filter((person) => person.id_job !== profession.id_job);
    dispatch(setCollabsData({ filterProf: selected }));
  };

  const searchByKeyword = (event) => {
    const keyword = event.target.value.toLowerCase();
    const filteredCity = allCity.filter((item) =>
      item.city_name.toLowerCase().includes(keyword)
    );
    setNewAllCity(filteredCity);
    const filteredProf = allJobs.filter((item) =>
      item.job_title.toLowerCase().includes(keyword)
    );
    setNewAllJobs(filteredProf);
  };

  const renderModalCity = () => {
    return (
      <>
        <ModalConfirmation
          modalClass="x-modal-city"
          backdrop={"static"}
          show={showModalCity}
          size={"lg"}
          onHide={() => setShowModalCity(false)}
          customTitle={
            <div className="x-modal modal-lg">
              <div className="d-flex align-items-center">
                <h1 className="heading-md-bold line-height-lg me-auto m-0 color-neutral-80">
                  City
                </h1>
                <InputText
                  disableError={true}
                  placeholder={"Find city"}
                  inlineIconSmall={"search"}
                  style={{ width: "679px", height: "40px", margin: "8px" }}
                  onChangeText={(event) => searchByKeyword(event)}
                />
                <span
                  className="material-icons-round cursor-pointer font-weight-bolder color-neutral-old-80"
                  onClick={() => {
                    setShowModalCity(false);
                    filtering();
                  }}
                >
                  clear
                </span>
              </div>
              <div className="x-separator mt-1 mb-2 ps-0 pe-0 ms-0 me-0" />
              <div className="x-list-city ps-4 pe-4 text-sm-light align-items-center">
                <div className="x-city-word d-flex mt-1">
                  <p className="m-0 text-md-normal">Popular City</p>
                </div>
                <Row>
                  {topCity.map((item, idx) => {
                    return (
                      <Col md={4} className="align-items-center" key={idx}>
                        <Form.Check
                          onChange={onAddCity}
                          name="group1"
                          type={"checkbox"}
                          id={"UI/UX"}
                          label={item.city_name}
                          value={JSON.stringify(item)}
                          checked={filterCity.some((user) =>
                            user.id_city === item.id_city ? true : false
                          )}
                        />
                      </Col>
                    );
                  })}
                  {newAllCity.map((item, idx) => {
                    return (
                      <Fragment key={idx}>
                        {newAllCity[-1 + idx]?.city_name.slice(0, 1) !==
                          newAllCity[idx]?.city_name.slice(0, 1) && (
                          <div className="x-city-word d-flex mt-1">
                            <p className="m-0 text-md-normal">
                              {newAllCity[idx].city_name.slice(0, 1)}
                            </p>
                          </div>
                        )}

                        <Col md={4} className="align-items-center">
                          <Form.Check
                            name="group1"
                            type={"checkbox"}
                            onChange={onAddCity}
                            id={"UI/UX"}
                            checked={filterCity.some((user) =>
                              user.id_city === item.id_city ? true : false
                            )}
                            label={item.city_name}
                            value={JSON.stringify(item)}
                          />
                        </Col>
                      </Fragment>
                    );
                  })}
                </Row>
              </div>
            </div>
          }
          customFooter={
            <div className="d-flex x-footer2 align-items-center">
              <div className="ms-auto">
                <div className="d-flex align-items-center">
                  <Button
                    className="btn-cancel m-2 mb-0"
                    onClick={() => dispatch(setCollabsData({ filterCity: [] }))}
                  >
                    Reset
                  </Button>
                  <Button
                    className="btn-primary m-2 mb-0"
                    onClick={() => {
                      setShowModalCity(false);
                      filtering();
                    }}
                  >
                    Apply
                  </Button>
                </div>
              </div>
            </div>
          }
        />
      </>
    );
  };

  const renderModalTopProfession = () => {
    return (
      <>
        <ModalConfirmation
          modalClass="x-modal-profession"
          backdrop={"static"}
          show={showModalTopProf}
          size={"lg"}
          onHide={() => setShowModalTopProf(false)}
          customTitle={
            <div className="x-modal w-100">
              <div
                className="d-flex align-items-center"
                style={{ width: "700px" }}
              >
                <h1 className="heading-md-bold line-height-lg me-auto m-0 ps-0 pe-0 me-5 color-neutral-80">
                  Filter
                </h1>
                <span
                  className="material-icons-round cursor-pointer font-weight-bolder color-neutral-old-80"
                  onClick={() => setShowModalTopProf(false)}
                >
                  clear
                </span>
              </div>
              <div className="x-separator mt-1 mb-2 ps-0 pe-0 ms-0 me-0" />
              <div className="d-flex align-items-center ms-3 me-3">
                <h1 className="heading-sm-normal m-0 color-neutral-70">
                  Profession
                </h1>
                <h1
                  className="heading-sm-normal m-0 ms-auto cursor-pointer color-neutral-old-80"
                  onClick={() => {
                    setShowModalProf(true);
                    setShowModalTopProf(false);
                  }}
                >
                  See All
                </h1>
              </div>
              <div className="d-flex align-items-center flex-wrap mt-2 ms-3 me-3">
                <div className="x-prof d-flex align-items-center">
                  {filterProf.map((item, idx) => {
                    return (
                      <p
                        className="x-top-prof text-sm-light m-0 ms-2 me-2 color-base-10"
                        key={idx}
                        style={{
                          backgroundColor: "var(--primary-main)",
                        }}
                      >
                        {item.job_title}
                      </p>
                    );
                  })}
                </div>
              </div>
            </div>
          }
          customFooter={
            <div className="d-flex x-footer2 align-items-center">
              <div className="ms-auto">
                <div className="d-flex align-items-center">
                  <Button
                    className="btn-cancel m-2 mb-0"
                    onClick={() => dispatch(setCollabsData({ filterProf: [] }))}
                  >
                    Reset
                  </Button>
                  <Button
                    className="btn-primary m-2 mb-0"
                    onClick={() => {
                      setShowModalTopProf(false);
                      filtering();
                    }}
                  >
                    Apply
                  </Button>
                </div>
              </div>
            </div>
          }
        />
      </>
    );
  };

  const renderModalProfession = () => {
    return (
      <>
        <ModalConfirmation
          modalClass="x-modal-city"
          backdrop={"static"}
          show={showModalProf}
          size={"lg"}
          onHide={() => setShowModalProf(false)}
          customTitle={
            <div className="x-modal modal-lg">
              <div className="d-flex align-items-center">
                <h1 className="heading-md-bold line-height-lg me-auto m-0 color-neutral-80">
                  Profession
                </h1>
                <InputText
                  disableError={true}
                  placeholder={"Find Profession"}
                  inlineIconSmall={"search"}
                  style={{ width: "550px", height: "40px", margin: "8px" }}
                  onChangeText={(event) => searchByKeyword(event)}
                />
                <span
                  className="material-icons-round cursor-pointer font-weight-bolder color-neutral-old-80"
                  onClick={() => {
                    setShowModalProf(false);
                    filtering();
                  }}
                >
                  clear
                </span>
              </div>
              <div className="x-separator mt-1 mb-2 ps-0 pe-0 ms-0 me-0" />
              <div className="x-list-city text-sm-light align-items-center">
                <div className="x-city-word d-flex mt-1">
                  <p className="m-0 text-md-normal">Popular Profession</p>
                </div>
                <Row>
                  {topJobs.map((item, idx) => {
                    return (
                      <Col md={4} className="align-items-center" key={idx}>
                        <Form.Check
                          onChange={onAddProfession}
                          name="group1"
                          type={"checkbox"}
                          id={"UI/UX"}
                          label={item.job_title}
                          value={JSON.stringify(item)}
                          checked={filterProf.some((user) =>
                            user.id_job === item.id_job ? true : false
                          )}
                        />
                      </Col>
                    );
                  })}
                  {newAllJobs.map((item, idx) => {
                    return (
                      <Fragment key={idx}>
                        {newAllJobs[-1 + idx]?.job_title.slice(0, 1) !==
                          newAllJobs[idx]?.job_title.slice(0, 1) && (
                          <div className="x-city-word d-flex mt-1">
                            <p className="m-0 text-md-normal">
                              {newAllJobs[idx].job_title.slice(0, 1)}
                            </p>
                          </div>
                        )}

                        <Col md={4} className="align-items-center" key={idx}>
                          <Form.Check
                            name="group1"
                            type={"checkbox"}
                            onChange={onAddProfession}
                            id={"UI/UX"}
                            checked={filterProf.some((user) =>
                              user.id_job === item.id_job ? true : false
                            )}
                            label={item.job_title}
                            value={JSON.stringify(item)}
                          />
                        </Col>
                      </Fragment>
                    );
                  })}
                </Row>
              </div>
            </div>
          }
          customFooter={
            <div className="d-flex x-footer2 align-items-center">
              <div className="ms-auto">
                <div className="d-flex align-items-center">
                  <Button
                    className="btn-cancel m-2 mb-0"
                    onClick={() => dispatch(setCollabsData({ filterProf: [] }))}
                  >
                    Reset
                  </Button>
                  <Button
                    className="btn-primary m-2 mb-0"
                    onClick={() => {
                      setShowModalProf(false);
                      filtering();
                    }}
                  >
                    Apply
                  </Button>
                </div>
              </div>
            </div>
          }
        />
      </>
    );
  };

  return (
    <Layout title="My Network - Indigo">
      <div className="x-mynetwork-page">
        <MainLayout size="md" Breadcrumb>
          <div className="x-mynetwork">
            <h3 className="heading-md-bold mb-1">Explore Oppurtunities</h3>
            <p className="text-lg-light mb-2 color-grey-3">
              Expand your network based on interest
            </p>
            <div className="d-flex align-items-center mt-2">
              <span
                className="material-icons-round color-neutral-old-80"
                style={{ fontSize: "20px" }}
              >
                location_on
              </span>
              <div className="d-flex gap-1">
                <span className="m-0 ms-2 text-lg-light line-height-sm color-neutral-70">
                  {"Collaboration at :"}
                </span>
                <span className="text-lg-normal m-0 line-height-sm color-neutral-old-80">
                  {!isEmpty(filterCity)
                    ? filterCity.map((item, idx) => {
                        const separator =
                          idx !== filterCity.length - 1 ? ", " : ""; // here
                        return item.city_name + separator;
                      })
                    : "All City"}
                </span>
              </div>
              <div className="ms-auto">
                <p
                  className="m-0 cursor-pointer heading-sm-normal color-neutral-old-80"
                  onClick={() => {
                    setShowModalCity(true);
                  }}
                >
                  Edit
                </p>
              </div>
            </div>
            <Button
              className="btn-disable w-auto d-flex justify-content-center align-items-center py-1 px-2 my-3"
              onClick={() => {
                setShowModalTopProf(true);
              }}
            >
              <div className="x-icon-filter" />
              <p className="m-0 ms-1 text-sm-normal line-height-sm color-neutral-70">
                Filter
              </p>
              {!isEmpty(filterProf.length) && (
                <p className="text-sm-normal line-height-sm m-0 me-1 color-neutral-70">
                  {" "}
                  ({filterProf.length})
                </p>
              )}
            </Button>

            <ContentBox
              title="Top Collaborator"
              NoDivider
              customHeaderContent={
                !isEmpty(collaborationDatas) && (
                  <Row className="x-top-collaboration">
                    {collaborationDatas.map(
                      (item, idx) =>
                        idx < 6 && (
                          <Col
                            lg={2}
                            key={idx}
                            className="cursor-pointer"
                            onClick={() =>
                              router.push({
                                pathname: `/home/collaboration/my-network/profile`,
                                query: { id: item.id_user },
                              })
                            }
                          >
                            <div className="x-img-profile">
                              {!isEmpty(item.profile_picture) ? (
                                <img
                                  src={item.profile_picture}
                                  alt=""
                                  className="x-image-account"
                                />
                              ) : (
                                <div className="x-empty-image">
                                  <h1 className="heading-lg-light m-0 color-base-10">
                                    {item.full_name.slice(0, 1).toUpperCase()}
                                  </h1>
                                </div>
                              )}
                            </div>
                            <p className="text-md-normal text-center color-body-60">
                              {item.full_name.length > 12
                                ? `${item.full_name.slice(0, 12)}...`
                                : item.full_name}
                            </p>
                          </Col>
                        )
                    )}
                  </Row>
                )
              }
            >
              {collaborationDatas.length > 5 && (
                <Fragment>
                  <hr />
                  <div className="mt-4">
                    {collaborationDatas?.map(
                      (item, idx) =>
                        idx > 5 && (
                          <div className="x-cardmember-wrapper" key={idx}>
                            <div
                              className="cursor-pointer w-75"
                              onClick={() =>
                                router.push({
                                  pathname: `/home/collaboration/my-network/profile`,
                                  query: { id: item.id_user },
                                })
                              }
                            >
                              <CardUser
                                image={item.profile_picture}
                                name={item.full_name}
                                jobTitle={item.job_title}
                                city={item.city_name}
                              />
                            </div>

                            <div className="p-0 d-flex align-items-center">
                              <Button
                                className={classnames(
                                  "w-auto",
                                  item.followed_by === false
                                    ? "btn-primary"
                                    : "btn-delete"
                                )}
                                onClick={() =>
                                  handleAction(
                                    item,
                                    item.followed_by === false
                                      ? "follow"
                                      : "unfollow"
                                  )
                                }
                              >
                                {item.followed_by === false
                                  ? "Follow"
                                  : "Followed"}
                              </Button>
                            </div>
                          </div>
                        )
                    )}
                  </div>
                </Fragment>
              )}
            </ContentBox>
          </div>
          {renderModalCity()}
          {renderModalTopProfession()}
          {renderModalProfession()}
        </MainLayout>
      </div>
    </Layout>
  );
};

export default appRoute(MyNetwork);
*/
