import React, { Fragment, useEffect, useState } from "react";
import BannerCarousel from "/components/home/BannerCarousel";
import ContentBox from "/components/home/ContentBox";
import Card from "/components/home/Card";
import CardMenu from "/components/home/CardMenu";
import PostCard from "/components/cards/PostCard";
import { isEmpty } from "lodash-es";
import { getAds, getAllAds } from "/services/actions/api";
import { getAuth, apiDispatch, dateFormatter } from "/services/utils/helper";
import MainLayout from "/components/layout/MainLayout";
import { useRouter } from "next/router";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";
import { Row } from "react-bootstrap";
import { Loaders } from "/components/loaders/Loaders";

const Home = () => {
  const [data, setData] = useState({
    activity: [],
    agenda: [],
    submission: [],
    podcast: [],
    video: [],
    collaboration: [],
  });
  const [banner, setBanner] = useState([]);
  const [toggleMediaLearning, setToggleMediaLearning] = useState("Podcast");
  const [isLoadingBanner, setIsLoadingBanner] = useState(true);
  const [isLoadingData, setIsLoadingData] = useState(true);

  const [path] = useState({
    Agenda: "/home/agenda",
    Collaboration: "/home/collaboration",
    Submission: "/home/submission",
    "Media Learning": "/home/media-learning",
  });

  useEffect(() => getData(), []);

  const router = useRouter();

  const getData = () => {
    const { id_user } = getAuth();

    apiDispatch(
      getAllAds,
      {
        start: 0,
        limit: 3,
        id_user,
      },
      true
    )
      .then((response) => {
        setIsLoadingData(false);
        if (response.code === 200) {
          const result = response.result;
          setData({
            ...data,
            activity: result.category,
            agenda: result.sc_session,
            submission: result.submission,
            podcast: result.sp_album,
            video: result.video,
            collaboration: result.collaboration,
          });
        }
      })
      .catch((err) => console.log(err));

    apiDispatch(
      getAds,
      {
        start: 0,
        limit: 5,
        status: "active",
        category: "announcement",
        id_user,
      },
      true
    ).then((response) => {
      setIsLoadingBanner(false);
      if (response.code === 200) {
        const result = response.result;
        setBanner(result);
      }
    });
  };

  const subTitleTextJoin = (speakers, date, duration) => {
    let speaker = !isEmpty(speakers)
      ? speakers.map((item, idx) => {
        const currentIdx = idx + 1;
        const nextIdx = idx + 2;
        let join = "";

        if (nextIdx < speakers.length) join = ", ";
        else if (currentIdx === speakers.length) join = "";
        else if (currentIdx < speakers.length) join = " dan ";

        return item.title.concat(join);
      })
      : "";

    const divider = (
      <span
        className="material-icons-round color-neutral-old-50 px-1"
        style={{ fontSize: "8px" }}
      >
        fiber_manual_record
      </span>
    );

    return (
      <>
        {!isEmpty(speaker) && speaker}
        {!isEmpty(speaker) && (!isEmpty(date) || !isEmpty(duration)) && divider}
        {!isEmpty(date) && dateFormatter(date)}
        {!isEmpty(date) && !isEmpty(duration) && divider}
        {!isEmpty(duration) && duration}
      </>
    );
  };

  const emptyState = (text) => (
    <h4 className="heading-md-light m-0 text-center w-100 p-3 color-neutral-40">
      {text}
    </h4>
  );

  const { activity, agenda, submission, podcast, video, collaboration } = data;

  return (
    <Layout title="Home - Indigo">
      <MainLayout size="lg">
        <div className="x-home-page">
          <BannerCarousel
            data={banner}
            carouselIsClick={true}
            isLoadingBanner={isLoadingBanner}
          />

          <ContentBox
            title={"Aktivitas"}
            subTitle={"Cari aktivitas sesuai dengan kebutuhan kamu!"}
          >
            <Row className="m-0 py-1 mt-2">
              <Loaders.Elements
                isLoading={isLoadingData}
                height={144}
                className="px-3"
                md={3}
                count={4}
                emptyStateContent={emptyState("Maaf, Menu belum tersedia")}
              >
                {activity.map((item, idx) => (
                  <Fragment key={idx}>
                    <CardMenu
                      col={3}
                      onClick={() => router.push(path[item.label])}
                      image={item.image_web}
                      icon={item.icon}
                      label={item.label}
                    />
                  </Fragment>
                ))}
              </Loaders.Elements>
            </Row>
          </ContentBox>

          <ContentBox
            fullDivider
            title={"Webinar"}
            subTitle={"Sesi agenda bareng para expert"}
            cta={() => !isEmpty(agenda) && router.push("/home/agenda")}
          >
            <Row className="m-0 py-1">
              <Loaders.Card
                isLoading={isLoadingData}
                emptyStateContent={emptyState("Maaf, Webinar belum tersedia")}
              >
                {agenda.map((item, idx) => (
                  <Fragment key={idx}>
                    <Card
                      col={4}
                      image={item.image}
                      title={item.title}
                      subTitle={subTitleTextJoin(
                        item.speaker,
                        item.date_session.replaceAll(`"`, "") // experimental, value has double string
                      )}
                      onClick={() =>
                        router.push({
                          pathname: "/home/agenda/detail",
                          query: { id: item.id_ads },
                        })
                      }
                    />
                  </Fragment>
                ))}
              </Loaders.Card>
            </Row>
          </ContentBox>

          <ContentBox
            fullDivider
            title={"Submission"}
            subTitle={"Ikuti submission baru di event kami"}
            cta={() => !isEmpty(submission) && router.push("/home/submission")}
          >
            <Row className="m-0 py-1">
              <Loaders.Card
                isLoading={isLoadingData}
                emptyStateContent={emptyState(
                  "Maaf, Submission belum tersedia"
                )}
              >
                {submission.map((item, idx) => (
                  <Fragment key={idx}>
                    <Card
                      col={4}
                      image={item.image}
                      title={item.title}
                      onClick={() =>
                        router.push({
                          pathname: "/home/submission/detail",
                          query: { id: item.id_ads },
                        })
                      }
                    />
                  </Fragment>
                ))}
              </Loaders.Card>
            </Row>
          </ContentBox>

          <ContentBox
            fullDivider
            title={"Media Learning"}
            subTitle={"Media untuk aktivitas kamu"}
            cta={() => (!isEmpty(podcast) || !isEmpty(video)) && router.push("/home/media-learning")}
            toggles={["Podcast", "Video"]}
            toggleActive={toggleMediaLearning}
            setToggleActive={(toggle) => setToggleMediaLearning(toggle)}
          >
            <Row className="m-0 py-1">
              <Loaders.Card
                isLoading={isLoadingData}
                emptyStateContent={emptyState(`Maaf, ${toggleMediaLearning} belum tersedia`)}
              >
                {(toggleMediaLearning === "Podcast" ? podcast : video).map(
                  (item, idx) => (
                    <Fragment key={idx}>
                      <Card
                        col={4}
                        image={item.image}
                        title={item.title}
                        subTitle={
                          toggleMediaLearning === "Podcast"
                            ? subTitleTextJoin(item.speaker)
                            : subTitleTextJoin(
                                null,
                                item.created_at,
                                item.video_duration
                              )
                        }
                        onClick={() =>
                          router.push({
                            pathname: `/home/media-learning/${toggleMediaLearning.toLowerCase()}`,
                            query: { id: item.id_ads },
                          })
                        }
                      />
                    </Fragment>
                  )
                )}
              </Loaders.Card>
            </Row>
          </ContentBox>

          <ContentBox
            fullDivider
            title={"Collaborations"}
            subTitle={"Berkolaborasi berkarya bersama"}
            cta={() => !isEmpty(collaboration) && router.push("/home/collaboration")}
          >
            <Row className="py-1">
              <Loaders.PostCard
                isLoading={isLoadingData}
                count={2}
                emptyStateContent={emptyState("Maaf, Collaboration belum tersedia")}
              >
                {collaboration.map((item, idx) => {
                  const data = JSON.parse(item.additional_data);
                  return (
                    <Fragment key={idx}>
                      <PostCard
                        image={item.image}
                        HasHr
                        name={item.created_by}
                        jobTitle={item.job_title}
                        city={item.city_name}
                        postTimeAgo={item.time_ago}
                        title={item.title}
                        totalView={item.total_view}
                        totalComment={item.total_comment}
                        profession={data.profession}
                        onCta={() => {
                          router.push({
                            pathname: "/home/collaboration/detail",
                            query: {
                              id: item.id_ads,
                            },
                          });
                        }}
                        onCtaCard={() => {
                          router.push({
                            pathname: "/home/collaboration/detail",
                            query: {
                              id: item.id_ads,
                            },
                          });
                        }}
                      />
                    </Fragment>
                  );
                })}
              </Loaders.PostCard>
            </Row>
          </ContentBox>
        </div>
      </MainLayout>
    </Layout>
  );
};

export default appRoute(Home);
