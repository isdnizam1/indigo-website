import React, { Fragment, useEffect, useState } from "react";
import Image from "/public/assets/images/img-webinar-cover.png";
import {
  getNotification,
  getCountUnreadNotif,
  postReadNotification,
  postEvaluateCertificate,
  getCertificates,
} from "/services/actions/api";
import classnames from "classnames";
import ModalConfirmation from "/components/modal/ModalConfirmation";
import { getAuth, formatDate, apiDispatch } from "/services/utils/helper";
import MainLayout from "/components/layout/MainLayout";
import { isEmpty } from "lodash-es";
import Layout from "/components/common_layout/Layout";
import appRoute from "../../../components/routes/appRoute";
import { LoaderContent } from "/components/loaders/Loaders";
import SkeletonElement from "/components/skeleton/SkeletonElement";
import MaterialIcon from "components/utils/MaterialIcon";
import CardLv2 from "/components/home/CardLv2";
import { Button } from "react-bootstrap";
import { useFormik } from "formik";
import InputText from "/components/input/InputText";
import { useRouter } from "next/router";
import InputSelectV2 from "/components/input/InputSelectV2";
import { evaluateFormCertificateSchema } from "/services/constants/Schemas";

const Notification = () => {
  const [allNotifications, setAllNotifications] = useState([]);
  const [isActiveTab, setIsActiveTab] = useState("update");
  const [updateUnread, setUpdateUnread] = useState("");
  const [activitiesUnread, setActivitiesUnread] = useState("");
  const [certificates, setCertificates] = useState([]);
  const [certificateModalShown, setCertificateModalShown] = useState("list");
  const [selectedCertificate, setSelectedCertificate] = useState({});
  const [isActiveFilter, setIsActiveFilter] = useState("All");
  const [isActiveSort, setIsActiveSort] = useState("");
  const [isActiveType, setIsActiveType] = useState("");
  const [isSubmit, setIsSubmit] = useState(false);
  const [isRead, setIsRead] = useState("");
  const [isLoadingTab, setIsLoadingTab] = useState(true);
  const [isLoadingNotif, setIsLoadingNotif] = useState(true);
  const [modal, setModal] = useState(false);
  const initialEvaluateFormCertificate = {
    id_sc_user: null,
    startup_level: undefined,
    topik_acara: undefined,
    pengisi_acara: undefined,
    fasilitas_acara: undefined,
    topik_berikutnya: "",
    ikut_program_indigo: [],
    saran: "",
  };

  const { push } = useRouter();

  const formik = useFormik({
    initialValues: initialEvaluateFormCertificate,
    enableReinitialize: true,
    validateOnMount: true,
    validationSchema: evaluateFormCertificateSchema,
    onSubmit: (values) => postEvaluateCertificateSubmit(values),
  });

  const formOptions = {
    startupLevel: [
      { value: "Talent", title: "Talent" },
      {
        value: "Startup Founder (Non-program)",
        title: "Startup Founder (Non-program)",
      },
      {
        value: "Startup Founder Alumni (Post Indigo)",
        title: "Startup Founder Alumni (Post Indigo)",
      },
      { value: "Mentor", title: "Mentor" },
      { value: "Investor/VC", title: "Investor/VC" },
      { value: "Other", title: "Other" },
    ],
    programs: [
      {
        value: "Indigo Startup Clinic",
        title: "Indigo Startup Clinic",
      },
      {
        value: "Indigo Hackathon",
        title: "Indigo Hackathon",
      },
      {
        value: "Indigo Challenge",
        title: "Indigo Challenge",
      },
      {
        value: "Indigo Betalist",
        title: "Indigo Betalist",
      },
      {
        value: "Indigo Intake",
        title: "Indigo Intake",
      },
    ],
    pointLevel: [
      { value: "10", title: "10 (sangat suka)" },
      { value: "9", title: "9" },
      { value: "8", title: "8" },
      { value: "7", title: "7" },
      { value: "6", title: "6" },
      { value: "5", title: "5" },
      { value: "4", title: "4" },
      { value: "3", title: "3" },
      { value: "2", title: "2" },
      { value: "1", title: "1 (sangat tidak suka)" },
    ],
  };

  const { id_user } = getAuth();
  const MAX_LENGTH = 80;
  let dynamicAllNotifications = isLoadingNotif ? [1, 2, 3] : allNotifications;

  useEffect(() => {
    getNotif(id_user, "other");
    getUnreadCount(id_user);
    getListCertificates();
  }, []);

  const getListCertificates = () => {
    apiDispatch(getCertificates, { id_user }, true).then((response) => {
      response.code === 200 && setCertificates(response.result);
    });
  };

  const getNotif = (id_user, type, category, sort_by) => {
    apiDispatch(
      getNotification,
      { id_user, type, category, sort_by },
      true
    ).then((response) => {
      setIsLoadingNotif(false);
      if (response.code === 200) {
        const data = response.result;
        const newData = data.map((item) => {
          item.created_at = formatDate(item.created_at);
          return item;
        });
        setAllNotifications(newData);
        setIsActiveFilter(category);
      }
    });
  };

  const getUnreadCount = (id_user) => {
    apiDispatch(getCountUnreadNotif, { id_user }, true).then((response) => {
      setIsLoadingTab(false);
      if (response.code === 200) {
        const result = response.result;
        setUpdateUnread(result.total_other_notif);
        setActivitiesUnread(result.total_project_notif);
      }
    });
  };

  const updateNotifRead = (id_notif) => {
    apiDispatch(postReadNotification, { id_notif }, true).then((response) => {
      response.code === 200 && setIsRead(true);
    });
  };

  const handleSort = () => {
    setIsActiveSort(
      isActiveSort === ""
        ? "new"
        : isActiveSort === "new"
        ? "old"
        : isActiveSort === "old" && "new"
    );
  };

  const renderModalCertificates = () => {
    function header() {
      return (
        <div className="d-flex justify-content-between align-items-center p-1 w-100">
          <div className="d-flex align-items-center gap-2">
            <div className="rounded-circle bg-primary-40 p-1 d-flex justify-content-center align-items-center">
              <MaterialIcon
                action="description"
                size={16}
                color="primary-100"
              />
            </div>
            <p className="text-lg-bold color-primary-pr-120 m-0">
              My Certificate
            </p>
          </div>
          <MaterialIcon
            action="clear"
            size={28}
            weight={500}
            className="cursor-pointer"
            color="semantic-main"
            onClick={() => {
              setModal(false);
              setTimeout(() => setCertificateModalShown("list"), 250);
            }}
          />
        </div>
      );
    }
    function onHide() {
      setModal(false);
      setSelectedCertificate({});
      formik.handleReset();
      setTimeout(() => setCertificateModalShown("list"), 250);
    }
    function certificateLists() {
      return certificates?.map((item, idx) => (
        <Fragment key={idx}>
          <CardLv2
            className="x-card-ecertificate"
            image={item.image}
            customBody={
              <div className="d-flex flex-column justify-content-center h-100">
                <p className="text-lg-bold color-neutral-old-80 line-height-md m-0">
                  {item.title}
                </p>
                <p className="text-sm-light color-neutral-500 m-0">
                  {item.speaker.map((speaker, idx) => (
                    <Fragment key={idx}>
                      {speaker.title}
                      {idx !== item.speaker.length - 1 ? ", " : ""}
                    </Fragment>
                  ))}
                </p>
                <div>
                  <Button
                    className="w-auto py-1 px-2 mt-2 d-flex justify-content-center align-items-center"
                    onClick={() => {
                      if (item.is_submit_evaluation_form === "1") {
                        return push({
                          pathname: "/agenda-attendance/e-certificate",
                          query: { id: item.id_sc_user },
                        });
                      }

                      setCertificateModalShown("form");
                      formik.handleReset();
                      formik.setValues({
                        ...formik.values,
                        id_sc_user: item.id_sc_user,
                      });
                      formik.validateForm();
                      setSelectedCertificate(item);
                    }}
                  >
                    <MaterialIcon
                      type="outlined"
                      action="file_download"
                      size={14}
                    />
                    <span className="text-sm-light m-0 px-1">
                      Download E-Certificate
                    </span>
                    <MaterialIcon
                      type="outlined"
                      action="file_upload"
                      size={14}
                    />
                  </Button>
                </div>
              </div>
            }
          />
        </Fragment>
      ));
    }
    function formEvaluate() {
      return (
        <Fragment>
          <h3 className="heading-md-normal color-neutral-old-80 text-center">
            Help us to improve our programs
          </h3>
          <p className="text-lg-light color-neutral-old-40 text-center">
            Silahkan isi form evaluasi ini untuk membantu kami mengembangkan
            event terbaik untuk kamu
          </p>
          <form onSubmit={formik.handleSubmit}>
            <div className="d-flex flex-column gap-3">
              <InputSelectV2
                value={formik.values.startup_level}
                name="startup_level"
                className="x-input-select"
                onChange={formik.handleChange}
                title={
                  <span className="text-lg-bold color-neutral-950 m-0">
                    Silahkan pilih Startup Level kamu
                  </span>
                }
                disabled={isSubmit}
                placeholder="Pilih salah satu"
                customOptions={formOptions.startupLevel.map((item, idx) => (
                  <option
                    key={idx}
                    value={item.value}
                    selected={formik.values.startup_level}
                  >
                    {item.title}
                  </option>
                ))}
              />

              <InputSelectV2
                value={formik.values.topik_acara}
                name="topik_acara"
                title={
                  <span className="text-lg-bold color-neutral-950 m-0">
                    Apakah kamu puas dengan topik acara?
                  </span>
                }
                disabled={isSubmit}
                placeholder="Pilih salah satu"
                onChange={formik.handleChange}
                customOptions={formOptions.pointLevel.map((item, idx) => (
                  <option
                    key={idx}
                    value={item.value}
                    selected={formik.values.topik_acara}
                  >
                    {item.title}
                  </option>
                ))}
              />

              <InputSelectV2
                value={formik.values.pengisi_acara}
                name="pengisi_acara"
                title={
                  <span className="text-lg-bold color-neutral-950 m-0">
                    Apakah kamu puas dengan pengisi acara?
                  </span>
                }
                disabled={isSubmit}
                placeholder="Pilih salah satu"
                onChange={formik.handleChange}
                customOptions={formOptions.pointLevel.map((item, idx) => (
                  <option
                    key={idx}
                    value={item.value}
                    selected={formik.values.topik_acara}
                  >
                    {item.title}
                  </option>
                ))}
              />

              <InputSelectV2
                value={formik.values.fasilitas_acara}
                name="fasilitas_acara"
                title={
                  <span className="text-lg-bold color-neutral-950 m-0">
                    Apakah kamu puas dengan fasilitas acara?
                  </span>
                }
                disabled={isSubmit}
                placeholder="Pilih salah satu"
                onChange={formik.handleChange}
                customOptions={formOptions.pointLevel.map((item, idx) => (
                  <option
                    key={idx}
                    value={item.value}
                    selected={formik.values.topik_acara}
                  >
                    {item.title}
                  </option>
                ))}
              />

              <InputText
                title={
                  <span className="text-lg-bold color-neutral-950 m-0">
                    Silahkan berikan saran topik berikutnya yang kamu minati
                  </span>
                }
                disabled={isSubmit}
                disableError
                value={formik.values.topik_berikutnya}
                name="topik_berikutnya"
                type="text"
                onChangeText={formik.handleChange}
                onBlur={formik.handleBlur}
                placeholder="Isi disini"
              />

              <InputSelectV2
                value={"Boleh pilih lebih dari satu"}
                title={
                  <span className="text-lg-bold color-neutral-950 m-0">
                    Apakah kamu sebelumnya pernah mengikuti program Indigo?
                  </span>
                }
                disabled={isSubmit}
                placeholder="Boleh pilih lebih dari satu"
                onChange={(e) => {
                  formik.setValues({
                    ...formik.values,
                    ikut_program_indigo: [
                      ...formik.values.ikut_program_indigo,
                      e.target.value,
                    ],
                  });
                }}
                customOptions={
                  <Fragment>
                    <option key="blankChoice" hidden value>
                      Boleh pilih lebih dari satu
                    </option>
                    {formOptions.programs.map((item, idx) => {
                      const existed = formik.values.ikut_program_indigo.filter(
                        (program) => program === item.value
                      );

                      return (
                        <option
                          key={idx}
                          value={item.value}
                          disabled={!isEmpty(existed)}
                        >
                          {item.title}
                        </option>
                      );
                    })}
                  </Fragment>
                }
              />

              {!isEmpty(formik.values.ikut_program_indigo) && (
                <div className="d-flex align-items-center flex-wrap gap-2">
                  {formik.values.ikut_program_indigo?.map((item, idx) => (
                    <p
                      className="x-program-title"
                      key={idx}
                      onClick={() => {
                        formik.setValues({
                          ...formik.values,
                          ikut_program_indigo:
                            formik.values.ikut_program_indigo.filter(
                              (program) => program !== item
                            ),
                        });
                      }}
                    >
                      {item}{" "}
                      <MaterialIcon action="clear" size={14} weight="bold" />
                    </p>
                  ))}
                </div>
              )}

              <InputText
                title={
                  <span className="text-lg-bold color-neutral-950 m-0">
                    Saran atau kritik untuk memperbaiki kualitas program atau
                    acara kami
                  </span>
                }
                disableError
                disabled={isSubmit}
                value={formik.values.saran}
                name="saran"
                type="text"
                onChangeText={formik.handleChange}
                onBlur={formik.handleBlur}
                placeholder="Isi disini"
              />

              <Button
                className="mt-2 mb-4"
                type="submit"
                disabled={!isEmpty(formik.errors) || isSubmit}
              >
                {!isSubmit ? "Kirim" : "Sending..."}
              </Button>
            </div>
          </form>
        </Fragment>
      );
    }
    function success() {
      return (
        <div className="x-formevaluate-success">
          <div className="x-icon-success" />
          <h3 className="heading-md-bold color-neutral-old-70">
            Thanks for your help!
          </h3>
          <p className="text-lg-light color-neutral-650">
            Silahkan klik tombol dibawah ini untuk mendowload{" "}
            <span className="color-neutral-950 text-lg-normal m-0">
              E-Certificate
            </span>{" "}
            kamu
          </p>
          <Button
            className="d-flex justify-content-center align-items-center gap-2"
            onClick={() => {
              setTimeout(() => setCertificateModalShown("list"), 250);
              setModal(false);
              push({
                pathname: "/agenda-attendance/e-certificate",
                query: { id: selectedCertificate.id_sc_user },
              });
            }}
          >
            <MaterialIcon type="outlined" action="file_download" />
            <span>Download E-Certificate</span>
          </Button>
        </div>
      );
    }

    return (
      <ModalConfirmation
        backdrop="static"
        size="md"
        show={modal}
        modalClass="x-ecertificate-modal"
        onHide={() => onHide()}
        customTitle={certificateModalShown !== "success" && header()}
        customBody={
          <div className="d-flex flex-column gap-2 overflow-auto px-3">
            {certificateModalShown === "list"
              ? certificateLists()
              : certificateModalShown === "form"
              ? formEvaluate()
              : certificateModalShown === "success" && success()}
          </div>
        }
      />
    );
  };

  const postEvaluateCertificateSubmit = (params) => {
    setIsSubmit(true);

    apiDispatch(postEvaluateCertificate, params, true).then((response) => {
      if (response.status === "success") {
        formik.handleReset();
        formik.validateForm();
        setCertificateModalShown("success");
        setIsSubmit(false);
      }
    });
  };

  return (
    <Layout title="Notification - Indigo">
      <div className="x-notification-page">
        <MainLayout size="md">
          <div className="d-flex align-items-center justify-content-between mt-5">
            <p className="m-0 heading-md-bold color-semantic-main">
              NOTIFICATION
            </p>
            <div className="d-flex justify-content-center align-items-center gap-3">
              <div className="d-flex justify-content-center align-items-center cursor-pointer gap-2">
                <div className="x-rounded-icon">
                  <MaterialIcon
                    action="confirmation_number"
                    size={16}
                    color="primary-100"
                  />
                </div>
                <p className="m-0 text-lg-bold color-neutral-80">My Voucher</p>
              </div>

              <div
                className="d-flex justify-content-center align-items-center cursor-pointer gap-2"
                onClick={() => setModal(true)}
              >
                <div className="x-rounded-icon">
                  <MaterialIcon
                    action="description"
                    size={16}
                    color="primary-100"
                  />
                </div>
                <p className="m-0 text-lg-bold color-neutral-80">
                  My Certificate
                </p>
              </div>
            </div>
          </div>

          <div className="x-notif">
            <div className="x-notif-navbar">
              <nav className="navbar navbar-expand-lg navbar-light">
                <LoaderContent
                  height={25}
                  width={120}
                  className="mx-2 my-1"
                  count={2}
                  isLoading={isLoadingTab}
                >
                  <ul className="navbar-nav me-auto">
                    <li className="nav-item">
                      <p
                        className={classnames(
                          "nav-link m-0 cursor-pointer heading-sm-bold",
                          isActiveTab === "update" ? "active" : "inactive"
                        )}
                        onClick={() => {
                          setIsActiveTab("update");
                          getNotif(id_user, "Other", "", "");
                        }}
                      >
                        UPDATES ({updateUnread})
                      </p>
                    </li>
                    <li className="nav-item">
                      <p
                        className={classnames(
                          "nav-link m-0  cursor-pointer heading-sm-bold",
                          isActiveTab === "activities" ? "active" : "inactive"
                        )}
                        onClick={() => {
                          setIsActiveTab("activities");
                          getNotif(id_user, "Project", "All", "");
                        }}
                      >
                        ACTIVITIES ({activitiesUnread})
                      </p>
                    </li>
                  </ul>
                </LoaderContent>
              </nav>
              <div className="x-separator" />
            </div>

            {/* activities filter */}
            {(() => {
              if (isActiveTab === "activities") {
                return (
                  <div className="x-filter-activities">
                    <div className="m-0 d-flex align-items-center">
                      <div className="x-btn-sort cursor-pointer">
                        <div className="m-0 button-sort d-flex align-items-center">
                          <span
                            className="material-icons-round"
                            style={{ color: "var(--grey-4)" }}
                          >
                            filter_list
                          </span>
                          <p
                            className="m-0 text-sm-normal line-height-sm"
                            onClick={() => {
                              handleSort();
                              getNotif(
                                id_user,
                                "Project",
                                isActiveFilter,
                                isActiveSort
                              );
                            }}
                          >
                            Sort
                          </p>
                        </div>
                      </div>
                      <div
                        className={classnames(
                          "x-btn cursor-pointer",
                          isActiveFilter === "All" ? "active" : null
                        )}
                        onClick={() => {
                          getNotif(id_user, "Project", "All", isActiveSort);
                        }}
                      >
                        <p className="m-0 text-sm-normal line-height-sm">All</p>
                      </div>
                      <div
                        className={classnames(
                          "x-btn cursor-pointer",
                          isActiveFilter === "Joined" ? "active" : null
                        )}
                        onClick={() => {
                          getNotif(id_user, "Project", "Joined", isActiveSort);
                        }}
                      >
                        <p className="m-0 text-sm-normal line-height-sm">
                          Joined
                        </p>
                      </div>
                      <div
                        className={classnames(
                          "x-btn cursor-pointer",
                          isActiveFilter === "Created" ? "active" : null
                        )}
                        onClick={() => {
                          getNotif(id_user, "Project", "Created", isActiveSort);
                        }}
                      >
                        <p className="m-0 text-sm-normal line-height-sm">
                          Created
                        </p>
                      </div>
                    </div>
                  </div>
                );
              }
            })()}

            {/* update */}
            {dynamicAllNotifications.map((item, idx) => {
              return (
                <div className="x-list-data-sub" key={idx}>
                  {(() => {
                    if (item.url_web !== null) {
                      return (
                        <div
                          className="x-data cursor-pointer"
                          key={idx}
                          onClick={() => {
                            // router.push(`/${item.url_web}`);
                            updateNotifRead(item.id_notif);
                          }}
                        >
                          {(() => {
                            if (isActiveTab === "update") {
                              return (
                                <div className="x-notif-card">
                                  <div className="col-1 p-0">
                                    <LoaderContent
                                      width={48}
                                      height={48}
                                      type="rounded-circle"
                                      isLoading={isLoadingNotif}
                                    >
                                      {(() => {
                                        if (
                                          item.profile_picture !== null &&
                                          item.profile_picture !== "" &&
                                          item.notif_from !== "1"
                                        ) {
                                          return (
                                            <img
                                              src={item.profile_picture}
                                              className="x-image-account"
                                              alt=""
                                            />
                                          );
                                        } else if (item.notif_from !== "1") {
                                          return (
                                            <div className="x-empty-image">
                                              <p
                                                className="text-xl-normal m-0"
                                                style={{
                                                  color: "var(--base-10)",
                                                }}
                                              >
                                                {item.full_name
                                                  .slice(0, 1)
                                                  .toUpperCase()}
                                              </p>
                                            </div>
                                          );
                                        }
                                        return (
                                          <div className="x-rounded-icons">
                                            <div className="x-notif-icon" />
                                          </div>
                                        );
                                      })()}
                                    </LoaderContent>
                                  </div>

                                  <div className="col-10 p-0">
                                    {(() => {
                                      if (isLoadingNotif) {
                                        return (
                                          <Fragment>
                                            <SkeletonElement
                                              width="90%"
                                              height={16}
                                            />
                                            <SkeletonElement
                                              width="30%"
                                              height={14}
                                              className="my-2"
                                            />
                                          </Fragment>
                                        );
                                      } else if (
                                        item.content.slice(
                                          0,
                                          item.full_name?.length
                                        ) === item.full_name
                                      ) {
                                        return (
                                          <p
                                            className="m-0 ms-2 text-lg-bold line-height-md"
                                            style={{
                                              color: "var(--neutral-80)",
                                            }}
                                          >
                                            {item.content.slice(
                                              0,
                                              item.full_name?.length
                                            )}
                                            {item.content.length >
                                            MAX_LENGTH ? (
                                              <span
                                                className="m-0 text-lg-light line-height-md"
                                                style={{
                                                  color: "var(--neutral-80)",
                                                }}
                                              >
                                                {item.content.slice(
                                                  item.full_name?.length,
                                                  MAX_LENGTH
                                                )}
                                                ..
                                              </span>
                                            ) : (
                                              <span
                                                className="m-0 text-lg-light line-height-md"
                                                style={{
                                                  color: "var(--neutral-80)",
                                                }}
                                              >
                                                {item.content.slice(
                                                  item.full_name?.length,
                                                  item.content.length
                                                )}
                                              </span>
                                            )}
                                          </p>
                                        );
                                      }
                                      return (
                                        <div className="x-notif-icon">
                                          {item.content.length > MAX_LENGTH ? (
                                            <p
                                              className="m-0 ms-2 text-lg-light line-height-md"
                                              style={{
                                                color: "var(--neutral-80)",
                                              }}
                                            >
                                              {item.content.slice(
                                                0,
                                                MAX_LENGTH
                                              )}
                                              ..
                                            </p>
                                          ) : (
                                            <p
                                              className="m-0 ms-2 text-lg-light line-height-md"
                                              style={{
                                                color: "var(--neutral-80)",
                                              }}
                                            >
                                              {item.content}
                                            </p>
                                          )}
                                        </div>
                                      );
                                    })()}

                                    <p
                                      className="m-0 ms-2 text-md-light line-height-md"
                                      style={{ color: "var(--neutral-50)" }}
                                    >
                                      {item.created_at}
                                    </p>
                                  </div>
                                  {(() => {
                                    if (isLoadingNotif) {
                                      return (
                                        <div className="col-1 p-0">
                                          <SkeletonElement
                                            width={16}
                                            height={16}
                                            type="rounded-circle"
                                            className="x-read"
                                          />
                                        </div>
                                      );
                                    }
                                    if (item.status === "unread") {
                                      return (
                                        <div className="col-1 p-0">
                                          <div className="x-read" />
                                        </div>
                                      );
                                    }
                                  })()}
                                </div>
                              );
                            } else if (isActiveTab === "activities") {
                              return (
                                <div className="x-notif-card">
                                  <div className="col-2 p-0">
                                    <div className="x-activities-cover">
                                      {!isEmpty(item.data_ads?.image) ? (
                                        <img
                                          src={item.data_ads?.image}
                                          className="x-image cursor-pointer"
                                          alt=""
                                        />
                                      ) : !isLoadingNotif ? (
                                        <img
                                          src={Image.src}
                                          className="x-image cursor-pointer"
                                          alt=""
                                        />
                                      ) : (
                                        <SkeletonElement
                                          height={47.188}
                                          width={84.125}
                                        />
                                      )}
                                    </div>
                                  </div>
                                  {!isLoadingNotif ? (
                                    <div className="col-9 p-0 ms-3">
                                      <p
                                        className="m-0 text-sm-normal line-height-sm mb-1"
                                        style={{
                                          color: "var(--neutral-50)",
                                        }}
                                      >
                                        {item.content}
                                      </p>
                                      <p
                                        className="m-0 text-md-bold line-height-sm mb-1"
                                        style={{
                                          color: "var(--neutral-70)",
                                        }}
                                      >
                                        {item.data_ads?.title}
                                      </p>
                                      {(() => {
                                        if (
                                          item.data_ads?.label_status ===
                                          "Terdaftar"
                                        ) {
                                          return (
                                            <div className="x-status-success mb-1">
                                              <p className="m-0 text-sm-light line-height-sm">
                                                Registered
                                              </p>
                                            </div>
                                          );
                                        } else if (
                                          item.data_ads?.label_status ===
                                          "Terpilih"
                                        ) {
                                          return (
                                            <div className="x-status-success mb-1">
                                              <p className="m-0 text-sm-light line-height-sm">
                                                Selected
                                              </p>
                                            </div>
                                          );
                                        }
                                        return (
                                          <div className="x-status-danger mb-1">
                                            <p className="m-0 text-sm-light line-height-sm">
                                              Not Selected
                                            </p>
                                          </div>
                                        );
                                      })()}
                                      <p
                                        className="m-0 text-sm-light line-height-sm"
                                        style={{
                                          color: "var(--neutral-50)",
                                        }}
                                      >
                                        {item.created_at}
                                      </p>
                                    </div>
                                  ) : (
                                    <div className="col-9 p-0 ms-3">
                                      <SkeletonElement
                                        height={14}
                                        width="70%"
                                        count={4}
                                        className="my-2"
                                      />
                                    </div>
                                  )}
                                  {(() => {
                                    if (isLoadingNotif) {
                                      return (
                                        <div className="col-1 p-0">
                                          <SkeletonElement
                                            width={16}
                                            height={16}
                                            type="rounded-circle"
                                            className="x-read"
                                          />
                                        </div>
                                      );
                                    } else if (item.status === "unread") {
                                      return (
                                        <div className="col-1 p-0">
                                          <div className="x-read" />
                                        </div>
                                      );
                                    }
                                  })()}
                                </div>
                              );
                            }
                          })()}
                        </div>
                      );
                    } else {
                      return (
                        <div
                          className="x-update-status-read"
                          onClick={() => updateNotifRead(item.id_notif)}
                        >
                          <div
                            className="x-data cursor-pointer"
                            key={idx}
                            onClick={() =>
                              getNotif(
                                id_user,
                                isActiveType,
                                isActiveFilter,
                                isActiveSort
                              )
                            }
                          >
                            {(() => {
                              if (isActiveTab === "update") {
                                return (
                                  <div className="x-notif-card">
                                    <div className="col-1 p-0">
                                      {(() => {
                                        if (isLoadingNotif) {
                                          return (
                                            <SkeletonElement
                                              width={48}
                                              height={48}
                                              type="rounded-circle"
                                            />
                                          );
                                        } else if (
                                          item.profile_picture !== null &&
                                          item.profile_picture !== "" &&
                                          item.notif_from !== "1"
                                        ) {
                                          return (
                                            <img
                                              src={item.profile_picture}
                                              className="x-image-account"
                                              alt=""
                                            />
                                          );
                                        } else if (item.notif_from !== "1") {
                                          return (
                                            <div className="x-empty-image">
                                              <p
                                                className="text-xl-normal m-0"
                                                style={{
                                                  color: "var(--base-10)",
                                                }}
                                              >
                                                {item.full_name
                                                  .slice(0, 1)
                                                  .toUpperCase()}
                                              </p>
                                            </div>
                                          );
                                        }
                                        return (
                                          <div className="x-rounded-icons">
                                            <div className="x-notif-icon" />
                                          </div>
                                        );
                                      })()}
                                    </div>

                                    <div className="col-10 p-0">
                                      {(() => {
                                        if (isLoadingNotif) {
                                          return (
                                            <SkeletonElement
                                              width={300}
                                              height={16}
                                              className="ms-2"
                                            />
                                          );
                                        } else if (
                                          item.content.slice(
                                            0,
                                            item.full_name?.length
                                          ) === item.full_name
                                        ) {
                                          return (
                                            <p
                                              className="m-0 ms-2 text-lg-bold line-height-md"
                                              style={{
                                                color: "var(--neutral-80)",
                                              }}
                                            >
                                              {item.content.slice(
                                                0,
                                                item.full_name?.length
                                              )}
                                              {item.content.length >
                                              MAX_LENGTH ? (
                                                <span
                                                  className="m-0 text-lg-light line-height-md"
                                                  style={{
                                                    color: "var(--neutral-80)",
                                                  }}
                                                >
                                                  {item.content.slice(
                                                    item.full_name?.length,
                                                    MAX_LENGTH
                                                  )}
                                                  ..
                                                </span>
                                              ) : (
                                                <span
                                                  className="m-0 text-lg-light line-height-md"
                                                  style={{
                                                    color: "var(--neutral-80)",
                                                  }}
                                                >
                                                  {item.content.slice(
                                                    item.full_name?.length,
                                                    item.content.length
                                                  )}
                                                </span>
                                              )}
                                            </p>
                                          );
                                        }
                                        return (
                                          <div className="x-notif-icon">
                                            {item.content.length >
                                            MAX_LENGTH ? (
                                              <p
                                                className="m-0 ms-2 text-lg-light line-height-md"
                                                style={{
                                                  color: "var(--neutral-80)",
                                                }}
                                              >
                                                {item.content.slice(
                                                  0,
                                                  MAX_LENGTH
                                                )}
                                                ..
                                              </p>
                                            ) : (
                                              <p
                                                className="m-0 ms-2 text-lg-light line-height-md"
                                                style={{
                                                  color: "var(--neutral-80)",
                                                }}
                                              >
                                                {item.content}
                                              </p>
                                            )}
                                          </div>
                                        );
                                      })()}

                                      {!isLoadingNotif ? (
                                        <p
                                          className="m-0 ms-2 text-md-light line-height-md"
                                          style={{
                                            color: "var(--neutral-50)",
                                          }}
                                        >
                                          {item.created_at}
                                        </p>
                                      ) : (
                                        <SkeletonElement
                                          width={120}
                                          height={14}
                                          className="ms-2 mt-1"
                                        />
                                      )}
                                    </div>
                                    {(() => {
                                      if (isLoadingNotif) {
                                        return (
                                          <div className="col-1 p-0">
                                            <SkeletonElement
                                              width={14}
                                              height={14}
                                              type="rounded-circle"
                                            />
                                          </div>
                                        );
                                      } else if (item.status === "unread") {
                                        return (
                                          <div className="col-1 p-0">
                                            <div className="x-read" />
                                          </div>
                                        );
                                      }
                                    })()}
                                  </div>
                                );
                              } else if (isActiveTab === "activities") {
                                return (
                                  <div className="x-notif-card">
                                    <div className="col-2 p-0">
                                      <div className="x-activities-cover">
                                        {!isEmpty(item.data_ads?.image) ? (
                                          <img
                                            src={item.data_ads?.image}
                                            className="x-image cursor-pointer"
                                            alt=""
                                          />
                                        ) : (
                                          <img
                                            src={Image.src}
                                            className="x-image cursor-pointer"
                                            alt=""
                                          />
                                        )}
                                      </div>
                                    </div>
                                    <div className="col-9 p-0 ms-3">
                                      <p
                                        className="m-0 text-sm-normal line-height-sm mb-1"
                                        style={{
                                          color: "var(--neutral-50)",
                                        }}
                                      >
                                        {item.content}
                                      </p>
                                      <p
                                        className="m-0 text-md-bold line-height-sm mb-1"
                                        style={{
                                          color: "var(--neutral-70)",
                                        }}
                                      >
                                        {item.data_ads?.title}
                                      </p>
                                      {(() => {
                                        if (
                                          item.data_ads?.label_status ===
                                          "Terdaftar"
                                        ) {
                                          return (
                                            <div className="x-status-success mb-1">
                                              <p className="m-0 text-sm-light line-height-sm">
                                                Registered
                                              </p>
                                            </div>
                                          );
                                        } else if (
                                          item.data_ads?.label_status ===
                                          "Terpilih"
                                        ) {
                                          return (
                                            <div className="x-status-success mb-1">
                                              <p className="m-0 text-sm-light line-height-sm">
                                                Selected
                                              </p>
                                            </div>
                                          );
                                        }
                                        return (
                                          <div className="x-status-danger mb-1">
                                            <p className="m-0 text-sm-light line-height-sm">
                                              Not Selected
                                            </p>
                                          </div>
                                        );
                                      })()}
                                      <p
                                        className="m-0 text-sm-light line-height-sm"
                                        style={{
                                          color: "var(--neutral-50)",
                                        }}
                                      >
                                        {item.created_at}
                                      </p>
                                    </div>
                                    {(() => {
                                      if (item.status === "unread") {
                                        return (
                                          <div className="col-1 p-0">
                                            <div className="x-read" />
                                          </div>
                                        );
                                      }
                                    })()}
                                  </div>
                                );
                              }
                            })()}
                          </div>
                        </div>
                      );
                    }
                  })()}
                </div>
              );
            })}
          </div>
        </MainLayout>
        {renderModalCertificates()}
      </div>
    </Layout>
  );
};

export default appRoute(Notification);
