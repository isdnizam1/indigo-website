import React, { Fragment, useEffect, useState } from "react";
import { Container, Row, Form, Col } from "react-bootstrap";
import MediaBackground from "/public/assets/images/img-mediastreaming-header.png";
import {
  getAds,
  getVideoCategoryMedia,
  getPodcastCategory,
} from "/services/actions/api";
import classnames from "classnames";
import { getAuth, monthFormatter, apiDispatch } from "/services/utils/helper";
import MainLayout from "/components/layout/MainLayout";
import { useRouter } from "next/router";
import TabBar from "/components/home/TabBar";
import RightBarBox from "/components/home/RightBarBox";
import CardLv2 from "/components/home/CardLv2";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";
import { isEmpty } from "lodash-es";
import SkeletonElement from "/components/skeleton/SkeletonElement";

const MediaLearning = () => {
  const [isActiveTab, setIsActiveTab] = useState("sp_album");
  const [allCategory, setAllCategory] = useState([]);
  const { id_user } = getAuth();
  const [dataChecked, setDataChecked] = useState([]);
  const [queryParam, setQueryParam] = useState(null);
  const [medias, setMedias] = useState([]);
  const [isLoadingCategory, setIsLoadingCatergory] = useState(true);
  const [isLoadingMedias, setIsLoadingMedias] = useState(true);
  let dynamicMedias = isLoadingMedias ? [1, 2, 3] : medias;

  const path = {
    sp_album: "podcast",
    video: "video",
  };

  const tabItems = [
    {
      name: "PODCAST",
      type: "sp_album",
    },
    {
      name: "VIDEO",
      type: "video",
    },
  ];

  const { push } = useRouter();

  useEffect(() => {
    if (isActiveTab === "sp_album") {
      apiDispatch(
        getAds,
        {
          category: "sp_album",
          status: "active",
          id_viewer: id_user,
          id_sp_album_category: dataChecked,
        },
        true
      ).then((response) => {
        setIsLoadingMedias(false);
        if (response.code === 200) {
          const dataPodcast = response.result;
          setMedias(dataPodcast);
        }
      });
    } else if (isActiveTab === "video") {
      apiDispatch(
        getAds,
        {
          category: "video",
          status: "active",
          id_viewer: id_user,
          video_category: dataChecked,
        },
        true
      ).then((response) => {
        setIsLoadingMedias(false);
        if (response.code === 200) {
          const dataVideo = response.result;
          setMedias(dataVideo);
        }
      });
    }
  }, [isActiveTab, dataChecked]);

  useEffect(() => {
    if (isActiveTab === "sp_album") {
      apiDispatch(getPodcastCategory, { id_user }, true).then((response) => {
        setIsLoadingCatergory(false);
        if (response.code === 200) {
          const dataCatPodcast = response.result;
          setAllCategory(dataCatPodcast);
        }
      });
    } else if (isActiveTab === "video") {
      apiDispatch(getVideoCategoryMedia, { id_user }, true).then((response) => {
        setIsLoadingCatergory(false);
        if (response.code === 200) {
          const dataCatVideo = response.result;
          setAllCategory(dataCatVideo);
        }
      });
    }
  }, [isActiveTab]);

  useEffect(() => {
    let params = "";
    dataChecked.forEach((e) => {
      setQueryParam({ param: e });
    });
    setQueryParam(params);
  }, [dataChecked]);

  const MAX_LENGTH = 150;
  const handleFooter = (item) => {
    if (isActiveTab === "sp_album") {
      return "| Duration";
    } else if (isActiveTab === "video") {
      return `| ${item.total_view} x ditonton`;
    }
  };
  const handleTotal = (item) => {
    if (isActiveTab === "sp_album") {
      return `${item.total_podcast}`;
    } else if (isActiveTab === "video") {
      return `${item.total_video}`;
    }
  };
  const handleClick = (e) => {
    if (e.target.checked) {
      setDataChecked((prev) => [...prev, e.target.value]);
    } else {
      setDataChecked((prev) => {
        let prevData = [...prev];
        prevData = prevData.filter((item) => item !== e.target.value);
        return prevData;
      });
    }
  };

  const handleValue = (item) => {
    if (isActiveTab === "sp_album") {
      return `${item.id_sp_album_category}`;
    } else if (isActiveTab === "video") return `${item.category_name}`;
  };

  const renderRightBarCategories = (data) => {
    let dynamicData = isLoadingCategory ? [1, 2, 3] : data;
    if (isEmpty(data) && !isLoadingCategory){
      return null;
    }
    return (
      <Container>
          <RightBarBox
            title={
              isActiveTab === "sp_album"
                ? "Podcast Categories"
                : "Video Categories"
            }
          >
            <div className="py-3">
              {dynamicData.map((item, idx) => (
                <Fragment key={idx}>
                  <Form className="x-categories">
                    <Row className="m-0 py-1">
                      <Col md={10} className="p-0">
                        <Form.Check
                          onClick={handleClick}
                          name="podcast-category"
                          type={"checkbox"}
                          id={"category-" + idx}
                          label={
                            !isLoadingCategory ? <p className="text-md-normal m-0 cursor-pointer color-neutral-old-80">
                              {item.category_name}
                            </p> : <SkeletonElement width={150} height={14} />
                          }
                          value={handleValue(item)}
                        />
                      </Col>
                      <Col md={2} className="d-flex justify-content-end p-0">
                        <p className="text-md-normal cursor-pointer color-neutral-40 m-0">
                          {isActiveTab === "sp_album"
                            ? item?.total_podcast
                            : item?.total_video}
                        </p>
                      </Col>
                    </Row>
                  </Form>
                </Fragment>
              ))}
            </div>
          </RightBarBox>
      </Container>
    );
  };

  return (
    <Layout title="Media Learning - Indigo">
      <div className="x-medialearning-page">
        <MainLayout
          size="sm"
          Breadcrumb
          banner={MediaBackground.src}
          rightBarContent={renderRightBarCategories(allCategory)}
          rightBarClassName="x-rightbar-categories"
          StickyRightBar
        >
          <div className="mb-3">
            <TabBar
              tabItems={tabItems}
              isActiveTab={isActiveTab}
              onClick={(e) => setIsActiveTab(e.target.getAttribute("type"))}
            />

            {dynamicMedias?.map((item, idx) => (
              <div className="py-2" key={idx}>
                <CardLv2
                isLoading={isLoadingMedias}
                  image={item.image}
                  title={item.title}
                  onClick={() =>
                    push({
                      pathname: `/home/media-learning/${path[isActiveTab]}`,
                      query: { id: item.id_ads },
                    })
                  }
                  customSubTitle={
                    !isLoadingMedias ? <div
                      className="heading-sm-normal m-0 pt-2 color-neutral-old-40 line-height-md"
                      dangerouslySetInnerHTML={{
                        __html:
                          item.description.length > MAX_LENGTH
                            ? item.description.substring(0, MAX_LENGTH) + "..."
                            : item.description,
                      }}
                    /> : <SkeletonElement width="100%" height={14} className="mt-2" count={3}/>
                  }
                  customFooter={
                    <div className="d-flex justify-content-between align-items-center">
                      <div
                        className={classnames(
                          "d-flex align-items-center cursor-pointer x-web-pro m-0 x-media-footer"
                        )}
                      >
                        {!isLoadingMedias ?
                        <Fragment>
                        <p className="m-0 pe-2 heading-sm-normal color-neutral-old-40">
                          {monthFormatter(`"${item.created_at}"`)}
                        </p>
                        <p className="m-0 heading-sm-normal color-neutral-old-40">
                          {handleFooter(item)}
                        </p>
                        </Fragment> : <SkeletonElement width={150} height={16} />}
                      </div>
                      {/* {isActiveTab === "active" &&
                        !item.user_joined &&
                        item.available_seats > 0 && ( */}
                      {item.category === "sp_album" && (
                        <div
                          className="x-icon-play"
                          onClick={() =>
                            push({
                              pathname: `/home/media-learning/${path[isActiveTab]}`,
                              query: { id: item.id_ads },
                            })
                          }
                        />
                      )}
                      {/* )} */}
                    </div>
                  }
                />
              </div>
            ))}
          </div>
        </MainLayout>
      </div>
    </Layout>
  );
};

export default appRoute(MediaLearning);
