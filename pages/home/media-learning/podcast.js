import React, { Fragment, useEffect, useRef, useState } from "react";
import { getAdsDetail } from "/services/actions/api";
import { Button, Col, Row } from "react-bootstrap";
import { getAuth, apiDispatch } from "/services/utils/helper";
import MainLayout from "/components/layout/MainLayout";
import DefaultImage from "/public/assets/images/img-default-image.jpg";
import { isEmpty } from "lodash-es";
import AudioPlayer from "react-h5-audio-player";
import classnames from "classnames";

import previousIcon from "/public/assets/icons/icon-player-previous.svg";
import nextIcon from "/public/assets/icons/icon-player-next.svg";

import rewindIcon from "/public/assets/icons/icon-player-rewind.svg";
import forwardIcon from "/public/assets/icons/icon-player-forward.svg";

import playIcon from "/public/assets/icons/icon-player-play.svg";
import pauseIcon from "/public/assets/icons/icon-player-pause.svg";

import volumeOnIcon from "/public/assets/icons/icon-player-volumeon.svg";
import volumeOffIcon from "/public/assets/icons/icon-player-volumeoff.svg";

import { gsap } from "gsap";
import RightBarBox from "/components/home/RightBarBox";
import useKeypress from "react-use-keypress";
import { useRouter } from "next/router";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";

const DetailPodcast = () => {
  const [title, setTitle] = useState("");
  const [image, setImage] = useState(DefaultImage);
  const [description, setDescription] = useState("");
  const [buttonTitle, setButtonTitle] = useState("Play");
  const [speaker, setSpeaker] = useState([]);
  const [podcastCategories, setPodcastCategories] = useState([]);
  const [hidePlayer, setHidePlayer] = useState(true);
  const [currentEpisodeIdx, setCurrentEpisodeIdx] = useState(0);
  const [episodes, setEpisodes] = useState([]);
  const [episode, setEpisode] = useState({
    audio: "",
    title: "",
    speaker: "",
    image: "",
  });
  const [isMediaMinimize, setIsMediaMinimize] = useState(false);
  const [isDataLoaded, setIsDataLoaded] = useState(false);

  const playerRef = useRef();
  const mediaPlayer = playerRef?.current?.audio.current;
  const router = useRouter();
  const { id_user } = getAuth();

  useKeypress("Escape", () => {
    if (!isMediaMinimize && !hidePlayer) onMediaMinimize();
    else if (!hidePlayer) {
      mediaPlayer.pause();
      setHidePlayer(true);
      setCurrentEpisodeIdx(0);
      setButtonTitle("Play");
    }
  });

  useEffect(() => {
    if (!isDataLoaded && router.isReady) {
      getData(router.query.id);
      setIsDataLoaded(true);
    }
  }, [router.isReady]);

  const getData = (id_ads) => {
    apiDispatch(getAdsDetail, { id_ads, id_user }, true).then((response) => {
      if (response.code === 200) {
        const data = response.result;
        setTitle(data?.title);
        setImage(data?.image);
        setDescription(data?.description);
        setEpisodes(data?.additional_data_arr?.episodeList);
        setPodcastCategories(data?.podcast_category);
        setSpeaker(
          !isEmpty(data?.speaker)
            ? data?.speaker.map((speaker, idx) => {
                const currentIdx = idx + 1;
                const nextIdx = idx + 2;
                let join = "";

                if (nextIdx < data?.speaker?.length) join = ", ";
                else if (currentIdx === data?.speaker?.length) join = "";
                else if (currentIdx < data?.speaker?.length) join = " dan ";

                return speaker.title.concat(join);
              })
            : []
        );
      } else router.push("/home/media-learning");
    });
  };

  const onEpisodePlay = (idx) => {
    const prevHidePlayer = hidePlayer;
    const prevEpisodeIdx = currentEpisodeIdx;

    if (idx >= 0 && idx < episodes.length) {
      setCurrentEpisodeIdx(idx);
      setEpisode({
        ...episode,
        audio: episodes[idx].link,
        speaker: episodes[idx].speaker_name,
        title: episodes[idx].title,
        image: image,
      });
      setHidePlayer(false);

      if (prevEpisodeIdx !== idx) {
        setButtonTitle("Pause");
        mediaPlayer.play();
      } else {
        if (!mediaPlayer.paused && !prevHidePlayer) {
          setButtonTitle("Play");
          mediaPlayer.pause();
        } else {
          setButtonTitle("Pause");
          mediaPlayer.play();
        }
      }

      prevHidePlayer && onMediaMaximize(prevHidePlayer);
    }
  };

  const onMediaMinimize = () => {
    gsap.fromTo(
      ".x-audioplayer",
      { y: 0, opacity: 0 },
      {
        y: "88vh",
        duration: 0.6,
        opacity: 1,
        ease: "expo.out",
        onComplete: () => {
          setIsMediaMinimize(true);
        },
      }
    );
    gsap.fromTo(
      ".x-media-detail",
      { opacity: 1 },
      {
        opacity: 0,
        duration: 0.6,
        onComplete: () => {
          gsap.fromTo(
            ".x-media-player",
            { opacity: 0 },
            {
              opacity: 1,
              duration: 1,
            }
          );
        },
      }
    );
  };

  const onMediaMaximize = (isHidePlayer = false) => {
    setIsMediaMinimize(false);

    gsap.fromTo(
      ".x-media-detail",
      { opacity: 0 },
      {
        opacity: 1,
        duration: 1,
      }
    );
    gsap.fromTo(
      ".x-media-player",
      { opacity: 0 },
      {
        opacity: 1,
        duration: 0.8,
      }
    );
    gsap.fromTo(
      ".x-audioplayer",
      { y: isHidePlayer ? "100vh" : "88vh" },
      { y: 0, duration: 0.8, ease: "expo.out" }
    );
  };

  const renderBanner = () => {
    return (
      <div className="x-banner">
        <Row className="m-0 w-100">
          <Col lg={4} className="py-3">
            <img src={image} alt="" />
          </Col>
          <Col lg={8} className="py-3">
            <p
              className="text-lg-bold mb-1"
              style={{ color: "var(--base-10)" }}
            >
              ALBUM
            </p>
            <h2
              className="heading-lg-bolder mb-1"
              style={{ color: "var(--base-10)" }}
            >
              {title}
            </h2>
            <p
              className="text-lg-bold mb-4"
              style={{ color: "var(--base-10)" }}
            >
              {speaker}
            </p>
            <Button
              className="btn-primary"
              onClick={() => onEpisodePlay(currentEpisodeIdx)}
            >
              {buttonTitle}
            </Button>
          </Col>
        </Row>
      </div>
    );
  };

  const renderRightBarCategories = () => {
    return (
      <Fragment>
        {!isEmpty(podcastCategories) && (
          <RightBarBox title="Podcast Categories">
            <div className="x-categories">
              {podcastCategories.map((category, idx) => (
                <div className="x-tag" key={idx}>
                  <p className="text-lg-bold m-0 color-neutral-old-40">
                    {category}
                  </p>
                </div>
              ))}
            </div>
          </RightBarBox>
        )}
      </Fragment>
    );
  };

  return (
    <Layout title="Podcast - Indigo">
      <div className="x-podcast-page">
        <MainLayout
          size="sm"
          Breadcrumb
          customBanner={renderBanner()}
          rightBarContent={renderRightBarCategories()}
          rightBarClassName="x-rightbar-categories"
          CenteredRightBar
        >
          <div className="mx-3 mb-4">
            <p className="text-lg-bold mb-1 color-neutral-old-80">
              Tentang Podcast
            </p>
            <div
              className="text-lg-normal color-neutral-old-40"
              dangerouslySetInnerHTML={{ __html: description }}
            />
            <p className="text-lg-bold mb-3 mt-4 pt-2 color-neutral-old-80">
              Episode List
            </p>
            {episodes?.map((episode, idx) => (
              <div
                className="mb-1 cursor-pointer"
                key={idx}
                onClick={() => onEpisodePlay(idx)}
              >
                <p className="text-lg-bold mb-1 color-neutral-70">
                  {`${episode.id_episode}. ${episode.title}`}
                </p>
                <p className="text-md-normal mb-1 mt-2 color-neutral-old-40">
                  {`${episode.length} mins`}
                </p>
                <hr className="my-1" />
              </div>
            ))}
          </div>
        </MainLayout>
      </div>

      <div
        className={classnames(
          "x-audioplayer",
          hidePlayer && "x-visibility-hidden",
          isMediaMinimize && "x-padding"
        )}
      >
        {!isMediaMinimize && (
          <div className="x-media-detail">
            <div className="d-flex justify-content-between align-items-center pb-5">
              <div className="d-flex justify-content-center align-items-center">
                <div className="x-eventeer-icon" />
                <div>
                  <p className="text-md-normal m-0 line-height-md color-neutral-old-10">
                    NOW PLAYING FROM ALBUM
                  </p>
                  <p className="text-md-normal m-0 line-height-md color-neutral-old-10">
                    Podcast
                  </p>
                </div>
              </div>

              <div
                className="cursor-pointer user-select-none"
                onClick={() => onMediaMinimize()}
              >
                <span
                  className="material-icons-round font-weight-bold color-neutral-10"
                  style={{
                    fontSize: "28px",
                  }}
                >
                  expand_more
                </span>
              </div>
            </div>

            <Row className="w-100 mx-0 my-5">
              <Col
                lg={4}
                className="d-flex justify-content-center align-items-center"
              >
                <img className="x-image-episode" src={episode?.image} alt="" />
              </Col>
              <Col
                lg={8}
                className="d-flex flex-column justify-content-end pt-4"
              >
                <h1 className="heading-xl-bolder my-3 color-base-10">
                  {episode?.title}
                </h1>
                <p className="text-lg-bold m-0 color-base-10">
                  {episode?.speaker}
                </p>
              </Col>
            </Row>
          </div>
        )}

        <Row className="x-media-player">
          {isMediaMinimize && (
            <div className="x-player-desc">
              <img src={image} alt="" />
              <div>
                <p className="text-md-bold m-0 line-height-md color-base-10">
                  {episode?.title}
                </p>
                <p className="text-sm-normal m-0 line-height-md color-base-10">
                  {episode?.speaker}
                </p>
              </div>
            </div>
          )}
          <AudioPlayer
            ref={playerRef}
            src={episode?.audio}
            style={{ width: isMediaMinimize ? "76%" : "100%" }}
            progressJumpSteps={{ backward: 10000, forward: 10000 }}
            showSkipControls={true}
            onClickNext={() => onEpisodePlay(1 + currentEpisodeIdx)}
            onClickPrevious={() => onEpisodePlay(-1 + currentEpisodeIdx)}
            layout={!isMediaMinimize ? "stacked" : "stacked-reverse"}
            loop={false}
            customAdditionalControls={[]}
            customIcons={{
              play: (
                <div
                  className="x-icon-player"
                  style={{
                    backgroundImage: `url(${playIcon.src})`,
                  }}
                />
              ),
              pause: (
                <div
                  className="x-icon-player"
                  style={{
                    backgroundImage: `url(${pauseIcon.src})`,
                  }}
                />
              ),
              rewind: (
                <div
                  className="x-icon-player"
                  style={{
                    backgroundImage: `url(${rewindIcon.src})`,
                    height: "80%",
                  }}
                />
              ),
              forward: (
                <div
                  className="x-icon-player"
                  style={{
                    backgroundImage: `url(${forwardIcon.src})`,
                    height: "80%",
                  }}
                />
              ),
              next: (
                <div
                  className="x-icon-player"
                  style={{
                    backgroundImage: `url(${nextIcon.src})`,
                    height: "50%",
                  }}
                />
              ),
              previous: (
                <div
                  className="x-icon-player"
                  style={{
                    backgroundImage: `url(${previousIcon.src})`,
                    height: "50%",
                  }}
                />
              ),
              volume: (
                <div
                  className="x-icon-player"
                  style={{
                    backgroundImage: `url(${volumeOnIcon.src})`,
                    height: "100%",
                  }}
                />
              ),
              volumeMute: (
                <div
                  className="x-icon-player"
                  style={{
                    backgroundImage: `url(${volumeOffIcon.src})`,
                    height: "100%",
                  }}
                />
              ),
            }}
          />
          {isMediaMinimize && (
            <div
              className="d-flex justify-content-center align-items-center ps-5 cursor-pointer user-select-none"
              style={{ width: "4%" }}
              onClick={() => onMediaMaximize()}
            >
              <span
                className="material-icons-outlined color-base-10"
                style={{ fontSize: "28px" }}
              >
                expand_less
              </span>
            </div>
          )}
        </Row>
      </div>
    </Layout>
  );
};

export default appRoute(DetailPodcast);
