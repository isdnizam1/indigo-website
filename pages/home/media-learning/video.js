import React, { Fragment } from "react";
import { getAdsDetail } from "/services/actions/api";
import { useEffect, useRef, useState } from "react";
import { Button } from "react-bootstrap";
import { formatDate, getAuth, apiDispatch } from "/services/utils/helper";
import RightBarBox from "/components/home/RightBarBox";
import MainLayout from "/components/layout/MainLayout";
import VideoPlayer from "/components/home/media_learning/VideoPlayer";
import { findDOMNode } from "react-dom";
import screenfull from "screenfull";
import { useRouter } from "next/router";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";
import { isEmpty } from "lodash-es";

const DetailVideo = () => {
  const router = useRouter();
  const { id_user } = getAuth();
  const { id: id_ads } = router.query;
  const videoRef = useRef(null);
  const [data, setData] = useState({
    title: "",
    created_at: "",
    total_view: "",
    additional_data_arr: [],
    description: "",
  });

  useEffect(() => {
    router.isReady &&
      apiDispatch(getAdsDetail, { id_ads, id_user }, true).then((response) => {
        if (response.code === 200) {
          const data = response.result;
          setData(data);
        }
      });
  }, [router.isReady]);

  const handleFullscreen = () => {
    if (screenfull.isEnabled)
      screenfull.request(findDOMNode(videoRef.current.container));
  };

  const renderRightBarCategories = () => {
    return (
      <Fragment>
        {!isEmpty(video.category) && (
          <RightBarBox title="Video Categories">
            <div className="x-categories">
              {video.category.map((category, idx) => (
                <div className="x-tag" key={idx}>
                  <p className="text-lg-bold m-0 color-neutral-old-40">
                    {category}
                  </p>
                </div>
              ))}
            </div>
          </RightBarBox>
        )}
      </Fragment>
    );
  };

  const {
    title,
    created_at,
    total_view,
    additional_data_arr: video,
    description,
  } = data;

  return (
    <Layout title="Video - Indigo">
      <div className="x-video-page">
        <MainLayout
          size="sm"
          Breadcrumb
          rightBarContent={renderRightBarCategories()}
          rightBarClassName="x-rightbar-categories"
          CenteredRightBar
        >
          <div className="x-video-player">
            <VideoPlayer
              ref={videoRef}
              vidurl={video.url_video}
              thumb={video.thumbnail}
              fullscreen={true}
            />
          </div>

          <div className="mx-3">
            <div className="x-video-info">
              <h3 className="heading-md-bold color-neutral-old-80 my-3">
                {title}
              </h3>
              <div className="d-flex align-items-center">
                <div className="x-icon-eye me-2" />
                <p className="text-lg-normal color-grey-3 m-0">
                  {total_view} views
                  <span
                    className="material-icons-round px-2"
                    style={{ fontSize: "8px" }}
                  >
                    fiber_manual_record
                  </span>
                  {formatDate(created_at)}
                </p>
              </div>
              <div className="d-flex align-items-center">
                <div className="x-icon-clock me-2" />
                <p className="text-lg-normal color-grey-3 m-0">
                  Video duration : {video.video_duration}
                </p>
              </div>
            </div>

            <hr className="mb-4" />

            <div className="mb-5">
              <p className="text-lg-bold color-grey-3 mb-2">Tentang Video</p>
              <div
                className="text-lg-normal color-neutral-old-40"
                dangerouslySetInnerHTML={{ __html: description }}
              />
            </div>
            <div className="d-flex justify-content-center">
              <Button className="p-3 w-50" onClick={handleFullscreen}>
                Watch In Fullscreen
              </Button>
            </div>
          </div>
        </MainLayout>
      </div>
    </Layout>
  );
};

export default appRoute(DetailVideo);
