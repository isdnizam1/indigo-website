import React, { useEffect, useState } from "react";
import MainLayout from "/components/layout/MainLayout";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";
import { Button } from "react-bootstrap";
import { useRouter } from "next/router";
import {
  getAuth,
  apiDispatch,
  stringHtmlMentionParser,
} from "/services/utils/helper";
import { getAdsDetail } from "/services/actions/api";
import { isEmpty } from "lodash-es";
import { LoaderContent } from "components/loaders/Loaders";

const BannerDetail = () => {
  const [announcements, setAnnouncements] = useState({
    image: "",
    description: "",
    title: "",
    name: "",
    link: "",
  });
  const [isLoading, setIsLoading] = useState(true);
  const router = useRouter();
  const { id_user } = getAuth();

  // useEffect(() => getData(), []);
  useEffect(() => {
    if (router.isReady) {
      getData();
    }
  }, [router.isReady]);
  const getData = () => {
    apiDispatch(
      getAdsDetail,
      {
        id_ads: router.query.id,
        id_user: id_user,
      },
      true
    ).then((response) => {
      setIsLoading(false);
      if (response.code === 200) {
        const result = response.result;
        setAnnouncements(() => {
          return {
            title: result.title,
            image: result.image,
            description: result.description,
            name: result.additional_data_arr?.redirect.name,
            link: result.additional_data_arr?.redirect.link,
          };
        });
      }
    });
  };

  const { image, title, description, name, link } = announcements;

  return (
    <Layout title="Banner Detail - Eventeer">
      <MainLayout size="lg" Breadcrumb>
        <div className="x-banner-detail">
          <div className="x-banner-box">
            <LoaderContent
              isLoading={isLoading}
              width="100%"
              height={315}
              className="mb-2"
            >
              <img src={image} alt="banner" />
            </LoaderContent>
            <LoaderContent
              isLoading={isLoading}
              width="60%"
              height={28}
              className="my-3"
            >
              <h1 className="heading-lg-bold">{title}</h1>
            </LoaderContent>
            <LoaderContent
              isLoading={isLoading}
              width="100%"
              height={16}
              className="mb-2"
              count={4}
              Column
            >
              <div className="text-body">
                {stringHtmlMentionParser(description)}
              </div>
            </LoaderContent>
            <LoaderContent
              isLoading={isLoading}
              width="100%"
              height={32}
              className="my-2"
            >
              {!isEmpty(link) && (
                <Button onClick={() => router.push(link)}>
                  {isEmpty(name) ? "Learn More" : name}
                </Button>
              )}
            </LoaderContent>
          </div>
        </div>
      </MainLayout>
    </Layout>
  );
};

export default appRoute(BannerDetail);
