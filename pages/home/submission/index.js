import React, { Fragment, useEffect, useState } from "react";
import ImageSubHeader from "/public/assets/images/img-submission-header.png";
import DefaultCardImage from "/public/assets/images/img-default-image.jpg";
import {
  getAdsSubmission,
  getAdsSubmissionLocation,
} from "/services/actions/api";
import classnames from "classnames";
import { getAuth, apiDispatch } from "/services/utils/helper";
import MainLayout from "/components/layout/MainLayout";
import ModalConfirmation from "/components/modal/ModalConfirmation";
import { isEmpty } from "lodash-es";
import { useRouter } from "next/router";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";
import TabBar from "/components/home/TabBar";
import CardLv2 from "/components/home/CardLv2";
import SkeletonElement from "/components/skeleton/SkeletonElement";

const Submission = () => {
  const [data, setData] = useState({
    allSubmission: [],
    isStatus: "",
    isActiveLocation: "",
  });
  const [filterLocations, setFilterLocations] = useState([]);
  const [isActiveTab, setIsActiveTab] = useState("active");
  const [isLoadingLocation, setIsLoadingLocation] = useState(true);
  const [isLoadingSubmission, setIsLoadingSubmission]= useState(true);
  let dynamicAllSubmission = isEmpty(data.allSubmission) ? [1,2,3] : data.allSubmission.event_audition;

  const { id_user } = getAuth();
  const [modal, setModal] = useState(false);
  const MAX_LENGTH = 100;
  const router = useRouter();

  const tabItems = [
    {
      name: "SUBMISSION AVAILABLE",
      type: "active",
    },
    {
      name: "PREVIOUS SUBMISSION",
      type: "expired",
    },
  ];

  useEffect(() => {
    getSubmission("active", "");
    getLocation();
  }, []);

  const getSubmission = (status, location_name) => {
    apiDispatch(
      getAdsSubmission,
      { status, location_name, id_user },
      true
    ).then((response) => {
      setIsLoadingSubmission(false);
      if (response.code === 200) {
        const data = response.result;
        setData({
          ...data,
          allSubmission: data,
          isActiveLocation: location_name,
          isStatus: status,
        });
      } else {
        setData({
          ...data,
          allSubmission: [],
          isActiveLocation: "",
          isStatus: status,
        });
      }
    });
  };

  const getLocation = () => {
    apiDispatch(getAdsSubmissionLocation, { id_user }, true).then(
      (response) => {
        setIsLoadingLocation(false);
        if (response.code === 200) {
          const { result } = response;
          setFilterLocations(result);
        }
      }
    );
  };

  const renderModalFilterSearch = () => {
    return (
      <>
        <ModalConfirmation
          modalClass="x-filtersearch-modal"
          show={modal}
          onHide={() => setModal(false)}
          HasExitIcon
          title="Filter City/Location"
          subTitle="City/Location available"
          customBody={
            <>
              <div className="d-flex align-items-center flex-wrap gap-3 w-100 pt-1">
                <div
                  className={classnames(
                    "x-tag-location",
                    !data.isActiveLocation && "active"
                  )}
                  onClick={() => {
                    getSubmission(data.isStatus, "");
                    setModal(false);
                  }}
                >
                  <p className="text-lg-bold m-0 line-height-sm">All City</p>
                </div>
                {filterLocations?.map((item, idx) => (
                  <div
                    key={idx}
                    className={classnames(
                      "x-tag-location",
                      data.isActiveLocation === item && "active"
                    )}
                    onClick={() => {
                      getSubmission(data.isStatus, item);
                      setModal(false);
                    }}
                  >
                    <p className="text-lg-bold m-0 line-height-sm">{item}</p>
                  </div>
                ))}
              </div>
            </>
          }
        />
      </>
    );
  };

  return (
    <Layout title="Submission - Indigo">
      <div style={{ width: "100%", height: "100vh", overflowY: "scroll" }}>
        <div className="x-submission-page">
          <MainLayout size="sm" Breadcrumb banner={ImageSubHeader.src}>
            <div className="mb-3">
              <TabBar
                tabItems={tabItems}
                isActiveTab={isActiveTab}
                onClick={(e) => {
                  const type = e.target.getAttribute("type");
                  setIsActiveTab(type);
                  getSubmission(type, data.isActiveLocation);
                  type === "active" &&
                    isEmpty(filterLocations) &&
                    getLocation();
                }}
              />

              <div className="mb-3 mt-3">
                <div className="d-flex align-items-center">
                  <span
                    className="material-icons-round color-neutral-old-80"
                    style={{ fontSize: "20px" }}
                  >
                    location_on
                  </span>
                  {!isLoadingLocation ? <p className="text-lg-light line-height-sm color-neutral-70 m-0 ms-1">
                    {"Search at : "}
                    <span className="color-neutral-old-80">
                      {!isEmpty(data.isActiveLocation)
                        ? data.isActiveLocation
                        : "All City"}
                    </span>
                  </p> : <SkeletonElement width={150} height={16} />}
                  <div className="ms-auto">
                    <p
                      className="m-0 cursor-pointer heading-sm-normal color-neutral-old-80"
                      onClick={() => {
                        setModal(true);
                        getLocation();
                      }}
                    >
                      Edit
                    </p>
                  </div>
                </div>
              </div>

              {isLoadingSubmission ||!isEmpty(data.allSubmission.event_audition ) ? (
                dynamicAllSubmission.map((item, idx) => (
                  <div className="py-2" key={idx}>
                    <CardLv2
                    isLoading={isLoadingSubmission}
                      image={
                        !isEmpty(item.image) ? item.image : DefaultCardImage.src
                      }
                      customTitle={
                        <Fragment>
                          <div className="d-flex align-items-center mt-2 w-100">
                            <span
                              className="material-icons-round color-neutral-old-40"
                              style={{ fontSize: "20px" }}
                            >
                              location_on
                            </span>
                            {!isLoadingSubmission ? <p className="text-lg-light line-height-sm color-neutral-old-40 m-0 ms-2">
                              {JSON.parse(item.additional_data).location.name}
                            </p> : <SkeletonElement width={150} height={16} />}
                            <div className="ms-auto">
                              {item.day_left > 0 && (
                                <p className="x-countdown line-height-sm text-md-normal color-base-10 m-0">
                                  {item.day_left} Day Left
                                </p>
                              )}
                            </div>
                          </div>
                          {!isLoadingSubmission ? <h3 className="heading-md-bold m-0 mt-1">
                            {item.title}
                          </h3> : <SkeletonElement width="100%" height={22} className="py-1" />}
                        </Fragment>
                      }
                      customSubTitle={
                        <div className="d-flex pt-3">
                          <div className="x-icon-medal" />
                          {!isLoadingSubmission ? <div
                            className="text-lg-light color-neutral-70 m-0 ms-2 line-height-md"
                            dangerouslySetInnerHTML={{
                              __html:
                                item.description.length > MAX_LENGTH
                                  ? item.description.slice(0, MAX_LENGTH)
                                  : item.description,
                            }}
                          />: <SkeletonElement width={50} height={16} className="ms-2" />}
                        </div>
                      }
                      onClick={() =>
                        router.push({
                          pathname: "/home/submission/detail",
                          query: { id: item.id_ads },
                        })
                      }
                    />
                  </div>
                ))
              ) : (
                <div className="x-empty-submission">
                  <div className="x-rounded-icon">
                    <span className="material-icons-round">
                      insert_drive_file
                    </span>
                  </div>
                  <p className="heading-sm-bold color-neutral-70 m-0 mt-4">
                    There is no submission at this time
                  </p>
                  <p className="heading-sm-light color-neutral-old-40 m-0 mt-1">
                    Look forward submission for you
                  </p>
                </div>
              )}
            </div>
          </MainLayout>
        </div>
      </div>
      {renderModalFilterSearch()}
    </Layout>
  );
};

export default appRoute(Submission);
