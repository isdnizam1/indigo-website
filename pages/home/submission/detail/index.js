import React, { Fragment, useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { getAdsDetail } from "/services/actions/api";
import { getAuth, apiDispatch } from "/services/utils/helper";
import DefaultImage from "/public/assets/images/img-default-image.jpg";
import MainLayout from "/components/layout/MainLayout";
import ModalListUser from "/components/modal/ModalListUser";
import { useRouter } from "next/router";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";
import { isEmpty } from "lodash-es";
import BannerImage from "/public/assets/images/img-submissiondetail-banner.png";
import SkeletonElement from "/components/skeleton/SkeletonElement";

const SubmissionDetail = () => {
  const [data, setData] = useState({
    title: "",
    image: DefaultImage,
    createdBy: "",
    dateSession: "",
    location: "",
    benefits: [],
    status: "",
    termsCondition: "",
    interest: [],
    description: "",
    moreInformation: "",
    userIsJoined: false,
  });
  const [isLoadingData, setIsLoadingData] = useState(true);
  const [isHideTnc, setIsHideTnc] = useState(true);

  const [modal, setModal] = useState(false);
  const router = useRouter();
  const { id_user } = getAuth();
  const { id: id_ads } = router.query;

  const {
    title,
    image,
    createdBy,
    dateSession,
    location,
    benefits,
    termsCondition,
    interest,
    description,
    moreInformation,
    userIsJoined,
  } = data;

  const bannerLv3 = {
    banner: BannerImage.src,
    image: image,
    title: title,
    customTitle: (
      !isLoadingData ? <p className="m-0 heading-sm-light text-center color-neutral-old-40 pt-1">
        Promote by <span className="heading-sm-bold">{createdBy}</span>
      </p>: <SkeletonElement width={250} height={16} className="my-2" />
    ),
    customSubTitle: (
      <Fragment>
        <div className="d-flex align-items-center justify-content-center">
          <h3 className="heading-sm-light d-flex align-items-center color-neutral-old-40 m-0">
            <span className="material-icons color-secondary-main me-2">
              date_range
            </span>
            {!isLoadingData ? (dateSession && `${dateSession} days left`) : <SkeletonElement width={100} height={24} />}
          </h3>
          <span
            className="material-icons color-neutral-70 px-2"
            style={{ fontSize: "7px" }}
          >
            fiber_manual_record
          </span>
          {!isLoadingData ? <h3 className="heading-sm-light color-neutral-old-40 m-0">{location}</h3>
          : <SkeletonElement width={100} height={24} />}
        </div>
      </Fragment>
    ),
  };

  useEffect(() => getSubmissionDetail(), [router.isReady]);

  const getSubmissionDetail = () => {
    apiDispatch(getAdsDetail, { id_user, id_ads }, true).then((response) => {
      setIsLoadingData(false);
      if (response.code === 200) {
        const result = response.result;
        setData({
          ...data,
          title: result?.title,
          image: result?.image,
          createdBy: result?.vendor_name,
          dateSession: result?.day_left,
          location: result?.additional_data_arr?.location.name,
          benefits: result?.additional_data_arr?.benefit,
          termsCondition: result?.additional_data_arr?.terms_condition,
          interest: result?.additional_data_arr?.genre,
          description: result?.description,
          moreInformation: result?.additional_data_arr?.submission_reference,
          userIsJoined: result?.user_joined,
        });
      }
    });
  };

  const renderModal = () => {
    return (
      <>
        <ModalListUser
          backdrop={"static"}
          show={modal}
          onHide={() => setModal(false)}
          customTitle={
            <>
              <div className="x-like-user ms-auto mt-1">
                <div
                  className="x-icon-close cursor-pointer"
                  onClick={() => setModal(false)}
                />
              </div>
              <h1 className="heading-lg-bold">Request Sent</h1>
              <p
                className="pt-1 text-lg-normal"
                style={{ color: "var(--neutral-50)" }}
              >
                Thankyou for your enthusiasm for this session. You can check
                other session again.
              </p>
              <div className="d-flex justify-content-center">
                <Button
                  className="btn-primary m-2"
                  onClick={() => router.push("/home/agenda")}
                >
                  Find More Sessions
                </Button>
              </div>
            </>
          }
        />
      </>
    );
  };

  return (
    <Layout title="Submission Detail - Indigo">
      <div className="x-submissiondetail-page">
        <MainLayout size="sm" Breadcrumb bannerLv3={bannerLv3} isLoading={isLoadingData}>
          {/* benefit information */}
          {!isEmpty(benefits) && (
            <div className="mx-3">
              <p className="heading-sm-normal color-neutral-70 mb-1">
                Benefits :
              </p>
              {!isLoadingData ? <ul className="x-benefit-lists">
                {benefits.map((item, idx) => (
                  <li key={idx}>{item}</li>
                ))}
              </ul> : <SkeletonElement width="80%" height={20} className="my-2" count={3} />}
            </div>
          )}

          {/* term and condition */}
          {!isEmpty(termsCondition) && (
            <div className="x-termcondition-wrapper">
              {!isHideTnc ? (
                <>
                  <p className="text-lg-normal color-neutral-70">
                    Terms and Conditions
                  </p>
                  <div className="d-flex align-items-center">
                    <div
                      className="text-lg-light color-neutral-old-40 m-0"
                      dangerouslySetInnerHTML={{
                        __html: termsCondition,
                      }}
                    />
                  </div>
                  <div
                    className="d-flex mt-2 gap-1 cursor-pointer"
                    onClick={() => setIsHideTnc(true)}
                  >
                    <p className="text-lg-normal color-semantic-main m-0">
                      Hide
                    </p>
                    <span className="material-icons-round color-semantic-main">
                      keyboard_arrow_up
                    </span>
                  </div>
                </>
              ) : (
                <div
                  className="d-flex gap-1 cursor-pointer"
                  onClick={() => setIsHideTnc(false)}
                >
                  <p className="text-lg-normal color-semantic-main m-0">
                    View Terms and Conditions
                  </p>
                  <span className="material-icons-round color-semantic-main">
                    keyboard_arrow_down
                  </span>
                </div>
              )}
            </div>
          )}

          {/* interest */}
          {(isLoadingData || !isEmpty(interest)) && (
            <div className="mx-3">
              <p className="heading-sm-normal color-neutral-70 mb-1">
                Interest :
              </p>
               <div className="d-flex gap-2">
                {!isLoadingData ? interest.map((item, idx) => (
                  <div key={idx} className="x-interest-tag">
                    {item}
                  </div>
                )): <SkeletonElement width={150} height={24} className="my-2" count={3}/>}
              </div>
            </div>
          )}

          <hr className="my-4" />

          {/* description */}
          {(isLoadingData || !isEmpty(description)) && (
            <div className="mx-3 mb-4">
              <p className="heading-sm-normal color-neutral-70 mb-1">
                About this Audition :
              </p>
              {!isLoadingData ? <div className="d-flex align-items-center">
                <div
                  className="text-lg-light color-neutral-old-40 m-0"
                  dangerouslySetInnerHTML={{ __html: description }}
                />
              </div> : <SkeletonElement width="100%" height={16} className="my-2" count={3}/>}
            </div>
          )}

          {/* more information */}
          {!isEmpty(moreInformation) && (
            <div className="mx-3">
              <p className="heading-sm-normal color-neutral-70 mb-1">
                More information
              </p>
              {!isLoadingData ? <p className="text-lg-light color-neutral-old-40 m-0">
                {moreInformation}
              </p> : <SkeletonElement width="40%" height={20} className="my-2"/>}
            </div>
          )}
          <div className="d-flex justify-content-center my-5">
            <Button
              className="p-3 w-50"
              onClick={() =>
                router.push({
                  pathname: "/home/submission/detail/register",
                  query: { id: id_ads },
                })
              }
            >
              {userIsJoined ? "Change Submission Form" : "Register Now"}
            </Button>
          </div>
        </MainLayout>
      </div>
      {renderModal()}
    </Layout>
  );
};

export default appRoute(SubmissionDetail);
