import React, { Fragment, useEffect, useState } from "react";
import { Button, Form, Spinner } from "react-bootstrap";
import { getAdsDetail, submitSubmission } from "/services/actions/api";
import { getAuth, apiDispatch } from "/services/utils/helper";
import DefaultImage from "/public/assets/images/img-default-image.jpg";
import MainLayout from "/components/layout/MainLayout";
import ModalListUser from "/components/modal/ModalListUser";
import InputText from "/components/input/InputText";
import InputSelect from "/components/input/InputSelect";
import ModalSuccess from "/components/modal/ModalSuccess";
import ModalLoading from "/components/modal/ModalLoading";
import { useRouter } from "next/router";
import { isEmpty } from "lodash-es";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";
import BannerImage from "/public/assets/images/img-submissiondetail-banner.png";
import CardLv2 from "/components/home/CardLv2";
import DefaultCardImage from "/public/assets/images/img-default-image.jpg";

const SubmissionForm = () => {
  const { id_user, name_user } = getAuth();
  const router = useRouter();
  const { id: id_ads } = router.query;

  const [data, setData] = useState({
    title: "",
    image: DefaultImage.src,
    createdBy: "",
    dateSession: "",
    location: "",
  });
  const [forms, setForms] = useState([]);
  const [formOptions, setFormOptions] = useState([]);
  const [formDatas, setFormDatas] = useState({ id_ads, id_user, phone: "" });
  const [modalReshare, setModalReshare] = useState(false);
  const [modalSuccess, setModalSuccess] = useState(false);
  const [modalLoading, setModalLoading] = useState(false);
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);

  const { title, image, createdBy, dateSession, location } = data;

  const bannerLv3 = {
    banner: BannerImage.src,
    customDetail: (
      <CardLv2
        image={!isEmpty(image) ? image : DefaultCardImage.src}
        customTitle={
          <Fragment>
            <div className="d-flex align-items-center mt-2 w-100">
              <span
                className="material-icons-round color-neutral-old-40"
                style={{ fontSize: "20px" }}
              >
                location_on
              </span>
              <p className="text-lg-light line-height-sm color-neutral-old-40 m-0 ms-2">
                {location.name}
              </p>
              <div className="ms-auto">
                {dateSession > 0 && (
                  <p className="x-countdown line-height-sm text-md-normal color-base-10 m-0">
                    {dateSession} Day Left
                  </p>
                )}
              </div>
            </div>
            <h3 className="heading-md-bold m-0 mt-1">{title}</h3>
          </Fragment>
        }
      />
    ),
    // image,
    // title,
    // customTitle: (
    //   <p className="m-0 heading-sm-light text-center color-neutral-old-40 pt-1">
    //     Promote by <span className="heading-sm-bold">{createdBy}</span>
    //   </p>
    // ),
    // customSubTitle: (
    //   <Fragment>
    //     <div className="d-flex align-items-center justify-content-center">
    //       <h3 className="heading-sm-light d-flex align-items-center color-neutral-old-40 m-0">
    //         <span className="material-icons color-secondary-main me-2">
    //           date_range
    //         </span>
    //         {dateSession && `${dateSession} days left`}
    //       </h3>
    //       <span
    //         className="material-icons color-neutral-70 px-2"
    //         style={{ fontSize: "7px" }}
    //       >
    //         fiber_manual_record
    //       </span>
    //       <h3 className="heading-sm-light color-neutral-old-40 m-0">{location}</h3>
    //     </div>
    //   </Fragment>
    // ),
  };

  useEffect(() => getSubmissionDetail(), [router.isReady]);

  useEffect(() => {
    let validate = false;
    Object.values(formDatas).some((item) => {
      if (isEmpty(item) || item === undefined) validate = true;
    });
    setIsButtonDisabled(validate);
  }, [formDatas]);

  const getSubmissionDetail = () => {
    router.isReady &&
      apiDispatch(getAdsDetail, { id_user, id_ads }, true).then((response) => {
        if (response.code === 200) {
          const result = response.result;
          console.log(result);
          setData({
            ...data,
            title: result?.title,
            image: result?.image,
            createdBy: result?.vendor_name,
            dateSession: result?.day_left,
            location: result?.additional_data_arr?.location.name,
            form: result?.additional_data_arr?.submission_form,
          });
          const submissionForm = result?.additional_data_arr?.submission_form;
          const submissionData = result?.user_submission
            ? result.user_submission
            : {};
          const submissionAdditionalData = submissionData?.additional_data
            ? JSON.parse(submissionData.additional_data)
            : {};

          setForms(submissionForm);

          // define key value for form
          submissionForm?.map((item) => {
            setFormDatas((prev) => {
              console.log({ prev });
              return {
                ...prev,
                [item.form_name]: submissionAdditionalData[item.form_name]
                  ? submissionAdditionalData[item.form_name]
                  : "",
                id_ads,
                phone: submissionData?.phone ? submissionData.phone : "",
              };
            });
          });
        }
      });
  };

  const handleFormChange = (keyword, item) => {
    let datas = item?.data.filter((form) => {
      return form.toLowerCase().includes(keyword.toLowerCase());
    });

    setFormDatas({ ...formDatas, [item.form_name]: keyword });

    setFormOptions((prev) => {
      return {
        ...prev,
        [item.form_name]: datas,
      };
    });
  };

  const handleFormEnter = (item, suggestion) => {
    if (!suggestion) {
      for (let i = 0; i < item.data.length; i++) {
        if (
          item.data[i]
            ?.toLowerCase()
            .includes(formDatas[item.form_name]?.toLowerCase())
        ) {
          setFormDatas({
            ...formDatas,
            [item.form_name]: item.data[i],
          });

          setFormOptions((prev) => {
            return {
              ...prev,
              [item.form_name]: [],
            };
          });
          break;
        } else {
          setFormDatas({
            ...formDatas,
            [item.form_name]: "",
          });
        }
      }
    } else {
      setFormDatas({
        ...formDatas,
        [item.form_name]: suggestion,
      });

      setFormOptions((prev) => {
        return {
          ...prev,
          [item.form_name]: [],
        };
      });
    }
  };

  const postSubmission = () => {
    apiDispatch(submitSubmission, formDatas, true).then((response) => {
      response.code === 200 && submitSuccess();
    });
  };

  const renderModal = () => {
    return (
      <>
        <ModalListUser
          backdrop={"static"}
          show={modalReshare}
          onHide={() => setModalReshare(false)}
          customTitle={
            <>
              <div className="x-like-user ms-auto mt-1">
                <div
                  className="x-icon-close cursor-pointer"
                  onClick={() => setModalReshare(false)}
                />
              </div>
              <h1 className="heading-lg-bold">Submit Submission</h1>
              <p
                className="pt-1 text-lg-normal"
                style={{ color: "var(--neutral-50)" }}
              >
                Your submission process has not been completed. Continue to fill
                out the form?
              </p>
              <div className="justify-content-center">
                <Button
                  className="btn-primary m-2"
                  onClick={() => setModalReshare(false)}
                >
                  Continue
                </Button>
                <Button
                  className="btn m-2"
                  onClick={() => router.push("/home")}
                >
                  Cancel
                </Button>
              </div>
            </>
          }
        />
      </>
    );
  };

  const renderModalLoading = () => {
    return (
      <>
        <ModalLoading
          backdrop={"static"}
          show={modalLoading}
          onHide={() => setModalLoading(false)}
          customTitle={
            <>
              <Spinner
                animation="border"
                variant="primary"
                style={{ width: "100px", height: "100px" }}
              />

              <p
                className="pt-1 mt-2 text-lg-normal"
                style={{ color: "var(--neutral-80)" }}
              >
                Please wait a minute...
              </p>
            </>
          }
        />
      </>
    );
  };

  const renderModalSuccess = () => {
    return (
      <>
        <ModalSuccess
          backdrop={"static"}
          show={modalSuccess}
          onHide={() => setModalSuccess(false)}
          customTitle={
            <>
              <div className="x-icon-post" />

              <p
                className="pt-1 mt-2 text-lg-normal"
                style={{ color: "var(--neutral-80)" }}
              >
                Your Post Published !
              </p>
              <Button
                className="btn-primary m-2"
                onClick={() =>
                  router.push({
                    pathname: "/home/submission/detail",
                    query: { id: id_ads },
                  })
                }
              >
                Continue
              </Button>
            </>
          }
        />
      </>
    );
  };

  const submitSuccess = () => {
    setModalLoading(false);
    setModalSuccess(true);
  };

  const renderBannerSubmissionSubmit = () => {
    return (
      <div className="x-banner-wrapper">
        <img src={BannerImage.src} className="x-card-cover" alt="" />
        <div className="x-card">
          <img src={data.image} alt="img" />
          <div className="d-flex flex-column gap-1">
            <div className="d-flex align-items-center">
              <span
                className="material-icons color-neutral-old-40"
                style={{ fontSize: "20px" }}
              >
                location_on
              </span>
              <p className="m-0 ms-2 heading-sm-light color-neutral-old-40">
                {data.location}
              </p>
            </div>
            <h3 className="heading-md-bold mb-2 color-neutral-old-80">
              {data.title}
            </h3>
            <div className="m-0 align-items-center d-flex justify-content-start">
              <span
                className="material-icons color-secondary-main"
                style={{
                  fontsize: "10px",
                }}
              >
                date_range
              </span>
              <p className="m-0 ms-2 heading-sm-light color-neutral-old-40">
                {data.dateSession} days left
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  };

  return (
    <Layout title="Submission Detail - Indigo">
      <div className="x-submissionregister-page">
        <MainLayout
          size="sm"
          Breadcrumb
          customBanner={renderBannerSubmissionSubmit()}
        >
          <div className="mx-3">
            <Form>
              {/* profile section */}
              <Fragment>
                <p className="mb-3 text-lg-bold">PROFILE</p>
                <div className="card d-flex border-0">
                  <InputText title="Name" type="text" value={name_user} />
                </div>
                <div className="card d-flex border-0">
                  <InputText
                    title="No. Handphone"
                    type="tel"
                    value={formDatas?.phone}
                    onChangeText={(event) => {
                      const value = event.target.value;
                      const match = value.match(/[0-9]/);
                      (match || !value) &&
                        setFormDatas((prev) => {
                          return { ...prev, phone: value };
                        });
                    }}
                    placeholder="0812345678"
                  />
                </div>
              </Fragment>

              {/* submission form section */}
              {!isEmpty(forms) && (
                <Fragment>
                  <p className="mb-3 mt-4 text-lg-bold">SUBMISSION FORM</p>
                  {forms?.map((item, idx) => (
                    <Fragment key={idx}>
                      <div className="card d-flex border-0">
                        {item.type === "dropdown" || item.type === "select" ? (
                          <InputSelect
                            title={item.title}
                            value={formDatas[item.form_name]}
                            data={formOptions[item.form_name]}
                            onChangeText={(event) =>
                              handleFormChange(event.target.value, item)
                            }
                            onKeyPress={(event) =>
                              event.key === "Enter" && handleFormEnter(item)
                            }
                            onBlur={(event) => {
                              setTimeout(() => {
                                let match = false;
                                formOptions[item.form_name]?.forEach((form) => {
                                  if (form === event.target.value) match = true;
                                });
                                !match &&
                                  setFormDatas({
                                    ...formDatas,
                                    [item.form_name]: "",
                                  });
                                setFormOptions((prev) => {
                                  return {
                                    ...prev,
                                    [item.form_name]: [],
                                  };
                                });
                              }, 300);
                            }}
                            onFocus={() => {
                              setFormOptions((prev) => {
                                return {
                                  ...prev,
                                  [item.form_name]: item.data,
                                };
                              });
                            }}
                            suggestionOnClick={(gender) =>
                              handleFormEnter(item, gender)
                            }
                            placeholder={item.title}
                          />
                        ) : (
                          <InputText
                            title={item.title}
                            type={item.type === "number" ? "tel" : item.type}
                            value={formDatas[item.form_name]}
                            onChangeText={(event) => {
                              const value = event.target.value;
                              const match =
                                item.type !== "number"
                                  ? true
                                  : value.match(/[0-9]/);
                              (match || !value) &&
                                setFormDatas({
                                  ...formDatas,
                                  [item.form_name]: value,
                                });
                            }}
                            placeholder={item.title}
                          />
                        )}
                      </div>
                    </Fragment>
                  ))}
                </Fragment>
              )}

              <div className="d-flex justify-content-center my-5">
                <Button
                  className="p-3 w-50"
                  disabled={isButtonDisabled}
                  onClick={() => {
                    postSubmission();
                    setModalLoading(true);
                  }}
                >
                  Submit
                </Button>
              </div>
            </Form>
          </div>
        </MainLayout>
      </div>
      {renderModal()}
      {renderModalLoading()}
      {renderModalSuccess()}
    </Layout>
  );
};

export default appRoute(SubmissionForm);
