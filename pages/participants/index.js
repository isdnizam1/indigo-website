import { useState, useEffect } from "react";
import MainLayout from "/components/layout/MainLayout";
import { Button, Col } from "react-bootstrap";
import {
  getListContact,
  postProfileFollow,
  postProfileUnfollow,
  getProfileDetail,
} from "/services/actions/api";
import classnames from "classnames";
import { apiDispatch, getAuth } from "/services/utils/helper";
import { useDispatch } from "react-redux";
import InputText from "/components/input/InputText";
import { setChatData } from "/redux/slices/chatSlice";
import { useRouter } from "next/router";
import Layout from "/components/common_layout/Layout";
import appRoute from "../../components/routes/appRoute";
import { isEmpty } from "lodash-es";
import ProfileCard from "/components/profile/ProfileCard";
import CardUser from "/components/home/CardUser";
import { MdYoutubeSearchedFor } from "react-icons/md";

const Participants = () => {
  const [listContacts, setListContacts] = useState([]);
  const [contacts, setContacts] = useState([]);
  const [isFollowed, setIsFollowed] = useState(false);
  const [showProfileCard, setShowProfileCard] = useState(false);
  const [user, setUser] = useState({
    image: null,
    idUser: null,
    idGroupMessage: null,
    name: "",
    jobTitle: "",
    cityName: "",
    countryName: "",
    about: "",
    followersCount: 0,
    followingCount: 0,
    interests: [],
    idx: null,
  });

  const dispatch = useDispatch();
  const { push } = useRouter();
  const { id_user } = getAuth();

  useEffect(() => getData(), [isFollowed]);

  const getData = () => {
    apiDispatch(getListContact, { id_user }, true).then((response) => {
      if (response.code === 200) {
        const result = response.result;
        setContacts(result);
        setListContacts(result);
        setIsFollowed(result.status);
      }
    });
  };

  const handleAction = (item, idx, type) => {
    const action = {
      follow: {
        api: postProfileFollow,
        params: {
          id_user: item.id,
          followed_by: id_user,
        },
      },
      unfollow: {
        api: postProfileUnfollow,
        params: {
          id_user: id_user,
          id_user_following: item.id,
        },
      },
    };

    apiDispatch(action[type].api, action[type].params, true).then(
      (response) => {
        if (response.code === 200) {
          setIsFollowed(true);

          // setUser({...user, isFollowed: type === "follow" ? true : false});

          item.status =
            item.status === "followed" ? "not followed" : "followed";
          showProfileCard && handleProfileDetail(item, idx);
        }
      }
    );
  };

  const handleProfileDetail = (user, idx) => {
    apiDispatch(getProfileDetail, { id_user: user.id }, true).then(
      (response) => {
        if (response.code === 200) {
          const { result } = response;

          setUser({
            image: result.profile_picture,
            idUser: user.id,
            idGroupMessage: user.id_groupmessage,
            name: result.full_name,
            jobTitle: result.job_title,
            isFollowed: user.status === "followed" ? true : false,
            cityName: result.location.city_name,
            countryName: result.location.country_name,
            about: result.about_me,
            followersCount: result.total_followers,
            followingCount: result.total_following,
            interests: result.interest,
            idx,
          });

          setShowProfileCard(true);
        }
      }
    );
  };

  const handleSearch = (e) => {
    const keywords = e.target.value;
    let datas = contacts.filter((data) =>
      data.name.toLowerCase().includes(keywords.toLowerCase())
    );
    setListContacts(datas);
  }

  const rightBarProfileCard = () => {
    const {
      image,
      isFollowed,
      idUser,
      idGroupMessage,
      name,
      jobTitle,
      cityName,
      countryName,
      about,
      followersCount,
      followingCount,
      interests,
      idx,
    } = user;

    return (
      <ProfileCard
        className="x-profile-detail"
        onClick={() => push(`/profile?id=${idUser}`)}
        image={image}
        isFollowed={isFollowed}
        id={idUser}
        idGroupMessage={idGroupMessage}
        name={name}
        jobTitle={jobTitle}
        cityName={cityName}
        countryName={countryName}
        about={about}
        followersCount={followersCount}
        followingCount={followingCount}
        interests={interests}
        onFollowAction={() =>
          handleAction(
            listContacts[idx],
            idx,
            listContacts[idx].status === "not followed" ? "follow" : "unfollow"
          )
        }
      />
    );
  };

  return (
    <Layout title="Participant - Indigo">
      <div className="x-participant-page">
        <MainLayout
          size="md"
          rightBarContent={showProfileCard && rightBarProfileCard()}
          rightBarClassName="x-rightbar-profilecard"
          StickyRightBar
          CenteredRightBar
        >
          <div className="d-flex justify-content-between align-items-center m-0 mt-5">
            <p className="heading-md-bold color-neutral-900 m-0">
              Participant List
            </p>
            <InputText
              className="x-input-search"
              disableError={true}
              placeholder="Search participants"
              inlineIconRight="search"
              onChangeText={handleSearch}
            />
          </div>

          <div className="x-participant-wrapper">
            {!isEmpty(listContacts) ? (
              listContacts.map((item, idx) => (
                <div className="x-participant-card" key={idx}>
                  <Col
                    xs={7}
                    className="cursor-pointer"
                    onClick={() => handleProfileDetail(item, idx)}
                  >
                    <CardUser
                      image={item.avatar}
                      name={item.name}
                      jobTitle={item.job_title}
                      isAdminTenant={item.is_admin_tenant}
                    />
                  </Col>
                  <Col xs={5} className="d-flex justify-content-end">
                    {item.id !== id_user && (
                      <div className="d-flex align-items-center gap-4">
                        <Button
                          className="btn-delete"
                          onClick={() => {
                            dispatch(
                              setChatData({
                                selectedMessage: {
                                  id_user: item.id,
                                  full_name: item.name,
                                  image: item.avatar,
                                  id_groupmessage: item.id_groupmessage,
                                  type: "personal",
                                },
                              })
                            );
                            push({
                              pathname: "/chat",
                              query: {
                                id_message: !item.id_groupmessage
                                  ? false
                                  : item.id_groupmessage,
                                message_type: "message",
                                type: "personal",
                              },
                            });
                          }}
                        >
                          Message
                        </Button>

                        <Button
                          className={classnames(
                            item.status === "not followed"
                              ? "btn-primary"
                              : "btn-delete"
                          )}
                          onClick={() =>
                            handleAction(
                              item,
                              idx,
                              item.status === "not followed"
                                ? "follow"
                                : "unfollow"
                            )
                          }
                        >
                          {item.status === "not followed"
                            ? "Follow"
                            : "Followed"}
                        </Button>
                      </div>
                    )}
                  </Col>
                </div>
              ))
            ) : (
              <div className="x-emptystate-lv2">
                <div className="x-icon-wrapper">
                  <MdYoutubeSearchedFor
                    size={96}
                    color="var(--semantic-main)"
                    className="p-3"
                  />
                </div>
                <h3 className="heading-md-bold color-neutral-900">
                  {"Ups! Something wrong :("}
                </h3>
                <p className="text-lg-light color-neutral-500">
                  please check the name again
                </p>
              </div>
            )}
          </div>
        </MainLayout>
      </div>
    </Layout>
  );
};

export default appRoute(Participants);
