import React, { Fragment, useEffect, useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { getEventList } from '/services/actions/api';
import { apiDispatch } from "/services/utils/helper"
import EventSelectionList from '/components/event_selection/EventSelectionList';
import { getAuth } from '/services/utils/helper';
import AuthLayout from '/components/layout/AuthLayout';
import Layout from "/components/common_layout/Layout";
import { isEmpty } from "lodash-es";
import appRoute from "/components/routes/appRoute";
import authRoute from "/components/routes/authRoute";

const { event_title } = getAuth();

const EventSelections = () => {
  const [eventLists, setEventLists] = useState([]);
  const { id_user } = getAuth();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => getData(), []);

  const getData = () => {
    apiDispatch(getEventList, {
      status: "active",
      id_user
    }, true).then(response => {
      setIsLoading(false);
      if (response.code === 200) {
        const result = response.result;
        setEventLists(result);
      }
    }).catch(err => console.log(err));
  };

  return (
    <Layout title="Event Selection - Indigo">
      <AuthLayout>
        <div className="x-eventselection-page">
          <Container className="d-flex justify-content-center align-items-center w-100 h-100">
            <Row className="d-flex justify-content-center align-items-center w-100 h-100">
              <Row>
                <Col md={7}>
                  <h1 className="heading-xl-bold top-40 mb-3">
                    Event Selection
                  </h1>
                  <p className="pb-5 text-lg-light">
                    Looking for activity to do? Whether you're a local, new in
                    town or just cruising through we've got loads of great tips
                    and events. You can explore by location, what's popular, our
                    top picks, free stuff... you got this. Ready?
                  </p>
                </Col>
              </Row>

              <Row className="top-0">
                {!isLoading ? eventLists.map((row, idx) => (
                  <Fragment key={idx}>
                    <EventSelectionList data={row} isLoading={false}/>
                  </Fragment>
                )):
                Array(6).fill(0).map((item, idx) => (
                <Fragment key={idx}>
                    <EventSelectionList isLoading={true}/>
                  </Fragment>))}
              </Row>
            </Row>
          </Container>
        </div>
      </AuthLayout>
    </Layout>
  );
};

export default !isEmpty(event_title) ? appRoute(EventSelections) : authRoute(EventSelections);
