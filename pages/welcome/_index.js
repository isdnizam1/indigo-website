import { Component, Fragment } from 'react';
import HeaderLv0 from '/components/header/HeaderLv0';
import classnames from "classnames";
import { Enhance } from "/services/Enhancer";
import { Button } from 'react-bootstrap';
import { Route, Link } from "react-router-dom";

class Welcome extends Component {
  render() {
    const { isMobile } = this.props.dimension;

    return (
      <Fragment>
        <div className="x-welcome-page">
          <HeaderLv0 disableRoute={false} isWelcomeScreen={true} />

          <div className={classnames("x-welcome-page-form", isMobile && "pt-5")}>
            <h1 className={isMobile ? 'heading-lg-bold' : 'heading-xl-bold pb-2'}>Manage your</h1>
            <h1 className={classnames("mb-5", isMobile ? 'heading-lg-bold' : 'heading-xl-bold')}>Event Everywhere</h1>

            <Route className="pt-4" render={({history}) => (
              <Button className="btn-secondary mt-4" onClick={() => history.push("/login")}>
                <span className="material-icons-outlined me-4" style={{fontSize: isMobile && "24px"}}>email</span>

                <p className={classnames("m-0", isMobile ? 'text-lg-bold' : 'text-xl-bold')}>Continue with Email</p>
              </Button>
            )}/>

            <div className="x-form-separator">
              <div className="x-separator" />

              <p className="text-sm-bold ps-3 pe-3 m-0">OR</p>

              <div className="x-separator" />
            </div>

            <div className="d-flex">
              <p className={classnames("font-italic", isMobile ? "text-lg-light" : "text-xl-light")}>Belum punya akun?</p>
              <Link to="/register"><p className={classnames("ps-2 m-0", isMobile ? "text-lg-bold" : "text-xl-bold")} style={{color: "var(--base-10)"}}>Daftar disini</p></Link>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Enhance(Welcome);
