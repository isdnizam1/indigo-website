import React, { useState, useEffect, Fragment } from "react";
import { Row, Col, Button, DropdownButton, Dropdown } from "react-bootstrap";
import InputText from "/components/input/InputText";
import {
  getFeeds,
  postDeleteJourney,
  getNewActivities,
  getProfileDetail,
  getListTopics,
} from "/services/actions/api";
import ModalConfirmation from "/components/modal/ModalConfirmation";
import classnames from "classnames";
import {
  getAuth,
  formatDate,
  apiDispatch,
  stringHtmlMentionParser,
} from "/services/utils/helper";
import MainLayout from "/components/layout/MainLayout";
import { useRouter } from "next/router";
import { isEmpty } from "lodash-es";
import { MAPPER_FEEDS_EVENT_DATA } from "/services/constants/Constants";
import Layout from "/components/common_layout/Layout";
import appRoute from "/components/routes/appRoute";
import RightBarBox from "/components/home/RightBarBox";
import PostCard from "/components/cards/PostCard";
import ModalOnSubmit from "/components/modal/ModalOnSubmit";
import ModalPostFeed from "/components/modal/ModalPostFeed";
import { HiTrash } from "react-icons/hi";
import { FaEdit } from "react-icons/fa";

const Feeds = () => {
  const [modalFeedPost, setModalFeedPost] = useState(false);
  const [modalDeleteFeed, setModalDeleteFeed] = useState(false);
  const [modalDeleteFeedOnSubmit, setModalDeleteFeedOnSubmit] = useState(false);
  const [idJourney, setIdJourney] = useState(null);

  const [isLoading, setIsLoading] = useState(false);
  const [isActiveTab, setIsActiveTab] = useState("community");
  const [reshare, setReshare] = useState({});
  const [editData, setEditData] = useState({});

  const [feeds, setFeeds] = useState([]);
  const [activities, setActivities] = useState([]);
  const [user, setUser] = useState({});
  const [topics, setTopics] = useState([]);

  const { id_user } = getAuth();

  const { push, query, isReady } = useRouter();

  const { id } = query;

  const pathDetail = (category) => {
    if (category === "collaboration") {
      return "/home/collaboration/detail";
    } else if (category === "article") {
      // FIXME: redirect to detail article when clicked
      return "/home";
    } else if (category === "submission") {
      return "/home/submission/detail";
    } else if (category === "sc_session") {
      return "/home/agenda/detail";
    } else if (category === "sp_album") {
      return "/home/media-learning/podcast";
    } else if (category === "video") {
      return "/home/media-learning/video";
    }
  };

  useEffect(() => getData(), []);
  useEffect(() => isReady && getFeedLists(0, 10), [id]);

  const getData = () => {
    getActivities();
    getProfile();
    getTopics();
  };

  const getFeedLists = (start, limit, role = isActiveTab) => {
    apiDispatch(
      getFeeds,
      { role, id_user, start, limit, id_topic: id },
      true
    ).then((response) => {
      if (response.code === 200) {
        const data = response.result;
        const newData = data.map((item) => {
          item.created_at = formatDate(item.created_at);
          if (item.source === "journey")
            item.data_journey.created_at = formatDate(
              item.data_journey.created_at
            );
          if (item.topic_name !== undefined)
            item.topic_name = item.topic_name.split(",");
          return item;
        });
        setFeeds(start === 0 ? newData : feeds.concat(newData));
      }
    });
  };

  const getProfile = () => {
    apiDispatch(getProfileDetail, { id_user }, true).then((response) => {
      response.code === 200 && setUser(response.result);
    });
  };

  const getTopics = () => {
    apiDispatch(getListTopics, { id_user }, true).then((response) => {
      response.code === 200 && setTopics(response.result);
    });
  };

  const getActivities = () => {
    apiDispatch(getNewActivities, { id_user }, true).then((response) => {
      response.code === 200 && setActivities(response.result);
    });
  };

  const handleScroll = (e) => {
    const { scrollHeight, scrollTop, clientHeight } = e.target;
    const bottom = Math.abs(scrollHeight - (scrollTop + clientHeight)) <= 1;
    bottom && getFeedLists(feeds.length + 1, 5);
  };

  const postDeleteFeed = (id) => {
    setIsLoading(true);
    setModalDeleteFeed(false);

    setModalDeleteFeedOnSubmit(true);
    apiDispatch(postDeleteJourney, { id_journey: id }, true).then(
      (response) => {
        if (response.code === 200) {
          setIsLoading(false);
          setFeeds([]);
          getFeedLists(0, 10);
        }
      }
    );
  };

  const renderRightBarActivities = () => {
    return (
      <Fragment>
        <RightBarBox title="🎉&nbsp;&nbsp;&nbsp;New Activities">
          <div className="px-4 py-3">
            {activities?.sp_album?.map((item, idx) => (
              <Fragment key={idx}>
                {idx === 0 && (
                  <div className="d-flex pt-1">
                    <span
                      className="material-icons-outlined color-semantic-main pe-2"
                    >
                      podcasts
                    </span>
                    <p className="heading-sm-bold color-semantic-main">
                      Podcast
                    </p>
                  </div>
                )}
                <p
                  className="text-lg-light mb-2 cursor-pointer user-select-none"
                  onClick={() =>
                    push({
                      pathname: "/home/media-learning/podcast",
                      query: {
                        id: item.id_ads,
                      },
                    })
                  }
                >
                  {item.title}
                </p>
              </Fragment>
            ))}
            {activities?.video?.map((item, idx) => (
              <Fragment key={idx}>
                {idx === 0 && (
                  <div className="d-flex pt-3">
                    <span
                      className="material-icons-outlined color-semantic-main pe-2"
                    >
                      video_label
                    </span>
                    <p className="heading-sm-bold color-semantic-main">
                      Video
                    </p>
                  </div>
                )}
                <p
                  className="text-lg-light mb-2 cursor-pointer user-select-none"
                  onClick={() =>
                    push({
                      pathname: "/home/media-learning/video",
                      query: {
                        id: item.id_ads,
                      },
                    })
                  }
                >
                  {item.title}
                </p>
              </Fragment>
            ))}
          </div>
        </RightBarBox>
      </Fragment>
    );
  };

  const renderModalPostFeed = () => {
    return (
      <ModalPostFeed
        show={modalFeedPost}
        isAdminTenant={user?.is_admin_tenant}
        onHide={() => setModalFeedPost(false)}
        onShow={() => setModalFeedPost(true)}
        feedReshareData={reshare}
        feedEditData={editData}
        topics={topics}
        role={isActiveTab}
        onSuccess={() => getFeedLists(0, 10)}
      />
    );
  };

  const renderModalDeletePost = () => {
    return (
      <Fragment>
        <ModalConfirmation
          backdrop="static"
          size="md"
          show={modalDeleteFeed}
          onHide={() => setModalDeleteFeed(false)}
          modalClass="x-postfeeds-modalconfirmation"
          title="Hapus post?"
          subTitle="Apakah kamu yakin untuk menghapus post ini secara permanen?"
          customBody={
            <div className="d-flex justify-content-center gap-3 pt-2">
              <Button
                className="btn-delete w-auto"
                onClick={() => postDeleteFeed(idJourney)}
              >
                Hapus
              </Button>
              <Button
                className="w-auto"
                onClick={() => setModalDeleteFeed(false)}
              >
                Batalkan
              </Button>
            </div>
          }
        />
        <ModalOnSubmit
          backdrop={isLoading ? "static" : true}
          size="md"
          show={modalDeleteFeedOnSubmit}
          subTitle="Post has been deleted"
          onHide={() => !isLoading && setModalDeleteFeedOnSubmit(false)}
          isLoading={isLoading}
          ctaText="Back to feed"
          action="delete"
          cta={() => setModalDeleteFeedOnSubmit(false)}
        />
        cta={() => setModalDeleteFeedOnSubmit(false)}
      </Fragment>
    );
  };

  return (
    <Layout title="Feeds - Eventeer">
      <div
        style={{ width: "100%", height: "100vh", overflowY: "scroll" }}
        onScroll={handleScroll}
      >
        <div className="x-feeds-page">
          <MainLayout
            size="md"
            rightBarContent={renderRightBarActivities()}
            rightBarClassName="x-rightbar-activities"
          >
            <div className="x-content-feeds">
              <div className="x-feeds-navbar">
                <nav className="navbar navbar-expand-lg navbar-light">
                  <ul className="navbar-nav me-auto">
                    <li className="nav-item">
                      <p
                        className={classnames(
                          "nav-link m-0 cursor-pointer heading-sm-bold",
                          isActiveTab === "community" ? "active" : null
                        )}
                        onClick={() => {
                          setIsActiveTab("community");
                          setFeeds([]);
                          getFeedLists(0, 10, "community");
                        }}
                      >
                        Community
                      </p>
                    </li>
                    <li className="nav-item">
                      <p
                        className={classnames(
                          "nav-link m-0 cursor-pointer heading-sm-bold",
                          isActiveTab === "circle" ? "active" : null
                        )}
                        onClick={() => {
                          setIsActiveTab("circle");
                          setFeeds([]);
                          getFeedLists(0, 10, "circle");
                        }}
                      >
                        Circle
                      </p>
                    </li>
                  </ul>
                </nav>
              </div>

              <div
                className="x-card-post"
                onClick={() => {
                  setEditData({});
                  setReshare({});
                  setModalFeedPost(true);
                }}
              >
                <div className="x-postwrapper-absolute" />
                <div className="d-flex py-2 px-3 gap-3">
                  <div className="x-profile-picture">
                    {!isEmpty(user.profile_picture) ? (
                      <img src={user.profile_picture} alt="" />
                    ) : (
                      <span className="material-icons">account_circle</span>
                    )}
                  </div>

                  <InputText
                    as="textarea"
                    disableError
                    placeholder={"I am so excited right now !"}
                  />
                </div>

                <div className="x-footer-post">
                  <div className="w-100 px-3 py-2">
                    <div className="d-flex justify-content-between">
                      <div className="d-flex gap-3">
                        <div className="mt-2 mb-2">
                          <div className="d-flex align-items-center cursor-pointer">
                            <span className="material-icons-round color-semantic-main">
                              image
                            </span>
                            <p className="m-0 ms-2 heading-sm-normal color-semantic-main">
                              Image
                            </p>
                          </div>
                        </div>
                        <div className="mt-2 mb-2">
                          <div className="d-flex align-items-center">
                            <span className="material-icons-round color-semantic-main">
                              keyboard_arrow_down
                            </span>
                            <p className="m-0 ms-1 heading-sm-normal color-semantic-main">
                              Add topics
                            </p>
                          </div>
                        </div>
                      </div>
                      <Button className="w-auto">Post</Button>
                    </div>
                  </div>
                </div>
              </div>

              <Row>
                <Col lg={12}>
                  {feeds.map((data, idx) => (
                    <Fragment key={idx}>
                      <PostCard
                        image={data.profile_picture}
                        name={data.full_name}
                        className="x-feed-card"
                        idTimeline={data.id_timeline}
                        jobTitle={data.job_title}
                        isAdminTenant={data.is_admin_tenant}
                        customInformation={
                          <p className="text-sm-normal m-0 line-height-sm color-neutral-60 line-height-md">
                            {data.created_at}
                          </p>
                        }
                        onCtaCustom={
                          <Fragment>
                            {
                            (id_user === data.id_user ||
                              user?.is_admin_tenant) &&
                              (
                              <DropdownButton
                                className="x-card-dropdown"
                                title=""
                                onClick={(e) => e.stopPropagation()}
                              >
                                {user?.is_admin_tenant && (
                                  <Dropdown.Item
                                    onClick={() => {
                                      setReshare({});
                                      setEditData(data);
                                      setModalFeedPost(true);
                                    }}
                                  >
                                    <FaEdit size={20} color="var(--neutral-600)" />
                                    <p className="text-lg-bold color-neutral-old-50 m-0">
                                      Edit post
                                    </p>
                                  </Dropdown.Item>
                                )}

                                <Dropdown.Item
                                  onClick={() => {
                                    setIdJourney(data.id_journey);
                                    setModalDeleteFeed(true);
                                  }}
                                >
                                  <HiTrash size={20} color="var(--neutral-600)" />
                                  <p className="text-lg-bold color-neutral-old-50 m-0">
                                    Delete post
                                  </p>
                                </Dropdown.Item>
                              </DropdownButton>
                            )}
                          </Fragment>
                        }
                        title={stringHtmlMentionParser(data.description)}
                        customContent={
                          <Fragment>
                            {!isEmpty(data.attachment) && (
                              <div className="x-contentfeeds-wrapper">
                                <Row className="x-image-row">
                                  {data.attachment.map((item, idx) => {
                                    const length = data.attachment.length;
                                    const modulus = length % 3;
                                    const maxLoop =
                                      !modulus || length > 5 ? 3 : 2;
                                    const isEvenRow =
                                      length === 1 ||
                                      length === 2 ||
                                      length === 4 ||
                                      length === 5;
                                    return (
                                      <Fragment key={idx}>
                                        {idx < maxLoop && (
                                          <Col className="p-1 d-flex" key={idx}>
                                            <img
                                              className={classnames(
                                                length === 1
                                                  ? "x-height-xl"
                                                  : isEvenRow && "x-height-lg"
                                              )}
                                              src={item.attachment_file}
                                              alt=""
                                            />
                                          </Col>
                                        )}
                                      </Fragment>
                                    );
                                  })}
                                </Row>
                                <Row className="x-image-row">
                                  {data.attachment.length > 2 &&
                                    data.attachment.map((item, idx) => {
                                      const length = data.attachment.length;
                                      const modulus = length % 3;
                                      const maxLoop =
                                        !modulus || length > 6 ? 3 : 2;
                                      return (
                                        <Fragment key={idx}>
                                          {idx >= maxLoop && idx < 6 && (
                                            <Col className="p-1 d-flex">
                                              <img
                                                className={classnames(
                                                  length === 4 && "x-height-lg"
                                                )}
                                                src={item.attachment_file}
                                                alt=""
                                              />
                                            </Col>
                                          )}
                                        </Fragment>
                                      );
                                    })}
                                </Row>
                              </div>
                            )}
                            {!isEmpty(data.data_journey) && (
                              <div className="x-contentfeeds-wrapper">
                                <PostCard
                                  image={data.data_journey.profile_picture}
                                  name={data.data_journey.full_name}
                                  jobTitle={data.data_journey.job_title}
                                  isAdminTenant={data.data_journey.is_admin_tenant}
                                  customInformation={
                                    <p className="text-sm-normal m-0 line-height-sm color-neutral-60 p-0">
                                      {data.data_journey.created_at}
                                    </p>
                                  }
                                  title={stringHtmlMentionParser(data.data_journey.description)}
                                  customContent={
                                    <div
                                      className="x-contentfeeds-wrapper cursor-pointer"
                                      onClick={() => {
                                        push({
                                          pathname: pathDetail(
                                            data.data_journey.data_ads.category
                                          ),
                                          query: {
                                            id: data.data_journey.data_ads
                                              .id_ads,
                                          },
                                        });
                                      }}
                                    >
                                      {!isEmpty(
                                        data.data_journey.attachment
                                      ) && (
                                        <div className="mb-2">
                                          <Row className="x-image-row">
                                            {data.data_journey.attachment.map(
                                              (item, idx) => {
                                                const length =
                                                  data.data_journey.attachment
                                                    .length;
                                                const modulus = length % 3;
                                                const maxLoop =
                                                  !modulus || length > 5
                                                    ? 3
                                                    : 2;
                                                const isEvenRow =
                                                  length === 1 ||
                                                  length === 2 ||
                                                  length === 4 ||
                                                  length === 5;
                                                return (
                                                  <Fragment key={idx}>
                                                    {idx < maxLoop && (
                                                      <Col className="p-1 d-flex">
                                                        <img
                                                          className={classnames(
                                                            isEvenRow &&
                                                              "x-height-lg",
                                                            length === 1 &&
                                                              "x-height-xl"
                                                          )}
                                                          src={
                                                            item.attachment_file
                                                          }
                                                          alt=""
                                                        />
                                                      </Col>
                                                    )}
                                                  </Fragment>
                                                );
                                              }
                                            )}
                                          </Row>
                                          <Row className="x-image-row">
                                            {data.data_journey.attachment
                                              .length > 2 &&
                                              data.data_journey.attachment.map(
                                                (item, idx) => {
                                                  const length =
                                                    data.data_journey.attachment
                                                      .length;
                                                  const modulus = length % 3;
                                                  const maxLoop =
                                                    !modulus || length > 5
                                                      ? 3
                                                      : 2;
                                                  return (
                                                    <Fragment key={idx}>
                                                      {idx >= maxLoop &&
                                                        idx < 6 && (
                                                          <Col className="p-1 d-flex">
                                                            <img
                                                              className={classnames(
                                                                length === 4 &&
                                                                  "x-height-lg"
                                                              )}
                                                              src={
                                                                item.attachment_file
                                                              }
                                                              alt=""
                                                            />
                                                          </Col>
                                                        )}
                                                    </Fragment>
                                                  );
                                                }
                                              )}
                                          </Row>
                                        </div>
                                      )}
                                      {!isEmpty(
                                        data.data_journey?.data_ads
                                      ) && (
                                        <div className="x-contentfeeds-wrapper mx-3 mb-3">
                                          <Row className="x-cardevent-component">
                                            <Col lg={3} className="p-0">
                                              <img
                                                src={
                                                  data.data_journey?.data_ads
                                                    .image
                                                }
                                                alt=""
                                              />
                                            </Col>
                                            <Col
                                              lg={9}
                                              className="d-flex flex-column justify-content-center"
                                            >
                                              <p className="text-lg-normal color-neutral-old-80 mb-1">
                                                Join{" "}
                                                {
                                                  MAPPER_FEEDS_EVENT_DATA[
                                                    data.data_journey?.data_ads
                                                      .category
                                                  ]
                                                }
                                              </p>
                                              <p className="text-lg-bold color-neutral-old-80 text-start mb-1">
                                                {
                                                  data.data_journey?.data_ads
                                                    .title
                                                }
                                              </p>
                                              <p className="text-md-normal color-neutral-70 m-0">
                                                {
                                                  data.data_journey?.data_ads
                                                    .location.city_name
                                                }
                                              </p>
                                            </Col>
                                          </Row>
                                        </div>
                                      )}
                                    </div>
                                  }
                                  onCtaCard={() => {
                                    !isEmpty(data.data_journey.data_journey)
                                      ? push({
                                          pathname: "/profile",
                                          query: {
                                            id: data.data_journey.data_journey
                                              .id_user,
                                          },
                                        })
                                      : data.data_journey.type !==
                                        "artist_spotlight"
                                      ? push({
                                          pathname: "/feeds/detail",
                                          query: {
                                            id_timeline:
                                              data.data_journey.id_timeline,
                                          },
                                        })
                                      : push({
                                          pathname: "/profile",
                                          query: {
                                            id: data.data_journey.id_user,
                                          },
                                        });
                                  }}
                                  profession={reshare.topic_name}
                                  HideFooter
                                />
                              </div>
                            )}
                            {!isEmpty(data.data_ads) && (
                              <Fragment>
                                <div className="mx-3 mt-2">
                                  <Row
                                    className="x-cardevent-component cursor-pointer"
                                    onClick={() => {
                                      push({
                                        pathname: pathDetail(
                                          data.data_ads.category
                                        ),
                                        query: { id: data.data_ads.id_ads },
                                      });
                                    }}
                                  >
                                    <Col lg={3} className="p-0">
                                      <img src={data.data_ads.image} alt="" />
                                    </Col>
                                    <Col
                                      lg={9}
                                      className="d-flex flex-column justify-content-center align-items-start"
                                    >
                                      <p className="text-lg-normal color-neutral-old-80 mb-1">
                                        Join{" "}
                                        {
                                          MAPPER_FEEDS_EVENT_DATA[
                                            data.data_ads.category
                                          ]
                                        }
                                      </p>
                                      <p className="text-lg-bold color-neutral-old-80 text-start mb-1">
                                        {data.data_ads.title}
                                      </p>
                                      <p className="text-md-normal color-neutral-70 m-0">
                                        {data.data_ads.location.city_name}
                                      </p>
                                    </Col>
                                  </Row>
                                </div>
                              </Fragment>
                            )}
                          </Fragment>
                        }
                        totalLike={data.total_like}
                        totalComment={data.total_comment}
                        profession={data.topic_name}
                        onCtaCard={() => {
                          push({
                            pathname: "/feeds/detail",
                            query: {
                              id_timeline: data.id_timeline,
                            },
                          });
                        }}
                        onCtaReshare={() => {
                          setEditData({});
                          setReshare(data);
                          setModalFeedPost(true);
                        }}
                        isLiked={data.is_liked_by_viewer === "0" ? false : true}
                        onCtaComment={() => {
                          push({
                            pathname: "/feeds/detail",
                            query: {
                              id_timeline: data.id_timeline,
                            },
                          });
                        }}
                      />
                    </Fragment>
                  ))}
                </Col>
              </Row>
            </div>
          </MainLayout>
        </div>
      </div>
      {renderModalPostFeed()}
      {renderModalDeletePost()}
    </Layout>
  );
};

export default appRoute(Feeds);
