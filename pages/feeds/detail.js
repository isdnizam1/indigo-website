import {
  getDetailComments,
  getFeedDetail,
  getFeedLikes,
  postCreateComment,
  postSendLike,
  postProfileFollow,
  postProfileUnfollow,
  postFeedCommentLike,
  getListTopics,
} from "/services/actions/api";
import React, { Fragment, useEffect, useState } from "react";
import { Button, Col, Row } from "react-bootstrap";
import {
  formatDate,
  getAuth,
  apiDispatch,
  stringHtmlMentionParser,
} from "/services/utils/helper";
import CommentCard from "/components/comment/CommentCard";
import ModalListUser from "/components/modal/ModalListUser";
import BreadcrumbLv1 from "/components/header/BreadcrumbLv1";
import MainLayout from "/components/layout/MainLayout";
import { useRouter } from "next/router";
import { isEmpty } from "lodash-es";
import CardUser from "/components/home/CardUser";
import CommentPost from "/components/comment/CommentPost";
import Layout from "/components/common_layout/Layout";
import appRoute from "../../components/routes/appRoute";
import classnames from "classnames";
import SkeletonElement from "/components/skeleton/SkeletonElement";
import ModalPostFeed from "/components/modal/ModalPostFeed";

const FeedDetail = () => {
  const { id_user } = getAuth();

  const { isReady, query, push } = useRouter();
  const { id_timeline } = query;

  const initialData = {
    account_type: "",
    additional_data: null,
    attachment: [],
    city_name: "",
    created_at: "",
    description: "",
    full_name: "",
    id_event: "",
    id_journey: "",
    id_topic: "",
    id_user: "",
    is_followed_by_viewer: false,
    is_liked: 0,
    job_title: "",
    profile_picture: null,
    title: null,
    topic_name: [],
    total_comment: 0,
    total_like: 0,
    type: "",
    user_like: "",
    verified_status: "",
  };

  const [data, setData] = useState(initialData);
  const [topics, setTopics] = useState([]);
  const [comments, setComments] = useState([]);
  const [comment, setComment] = useState("");
  const [isFollowed, setIsFollowed] = useState(false);
  const [isLiked, setIsLiked] = useState(0);
  const [totalLike, setTotalLiked] = useState();
  const [totalCommented, setTotalCommented] = useState(0);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [allUserLike, setallUserLike] = useState([]);
  const [modal, setModal] = useState(false);
  const [updateCount, setUpdateCount] = useState(0);
  const [isLoadingData, setIsLoadingData] = useState(true);
  const [isLoadingComments, setIsLoadingComments] = useState(true);
  let dynamicComments = isLoadingComments ? [1, 2, 3] : comments;

  useEffect(() => {
    if (isReady) {
      isEmpty(id_timeline) && push("/feeds");

      apiDispatch(
        getFeedDetail,
        {
          id_user: id_user,
          id_timeline: query.id_timeline,
        },
        true
      ).then((response) => {
        setIsLoadingData(false);
        if (response.code === 200) {
          const data = response.result;

          setData(data);
          setIsFollowed(data.is_followed_by_viewer);
          setTotalLiked(data.total_like);
          setIsLiked(data.is_liked);
        } else push("/feeds");
      });

      apiDispatch(
        getDetailComments,
        {
          id_user: id_user,
          id_timeline: query.id_timeline,
        },
        true
      ).then((response) => {
        setIsLoadingComments(false);
        if (response.code === 200) {
          const data = response.result;

          setTotalCommented(data.length);
          setComments(data);
        }
      });
    }
  }, [isReady, updateCount]);

  useEffect(() => getTopics(), []);

  const getData = (id_timeline, id_viewer) => {
    apiDispatch(
      getFeedLikes,
      { id_timeline: id_timeline, id_viewer: id_viewer },
      true
    ).then((response) => {
      if (response.code === 200) {
        const data = response.result.user_like;
        setallUserLike(data);
      }
    });
  };

  const getTopics = () => {
    apiDispatch(getListTopics, { id_user }, true).then((response) => {
      response.code === 200 && setTopics(response.result);
    });
  };

  const handleIsFollowedChange = (idUser, idUserFollowed, type) => {
    const apiType = {
      follow: {
        api: postProfileFollow,
        dataProps: "followed_by",
      },
      unfollow: {
        api: postProfileUnfollow,
        dataProps: "id_user_following",
      },
    };

    let postData = {
      id_user: idUser,
      [apiType[type].dataProps]: idUserFollowed,
    };

    apiType[type]
      .api(postData)
      .then((response) => {
        const status = response.data.status;
        if (status === "success") {
          // setallUserLike([]); // initiate empty array

          let newAllUserLikes = allUserLike;
          const index = newAllUserLikes.findIndex(function (o) {
            let id = type === "unfollow" ? idUserFollowed : idUser;
            return o.id_user === id;
          });

          let newUserLike = newAllUserLikes[index];
          newUserLike.followed_by_viewer = type === "unfollow" ? false : true;
          newAllUserLikes[index] = newUserLike;

          setallUserLike(newAllUserLikes);
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const likedPlurar = (x) => {
    if (x > 1 || x > "1") return x + " people liked this post";
    if (x === 1 || x === "1") return "1 person liked this post";
    else return "0 person liked this post";
  };

  const handleCommentSubmit = (e) => {
    apiDispatch(
      postCreateComment,
      {
        related_to: "id_timeline",
        id_related_to: id_timeline ? id_timeline : "1856",
        comment,
        id_user,
      },
      true
    ).then((response) => {
      if (response.code === 200) {
        setComment("");
        setUpdateCount(1 + updateCount);
      }
    });

    e.preventDefault();
  };

  const handleFollow = () => {
    Promise.resolve(
      postProfileFollow({ id_user: id_post_user, followed_by: id_user })
    ).then(() => setIsFollowed(true));
  };

  const handleUnfollow = () => {
    Promise.resolve(
      postProfileUnfollow({
        id_user: id_user,
        id_user_following: id_post_user,
      })
    ).then(() => setIsFollowed(false));
  };

  const handleFeedLike = () => {
    Promise.resolve(
      postSendLike({
        related_to: "id_timeline",
        id_related_to: id_timeline,
        id_user,
      }).then((res) => {
        setTotalLiked(res.data.result.total_like);
        isLiked > 0 ? setIsLiked(0) : setIsLiked(1);
      })
    );
  };

  const {
    id_user: id_post_user,
    profile_picture,
    full_name,
    job_title,
    created_at,
    description,
    attachment,
    topic_name,
    total_comment,
    id_journey,
    is_liked,
    data_journey,
  } = data;

  return (
    <Layout title="Feed Detail - Eventeer">
      <MainLayout>
        <div className="x-feed-detail">
          <div className="x-breadcrumb-wrapper">
            <BreadcrumbLv1 />
          </div>
          <div className="x-content-feed">
            {(isLoadingData || data) && (
              <>
                <div className="detail-head">
                  <div className="d-flex w-100">
                    <CardUser
                      image={profile_picture}
                      name={full_name}
                      jobTitle={job_title}
                      city={formatDate(created_at)}
                    />
                  </div>
                  <div className="head-right">
                    {id_post_user !== id_user && (
                      <Button
                        className={classnames(isFollowed && "btn-cancel")}
                        onClick={!isFollowed ? handleFollow : handleUnfollow}
                      >
                        {!isFollowed ? "Follow" : "Following"}
                      </Button>
                    )}
                  </div>
                </div>
                <div className="detail-content">
                  {!isLoadingData ? (
                    <p className="text-lg-light mt-2">
                      {stringHtmlMentionParser(
                        description,
                        "semantic-main",
                        "/profile",
                        push
                      )}
                    </p>
                  ) : (
                    <SkeletonElement width="80%" height={16} className="mt-4" />
                  )}
                  {!isEmpty(attachment) && (
                    <div className="x-contentfeeds-wrapper">
                      <Row className="x-image-row">
                        {attachment.map((item, idx) => {
                          const length = data.attachment.length;
                          const modulus = length % 3;
                          const maxLoop = !modulus || length > 5 ? 3 : 2;
                          const isEvenRow =
                            length === 1 ||
                            length === 2 ||
                            length === 4 ||
                            length === 5;
                          return (
                            <Fragment key={idx}>
                              {idx < maxLoop && (
                                <Col className="p-1 d-flex" key={idx}>
                                  <img
                                    className={classnames(
                                      length === 1
                                        ? "x-height-xl"
                                        : isEvenRow && "x-height-lg"
                                    )}
                                    src={item.attachment_file}
                                    alt=""
                                  />
                                </Col>
                              )}
                            </Fragment>
                          );
                        })}
                      </Row>
                      <Row className="x-image-row">
                        {attachment.length > 2 &&
                          attachment.map((item, idx) => {
                            const length = data.attachment.length;
                            const modulus = length % 3;
                            const maxLoop = !modulus || length > 6 ? 3 : 2;
                            return (
                              <Fragment key={idx}>
                                {idx >= maxLoop && idx < 6 && (
                                  <Col className="p-1 d-flex">
                                    <img
                                      className={classnames(
                                        length === 4 && "x-height-lg"
                                      )}
                                      src={item.attachment_file}
                                      alt=""
                                    />
                                  </Col>
                                )}
                              </Fragment>
                            );
                          })}
                      </Row>
                    </div>
                  )}
                  {data_journey && (
                    <div className="x-journey-card">
                      <CardUser
                        image={data_journey.profile_picture}
                        name={data_journey.full_name}
                        jobTitle={data_journey.job_title}
                        city={formatDate(data_journey.created_at)}
                      />
                      <p className="text-lg-light mt-3">
                        {stringHtmlMentionParser(
                          data_journey.description,
                          "semantic-main",
                          "/profile",
                          push
                        )}
                      </p>

                      {!isEmpty(data_journey.attachment) && (
                        <div className="x-contentfeeds-wrapper">
                          <Row className="x-image-row">
                            {data_journey.attachment.map((item, idx) => {
                              const length = data.attachment.length;
                              const modulus = length % 3;
                              const maxLoop = !modulus || length > 5 ? 3 : 2;
                              const isEvenRow =
                                length === 1 ||
                                length === 2 ||
                                length === 4 ||
                                length === 5;
                              return (
                                <Fragment key={idx}>
                                  {idx < maxLoop && (
                                    <Col className="p-1 d-flex" key={idx}>
                                      <img
                                        className={classnames(
                                          length === 1
                                            ? "x-height-xl"
                                            : isEvenRow && "x-height-lg"
                                        )}
                                        src={item.attachment_file}
                                        alt=""
                                      />
                                    </Col>
                                  )}
                                </Fragment>
                              );
                            })}
                          </Row>
                          <Row className="x-image-row">
                            {data_journey.attachment.length > 2 &&
                              data_journey.attachment.map((item, idx) => {
                                const length = data.attachment.length;
                                const modulus = length % 3;
                                const maxLoop =
                                  !modulus || length > 6 ? 3 : 2;
                                return (
                                  <Fragment key={idx}>
                                    {idx >= maxLoop && idx < 6 && (
                                      <Col className="p-1 d-flex">
                                        <img
                                          className={classnames(
                                            length === 4 && "x-height-lg"
                                          )}
                                          src={item.attachment_file}
                                          alt=""
                                        />
                                      </Col>
                                    )}
                                  </Fragment>
                                );
                              })}
                          </Row>
                        </div>
                      )}
                    </div>
                  )}
                  <div className="content-tag">
                    {!isLoadingData ? (
                      topic_name.length > 0 &&
                      topic_name.split(",").map((item, idx) => (
                        <Button
                          className="btn-topic me-3"
                          style={{ width: "max-content" }}
                          key={idx}
                        >
                          {item}
                        </Button>
                      ))
                    ) : (
                      <div className="d-flex">
                        <SkeletonElement
                          width={100}
                          height={24}
                          count={3}
                          className="mx-1"
                        />
                      </div>
                    )}
                  </div>
                </div>
                <div className="detail-like">
                  <div className="like-icon-text">
                    <span
                      className="material-icons-round x-icon-like"
                      style={
                        isLiked
                          ? { color: "var(--semantic-main)" }
                          : { color: "var(--neutral-600)" }
                      }
                      onClick={handleFeedLike}
                    >
                      thumb_up_alt
                    </span>
                    {!isLoadingData ? (
                      <span
                        className="m-0 ms-1 text-md-light"
                        style={
                          isLiked
                            ? { color: "var(--semantic-main)" }
                            : { color: "var(--neutral-600)" }
                        }
                        onClick={() => {
                          setIsModalOpen(!isModalOpen);
                          getData(id_timeline, id_user);
                          postProfileUnfollow(id_user, id_post_user);
                        }}
                      >
                        {likedPlurar(totalLike)}
                      </span>
                    ) : (
                      <SkeletonElement width={100} height={14} />
                    )}
                  </div>
                  <div
                    className="x-icon-reshare"
                    onClick={() => setModal(true)}
                  />
                </div>
                <div className="detail-comment">
                  <div className="comment-post">
                    {!isLoadingData ? (
                      <span className="comment-count text-md-normal">
                        {total_comment}{" "}
                        {total_comment > 0 ? "comments" : "comment"}
                      </span>
                    ) : (
                      <SkeletonElement width={70} height={14} />
                    )}
                    <CommentPost
                      value={comment}
                      onChangeText={(e) => setComment(e.target.value)}
                      placeholder="Give your comments here"
                      onSubmit={handleCommentSubmit}
                    />
                  </div>
                  {(isLoadingComments || totalCommented > 0) && (
                    <div className="comment-list">
                      {dynamicComments?.map((comment, idx) => (
                        <Fragment key={idx}>
                          <CommentCard
                            isLoading={isLoadingComments}
                            data={comment}
                            postApiLike={{
                              api: postFeedCommentLike,
                              params: {
                                related_to: "id_comment",
                                id_related_to: comment.id_comment,
                                id_user,
                              },
                            }}
                            postApiSubmit={{
                              api: postCreateComment,
                              params: {
                                related_to: "id_comment",
                                id_related_to: comment.id_comment,
                                id_user,
                              },
                            }}
                            onCompleteSubmit={() =>
                              setUpdateCount(1 + updateCount)
                            }
                          />
                        </Fragment>
                      ))}
                    </div>
                  )}
                </div>
              </>
            )}
          </div>
        </div>
      </MainLayout>

      <ModalListUser
        backdrop={"static"}
        show={isModalOpen}
        onHide={() => setIsModalOpen(false)}
        customTitle={
          <>
            <div className="x-like-user w-500">
              <div className="d-flex justify-content-center align-items-center">
                <p className="me-100 m-0 heading-md-normal">Liked this post</p>
                <div className="ms-auto mt-1">
                  <div
                    className="x-icon-close cursor-pointer"
                    onClick={() => setIsModalOpen(false)}
                  />
                </div>
              </div>

              <div className="x-separator mt-2" />

              {allUserLike.map((item, idx) => {
                return (
                  <div className="x-list-like-user" key={idx}>
                    <div className="x-post-header mt-2 d-flex align-items-center justify-content-between">
                      {(() => {
                        if (
                          item.profile_picture !== null &&
                          item.profile_picture !== "0"
                        ) {
                          return (
                            <img
                              src={item.profile_picture}
                              className="x-image-account"
                              alt=""
                            />
                          );
                        }
                        return (
                          <div className="x-empty-image">
                            <p
                              className="text-xl-normal m-0"
                              style={{ color: "var(--base-10)" }}
                            >
                              {full_name.slice(0, 1).toUpperCase()}
                            </p>
                          </div>
                        );
                      })()}
                      <div className="x-identity ms-2">
                        <p className="line-height-sm text-md-normal m-0">
                          {item.full_name}
                        </p>
                        <h3
                          className="text-sm-normal line-height-sm"
                          style={{ color: "var(--neutral-600)" }}
                        >
                          {item.job_title}
                        </h3>
                      </div>
                      <div className="ms-auto me-4">
                        {(() => {
                          if (item.followed_by_viewer === true) {
                            return (
                              <Button
                                className="btn-cancel me-2"
                                onClick={() =>
                                  handleIsFollowedChange(
                                    id_user,
                                    item.id_user,
                                    "unfollow"
                                  )
                                }
                              >
                                Following
                              </Button>
                            );
                          } else if (
                            item.followed_by_viewer === false &&
                            id_user !== item.id_user
                          ) {
                            return (
                              <Button
                                className="btn-primary me-2"
                                onClick={() =>
                                  handleIsFollowedChange(
                                    id_user,
                                    item.id_user,
                                    "follow"
                                  )
                                }
                              >
                                Follow
                              </Button>
                            );
                          }
                        })()}
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </>
        }
      />
      {!isLoadingData && (
        <ModalPostFeed
          show={modal}
          onHide={() => setModal(false)}
          onShow={() => setModal(true)}
          feedReshareData={data}
          topics={topics}
          role={"community"}
        />
      )}
    </Layout>
  );
};

export default appRoute(FeedDetail);
