import { useEffect } from "react";
import DetailMessage from "/components/chat/DetailMessage";
import AddMember from "/components/chat/AddMember";
import GroupChat from "/components/chat/GroupChat";
import ListContact from "/components/chat/ListContact";
import ListMessage from "/components/chat/list_message/ListMessage";
import { useRouter } from "next/router";
import MessageState from "/components/chat/MessageState";
import { connect } from "mqtt";
import { setChatData } from "/redux/slices/chatSlice";
import { useDispatch, useSelector } from "react-redux";
import { isEmpty } from "lodash-es";
import appRoute from "/components/routes/appRoute";
import Layout from "/components/common_layout/Layout";
import { random } from "/services/utils/helper";
import HeaderLv1 from "/components/header/HeaderLv1";
import SkeletonElement from "/components/skeleton/SkeletonElement";

const Chat = () => {
  const router = useRouter();

  const { id_message, message_type } = router.query;

  const chatData = useSelector((state) => state.chatReducer.data);

  const {
    client,
    listMessages
  } = chatData;

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(
      setChatData({
        client: connect("wss://livechat.eventeer.id/mqtt:1883", {
          username: "username",
          password: "password",
        }),
      })
    );
    isEmpty(router.query) &&
      router.push({
        query: {
          message_type: "message",
        },
      });
  }, []);

  useEffect(() => {
    if (client) {
      client.on("connect", () => {
        console.log("connected");
      });

      client.on("reconnect", () => {
        console.log("reconnecting...");
      });

      client.on("error", (err) => {
        console.error("Connection error: ", err);
        client.end();
      });

      client.on("message", (topic, item) => {
        const data = JSON.parse(item);

        // console.log("data on client message: ", data);

        dispatch(
          setChatData({
            message: data,
            topic,
            listMessageUpdateCount: random(),
          })
        );

        data.message_type === "pdf" &&
          dispatch(
            setChatData({
              detailMessageUpdateCount: random(),
            })
          );
      });
    }

    return () => {
      client && client.end();
    }
  }, [client]);

  useEffect(
    () => client && !isEmpty(listMessages) && client.subscribe(listMessages),
    [client, listMessages]
  );

  return (
    <Layout title="Chat - Indigo">
      {/* <MainLayout HideSideNavigationBar> */}
        <HeaderLv1 />
        <div className="d-flex w-100">
          {message_type === "message" && <ListMessage />}
          {message_type === "contact" && <ListContact />}
          {message_type === "add_member" && <AddMember />}
          {message_type === "group_chat" && <GroupChat />}
          {id_message && <DetailMessage />}
          {!id_message && <MessageState />}
        </div>
      {/* </MainLayout> */}
    </Layout>
  );
};

export default appRoute(Chat);
