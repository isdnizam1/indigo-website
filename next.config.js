const path = require("path");

module.exports = {
  // future: {
  //   webpack5: false,
  // },
  webpack: (config, { watchOptions }) => {
    watchOptions = {
      poll: 1000,
      aggregateTimeout: 300,
      ignored: [path.join(__dirname, "node_modules")],
    };

    return config;
  },
  reactStrictMode: true,
  images: {
    domains: ["apidev.indigoconnect.id", "api.indigoconnect.id"],
  },
};
