import React from "react";
import Router from "next/router";
import { isLogin, getAuth } from "/services/utils/helper"
import { isEmpty } from "lodash-es";
import { REGISTRATION_ROUTES } from "/services/constants/Constants";

const appRoute = (Component = null) => {
  class AppRoute extends React.Component {
    state = {
      loading: true,
    };

    componentDidMount() {
      if (isLogin()) {
        const { registration_step, event_title } = getAuth();

        if (registration_step !== "finish") {
          return Router.push(REGISTRATION_ROUTES[registration_step]);
        }
        else if(registration_step === "finish" && isEmpty(event_title)) {
          return Router.push("/event-selection");
        }
        else {
          if (this.props.isMobileView) return Router.push("/register/success");
          this.setState({ loading: false });
        }
      }
      else {
        return Router.push("/");
      }
    }

    render() {
      if (this.state.loading) return <div />;
      return <Component {...this.props} />;
    }
  }

  AppRoute.getInitialProps = async (ctx) => {
    let isMobileView = (
      ctx.req ? ctx.req.headers["user-agent"] : navigator.userAgent
    ).match(
      /Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile|WPDesktop/i
    );

    //Returning the isMobileView as a prop to the component for further use.
    return {
      isMobileView: Boolean(isMobileView),
    };
  };

  return AppRoute;
};


export default appRoute;
