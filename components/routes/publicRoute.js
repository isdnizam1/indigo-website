import React from "react";
import Router from "next/router";
import { isLogin, getAuth } from "/services/utils/helper";
import { isEmpty } from "lodash-es";
import { REGISTRATION_ROUTES } from "/services/constants/Constants";

const publicRoute = (Component = null) => {
  class PublicRoute extends React.Component {
    state = {
      loading: true,
    };

    componentDidMount() {
        const { registration_step, event_title } = getAuth();

      if (isLogin()) {
        if(registration_step !== "finish") {
          return Router.push(REGISTRATION_ROUTES[registration_step])
        }
        else if(isEmpty(event_title)) {
          return Router.push("/event-selection");
        }
        else {
          return Router.push("/home");
        }
      } else {
        return this.setState({loading: false})
      }
    }

    render() {
      if (this.state.loading) return <div />;
      return <Component {...this.props} />;
    }
  }

  return PublicRoute;
};

export default publicRoute;
