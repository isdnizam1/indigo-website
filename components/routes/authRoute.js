import React from "react";
import Router from "next/router";
import { isLogin, getAuth } from "/services/utils/helper";
import { isEmpty } from "lodash-es";
import { REGISTRATION_ROUTES } from "/services/constants/Constants";

const authRoute = (Component = null) => {
  class AuthRoute extends React.Component {
    state = {
      loading: true,
    };

    componentDidMount() {
      if (isLogin()) {
        const { registration_step, event_title } = getAuth();

        const path = Router.pathname;

        if (registration_step !== "finish") {
          if (path.includes("/register")) return this.setState({loading: false});
          else {
            return this.setState({ loading: false }, () => {
              Router.push(REGISTRATION_ROUTES[registration_step]);
            });
          }
        } else {
          if (isEmpty(event_title)) {
            // return Router.push("/event-selection");
            if (path.includes("/event-selection") || path.includes("/register/success")) {
              return this.setState({loading: false})
            }
            else {
              return Router.push("/event-selection");
            }

          } else {
            Router.push("/home");
          }
        }
      } else {
        return Router.push("/");
      }
    }

    render() {
      if (this.state.loading) return <div />;
      return <Component {...this.props} />;
    }
  }

  return AuthRoute;
};

export default authRoute;
