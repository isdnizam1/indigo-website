import Head from "next/head";
import indigoIcon from "/public/indigo.png";

const Layout = ({
  children,
  title = "Indigo",
  description = "The All-In-One Event Platform for Event Organizer",
  image = indigoIcon.src,
}) => {
  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta name="theme-color" content="#000000" />
        <link rel="icon" href={image} />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, shrink-to-fit=no"
        />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp"
        ></link>
      </Head>
      {children}
    </>
  );
}

export default Layout;
