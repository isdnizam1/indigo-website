import React, { useEffect, useState, useRef } from "react";
import { Button } from "react-bootstrap";
import classnames from "classnames";
import WebinarBackground from "/public/assets/images/img-webinar-background.jpg";
import ModalConfirmation from "/components/modal/ModalConfirmation";
import ModalSuccess from "/components/modal/ModalSuccess";
import { getAuth } from "/services/utils/helper";
import ModalReshare from "../modal/ModalReshare";
import { isEmpty } from "lodash-es";
import { postSendLike } from "/services/actions/api";
import { useRouter } from "next/router";
import { setFeedsData } from "/redux/slices/feedsSlice";
import { useDispatch } from "react-redux";
import ModalImgZoom from "../modal/ModalImgZoom";

const FeedPost = (props) => {
  const { id_user } = getAuth();
  const { deletePost } = props;
  const [shownDelete, setShownDelete] = useState(false);
  const [modalConfirmIsOpened, setmodalConfirmIsOpened] = useState(false);
  const [modalDeleteIsOpened, setmodalDeleteIsOpened] = useState(false);
  const [modalsReshareIsOpened, setModalReshareIsOpened] = useState(false);
  const ref = useRef();
  const [dataReshare, setDataReshare] = useState(null);
  const { id_timeline } = props.item;
  const [isLiked, setIsLiked] = useState(parseInt(props.item.is_liked_by_viewer));
  const [totalLike, setTotalLiked] = useState(props.item.total_like);
  let sumLike = parseInt(totalLike);
  const router = useRouter();
  const dispatch = useDispatch();
  const [openModal, setOpenModal] = useState(false);
	const [tempData, setTempData] = useState([]);

  useEffect(() => {
    const handleShowDelete = (event) => {
      if (ref.current) {
        !ref.current.contains(event.target) &&
          !document.querySelector(".x-delete-wrapper").contains(event.target) &&
          setShownDelete(false);
      }
    };

    document.removeEventListener("mousedown", handleShowDelete);
    document.addEventListener("mousedown", handleShowDelete);

    return () => {
      document.removeEventListener("mousedown", handleShowDelete);
    };
  }, [ref]);

  const expandDelete = () => {
    setShownDelete(shownDelete ? false : true);
  };

  const expandConfirmDelete = () => {
    setmodalConfirmIsOpened(modalConfirmIsOpened ? false : true);
  };

  const expandDeleteSuccess = () => {
    setmodalDeleteIsOpened(modalDeleteIsOpened ? false : true);
  };

  const getDataImg = (img) => {
    let tempData = [img];
    setTempData((item) => [1, ...tempData]);
    return setOpenModal(true)
  }

  const dropdownDelete = () => {
    return (
      <div
        ref={ref}
        className="x-delete cursor-poin"
        onClick={() => expandConfirmDelete()}
      >
        <div
          className={classnames(
            "d-flex align-items-center cursor-pointer x-delete-detail"
          )}
        >
          <div className="x-icon-delete" />
          <p className="heading-sm-bold line-height-sm mt-3 ms-1">
            Delete post
          </p>
        </div>
      </div>
    );
  };

  const handleFeedLike = (id_user, id_timeline) => {
    Promise.resolve(
      postSendLike({
        related_to: "id_timeline",
        id_related_to: id_timeline,
        id_user: id_user,
      }).then((res) => {
        setTotalLiked(res.data.result.total_like);
        setIsLiked(isLiked ? 0 : 1);
      })
    );
  };

  const expandReshare = () => {
    setModalReshareIsOpened(modalsReshareIsOpened ? false : true);
  };

  const getDataFeeds = (props) => {
    setDataReshare(props.item);
    // console.log(props);
  };
  return (
    <div className="x-feeds-post">
      <div className="d-flex align-items-center">
        <div className="x-post-header align-items-center">
          {!isEmpty(props.item.profile_picture) ? (
            <img
              src={props.item.profile_picture}
              className="x-image-account"
              alt=""
            />
          ) : (
            <div className="x-empty-image">
              <p className="text-xl-normal m-0 color-base-10">
                {props.item.full_name.slice(0, 1).toUpperCase()}
              </p>
            </div>
          )}
          <div className="x-identity">
            <p className="text-lg-normal m-0 line-height-sm">
              {props.item.full_name}
            </p>
            <p className="text-sm-normal m-0 line-height-sm color-neutral-60">
              {props.item.job_title}
            </p>
            <p className="text-sm-normal m-0 line-height-sm color-neutral-60">
              {props.item.created_at}
            </p>
          </div>
        </div>
        {(() => {
          if (props.item.id_user === id_user) {
            return (
              <div className="x-delete-wrapper" onClick={() => expandDelete()}>
                <div className="x-icon-post-button color-neutral-old-80" />
                {shownDelete && dropdownDelete()}
              </div>
            );
          }
        })()}
      </div>

      <div className="x-post-content">
        <p
          onClick={() => {
            dispatch(setFeedsData({ selectedPost: props.item }));

            router.push({
              pathname: "/feeds/detail",
              query: {
                id_timeline: props.item.id_timeline,
              },
            });
          }}
          className="me-3 heading-sm-normal"
        >
          {props.item.description}
        </p>
        {props.item.attachment.map((image, idx) => {
          return (
            <img
              onClick={() => getDataImg(image.attachment_file)}
              className="x-post-image"
              src={image.attachment_file}
              alt=""
              key={idx}
            />
          );
        })}

        {(() => {
          if (props.item.data_journey !== "" && props.item.data_journey !== undefined) {
            return (
              <div className="x-post-reshare">
                <div className="x-feeds-post">
                  <div className="d-flex align-items-center">
                    <div className="x-post-header align-items-center justify-content-between">
                      {!props.item?.data_journey?.profile_picture ||
                      props.item.data_journey?.profile_picture !== "0" ? (
                        <div className="x-empty-image">
                          <p className="text-xl-normal m-0 color-base-10">
                            {props.item.data_journey?.full_name
                              .slice(0, 1)
                              .toUpperCase()}
                          </p>
                        </div>
                      ) : (
                        <img
                          src={props.item.data_journey?.profile_picture}
                          className="x-image-account"
                          alt=""
                        />
                      )}

                      <div className="x-identity">
                        <p className="text-lg-normal m-0 line-height-sm">
                          {props.item.data_journey?.full_name}
                        </p>
                        <p className="text-sm-normal m-0 line-height-sm color-neutral-60">
                          {props.item.data_journey?.job_title}
                        </p>
                        <p className="text-sm-normal m-0 line-height-sm color-neutral-60">
                          {props.item.data_journey?.created_at}
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="x-post-content">
                    <p className="me-3 heading-sm-normal">
                      {props.item.data_journey?.description}
                    </p>
                    {props.item.data_journey?.attachment.map((image, idx) => {
                      return (
                        <img
                          className="x-post-image"
                          src={image.attachment_file}
                          alt=""
                          key={idx}
                        />
                      );
                    })}

                    {(() => {
                      if (
                        props.item.data_journey?.data_ads !== "" &&
                        props.item.data_journey?.data_ads !== undefined
                      ) {
                        return (
                          <div className="x-data-ads">
                            {(() => {
                              if (
                                props.item.data_journey?.data_ads?.category ===
                                "video"
                              ) {
                                return (
                                  <div className="x-post-video cursor-pointer">
                                    {(() => {
                                      if (
                                        props.item.data_journey?.data_ads?.image !==
                                          null &&
                                        props.item.data_journey?.data_ads?.image !==
                                          ""
                                      ) {
                                        return (
                                          <img
                                            src={
                                              props.item.data_journey?.data_ads?.image
                                            }
                                            className="x-post-video"
                                            alt=""
                                          />
                                        );
                                      }
                                      return (
                                        <img
                                          src={WebinarBackground}
                                          className="x-post-video"
                                          alt=""
                                        />
                                      );
                                    })()}
                                    <div className="x-post-cardvid">
                                      <h1 className="ms-3 heading-md-bold color-neutral-old-80">
                                        {props.item.data_journey?.data_ads?.title}
                                      </h1>
                                      <h1 className="ms-3 text-lg-normal color-neutral-60">
                                        youtube.com
                                      </h1>
                                    </div>
                                  </div>
                                );
                              } else if (
                                props.item.data_journey?.data_ads?.category ===
                                "collaboration"
                              ) {
                                return (
                                  <div className="x-post-col cursor-pointer">
                                    {(() => {
                                      if (
                                        props.item.data_journey?.data_ads?.image !==
                                          null &&
                                        props.item.data_journey?.data_ads?.image !==
                                          ""
                                      ) {
                                        return (
                                          <img
                                            className="x-post-imgcol"
                                            src={
                                              props.item.data_journey?.data_ads?.image
                                            }
                                            alt=""
                                          />
                                        );
                                      }
                                      return (
                                        <img
                                          src={WebinarBackground}
                                          className="x-post-imgcol"
                                          alt=""
                                        />
                                      );
                                    })()}
                                    <div className="x-post-cardcol">
                                      <div className="x-post-cardcol-flex">
                                        <h3 className="text-sm-normal color-neutral-old-80">
                                          Join Collaboration
                                        </h3>
                                        <h1 className="heading-sm-bold color-neutral-old-80">
                                          {props.item.data_journey?.data_ads?.title}
                                        </h1>
                                        <h1 className="text-sm-normal color-neutral-60">
                                          {
                                            props.item.data_journey?.data_ads
                                              ?.location?.city_name
                                          }
                                        </h1>
                                      </div>
                                    </div>
                                  </div>
                                );
                              } else if (
                                props.item.data_journey?.data_ads?.category ===
                                "sc_session"
                              ) {
                                return (
                                  <div className="x-post-col cursor-pointer">
                                    {(() => {
                                      if (
                                        props.item.data_journey?.data_ads?.image !==
                                          null &&
                                        props.item.data_journey?.data_ads?.image !==
                                          ""
                                      ) {
                                        return (
                                          <img
                                            className="x-post-imgcol"
                                            src={
                                              props.item.data_journey?.data_ads?.image
                                            }
                                            alt=""
                                          />
                                        );
                                      }
                                      return (
                                        <img
                                          src={WebinarBackground}
                                          className="x-post-imgcol"
                                          alt=""
                                        />
                                      );
                                    })()}
                                    <div className="x-post-cardcol">
                                      <div className="x-post-cardcol-flex">
                                        <h3 className="text-sm-normal color-neutral-old-80">
                                          Join Webinar
                                        </h3>
                                        <h1 className="heading-sm-bold color-neutral-old-80">
                                          {props.item.data_journey?.data_ads?.title}
                                        </h1>
                                        <h1 className="text-sm-normal mt-1 color-neutral-60">
                                          {
                                            props.item.data_journey?.data_ads
                                              ?.location?.city_name
                                          }
                                        </h1>
                                      </div>
                                    </div>
                                  </div>
                                );
                              } else if (
                                props.item.data_journey?.data_ads?.category ===
                                "submission"
                              ) {
                                return (
                                  <div className="x-post-col cursor-pointer">
                                    {(() => {
                                      if (
                                        props.item.data_journey?.data_ads?.image !==
                                          null &&
                                        props.item.data_journey?.data_ads?.image !==
                                          ""
                                      ) {
                                        return (
                                          <img
                                            className="x-post-imgcol"
                                            src={
                                              props.item.data_journey?.data_ads?.image
                                            }
                                            alt=""
                                          />
                                        );
                                      }
                                      return (
                                        <img
                                          src={WebinarBackground}
                                          className="x-post-imgcol"
                                          alt=""
                                        />
                                      );
                                    })()}
                                    <div className="x-post-cardcol">
                                      <div className="x-post-cardcol-flex">
                                        <h3 className="text-sm-normal color-neutral-old-80">
                                          Submission
                                        </h3>
                                        <h1 className="heading-sm-bold color-neutral-old-80">
                                          {props.item.data_ads?.title}
                                        </h1>
                                      </div>
                                    </div>
                                  </div>
                                );
                              }
                            })()}
                          </div>
                        );
                      }
                    })()}
                  </div>
                  {/* </div> */}
                </div>
              </div>
            );
          }
        })()}

        {(() => {
          if (props.item.data_ads !== "" && props.item.data_ads !== undefined) {
            return (
              <div className="x-data-ads">
                {(() => {
                  if (props.item.data_ads?.category === "video") {
                    return (
                      <div className="x-post-video cursor-pointer">
                        {(() => {
                          if (
                            props.item.data_ads?.image !== null &&
                            props.item.data_ads?.image !== ""
                          ) {
                            return (
                              <img
                                src={
                                  "https://digilearn-bucket.s3.ap-southeast-1.amazonaws.com/content/video_cover/2022/5/2022517113332.png"
                                }
                                className="x-post-video"
                                alt=""
                              />
                            );
                          }
                          return (
                            <img
                              src={WebinarBackground}
                              className="x-post-video"
                              alt=""
                            />
                          );
                        })()}
                        <div className="x-post-cardvid">
                          <h1 className="ms-3 heading-md-bold color-neutral-old-80">
                            {props.item.data_ads?.title}
                          </h1>
                          <h1 className="ms-3 text-lg-normal color-neutral-60">
                            {
                              JSON.parse(props.item.data_ads.additional_data)
                                .url_video
                            }
                          </h1>
                        </div>
                      </div>
                    );
                  } else if (props.item.data_ads?.category === "collaboration") {
                    return (
                      <div className="x-post-col cursor-pointer">
                        <img
                          className="x-post-imgcol"
                          src={props.item.data_ads?.image}
                          alt=""
                        />
                        <div className="x-post-cardcol">
                          <div className="x-post-cardcol-flex">
                            <h3 className="text-sm-normal color-neutral-old-80">
                              Join Collaboration
                            </h3>
                            <h1 className="heading-sm-bold color-neutral-old-80">
                              {props.item.data_ads?.title}
                            </h1>
                            <h1 className="text-sm-normal color-neutral-60">
                              {props.item.data_ads?.location?.city_name}
                            </h1>
                          </div>
                        </div>
                      </div>
                    );
                  } else if (props.item.data_ads?.category === "sc_session") {
                    return (
                      <div className="x-post-col cursor-pointer">
                        {(() => {
                          if (
                            props.item.data_ads?.image !== null &&
                            props.item.data_ads?.image !== ""
                          ) {
                            return (
                              <img
                                className="x-post-imgcol"
                                src={props.item.data_ads?.image}
                                alt=""
                              />
                            );
                          }
                          return (
                            <img
                              src={WebinarBackground}
                              className="x-post-imgcol"
                              alt=""
                            />
                          );
                        })()}
                        <div className="x-post-cardcol">
                          <div className="x-post-cardcol-flex">
                            <h3 className="text-sm-normal color-neutral-old-80">
                              Join Webinar
                            </h3>
                            <h1 className="heading-sm-bold color-neutral-old-80">
                              {props.item.data_ads?.title}
                            </h1>
                            <h1 className="text-sm-normal mt-1 color-neutral-60">
                              {props.item.data_ads?.location?.city_name}
                            </h1>
                          </div>
                        </div>
                      </div>
                    );
                  } else if (props.item.data_ads?.category === "submission") {
                    return (
                      <div className="x-post-col cursor-pointer">
                        {(() => {
                          if (
                            props.item.data_ads?.image !== null &&
                            props.item.data_ads?.image !== ""
                          ) {
                            return (
                              <img
                                className="x-post-imgcol"
                                src={props.item.data_ads?.image}
                                alt=""
                              />
                            );
                          }
                          return (
                            <img
                              src={WebinarBackground}
                              className="x-post-imgcol"
                              alt=""
                            />
                          );
                        })()}
                        <div className="x-post-cardcol">
                          <div className="x-post-cardcol-flex">
                            <h3 className="text-sm-normal color-neutral-old-80">
                              Submission
                            </h3>
                            <h1 className="heading-sm-bold color-neutral-old-80">
                              {props.item.data_ads?.title}
                            </h1>
                          </div>
                        </div>
                      </div>
                    );
                  }
                })()}
              </div>
            );
          }
        })()}

        {props.item.topic_name &&
          props.item.topic_name[0] !== "" &&
          props.item.topic_name.map((item1, idx) => {
            return (
              <Button
                className="btn-topic me-3"
                style={{ width: "max-content" }}
                key={idx}
              >
                {item1}
              </Button>
            );
          })}
          {openModal && <ModalImgZoom img={tempData[1]} hide={() => setOpenModal(false)} />}
      </div>

      <div className="x-separator-post" />

      <div className="x-footer-feeds">
        <div className="x-footer-feeds-input">
          <div className="d-flex justify-content-between mx-3 my-1">
            <div className="d-flex">
              <div className="d-flex align-items-center me-3">
                <span
                  className={classnames(
                    "material-icons-round cursor-pointer user-select-none",
                    isLiked ? "color-neutral-old-80" : "color-neutral-60"
                  )}
                  onClick={() => handleFeedLike(id_user, props.item.id_timeline)}
                >
                  thumb_up_alt
                </span>

                <p
                  className={classnames(
                    "m-0 ms-2 text-lg-bold",
                    isLiked ? "color-neutral-old-80" : "color-neutral-60"
                  )}
                >
                  {sumLike}
                </p>
              </div>
              <div
                className="x-footer-comment"
                onClick={() => {
                  dispatch(setFeedsData({ selectedPost: props.item }));

                  router.push({
                    pathname: "/feeds/detail",
                    query: {
                      id_timeline: props.item.id_timeline,
                    },
                  });
                }}
              >
                <div className="d-flex align-items-center cursor-pointer">
                  <span className="material-icons-round ms-0 color-neutral-60">
                    chat
                  </span>
                  <span className="m-0 ms-2 heading-sm-normal color-neutral-60">
                    {props.item.total_comment}
                  </span>
                  <span className="m-0 ms-2 heading-sm-normal color-neutral-60">
                    Comments
                  </span>
                </div>
              </div>
            </div>

            <div className="x-footer-reshare ms-auto">
              <div className="d-flex align-items-center cursor-pointer">
                <span className="material-icons-round color-neutral-60">
                  sync
                </span>
                <p
                  className="m-0 ms-2 heading-sm-normal color-neutral-60"
                  onClick={() => {
                    expandReshare();
                    getDataFeeds(props);
                  }}
                >
                  Reshare
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <ModalConfirmation
        backdrop={"static"}
        show={modalConfirmIsOpened}
        onHide={() => setmodalConfirmIsOpened(false)}
        customTitle={
          <>
            <h1 className="heading-lg-bold">Hapus post?</h1>
            <p className="pt-1 text-lg-normal color-neutral-60">
              Apakah kamu yakin untuk menghapus post ini secara permanen?
            </p>
            <div className="d-flex justify-content-center">
              <Button
                className="btn-delete m-2 w-25"
                onClick={() => {
                  deletePost(props.item.id_journey);
                  expandDeleteSuccess();
                  setmodalConfirmIsOpened(false);
                }}
              >
                Hapus
              </Button>

              <Button
                className="btn-primary m-2 w-25"
                onClick={() => setmodalConfirmIsOpened(false)}
              >
                Batalkan
              </Button>
            </div>
          </>
        }
      />

      <ModalSuccess
        backdrop={"static"}
        show={modalDeleteIsOpened}
        onHide={() => setmodalDeleteIsOpened(false)}
        customTitle={
          <>
            <div className="x-icon-success" />

            <p className="pt-1 mt-2 text-lg-normal color-neutral-80">
              Post have been deleted
            </p>

            <Button
              className="btn-primary m-2"
              onClick={() => setmodalDeleteIsOpened(false)}
            >
              Back to feed
            </Button>
          </>
        }
      />
      <ModalReshare
        show={modalsReshareIsOpened}
        onHide={() => setModalReshareIsOpened(false)}
        dataReshare={dataReshare}
      />
    </div>
  );
};

export default FeedPost;
