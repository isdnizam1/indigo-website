import React from 'react';
import { Row } from 'react-bootstrap';
import { isEmpty } from 'lodash-es';
import classnames from "classnames";
import SkeletonElement from "/components/skeleton/SkeletonElement";

const IdentityCard = ({
  image,
  title,
  subTitle,
  onClick,
  isLoading,
}) => {
  const getInitials = (string) => {
    const [firstname, lastname] = string.toUpperCase().split(" ");
    const initials = firstname.substring(0, 1);
    return lastname
      ? initials.concat(lastname.substring(0, 1))
      : initials.concat(firstname.substring(1, 2));
  };

  return (
    <div className={classnames("x-person-card", onClick && "cursor-pointer")} onClick={onClick}>
      <Row className="w-100 m-0">
        <div className="x-speaker-identity align-items-center">
          <div className="x-profile-img">
            {!isEmpty(image) && !isLoading ? (
              <img src={image} className="x-image-account" alt="" />
            ) : (!isLoading ?
              <div className="x-empty-image">
                <p className="text-xl-normal m-0 color-base-10">
                  {getInitials(title)}
                </p>
              </div>
              : <SkeletonElement width={48} height={48} type="rounded-circle"/>
            )}
          </div>
          <div className="x-speaker-name">
            {!isLoading ? <p className="m-0 heading-sm-normal color-neutral-old-80">
              {title}
            </p> : <SkeletonElement width={150} height={16} className="mb-2"/> }
            {!isLoading ? <p className="m-0 text-sm-light color-neutral-old-40 line-height-sm">
              {subTitle}
            </p> : <SkeletonElement width="100%" height={12} />}
          </div>
        </div>
      </Row>
    </div>
  );
}

export default IdentityCard;
