import React, { useEffect, useState, Fragment } from "react";
import classnames from "classnames";
import ScrollTo from "react-scroll-into-view";
import { isEmpty } from "lodash-es";
import { Loaders } from "/components/loaders/Loaders";
import { useRouter } from "next/router";

const BannerCarousel = (props) => {
  const router = useRouter();

  const [bannerIdx, setBannerIdx] = useState(0);
  const { data, carouselIsClick, arrowAlwaysDisplay, isLoading } = props;

  const bannerScrollTo = (state) => {
    const index = state === "prevState" ? 1 + bannerIdx : -1 + bannerIdx;
    const element = document.getElementById(`carouselItem${index}`);

    if (index >= 0 && index < data?.length) {
      setBannerIdx(index);

      element.scrollIntoView({
        block: "nearest",
        behavior: "smooth",
        inline: "center",
      });
    }
  };
  if (isEmpty(data) && !isLoading) {
    return null;
  }
  return (
    <Fragment>
      <div className="x-carousel-section">
        {!isLoading && (bannerIdx !== 0 || arrowAlwaysDisplay) && (
          <div className="x-arrow-navigation x-left">
            <div
              className="x-arrow-button"
              onClick={() => bannerScrollTo("nextState")}
            >
              <span className="material-icons-round">chevron_left</span>
            </div>
          </div>
        )}

        {!isLoading && (bannerIdx !== data?.length - 1 || arrowAlwaysDisplay) && (
          <div className="x-arrow-navigation x-right">
            <div
              className="x-arrow-button"
              onClick={() => bannerScrollTo("prevState")}
            >
              <span className="material-icons-round">chevron_right</span>
            </div>
          </div>
        )}
        <div className="x-carousel-wrapper">
          <Loaders.Elements
            isLoading={isLoading}
            height={315}
            width={560}
            className="pe-4"
            count={3}
          >
            {data?.map((item, idx) => (
              <div
                className={classnames(
                  "x-carousel-item",
                  carouselIsClick && "cursor-pointer"
                )}
                key={idx}
                id={"carouselItem" + idx}
                // onClick={() => carouselIsClick && alert("carousel item " + idx)}
                onClick={() =>
                  carouselIsClick &&
                  router.push({
                    pathname: "/home/banner-detail",
                    query: { id: item.id_ads },
                  })
                }
              >
                <img src={item.image} alt="" />
              </div>
            ))}
          </Loaders.Elements>
        </div>

        <div className="x-indicator-wrapper">
          <ol className="x-indicator">
            <Loaders.Elements
              isLoading={isLoading}
              height={13}
              width={13}
              type="rounded-circle"
              className="px-2"
              count={3}
            >
              {!isEmpty(data) &&
                data?.map((item, idx) => (
                  <li key={idx}>
                    <ScrollTo
                      selector={`#carouselItem${idx}`}
                      scrollOptions={{ block: "nearest", inline: "center" }}
                      id={"indicatorItem"}
                      className={classnames(
                        "x-indicator-item",
                        bannerIdx === idx && "active"
                      )}
                      onClick={() => setBannerIdx(idx)}
                    />
                  </li>
                ))}
            </Loaders.Elements>
          </ol>
        </div>
      </div>
    </Fragment>
  );
};

export default BannerCarousel;
