import { isEmpty } from 'lodash-es';
import React from 'react';
import { Col, Row } from 'react-bootstrap';
import classnames from "classnames";
import SkeletonElement from "/components/skeleton/SkeletonElement";

const CardLv2 = ({
  className,
  customBody,
  title,
  customTitle,
  subTitle,
  customSubTitle,
  image,
  onClick,
  customFooter,
  isLoading
}) => {
  return (
    <div className={classnames("x-card-lv2", className && className)}>
      <Row className="w-100 m-0 p-4">
        <Col xl={4} className="ps-0 cursor-pointer" onClick={onClick}>
          {!isLoading ? image && <img src={image} className="x-image" alt="" />
            : <SkeletonElement width="100%" height={205} />}

        </Col>
        <Col xl={8} className="d-flex flex-column justify-content-between pe-0">
          {customBody && customBody}
          <div className="d-flex flex-column cursor-pointer" onClick={onClick}>
            {!isLoading ? title && <h3 className="heading-md-bold m-0 color-neutral-old-80">{title}</h3>
              : <SkeletonElement width="100%" height={24} />}
            {!isEmpty(customTitle) && customTitle}
            {subTitle && (
              <h4 className="heading-sm-normal m-0 color-neutral-old-40">
                {subTitle}
              </h4>
            )}
            {!isEmpty(customSubTitle) && customSubTitle}
          </div>

          {customFooter && customFooter}
        </Col>
      </Row>
    </div>
  );
}

export default CardLv2;
