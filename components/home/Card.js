import React, { Fragment } from 'react';
import { Col } from 'react-bootstrap';
import { isEmpty } from "lodash-es";
import SkeletonElement from '../skeleton/SkeletonElement';

const Card = ({
  onClick,
  image,
  title,
  subTitle,
  col,
  isLoading
}) => {

  return (
    <Col md={col} className="x-card-section">
      <div
        className="x-card"
        onClick={onClick}
      >
        {!isLoading ?
          <Fragment>
            <img src={image} alt="" />
            <div className="w-100 p-3">
              <p
                className="text-lg-bold m-0 line-height-md pb-2 color-neutral-old-80"
              >
                {!isEmpty(title) && title}
              </p>

              <p
                className="text-sm-light m-0 line-height-md color-neutral-old-50"
              >
                {!isEmpty(subTitle) && subTitle}
              </p>
            </div>
          </Fragment> :
          <Fragment>
            <SkeletonElement type="x-card-skeleton" />
            <div className='p-3'>
              <SkeletonElement type="x-title-skeleton mb-2" />
              <SkeletonElement type="x-subtitle-skeleton mb-2" count={2} />
            </div>
          </Fragment>
        }
      </div>
    </Col >
  );
}

export default Card;
