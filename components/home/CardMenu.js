import { Fragment } from "react";
import { Col } from "react-bootstrap";
import { isEmpty } from "lodash-es";

const MenuCard = ({
  onClick,
  image,
  icon,
  label,
  col,
  isLoadingData,
}) => {
  return (
    <Fragment>
      <Col md={col} className="x-cardmenu-section" onClick={onClick}>
        <div
          className="x-image"
          style={{ backgroundImage: `url(${image})` }}
        />
        <div className="x-section-detail">
          <div
            className="x-icon"
            style={{ backgroundImage: `url(${icon})` }}
          />
          <p
            className="text-lg-bold m-0 pt-2 text-center"
            style={{ color: "var(--base-10)" }}
          >
            {label}
          </p>
        </div>
      </Col>
    </Fragment>
  )
}

export default MenuCard;
