import React from "react";

const RightBarBox = ({ children, title }) => {
  return (
    <div className="x-rightbar-box">
      <div className="x-speaker-list">
        <div className="d-flex align-items-center">
          <div className="x-speaker-header">
            <span className="m-0 heading-sm-normal color-semantic-main">
              {title}
            </span>
          </div>
        </div>
        {children}
      </div>
    </div>
  );
};

export default RightBarBox;
