import React from "react";
import { Button } from "react-bootstrap";
import { isEmpty } from "lodash-es";
import classnames from "classnames";
import SkeletonElement from "components/skeleton/SkeletonElement";

const ContentBox = ({
  title,
  subTitle,
  customHeaderContent,
  className,
  cta,
  toggles,
  toggleActive,
  setToggleActive,
  children,
  NoBorder,
  NoDivider,
  fullDivider,
  filterOnClick,
  filterCount,
  isLoading,
}) => {
  return (
    <div className={classnames("x-contentbox-section", className && className, NoBorder && "x-border-none")}>
      {(title || subTitle || customHeaderContent || cta) && (
        <div className="d-flex align-items-center justify-content-between">
          <div className={classnames(!cta && "w-100")}>
            {title && <h3 className="heading-md-bold m-0 color-neutral-old-80">{title}</h3>}
            {subTitle && <p className="text-lg-light color-neutral-old-50">{subTitle}</p>}
            {customHeaderContent && customHeaderContent}
          </div>
          {cta && (
            <div className="pb-3">
              <Button className="btn-link" onClick={cta}>
                Lihat semua
              </Button>
            </div>
          )}
        </div>
      )}

      {!isLoading ? filterOnClick && (
        <Button
          className="btn-cancel d-flex justify-content-center align-items-center px-2 py-1 mt-2 mb-3 w-auto"
          onClick={filterOnClick}
        >
          <span
            className="material-icons-round pe-1"
            style={{ fontSize: "20px" }}
          >
            tune
          </span>
          <span className="pe-1">Filter</span>
          <span>({filterCount})</span>
        </Button>
      ): <SkeletonElement width={100} height={34} className="mb-3" />}

      {!isEmpty(toggles) && (
        <div className="x-toggle-wrapper">
          {toggles.map((toggle, idx) => (
            <div
              className="x-toggle"
              onClick={() => setToggleActive(toggle)}
              key={idx}
            >
              <p
                className={classnames(
                  "text-lg-bold m-0 pb-2",
                  toggle === toggleActive && "active"
                )}
              >
                {toggle}
              </p>
              {toggle === toggleActive && <div className="x-divider" />}
            </div>
          ))}
        </div>
      )}
      {!NoDivider && (
        <div
          className={classnames("x-divider", fullDivider && "x-divider-full")}
        />
      )}

      {children}
    </div>
  );
};

export default ContentBox;
