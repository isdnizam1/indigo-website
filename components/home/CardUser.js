import { isEmpty } from "lodash-es";
import React, { useEffect } from "react";
import classnames from "classnames";
import { LoaderContent } from "/components/loaders/Loaders";
import { useState } from "react";
import { getProfileDetail } from "/services/actions/api";
import { apiDispatch } from "/services/utils/helper";
import { HiShieldCheck } from "react-icons/hi";

const CardUser = ({
  image,
  name,
  id,
  isAdminTenant,
  jobTitle,
  city,
  customInformation,
  isLoading,
  className,
  onClick,
  TextCentered
}) => {
  const [isAdmin, setIsAdmin] = useState(isAdminTenant);

  useEffect(() => {
    id &&
      apiDispatch(getProfileDetail, { id_user: id }, true).then(
        (response) =>
          response.code === 200 && setIsAdmin(response.result.is_admin_tenant)
      );
  }, []);

  return (
    <div
      className={classnames(
        "x-carduser-component",
        className && className,
        onClick && "cursor-pointer"
      )}
      onClick={onClick}
    >
      <LoaderContent
        isLoading={isLoading}
        height={48}
        width={48}
        type="rounded-circle"
      >
        {!isEmpty(image) ? (
          <img className="x-profile-picture x-image" src={image} alt="" />
        ) : (
          <div className="x-profile-picture x-initial">
            <p className="text-xl-normal m-0 color-base-10">
              {name?.slice(0, 1).toUpperCase()}
            </p>
          </div>
        )}
      </LoaderContent>
      <div className={classnames("x-detail-wrapper", TextCentered && "justify-content-center")}>
        <LoaderContent
          isLoading={isLoading}
          height={12}
          width={65}
          className="mb-2"
        >
          {name && (
            <div className="d-flex gap-1 align-items-center">
              <p className="text-lg-normal text-start m-0 color-neutral-old-80 line-height-md">
                {name}
              </p>
              {isAdmin && (
                <div className="x-admin-badge">
                  <HiShieldCheck size={8} />
                  <span>Admin</span>
                </div>
              )}
            </div>
          )}
        </LoaderContent>
        <LoaderContent
          isLoading={isLoading}
          height={10}
          width={150}
          className="mb-2"
        >
          {jobTitle && (
            <p className="text-sm-normal m-0 line-height-sm color-neutral-old-50 line-height-md">
              {jobTitle}
            </p>
          )}
        </LoaderContent>
        <LoaderContent
          isLoading={isLoading}
          height={10}
          width={150}
          className="mb-2"
        >
          {(city || customInformation) && (
            <div className="d-flex flex-column justify-content-center">
              {city && (
                <p className="text-sm-light m-0 color-neutral-old-50 line-height-md">
                  {city}
                </p>
              )}
              {customInformation && customInformation}
            </div>
          )}
        </LoaderContent>
      </div>
    </div>
  );
};

export default CardUser;
