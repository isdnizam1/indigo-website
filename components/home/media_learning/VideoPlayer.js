import React, { Component } from "react";
import { findDOMNode } from "react-dom";
import ReactPlayer from "react-player";
import screenfull from "screenfull";

class VideoPlayer extends Component {
  state = {
    pip: false,
    playing: false,
    controls: false,
    volume: 0.8,
    muted: false,
    played: 0,
    loaded: 0,
    duration: 0,
    playbackRate: 1.0,
    loop: false,
    displayControl: "x-control",
    displayFullscreenControl: "x-fullscreen-control",
    displayVolumeControl: false,
  };

  load = (url) => {
    this.setState({
      url,
      played: 0,
      loaded: 0,
      pip: false,
    });
  };

  handlePlayPause = () => {
    this.setState({ playing: !this.state.playing });
  };

  handleStop = () => {
    this.setState({ url: null, playing: false });
  };

  handleToggleControls = () => {
    const url = this.state.url;
    this.setState(
      {
        controls: !this.state.controls,
        url: null,
      },
      () => this.load(url)
    );
  };

  handleToggleLoop = () => {
    this.setState({ loop: !this.state.loop });
  };

  handleVolumeChange = (e) => {
    this.setState({ volume: parseFloat(e.target.value) });
  };

  handleToggleMuted = () => {
    this.setState({ muted: !this.state.muted });
  };

  handleSetPlaybackRate = (e) => {
    this.setState({ playbackRate: parseFloat(e.target.value) });
  };

  handleOnPlaybackRateChange = (speed) => {
    this.setState({ playbackRate: parseFloat(speed) });
  };

  handleTogglePIP = () => {
    this.setState({ pip: !this.state.pip });
  };

  handlePlay = () => {
    this.setState({ playing: true });
  };

  handleEnablePIP = () => {
    this.setState({ pip: true });
  };

  handleDisablePIP = () => {
    this.setState({ pip: false });
  };

  handlePause = () => {
    this.setState({ playing: false });
  };

  handleSeekMouseDown = (e) => {
    this.setState({ seeking: true });
  };

  handleSeekChange = (e) => {
    this.setState({ played: parseFloat(e.target.value) });
  };

  handleSeekMouseUp = (e) => {
    this.setState({ seeking: false });
    this.player.seekTo(parseFloat(e.target.value));
  };

  handleProgress = (state) => {
    // We only want to update time slider if we are not currently seeking
    if (!this.state.seeking) {
      this.setState(state);
    }
  };

  handleEnded = () => {
    this.setState({ playing: this.state.loop });
  };

  handleDuration = (duration) => {
    this.setState({ duration });
  };

  handleClickFullscreen = () => {
    if (screenfull.isFullscreen) {
      screenfull.exit();
    } else {
      screenfull.request(findDOMNode(this.container));
    }
  };

  renderLoadButton = (url, label) => {
    return <button onClick={() => this.load(url)}>{label}</button>;
  };

  timer = null;

  handleMouseMove = () => {
    if (this.timer) {
      if (screenfull.isFullscreen) {
        this.setState({
          displayFullscreenControl: "x-fullscreen-control",
        });
      } else {
        this.setState({
          displayControl: "x-control",
        });
      }
      clearTimeout(this.timer);
    }
    this.timer = setTimeout(() => {
      this.setState({
        displayControl: "x-control-hide",
        displayFullscreenControl: "x-control-hide",
      });
    }, 2000);
  };

  handleProgressWidth = (x) => {
    return (x * 100).toFixed(2) + "%";
  };

  playerRef = (player) => {
    this.player = player;
  };

  containerRef = (container) => {
    this.container = container;
  };

  render() {
    const {
      playing,
      controls,
      volume,
      muted,
      loop,
      played,
      loaded,
      duration,
      playbackRate,
      pip,
      displayControl,
      displayFullscreenControl,
      displayVolumeControl,
    } = this.state;

    const url = this.props.vidurl;

    const SEPARATOR = " · ";

    return (
      <div className="">
        <div
          ref={this.containerRef}
          className="x-video-player"
          tabIndex="0"
          // onKeyDown={(e) => {
          //   if (e.keyCode === 32) ;
          // }}
          onMouseEnter={() => {
            screenfull.isFullscreen
              ? this.setState({
                  displayFullscreenControl: "x-fullscreen-control",
                })
              : this.setState({
                  displayControl: "x-control",
                });
          }}
          onMouseLeave={() =>
            this.setState({
              displayControl: "x-control-hide",
            })
          }
          onMouseMove={this.handleMouseMove}
        >
          <ReactPlayer
            ref={this.playerRef}
            className="react-player"
            width="100%"
            height="100%"
            url={url}
            pip={pip}
            playing={playing}
            controls={controls}
            loop={loop}
            playbackRate={playbackRate}
            volume={volume}
            muted={muted}
            onReady={() => console.log("onReady")}
            onStart={() => console.log("onStart")}
            onPlay={this.handlePlay}
            onEnablePIP={this.handleEnablePIP}
            onDisablePIP={this.handleDisablePIP}
            onPause={this.handlePause}
            onBuffer={() => console.log("onBuffer")}
            onPlaybackRateChange={this.handleOnPlaybackRateChange}
            onSeek={(e) => console.log("onSeek", e)}
            onEnded={this.handleEnded}
            onError={(e) => console.log("onError", e)}
            onProgress={this.handleProgress}
            onDuration={this.handleDuration}
          />
          {/* <div
            onMouseDown={(e) => {
              this.setState({ seeking: true });
            }}
            onMouseUp={(e) => {
              this.setState({ seeking: false });
              const temp = (
                e.nativeEvent.offsetX / e.currentTarget.offsetWidth
              ).toFixed(4);
              this.player.seekTo(parseFloat(temp));
            }}
            onClick={(e) => {
              const temp = (
                e.nativeEvent.offsetX / e.currentTarget.offsetWidth
              ).toFixed(4);
              this.setState({ played: parseFloat(temp) });
            }}
          >
            <div
              className="x-progress-current"
              style={{ width: this.handleProgressWidth(played) }}
              // onMouseDown={this.handleSeekMouseDown}
              // onChange={this.handleSeekChange}
              // onMouseUp={this.handleSeekMouseUp}
            />
            <div className="thumb-container">
              <div
                className="x-progress-thumb"
                style={{ left: this.handleProgressWidth(played) }}
              />
            </div>
            <div
              className="x-progress-loaded"
              style={{ width: this.handleProgressWidth(loaded) }}
            />
            <div className="x-progress-container" />
          </div> */}

          {screenfull.isFullscreen ? (
            <div className={displayFullscreenControl}>
              <div className="x-control-top">
                <div className="x-icon-share" />
                <div
                  onClick={this.handleClickFullscreen}
                  className="x-icon-close"
                />
              </div>
              <div className="x-control-bottom">
                <div className="x-progress-bar">
                  <input
                    className="x-seek"
                    type="range"
                    min={0}
                    max={0.999999}
                    step="any"
                    value={played}
                    onMouseDown={this.handleSeekMouseDown}
                    onChange={this.handleSeekChange}
                    onMouseUp={this.handleSeekMouseUp}
                    style={{
                      backgroundImage: `linear-gradient(to right,
                      var(--primary-main) ${played * 100}%,
                      var(--neutral-20) ${played * 100}%,
                      var(--neutral-20) ${loaded * 100}%,
                      var(--base-10) ${loaded * 100}%
                      `,
                    }}
                  />
                </div>
                <div
                  className="x-control-buttons"
                  onMouseLeave={() => {
                    setTimeout(
                      () => this.setState({ displayVolumeControl: false }),
                      700
                    );
                  }}
                >
                  <div
                    onClick={this.handlePlayPause}
                    className={`${playing ? `x-icon-pause` : `x-icon-play`}`}
                  />
                  <div className="x-icon-next" />
                  <div
                    onMouseEnter={() =>
                      this.setState({ displayVolumeControl: true })
                    }
                    className="x-icon-volume"
                  />

                  {displayVolumeControl && (
                    <input
                      className="x-control-volume"
                      type="range"
                      min={0}
                      max={0.999999}
                      step="any"
                      value={volume}
                      onChange={this.handleVolumeChange}
                      style={{
                        backgroundImage: `linear-gradient(to right,
                        var(--primary-main) ${volume * 100}%,
                        var(--base-10) ${volume * 100}%`,
                      }}
                    />
                  )}
                  <span>
                    <Duration seconds={duration * played} /> /{" "}
                    <Duration seconds={duration} />
                  </span>
                </div>
              </div>
            </div>
          ) : (
            <div className={displayControl}>
              <div
                onClick={this.handlePlayPause}
                className={`x-absolute-center ${
                  playing ? `x-icon-pause-circle` : `x-icon-play-circle`
                }`}
              />
              <div
                onClick={this.handleClickFullscreen}
                className="x-icon-fullscreen"
              />
              <input
                className="x-seek"
                type="range"
                min={0}
                max={0.999999}
                step="any"
                value={played}
                onMouseDown={this.handleSeekMouseDown}
                onChange={this.handleSeekChange}
                onMouseUp={this.handleSeekMouseUp}
                style={{
                  backgroundImage: `linear-gradient(to right,
              var(--primary-main) ${played * 100}%,
              var(--neutral-30) ${played * 100}%,
              var(--neutral-30) ${loaded * 100}%`,
                }}
              />
            </div>
          )}
        </div>
      </div>
    );
  }
}

function Duration({ className, seconds }) {
  return (
    <time dateTime={`P${Math.round(seconds)}S`} className={className}>
      {format(seconds)}
    </time>
  );
}

function format(seconds) {
  const date = new Date(seconds * 1000);
  const hh = date.getUTCHours();
  const mm = date.getUTCMinutes();
  const ss = pad(date.getUTCSeconds());
  if (hh) {
    return `${hh}:${pad(mm)}:${ss}`;
  }
  return `${mm}:${ss}`;
}

function pad(string) {
  return ("0" + string).slice(-2);
}

export default VideoPlayer;
