import React from "react";
import classnames from "classnames";

const TabBar = ({ isActiveTab, tabItems, onClick }) => {
  return (
    <div className="x-tab-bar">
      <div className="x-tab">
        {tabItems?.map((item, idx) => (
          <p
            key={idx}
            className={classnames(
              "cursor-pointer w-50 heading-sm-bold",
              isActiveTab === item.type && "active"
            )}
            type={item.type}
            onClick={onClick}
          >
            {item.name}
          </p>
        ))}
      </div>
    </div>
  );
};

export default TabBar;
