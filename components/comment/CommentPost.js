import React from 'react';
import { Button } from 'react-bootstrap';
import InputText from '/components/input/InputText';
import { getAuth } from "/services/utils/helper";

const CommentPost = ({ onChangeText, onSubmit, placeholder, value }) => {
  const { name_user, picture_user } = getAuth();

  return (
    <form onSubmit={onSubmit} className="x-comment-post">
      <div className="comment-post-pict">
        {picture_user ? (
          <img src={picture_user} alt="profile" />
        ) : (
          <div className="x-empty-image">
            <p
              className="text-xl-normal m-0"
              style={{ color: "var(--base-10)" }}
            >
              {name_user?.slice(0, 1).toUpperCase()}
            </p>
          </div>
        )}
      </div>
      <div className="comment-input">
        <InputText
          onChangeText={onChangeText}
          value={value}
          placeholder={placeholder}
          disableError
        />
      </div>
      <div className="comment-button">
        <Button type="submit" disabled={value.length === 0}>
          Post
        </Button>
      </div>
    </form>
  );
}

export default CommentPost;
