import { useState } from "react";
import { formatDate, apiDispatch } from "/services/utils/helper";

const ReplyCommentCard = ({ data, postApiLike }) => {
  const {
    profile_picture,
    full_name,
    created_at,
    comment,
    total_like,
  } = data;

  const [likeCount, setLikeCount] = useState(total_like);

  const handleLike = () => {
    const { api, params } = postApiLike;
    apiDispatch(api, params, true).then(response => {
      response.code === 200 && setLikeCount(response.result.total_like);
    });
  };

  return (
    <div className="x-comment-card">
      <div className="x-comment-profile">
        {profile_picture ? (
          <img src={profile_picture} alt="profile" />
        ) : (
          <div className="x-empty-image">
            <p
              className="text-xl-normal m-0"
              style={{ color: "var(--base-10)" }}
            >
              {full_name.slice(0, 1).toUpperCase()}
            </p>
          </div>
        )}
      </div>
      <div className="x-comment-content">
        <div className="content-top">
          <div className="top-name">
            <span className="text-md-bold">{full_name}</span>
            <span
              className="text-sm-light"
              style={{ color: "var(--neutral-70)" }}
            >
              {" "}
              •{" "}
            </span>
            <span
              className="text-md-light"
              style={{ color: "var(--neutral-70)" }}
            >
              {formatDate(created_at)}
            </span>
          </div>
          <div className="top-desc">
            <span
              className="text-md-light"
              style={{ color: "var(--neutral-70)" }}
            >
              {comment}
            </span>
          </div>
        </div>
        <div className="x-content-bottom">
          <div onClick={handleLike} className="x-icon-like" />
          <span
            style={
              likeCount >= 1
                ? { color: "var(--primary)" }
                : { color: "var(--neutral-70)" }
            }
          >
            {likeCount}
          </span>
        </div>
      </div>
    </div>
  );
};

export default ReplyCommentCard;
