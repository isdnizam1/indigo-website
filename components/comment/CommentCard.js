import { Fragment, useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { formatDate, apiDispatch } from "/services/utils/helper";
import InputText from "/components/input/InputText";
import ReplyCommentCard from "/components/comment/ReplyCommentCard";
import SkeletonElement from "/components/skeleton/SkeletonElement";

const CommentCard = ({ data, postApiLike, postApiSubmit, onCompleteSubmit, isLoading }) => {
  const {
    profile_picture,
    full_name,
    created_at,
    comment,
    reply_comment,
    total_like,
  } = data;
  
  const [likeCount, setLikeCount] = useState(total_like);
  const [showReplyInput, setShowReplyInput] = useState(false);
  const [reply, setReply] = useState("");
  const [replies, setReplies] = useState([]);

  useEffect(() => setReplies(reply_comment), [reply_comment]);

  const handleLike = () => {
    apiDispatch(postApiLike.api, postApiLike.params, true).then((response) => {
      response.code === 200 && setLikeCount(response.result.total_like);
    });
  };

  const handleReplySubmit = (e) => {
    apiDispatch(postApiSubmit.api, {
      ...postApiSubmit.params,
      comment: reply
    }, true).then(response => {
      if(response.code === 200) {
        onCompleteSubmit();
        setReply("");
      }
    });
    e.preventDefault();
  };

  return (
    <div className="x-comment-card">
      <div className="x-comment-profile">
        {(!isLoading && profile_picture) ? (
          <img src={profile_picture} alt="profile" />
        ) : (!isLoading ?
          <div className="x-empty-image">
            <p
              className="text-xl-normal m-0"
              style={{ color: "var(--base-10)" }}
            >
              {full_name.slice(0, 1).toUpperCase()}
            </p>
          </div> : <SkeletonElement width={48} height={48} type="rounded-circle" />
        )}
      </div>
      <div className="x-comment-content">
        <div className="content-top">
          <div className="top-name">
            {!isLoading ?
            <Fragment>
            <span className="text-md-bold">{full_name}</span>
            <span
              className="text-sm-light"
              style={{ color: "var(--neutral-70)" }}
            >
              {" "}
              •{" "}
            </span>
            <span
              className="text-md-light"
              style={{ color: "var(--neutral-70)" }}
            >
              {formatDate(created_at)}
            </span>
            </Fragment>
            : <SkeletonElement width={85} height={14} className="my-1"/>}
          </div>
          <div className="top-desc">
          {!isLoading ? <span
              className="text-md-light"
              style={{ color: "var(--neutral-70)" }}
            >
              {comment}
            </span> : <SkeletonElement width="100%" height={12} className="my-2" count={2}/> }
          </div>
        </div>
        <div className="x-content-bottom">
          <div onClick={handleLike} className="x-icon-like" />
          {!isLoading ? <span
            style={
              likeCount >= 1
                ? { color: "var(--primary)" }
                : { color: "var(--neutral-70)" }
            }
          >
            {likeCount}
          </span>: <SkeletonElement width={35} height={14} /> }
          <span>|</span>

          {reply_comment && reply_comment.length > 0 ? (
            <span
              onClick={() => setShowReplyInput(!showReplyInput)}
              style={{ cursor: "pointer" }}
            >
              {reply_comment.length} balasan
            </span>
          ) : (
            <span
              onClick={() => setShowReplyInput(!showReplyInput)}
              style={{ cursor: "pointer" }}
            >
              balas
            </span>
          )}
        </div>
        {showReplyInput && (
          <form onSubmit={handleReplySubmit} className="x-input-reply">
            <InputText
              value={reply}
              onChangeText={(e) => setReply(e.target.value)}
              placeholder="Give your reply here"
              disableError
            />
            <div>
              <Button type="submit" disabled={reply.length === 0}>
                Post
              </Button>
            </div>
          </form>
        )}
        <div className="mt-3">
          {replies && replies.length > 0 &&
            replies.map((reply, idx) => (
              <Fragment key={idx}>
                <ReplyCommentCard
                  data={reply}
                  postApiLike={{
                    ...postApiLike,
                    params: {
                      ...postApiLike.params,
                      id_related_to: reply.id_comment,
                    },
                  }}
                />
              </Fragment>
            ))}
        </div>
      </div>
    </div>
  );
}

export default CommentCard;
