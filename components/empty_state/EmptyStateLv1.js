import React from "react";
import Image from "next/image";

const EmptyStateLv1 = ({ materialIcons, customIcon, title, subTitle }) => {
  return (
    <div className="x-emptystate-component">
      <div className="x-rounded-icon">
        {materialIcons && (
          <span className="material-icons-round">{materialIcons}</span>
        )}
        {customIcon && <Image src={customIcon} alt="" />}
      </div>
      {title && <h4>{title}</h4>}
      {subTitle && <p>{subTitle}</p>}
    </div>
  );
};

export default EmptyStateLv1;
