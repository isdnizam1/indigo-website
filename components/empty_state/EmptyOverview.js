import React from 'react';
import { Button } from 'react-bootstrap';

const EmptyOverview = (props) => {
  const {
    head,
    title,
    subtitle,
    icon,
    buttonText,
    onClick
  } = props;

  return (
    <div>
      <h3>{head}</h3>
      <div className="x-details-empty">
        {icon}
        <p className="x-empty-title">{title}</p>
        <p className="x-empty-subtitle">{subtitle}</p>
        <div>
          <Button onClick={onClick}>{buttonText}</Button>
        </div>
      </div>
    </div>
  );
}

export default EmptyOverview;
