import { Component } from "react";
import { Enhance } from "/services/Enhancer";
import { Row, Col, Button, Card, Image, Badge } from "react-bootstrap";
import InputText from "/components/input/InputText";
import { postEventParticipant } from "/services/actions/api";
import { capitalizeFirstLetter, dateFormatter, getAuth, setAuth } from "/services/utils/helper";
import ModalConfirmation from "/components/modal/ModalConfirmation";
import ModalSuccess from "/components/modal/ModalSuccess";
import { isEmpty } from "lodash-es";
import Router from "next/router";
import SkeletonElement from "components/skeleton/SkeletonElement";

class EventSelectionList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      showModalBlock: false,
      eventList: [],
      isDisableButton: true,
      eventCode: false,
    };
  }
  handleClose = () => {
    this.setState({ showModal: false });
    this.setState({ showModalBlock: false });
  };

  expandRequest = () => {
    this.setState({ showModalBlock: true })
  }

  onSubmit = () => {
    const { data } = this.props;
    if (data.user_joined === true) this.handleSubmit();
    else this.setState({ showModal: true });
  };

  handleSubmit = () => {
    const { data, apiDispatch } = this.props;
    const { eventCode } = this.state;
    const user = getAuth();

    // apiDispatch from Enhancer
    apiDispatch(
      postEventParticipant,
      {
        id_event: data.id_event,
        id_user: user.id_user,
        referral_code: !data.user_joined ? eventCode.toUpperCase() : null,
      },
      true
    ).then((response) => {
      // callback from apiDispatch
      if (response.code === 200) {
        const { event_title } = response.result;

        // set new auth for assign new event title
        setAuth({
          ...getAuth(),
          id_user: user.id_user,
          email: user.email,
          registration_step: user.registration_step,
          token: user.token,
          event_title,
        }).then((auth) => {
          // callback from setAuth
          // redirect to home
          Router.push("/home");
        });
      }
    });
  };

  eventCodeOnChange = (event) => {
    const { data } = this.props;
    const code = event.target.value;

    if (code.toLowerCase() !== data.referral_code.toLowerCase()) {
      this.setState({ codeIsError: true, isDisableButton: true });
    } else {
      this.setState({
        codeIsError: false,
        isDisableButton: false,
        eventCode: code,
      });
    }
  };

  renderModal() {
    const { showModal, codeIsError, isDisableButton } = this.state;
    return (
      <>
        <ModalConfirmation
          backdrop={"static"}
          show={showModal}
          // onHide={() => this.setState({ modalIsOpened: false })}
          customTitle={<h2 className="heading-md-bold color-neutral-old-80">{`Event Code`}</h2>}
          subTitle={
            <>
              <p
                className="text-lg-normal pt-1 color-neutral-60"
              >
                Silahkan isi event code dari event yang ingin kamu ikuti
              </p>
              <InputText
                type="text"
                onChangeText={this.eventCodeOnChange.bind(this)}
                placeholder="Masukkan event code"
                errorSign=""
                extraErrorSignLink=""
                linkTo="/home"
                isError={codeIsError}
              />
            </>
          }
          customFooter={
            <>
              <Button
                className="btn-primary w-100 mt-4"
                onClick={() => this.handleSubmit()}
                disabled={isDisableButton}
              >
                Masuk
              </Button>

              <Button
                className="btn-cancel w-100 mt-3"
                onClick={() =>
                  this.setState({
                    showModal: false,
                  })
                }
              >
                Batalkan
              </Button>
            </>
          }
        />
      </>
    );
  }

  renderModalBlock() {
    const { showModalBlock } = this.state;
    return (
      <>
        <ModalSuccess
          backdrop={"static"}
          show={showModalBlock}
          onHide={() => this.setState({ showModalBlock: false })}

          customTitle={
            <>
              <div className="x-icon-blocked" />
              <h2 className="heading-md-bold">You’ve been blocked from this event for several reasons</h2>
            </>
          }
          subTitle={
            <>
              <p
                className="text-lg-normal pt-1"
                style={{ color: "var(--neutral-60)" }}
              >
                Check your email or contact your event organizer for re-activate your account to this event.
              </p>

              <Button
                className="btn-primary w-100 mt-4"
                onClick={() => this.setState({
                  showModalBlock: false,
                })}
              >
                Yes, I understand
              </Button>
            </>
          }
        />
      </>
    );
  }

  render() {
    const { data } = this.props;

    return (
      <>
        <Col lg={4}>
          <Card className="x-card-event">
            {!this.props.isLoading && (data.user_joined || data.user_blocked) && (
              <Badge
                bg="danger"
                style={{
                  position: "absolute",
                  zIndex: 10,
                  top: 0,
                  color: "white",
                  borderTopLeftRadius: 6,
                  borderTopRightRadius: 0,
                  borderBottomRightRadius: 6,
                  borderBottomLeftRadius: 0,
                  justifyContent: "center",
                  alignItems: "flex-end",
                  paddingRight: "10px",
                  paddingLeft: "10px",
                }}
              >
                {data.user_joined ? "Diikuti" : data.user_blocked && "Blocked"}
              </Badge>
            )}
            {!this.props.isLoading ? <Card.Img
              variant="top"
              src={data.image}
              className="image-card"
              style={{ height: "180px" }}
            /> : <SkeletonElement type="image-card" />}
            <Card.Body>
              <Card.Title className="heading-md-bold">
                {!this.props.isLoading ? data.event_title : <SkeletonElement height={24} width="70%" />}
              </Card.Title>
              <div className="d-flex align-items-center">
                {!this.props.isLoading ? <p
                  className="text-lg-light m-0"
                  style={{ color: "var(--neutral-60)" }}
                >
                  {capitalizeFirstLetter(data.event_location)}
                  <span
                    className="material-icons ps-1 pe-1"
                    style={{ fontSize: "7px" }}
                  >
                    fiber_manual_record
                  </span>
                  {dateFormatter(data.start_date)}
                  {" - "}
                  {dateFormatter(data.end_date)}
                </p> : <SkeletonElement width={150} height={16}/>}
              </div>
            </Card.Body>

            <Card.Footer className="card-footer">
              <Row className="m-0 w-100 p-0">
                <Col
                  lg={12}
                  className="d-flex align-items-center justify-content-between p-0"
                >
                  <div className="d-flex align-items-center">
                    {!this.props.isLoading && !isEmpty(data.vendor_image) ? (
                      <Image
                        variant="top"
                        src={data.vendor_image}
                        className="image-rounded"
                      />
                    ) :
                      !this.props.isLoading ? <div className="x-default-vendor">
                        <p
                          className="text-lg-light m-0"
                          style={{ color: "var(--base-10)" }}
                        >
                          {data.vendor_name.charAt(0).toUpperCase()}
                        </p>
                      </div>
                    : <SkeletonElement type="rounded-circle" height={40} width={40} />}

                    {!this.props.isLoading ?<p className="text-lg-bold m-0 ps-2">{data.vendor_name}</p>
                     : <SkeletonElement width={80} height={16} className="ms-2"/>}
                  </div>

                  {!this.props.isLoading && data.user_blocked ? (
                    <Button
                      className="btn-primary"
                      onClick={() => {
                        this.expandRequest();
                      }}
                    >
                      Details
                    </Button>
                  ) : (
                    <Button className="btn-primary" onClick={this.onSubmit}>
                      Ikuti Event
                    </Button>
                  )}
                </Col>
              </Row>
            </Card.Footer>
          </Card>
        </Col>
        {this.renderModal()}
        {this.renderModalBlock()}
      </>
    );
  }
}
export default Enhance(EventSelectionList);
