import React, { Fragment } from "react";
import { isEmpty } from "lodash-es";
import { capitalizeFirstLetter, logout } from "/services/utils/helper";
import { Button, Col, Row } from "react-bootstrap";
import { useRouter } from "next/router";
import classnames from "classnames";
import { useDispatch } from "react-redux";
import { setChatData } from "/redux/slices/chatSlice";

const ProfileCard = ({
  className,
  onClick,
  image,
  isFollowed,
  id,
  idGroupMessage,
  name,
  jobTitle,
  cityName,
  countryName,
  about,
  followersCount,
  followingCount,
  interests,
  onFollowAction,
}) => {
  const { push } = useRouter();
  const dispatch = useDispatch();

  return (
    <div
      className={classnames("x-profilecard-content", className && className)}
    >
      <div className="x-detail" onClick={onClick}>
        <div className="d-flex justify-content-center w-100">
          {image ? (
            <img src={image} alt="" />
          ) : (
            <div className="x-empty-image">
              <h1 className="heading-xl-normal color-base-10 m-0">
                {name.slice(0, 1).toUpperCase()}
              </h1>
            </div>
          )}
        </div>

        <h3 className="heading-md-bold text-center pt-4 m-0 color-neutral-old-80">
          {!isEmpty(name) && capitalizeFirstLetter(name)}
        </h3>

        <p className="text-md-normal text-center m-0 mb-3 color-neutral-old-40">
          {!isEmpty(jobTitle) && jobTitle}
        </p>

        {!isEmpty(about) && (
          <>
            <p className="text-md-light line-height-md text-justify m-0 color-neutral-old-40">
              {about}
            </p>

            <p className="text-md-normal text-start mt-2 mb-0 color-neutral-70">
              {`${cityName}, ${countryName}`}
            </p>
          </>
        )}
      </div>

      <div className="x-separator" />

      <Row className="pt-3 pb-2 px-4 w-100">
        <Col md={6}>
          <p className="text-sm-normal text-center m-0 color-neutral-old-40">
            Followers
          </p>
          <h3 className="heading-md-bold text-center m-0">
            {!isEmpty(followersCount) ? followersCount : 0}
          </h3>
        </Col>

        <Col md={6}>
          <p className="text-sm-normal text-center m-0 color-neutral-old-40">
            Following
          </p>
          <h3 className="heading-md-bold text-center m-0">
            {!isEmpty(followingCount) ? followingCount : 0}
          </h3>
        </Col>
      </Row>

      <div className="x-separator" />

      {!id ? (
        <div className="x-action-personal">
          <Button
            className="btn-cancel d-flex align-items-center justify-content-center"
            onClick={() => logout()}
          >
            <span className="material-icons-round">open_in_new</span>
            Log Out
          </Button>

          {!isEmpty(interests) && (
            <Fragment>
              <p className="text-sm-normal pt-3 m-0 color-neutral-60">
                Interest
              </p>

              <div className="x-interest-wrapper">
                {interests.map((interest, idx) => (
                  <div className="x-interest" key={idx}>
                    <p>{interest.interest_name}</p>
                  </div>
                ))}
              </div>
            </Fragment>
          )}
        </div>
      ) : (
        <div className="x-action-user">
          {!isEmpty(interests) && (
            <Fragment>
              <p className="text-sm-normal m-0 color-neutral-60">Interest</p>

              <div className="x-interest-wrapper">
                {interests.map((interest, idx) => (
                  <div className="x-interest" key={idx}>
                    <p>{interest.interest_name}</p>
                  </div>
                ))}
              </div>

              <div className="x-button-wrapper">
                <Button
                  className="btn-delete d-flex align-items-center justify-content-center"
                  onClick={() => push({ pathname: "/profile", query: { id } })}
                >
                  Visit Profile
                </Button>

                <Button
                  className="btn-delete d-flex align-items-center justify-content-center"
                  onClick={() => {
                    dispatch(
                      setChatData({
                        selectedMessage: {
                          id_user: id,
                          full_name: name,
                          image: image,
                          id_groupmessage: idGroupMessage,
                          type: "personal",
                        },
                      })
                    );
                    push({
                      pathname: "/chat",
                      query: {
                        id_message: !idGroupMessage ? false : idGroupMessage,
                        message_type: "message",
                        type: "personal",
                      },
                    });
                  }}
                >
                  Message
                </Button>
              </div>

              <Button
                className={classnames(
                  "d-flex align-items-center justify-content-center",
                  isFollowed && "btn-delete"
                )}
                onClick={onFollowAction}
              >
                {isFollowed ? "Followed" : "Follow"}
              </Button>
            </Fragment>
          )}
        </div>
      )}
    </div>
  );
};

export default ProfileCard;
