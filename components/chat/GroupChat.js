import React, { useState } from "react";
import { Button } from "react-bootstrap";
import { postCreateGroupMessage } from "/services/actions/api";
import { getAuth, convertToBase64 } from "/services/utils/helper";
import { Enhance } from "/services/Enhancer";
import DefaultPicture from "/public/assets/images/img-default-groupchat-photo.svg";
import { isEmpty } from "lodash-es";
import InputText from "/components/input/InputText";
import AddPhoto from "/components/input/AddPhoto";
import { useRouter } from "next/router";
import ModalConfirmation from "/components/modal/ModalConfirmation";
import { useDispatch, useSelector } from "react-redux";
import { setChatData } from "/redux/slices/chatSlice";
import MaterialIcon from "components/utils/MaterialIcon";

const GroupChat = (props) => {
  const { id_user, picture_user, name_user } = getAuth();
  const [profilePicture, setProfilePicture] = useState(null);
  const [title, setTitle] = useState("");
  const router = useRouter();
  const [modal, setModal] = useState(false);

  const chatData = useSelector((state) => state.chatReducer.data);

  const { selectedMembers, allFeatureUpdateCount, id_ads } = chatData;
  const dispatch = useDispatch();
  const { apiDispatch } = props;

  console.log(chatData);
  const createGroup = () => {
    let selectedMembersById = [id_user];
    selectedMembers.forEach((item) => selectedMembersById.push(item.id_user));

    apiDispatch(
      postCreateGroupMessage,
      {
        id_user,
        title,
        list_user: selectedMembersById,
        image: profilePicture,
        id_ads,
      },
      true
    ).then((response) => {
      if (response.code === 200) {
        const result = response.result;

        dispatch(
          setChatData({
            selectedMessage: result,
            selectedMembers: [],
            allFeatureUpdateCount: 1 + allFeatureUpdateCount,
          })
        );

        router.push({
          query: {
            id_message: result.id_groupmessage,
            message_type: "message",
            type: !result.type ? "personal" : result.type,
          },
        });
      }
    });
  };

  const onImageChange = (event) => {
    if (event.target.files && event.target.files[0]) {
      let img = event.target.files[0];
      convertToBase64(img).then((result) => setProfilePicture(result));
    }
  };

  const deleteMember = (id_user) => {
    selectedMembers.length <= 1 &&
      router.push({
        query: router.query?.id_message
          ? { id_message: router.query.id_message, message_type: "add_member" }
          : { message_type: "add_member" },
      });
    dispatch(
      setChatData({
        selectedMembers: selectedMembers.filter(
          (member) => member.id_user !== id_user
        ),
      })
    );
  };

  const renderModal = () => {
    return (
      <>
        <ModalConfirmation
          show={modal}
          onHide={() => setModal(false)}
          customExitIcon={
            <MaterialIcon
              action="clear"
              className="font-weight-bolder"
              size={28}
              color="neutral-70"
            />
          }
          customTitle={
            <>
              <h1 className="heading-lg-bold mt-2">Create Group Chat</h1>
              <p
                className="pt-1 text-lg-normal"
                style={{ color: "var(--neutral-50)" }}
              >
                Creating your new group chat is not finished. Continue the
                process?
              </p>
              <div className="justify-content-center">
                <Button
                  className="btn-primary m-2"
                  onClick={() => setModal(false)}
                >
                  Continue
                </Button>
                <Button
                  className="btn-cancel m-2"
                  onClick={() => {
                    dispatch(setChatData({ selectedMembers: [] }));
                    router.push({
                      query: router.query?.id_message
                        ? {
                            id_message: router.query.id_message,
                            message_type: "contact",
                          }
                        : { message_type: "contact" },
                    });
                  }}
                >
                  Cancel
                </Button>
              </div>
            </>
          }
        />
      </>
    );
  };

  return (
    <div className="x-groupchat">
      <div className="x-contact-list-container">
        <div className="x-header-top">
          <div className="x-icon-back" onClick={() => setModal(true)}></div>
          <h3>Group Chat</h3>
        </div>
        <div className="msg-hr mb-2" />
        <div className="contact-list">
          <div className="contact-card-container d-flex justify-content-center">
            <AddPhoto
              image={
                !isEmpty(profilePicture) ? profilePicture : DefaultPicture.src
              }
              onClick={(e) => onImageChange(e)}
            />
          </div>
        </div>

        <div className="contact-list pb-0">
          <div className="contact-card-container">
            <InputText
              title="Group Chat"
              type="text"
              value={title}
              onChangeText={(event) => setTitle(event.target.value)}
              placeholder="Write Group Name"
            />
          </div>
        </div>
        <div className="d-flex flex-row ms-3 me-3 justify-content-between">
          <p
            className="text-sm-normal m-0"
            style={{ color: "var(--neutral-50" }}
          >
            {selectedMembers.length + 1} Member
          </p>
        </div>
        <div
          className="x-add-member d-flex align-items-center cursor-pointer"
          onClick={() => {
            router.push({
              query: router.query?.id_message
                ? {
                    id_message: router.query.id_message,
                    message_type: "add_member",
                  }
                : { message_type: "add_member" },
            });
          }}
        >
          <div className="x-add">
            <MaterialIcon action="add" size={32} color="base-10" />
          </div>
          <p
            className="m-0 ms-2 text-md-bold line-height-sm"
            style={{ color: "var(--neutral-80)" }}
          >
            Add Member
          </p>
        </div>
        <div className="x-divider" />
        <div className="x-list-wrapper">
          <div className="contact-list">
            <div className="contact-card-container">
              <div className="contact-wrapper">
                <div className="x-wrapper-content">
                  <div className="img-container">
                    {picture_user ? (
                      <img src={picture_user} alt="profile" />
                    ) : (
                      <div className="x-empty-image">
                        <p
                          className="text-xl-normal m-0"
                          style={{ color: "var(--base-10)" }}
                        >
                          {name_user?.slice(0, 1).toUpperCase()}
                        </p>
                      </div>
                    )}
                  </div>
                  <div className="contact-body d-flex align-items-center justify-content-between">
                    <h3 className="contact-name mb-1">You</h3>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="x-divider" />
          {selectedMembers.map((item, idx) => {
            return (
              <div key={idx}>
                <div className="contact-list">
                  <div className="contact-card-container">
                    <div className="contact-wrapper">
                      <div className="x-wrapper-content">
                        <div className="img-container">
                          {item.profile_picture ? (
                            <img src={item.profile_picture} alt="profile" />
                          ) : (
                            <div className="x-empty-image">
                              <p
                                className="text-xl-normal m-0"
                                style={{ color: "var(--base-10)" }}
                              >
                                {item?.full_name?.slice(0, 1).toUpperCase()}
                              </p>
                            </div>
                          )}
                        </div>
                        <div className="contact-body d-flex align-items-center justify-content-between">
                          <h3 className="contact-name mb-1">
                            {item.full_name}
                          </h3>
                        </div>
                      </div>
                      <div
                        className="x-delete-member d-flex align-items-center"
                        onClick={() => deleteMember(item.id_user)}
                      >
                        <div className="x-close">
                          <MaterialIcon
                            action="close"
                            size={15}
                            color="base-10"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="x-divider" />
              </div>
            );
          })}
        </div>
        <div className="x-btn-wrapper">
          <Button
            className="x-btn-create"
            onClick={() => createGroup()}
            disabled={isEmpty(title) || isEmpty(selectedMembers)}
          >
            Create
          </Button>
        </div>
      </div>
      {renderModal()}
    </div>
  );
};

export default Enhance(GroupChat);
