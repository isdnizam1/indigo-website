import { useState, useEffect, Fragment } from "react";
import {
  getProfileFollowing,
  getSuggestedContact,
} from "/services/actions/api";
import { getAuth } from "/services/utils/helper";
import { Enhance } from "/services/Enhancer";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { setChatData } from "/redux/slices/chatSlice";
import EmptyStateLv1 from "/components/empty_state/EmptyStateLv1";
import { Button } from "react-bootstrap";
import { isEmpty } from "lodash-es";
import MaterialIcon from "components/utils/MaterialIcon";
import SkeletonElement from "components/skeleton/SkeletonElement";

const ListContact = (props) => {
  const { apiDispatch } = props;
  const [listContact, setListContact] = useState([]);
  const [suggestedContact, setSuggestedContact] = useState([]);
  const { id_user } = getAuth();
  const router = useRouter();
  const [isLoadingSuggested, setIsLoadingSuggested] = useState(true);
  const [isLoadingList, setIsLoadingList] = useState(true);
  let dynamicSuggestedContact = isLoadingSuggested ? [1] : suggestedContact;
  let dynamicListContact = isLoadingList ? [1] : listContact;


  const dispatch = useDispatch();
  const chatData = useSelector((state) => state.chatReducer.data);

  const { detailMessageUpdateCount } = chatData;

  useEffect(() => {
    apiDispatch(getProfileFollowing, { followed_by: id_user }, true).then(
      (response) => {
        setIsLoadingList(false);
        if (response.code === 200) {
          const result = response.result;
          setListContact(result);
        }
      }
    );
    apiDispatch(getSuggestedContact, { id_user }, true).then((response) => {
      setIsLoadingSuggested(false);
      if (response.code === 200) {
        const result = response.result;
        setSuggestedContact(result);
      }
    });
  }, []);

  const setChatPersonal = (user) => {
    dispatch(
      setChatData({
        selectedMessage: {
          id_groupmessage: user.id_groupmessage,
          id_user: user.id_user,
          full_name: user.full_name,
          title: user.full_name,
          // updated_at: result.created_at,
          // status: result.status,
          profile_picture: user.profile_picture,
          type: "personal",
        },
        detailMessageUpdateCount: 1 + detailMessageUpdateCount,
      })
    );

    router.push({
      query: {
        id_message: !user.id_groupmessage ? false : user.id_groupmessage,
        message_type: "contact",
        type: "personal",
      },
    });
  };

  return (
    <div className="x-listcontacts-page">
      <div className="x-contact-list-container">
        <div className="x-header-top">
          <div
            className="x-icon-back"
            onClick={() =>
              router.push({
                query: router.query?.id_message
                  ? {
                    id_message: router.query.id_message,
                    message_type: "message",
                  }
                  : { message_type: "message" },
              })
            }
          ></div>
          <h3>Contact</h3>
        </div>
        <div className="msg-hr mb-2" />
        <div className="contact-list cursor-pointer">
          <div className="contact-card-container">
            <div className="contact-wrapper">
              <div className="x-create-group d-flex align-items-center">
                <div className="x-add">
                  <MaterialIcon action="group_add" color="base-10" />
                </div>
                {/* <p
                  className="m-0 ms-2 text-md-bold line-height-sm"
                  style={{ color: "var(--neutral-80)" }}
                >
                  Add Member
                </p> */}
              </div>
              {/* <div className="img-container">
                <img src={LogoEventeer.src} />
              </div> */}
              <div
                className="contact-body"
                onClick={() => {
                  dispatch(setChatData({ existingMembers: [] }));

                  router.push({
                    pathname: `/chat`,
                    query: router.query?.id_message
                      ? {
                        id_message: router.query.id_message,
                        message_type: "add_member",
                      }
                      : { message_type: "add_member" },
                  });
                }}
              >
                <h3 className="contact-name mb-1">Add Group Chat</h3>
                <p className="contact-text">
                  Invite your friends to discuss together
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="msg-hr mb-2" />
        <p
          className="text-sm-normal mb-1 ms-3"
          style={{ color: "var(--neutral-50" }}
        >
          Suggested
        </p>
        <div className="x-list-wrapper x-suggested">
          {dynamicSuggestedContact.map(
            (item, idx) =>
              item.id_user !== id_user && (
                <div
                  className="contact-list"
                  key={idx}
                  onClick={() => setChatPersonal(item)}
                >
                  <div className="contact-card-container">
                    <div className="contact-wrapper">
                      <div className="img-container">
                        {item.profile_picture ? (
                          <img src={item.profile_picture} alt="profile" />
                        ) : (
                          !isLoadingSuggested ? <div className="x-empty-image">
                            <p
                              className="text-xl-normal m-0"
                              style={{ color: "var(--base-10)" }}
                            >
                              {item.full_name.slice(0, 1).toUpperCase()}
                            </p>
                          </div> : <SkeletonElement type="rounded-circle" height={48} width={48} />
                        )}
                      </div>
                      <div className="contact-body">
                        {!isLoadingSuggested ?
                          <Fragment>
                            <h3 className="contact-name mb-1">{item.full_name}</h3>
                            <p className="contact-text">{item.job_title}</p>
                          </Fragment> :
                          <SkeletonElement width={120} height={12} count={2} className="my-1" />}
                      </div>
                    </div>
                  </div>
                </div>
              )
          )
          }
        </div>
        <div className="msg-hr"></div>
        {(!isLoadingList && isEmpty(listContact)) ? (
          <div className="x-contact x-empty-contact">
            <div className="w-75">
              <EmptyStateLv1
                materialIcons="group_add"
                title="Add friend"
                subTitle="Find more users in the Indigo community"
              />
            </div>
            <Button
              onClick={() => {
                router.push("/participants");
              }}
              className="w-50 mt-3"
            >
              Search friend
            </Button>
          </div>
        ) : (
          <>
            <div className="d-flex mx-3 pt-2 justify-content-between">
              <p
                className="text-sm-normal"
                style={{ color: "var(--neutral-50" }}
              >
                Friend List
              </p>
              <p
                className="text-sm-normal cursor-pointer color-semantic-main"
                onClick={() => {
                  router.push("/participants");
                }}
              >
                Search Friend
              </p>
            </div>
            <div className="x-list-wrapper x-contact">
              {dynamicListContact.map(
                (item, idx) =>
                  item.id_user !== id_user && (
                    <div
                      className="contact-list"
                      key={idx}
                      onClick={() => setChatPersonal(item)}
                    >
                      <div className="contact-card-container">
                        <div className="contact-wrapper">
                          <div className="img-container">
                            {item.profile_picture ? (
                              <img src={item.profile_picture} alt="profile" />
                            ) : (
                              !isLoadingList ? <div className="x-empty-image">
                                <p
                                  className="text-xl-normal m-0"
                                  style={{ color: "var(--base-10)" }}
                                >
                                  {item.full_name.slice(0, 1).toUpperCase()}
                                </p>
                              </div> : <SkeletonElement type="rounded-circle" height={48} width={48} />
                            )}
                          </div>
                          <div className="contact-body">
                            {!isLoadingList ?
                              <Fragment><h3 className="contact-name mb-1">
                                {item.full_name}
                              </h3>
                                <p className="contact-text">{item.job_title}</p>
                              </Fragment> :
                              <SkeletonElement width={120} height={12} count={2} className="my-1" />}
                          </div>
                        </div>
                      </div>
                    </div>
                  )
              )}
            </div>
          </>
        )}
      </div>
    </div>
  );
};

export default Enhance(ListContact);
