import React from 'react';
import classNames from 'classnames';
import ModalSuccess from "/components/modal/ModalSuccess";
import { Button } from 'react-bootstrap';

const ModalLeaveGroupSuccess = (props) => {
  const {
    show,
    onHide,
    successAction
  } = props;

  return (
    <ModalSuccess
      show={show}
      onHide={onHide}
      customTitle={
        <>
          <div className="m-0 x-icon-success-delete" />
          <h1 className="heading-lg-bold">Success</h1>
          <p
            className={classNames("pt-1 mt-2 text-lg-normal")}
            style={{ color: "var(--neutral-80)" }}
          >
            You have left the group
          </p>

          <Button
            className="btn-primary m-2"
            onClick={successAction}
          >
            Back to message
          </Button>
        </>
      }
    />
  );
}

export default ModalLeaveGroupSuccess;
