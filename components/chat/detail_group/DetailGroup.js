import React, { Fragment } from "react";
import { Form } from "react-bootstrap";
import {
  getAuth,
  convertToBase64,
  detGroupFormatter,
  apiDispatch,
} from "/services/utils/helper";
import classnames from "classnames";
import InputText from "/components/input/InputText";
import { useEffect, useState } from "react";
import { isEmpty } from "lodash-es";
import {
  getDetailMessage,
  putEditGroupChat,
  putEditMember,
  postRemoveMember,
  // getListMessage,
  postDeleteChat,
} from "/services/actions/api";
import MemberList from "/components/chat/MemberList";
import AddPhoto from "/components/input/AddPhoto";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { setChatData } from "/redux/slices/chatSlice";
import ModalLeaveGroupConfirmation from "./ModalLeaveGroupConfirmation";
import ModalLeaveGroupSuccess from "./ModalLeaveGroupSuccess";
import ModalSetAdminConfirmation from "./ModalSetAdminConfirmation";
import MaterialIcon from "components/utils/MaterialIcon";
import SkeletonElement from "components/skeleton/SkeletonElement";

const DetailGroup = (props) => {
  const { id_user } = getAuth();
  const router = useRouter();
  const [header, setHeader] = useState([]);
  const [member, setMember] = useState([]);
  // const [listMessage, setListMessage] = useState([]);
  const [modalChangeAdmin, setmodalChangeAdmin] = useState(false);
  const [modalLeaveGroup, setmodalLeaveGroup] = useState(false);
  const [modalSuccessIsOpened, setmodalSuccessIsOpened] = useState(false);
  const [profileImage, setProfileImage] = useState("");
  const [titleGroup, setTitleGroup] = useState("");
  const [shownEditTitle, setShownEditTitle] = useState(false);
  const [isAdmin, setIsAdmin] = useState(false);
  const [changeAdmin, setChangeAdmin] = useState(false);
  const [leaveGroup, setLeaveGroup] = useState(false);
  const [totalDisplayedMember, setTotalDisplayedMember] = useState(5);
  const [isLoading, setIsLoading] = useState(true);

  let dynamicMember = isLoading ? [1,2,3] : member;

  const chatData = useSelector((state) => state.chatReducer.data);
  const { allFeatureUpdateCount, selectedMessage, detailGroupUpdateCount } =
    chatData;
  const { id_groupmessage, type } = selectedMessage;
  const dispatch = useDispatch();

  useEffect(() => {
    apiDispatch(
      getDetailMessage,
      {
        id_groupmessage,
        id_user,
        type: type === "group" ? 1 : 0,
      },
      true
    ).then((response) => {
      setIsLoading(false);
      if (response.code === 200) {
        const result = response.result;

        setHeader(result.header);
        setProfileImage(result.header?.image);
        setMember(result.member);
        setTitleGroup(result.header?.title);

        result.member.map((item) => {
          if (item.id_user === id_user && item.role === "admin")
            setIsAdmin(true);
          else if (item.id_user === id_user && item.role !== "admin")
            setIsAdmin(false);
          else if (item.id_user !== id_user && item.id_user === "admin")
            setLeaveGroup(true);
        });
      }
    });
  }, [allFeatureUpdateCount, detailGroupUpdateCount]);

  useEffect(() => {
    // apiDispatch(
    //   getListMessage,
    //   {
    //     id_user,
    //   },
    //   true
    // ).then((response) => {
    //   if (response.code === 200) {
    //     const dataMessage = response.result;
    //     setListMessage(dataMessage);
    //   }
    // });
  }, []);

  const deleteChat = (id_groupmessage) => {
    apiDispatch(postDeleteChat, { id_groupmessage }, true)
      .then((response) => {
        if (response.code === 200) {
          // const newAllMessage = listMessage;
          // const index = newAllMessage.findIndex(
          //   (item) => item.id_groupmessage === id_groupmessage
          // );
          // index !== -1 && newAllMessage.splice(index, 1);
          // setListMessage(newAllMessage);
        }
      })
      .catch((err) => console.log(err));
  };

  const changeTitleGroup = () => {
    apiDispatch(
      putEditGroupChat,
      {
        id_groupmessage,
        title: titleGroup,
        id_user: id_user,
      },
      true
    ).then((response) => {
      if (response.code === 200) {
        dispatch(
          setChatData({
            allFeatureUpdateCount: 1 + allFeatureUpdateCount,
            selectedMessage: {
              ...selectedMessage,
              title: titleGroup,
            },
          })
        );
        setTitleGroup("");
      }
    });
  };

  const onImageChange = (event) => {
    if (event.target.files && event.target.files[0]) {
      let img = event.target.files[0];
      convertToBase64(img).then((result) => {
        setProfileImage(result);
        apiDispatch(
          putEditGroupChat,
          {
            id_groupmessage,
            id_user: id_user,
            image: result,
          },
          true
        ).then((response) => {
          if (response.code === 200) {
            dispatch(
              setChatData({
                allFeatureUpdateCount: 1 + allFeatureUpdateCount,
                selectedMessage: {
                  ...selectedMessage,
                  image: result,
                },
              })
            );
          }
        });
      });
    }
  };

  const handleRoleMember = (id_user, id_groupmessage, role, targetId) => {
    apiDispatch(
      putEditMember,
      {
        id_user: id_user,
        id_groupmessage,
        role: role,
        target_id: targetId,
      },
      true
    )
      .then((response) => {
        if (response.code === 200) {
          const newMember = member;
          const index = newMember.findIndex(function (o) {
            return o.id_user === targetId;
          });
          let newMemberRole = newMember[index];
          newMemberRole.role = role;
          newMember[index] = newMemberRole;
          setMember(newMember);

          dispatch(
            setChatData({ allFeatureUpdateCount: 1 + allFeatureUpdateCount })
          );
        }
      })
      .catch((err) => console.log(err));
  };

  const removeMember = (id_user, id_user_insert, id_groupmessage) => {
    apiDispatch(
      postRemoveMember,
      {
        id_user,
        id_user_insert,
        id_groupmessage,
      },
      true
    )
      .then((response) => {
        if (response.code === 200) {
          const newAllMember = member;
          const index = newAllMember.findIndex(function (o) {
            return o.id_user === id_user_insert;
          });
          index !== -1 && newAllMember.splice(index, 1);
          setMember(newAllMember);
          dispatch(
            setChatData({ allFeatureUpdateCount: 1 + allFeatureUpdateCount })
          );

          id_user === id_user_insert && setmodalSuccessIsOpened(true);
        }
      })
      .catch((err) => console.log(err));
  };

  const validateLeaveGroupAction = () => {
    if (isAdmin === true && header?.total_admin === "1")
      setmodalChangeAdmin(true);
    else if (isAdmin !== true && leaveGroup !== true) setmodalLeaveGroup(true);
    else if (isAdmin !== true && leaveGroup === true)
      deleteChat(id_groupmessage);
    else setmodalLeaveGroup(true);
  };

  return (
    <div className="x-detail-group">
      <div className="x-detail-header">
        <MaterialIcon
          action="close"
          className="font-weight-bolder cursor-pointer"
          size={28}
          color="primary-pressed"
          onClick={() => props.setShowDetailGroup(false)}
        />
        <p
          className="m-0 ms-4 heading-sm-bold"
          style={{ color: "var(--primary-pressed)" }}
        >
          Detail Group
        </p>
      </div>
      <div className="x-detail-content">
        <div className="x-profile-photo-group">
          {type === "group" && (
            <AddPhoto
              image={profileImage}
              initialName={header?.title?.slice(0, 1).toUpperCase()}
              isEmptyImage={!isEmpty(profileImage) ? false : true}
              isUploadDisable={!isAdmin}
              isLoading={isLoading}
              onClick={(e) => onImageChange(e)}
            />
          )}
          <div className="x-group-name align-items-center">
            {!isLoading ? shownEditTitle ? (
              <div>
                <form
                  onSubmit={() => {
                    changeTitleGroup();
                    setShownEditTitle(false);
                  }}
                  className="input-reply"
                >
                  <InputText
                    value={titleGroup}
                    onChangeText={(e) => setTitleGroup(e.target.value)}
                    inlineIconRight={"check"}
                    disableError
                  />
                </form>
              </div>
            ) : (
              <div
                className={classnames(
                  "x-group-name",
                  isAdmin && "cursor-pointer"
                )}
              >
                <p
                  className="m-0 text-lg-bold line-height-sm"
                  style={{ color: "var(--neutral-80)" }}
                  onClick={() => isAdmin && setShownEditTitle(true)}
                >
                  {header?.title}
                </p>
                {isAdmin && (
                  <div
                    className="ms-2 x-icon-pencil-alt"
                    onClick={() => isAdmin && setShownEditTitle(true)}
                  />
                )}
              </div>
            ) : <SkeletonElement height={18} width={150} />}
          </div>
        </div>
        <div className="x-separator-profile" />
        <div className="x-member-group">
          {!isLoading ? <p
            className="m-0 text-sm-normal line-height-sm"
            style={{ color: "var(--neutral-50)" }}
          >
            {member?.length} Member
          </p> : <SkeletonElement width={100} height={12} />}
        </div>
        {!changeAdmin ? (
          <div className="x-change-adm-false">
            <div className="x-member-wrap">
              {!isLoading && isAdmin && (
                <>
                  <div
                    className="x-add-member d-flex align-items-center cursor-pointer"
                    onClick={() => {
                      let existingMembers = [];
                      member.map((item) => existingMembers.push(item.id_user));

                      dispatch(setChatData({ existingMembers }));

                      router.push({
                        query: {
                          id_message: id_groupmessage,
                          message_type: "add_member",
                          type: "group",
                        },
                      });
                    }}
                  >
                    <div className="x-add">
                      <MaterialIcon action="add" size={32} color="base-10" />
                    </div>
                    <p
                      className="m-0 ms-2 text-md-bold line-height-sm"
                      style={{ color: "var(--neutral-80)" }}
                    >
                      Add Member
                    </p>
                  </div>

                  <div className="x-separator-member" />
                </>
              )}

              <div className="x-wrap-member">
                {dynamicMember.map(
                  (item, idx) =>
                    !isLoading ?(idx < totalDisplayedMember) && (
                      <div key={idx}>
                        <MemberList
                          item={item}
                          handleRoleMember={handleRoleMember}
                          removeMember={removeMember}
                          isAdmin={isAdmin}
                          isLoading={isLoading}
                        />
                      </div>
                    ):
                    <div key={idx}>
                        <MemberList
                          isLoading={isLoading}
                        />
                      </div>
                )}
              </div>
              {member.length > totalDisplayedMember && (
                <div className="x-showmore">
                  <p
                    className="m-0 text-sm-normal"
                    onClick={() =>
                      setTotalDisplayedMember(totalDisplayedMember + 5)
                    }
                    style={{ color: "var(--primary-main)" }}
                  >
                    {member.length - totalDisplayedMember} more
                  </p>
                  <MaterialIcon action="expand_more" color="primary-main" />
                </div>
              )}
            </div>

            {!isLoading && <div
              className="x-leave-group cursor-pointer"
              onClick={() => validateLeaveGroupAction()}
            >
              <div
                className={classnames(
                  "x-leave",
                  !leaveGroup ? "x-grey" : "x-red"
                )}
              >
                <MaterialIcon action={leaveGroup ? "logout" : "delete"} />
                <p>{!leaveGroup ? "Leave the group" : "Delete the group"}</p>
              </div>
            </div>}

            <div className="x-dec-group">
              <div className="x-dec">
                {!isLoading ? <Fragment><p
                  className="m-0 text-sm-light line-height-sm"
                  style={{ color: "var(--grey-4)" }}
                >
                  Dibuat oleh {header?.full_name}
                </p>
                  {header?.created_at !== undefined && (
                    <p
                      className="m-0 text-sm-light line-height-sm"
                      style={{ color: "var(--grey-4)" }}
                    >
                      {detGroupFormatter(header?.created_at)}
                    </p>)}</Fragment>
                  : <SkeletonElement height={12} width={100} count={2} className="my-1" />}
              </div>
            </div>
          </div>
        ) : (
          <div className="x-change-adm-true">
            {member.map(
              (item, idx) =>
                item.id_user !== id_user && (
                  <div className="x-wrap" key={idx}>
                    <Form>
                      <div className="p-0 form-check">
                        <div className="x-member-des d-flex align-items-center">
                          <input
                            className="checkbox-round"
                            type="checkbox"
                            id="addMember"
                            value={item.id_user}
                            onClick={(e) =>
                              handleRoleMember(
                                id_user,
                                id_groupmessage,
                                e.target.checked ? "admin" : "member",
                                e.target.value
                              )
                            }
                          />
                          <label className="form-label" htmlfor="addMember">
                            <span className="checkmark" />
                            <div className="x-member-des d-flex align-items-center">
                              {!isEmpty(item.profile_picture) ? (
                                <img
                                  src={item.profile_picture}
                                  className="ms-2 x-image-account"
                                  alt="profile"
                                />
                              ) : (
                                <div className="ms-2 x-empty-image">
                                  <p
                                    className="text-xl-normal m-0"
                                    style={{
                                      color: "var(--base-10)",
                                    }}
                                  >
                                    {item.full_name.slice(0, 1).toUpperCase()}
                                  </p>
                                </div>
                              )}
                              <div className="x-member-des">
                                <p
                                  className="m-0 ms-2 text-md-bold line-height-sm"
                                  style={{
                                    color: "var(--neutral-80)",
                                  }}
                                >
                                  {item.full_name}
                                </p>
                                <p
                                  className="m-0 ms-2 text-sm-light line-height-sm"
                                  style={{
                                    color: "var(--neutral-50)",
                                  }}
                                >
                                  {item.job_title}
                                </p>
                              </div>
                            </div>
                          </label>
                        </div>
                      </div>
                    </Form>
                  </div>
                )
            )}
            <div
              className="x-finish-change-admin cursor-pointer"
              onClick={() => {
                setChangeAdmin(false);
                // setNewAdmin(true);
                handleRoleMember(id_user, id_groupmessage, "member", id_user);
              }}
            >
              <div className="x-finish d-flex align-items-center">
                <MaterialIcon action="check" color="neutral-80" />
                <p
                  className="m-0 ms-2 text-md-bold line-height-sm"
                  style={{ color: "var(--neutral-80)" }}
                >
                  Finish
                </p>
              </div>
            </div>
          </div>
        )}
        <ModalSetAdminConfirmation
          show={modalChangeAdmin}
          onHide={() => setmodalChangeAdmin(false)}
          changeAdminAction={() => {
            setmodalChangeAdmin(false);
            setChangeAdmin(true);
          }}
        />
        <ModalLeaveGroupConfirmation
          show={modalLeaveGroup}
          onHide={() => setmodalLeaveGroup(false)}
          leaveGroupAction={() => {
            setLeaveGroup(true);
            setmodalLeaveGroup(false);
            removeMember(id_user, id_user, id_groupmessage);
          }}
        />
        <ModalLeaveGroupSuccess
          show={modalSuccessIsOpened}
          onHide={() => {
            setmodalSuccessIsOpened(false);
            leaveGroup &&
              router.push({
                pathname: "/chat",
                query: {
                  message_type: "message",
                },
              });
          }}
          successAction={() => {
            setmodalSuccessIsOpened(false);
            router.push({
              pathname: "/chat",
              query: {
                message_type: "message",
              },
            });
          }}
        />
      </div>
    </div>
  );
};

export default DetailGroup;
