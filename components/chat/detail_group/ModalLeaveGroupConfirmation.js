import React from 'react';
import { Button } from 'react-bootstrap';
import ModalConfirmation from '/components/modal/ModalConfirmation';

const ModalLeaveGroup = (props) => {
  const {
    show,
    onHide,
    leaveGroupAction
  } = props;

  return (
    <ModalConfirmation
      backdrop={"static"}
      show={show}
      onHide={onHide}
      customExitIcon={
        <span
          className="material-icons-round"
          style={{ fontWeight: "bolder", color: "var(--neutral-70)" }}
        >
          clear
        </span>
      }
      customTitle={
        <>
          <h1 className="heading-lg-bold">Leave Group</h1>
          <p
            className="pt-1 text-lg-normal"
            style={{ color: "var(--neutral-50)" }}
          >
            Leave this group?
          </p>
          <div className="d-flex justify-content-center mt-4">
            <Button
              className="btn-primary"
              onClick={leaveGroupAction}
            >
              Leave the Group
            </Button>
          </div>
          <div className="d-flex justify-content-center mt-2">
            <Button
              className="btn-topic"
              onClick={onHide}
            >
              Cancel
            </Button>
          </div>
        </>
      }
    />
  );
}

export default ModalLeaveGroup;
