import React, { Fragment } from "react";
import { Button } from "react-bootstrap";
import { Enhance } from "/services/Enhancer";
import { getAuth } from "/services/utils/helper";
import { useEffect, useState, useRef } from "react";
import { isEmpty } from "lodash-es";
import ModalConfirmation from "/components/modal/ModalConfirmation";
import { useSelector } from "react-redux";
import MaterialIcon from "components/utils/MaterialIcon";
import SkeletonElement from "components/skeleton/SkeletonElement";

const MemberList = (props) => {
  const { id_user } = getAuth();
  const [shownEditMember, setShownEditMember] = useState(false);
  const [modalConfirmIsOpened, setmodalConfirmIsOpened] = useState(false);
  const [shownOptionMember, setShownOptionMember] = useState(false);
  const { handleRoleMember, getDetailMessage, sumMember, removeMember, isLoading } = props;
  const ref = useRef();

  const chatData = useSelector((state) => state.chatReducer.data);
  const { id_groupmessage } = chatData.selectedMessage;

  useEffect(() => {
    const handleEditMember = (event) => {
      if (ref.current) {
        !ref.current.contains(event.target) &&
          !document.querySelector(".x-edit-member").contains(event.target) &&
          setShownEditMember(false);
      }
    };

    document.removeEventListener("mousedown", handleEditMember);
    document.addEventListener("mousedown", handleEditMember);

    return () => {
      document.removeEventListener("mousedown", handleEditMember);
    };
  }, [ref]);

  const expandEditMember = () => {
    setShownEditMember(shownEditMember ? false : true);
  };

  const dropdownEditMember = () => {
    return (
      <div ref={ref} className="x-edit cursor-pointer">
        {props.item.role !== "admin" && (
          <div
            className="x-make-admin"
            onClick={() => {
              handleRoleMember(
                id_user,
                id_groupmessage,
                "admin",
                props.item.id_user
              );
              setShownEditMember(false);
            }}
          >
            <p
              className="p-3 m-0 text-md-normal line-height-sm"
              style={{ color: "var(--neutral-50)" }}
            >
              Make admin group
            </p>
          </div>
        )}

        {props.item.role !== "member" && (
          <div
            className="x-make-admin"
            onClick={() => {
              handleRoleMember(
                id_user,
                id_groupmessage,
                "member",
                props.item.id_user
              );
              setShownEditMember(false);
            }}
          >
            <p
              className="p-3 m-0 text-md-normal line-height-sm"
              style={{ color: "var(--neutral-50)" }}
            >
              Remove from admin
            </p>
          </div>
        )}

        <div className="x-remove-group" onClick={() => expandConfirmRemove()}>
          <p
            className="p-3 m-0 text-md-normal line-height-sm"
            style={{ color: "var(--neutral-50)" }}
          >
            Remove from group
          </p>
        </div>
      </div>
    );
  };

  const renderModalConfirmRemove = () => {
    return (
      <>
        <ModalConfirmation
          backdrop={"static"}
          show={modalConfirmIsOpened}
          onHide={() => setmodalConfirmIsOpened(false)}
          customExitIcon={
            <MaterialIcon
              action="clear"
              className="font-weight-bolder"
              size={28}
              color="neutral-70"
            />
          }
          customTitle={
            <>
              <h1 className="heading-lg-bold">Remove Member</h1>
              {!isLoading && <p
                className="pt-1 text-lg-normal"
                style={{ color: "var(--neutral-50)" }}
              >
                You select {props.item.full_name} to be removed from this group?
              </p>}
              <div className="d-flex justify-content-center mt-4">
                <Button
                  className="btn-primary"
                  onClick={() => {
                    removeMember(id_user, props.item.id_user, id_groupmessage);
                    setmodalConfirmIsOpened(false);
                  }}
                >
                  Remove Member {props.member}
                </Button>
              </div>
              <div className="d-flex justify-content-center mt-2">
                <Button
                  className="btn-topic"
                  onClick={() => setmodalConfirmIsOpened(false)}
                >
                  Cancel
                </Button>
              </div>
            </>
          }
        />
      </>
    );
  };

  const expandConfirmRemove = () => {
    setmodalConfirmIsOpened(modalConfirmIsOpened ? false : true);
  };

  return (
    <div className="x-member">
      <div className="x-member-list d-flex align-items-center">
        <div className="x-member-des d-flex align-items-center">
          {(!isLoading && !isEmpty(props.item.profile_picture)) ? (
            <img
              src={props.item.profile_picture}
              className="x-image-account"
              alt=""
            />
          ) : (!isLoading ?
            (<div className="x-empty-image">
              <p
                className="text-xl-normal m-0"
                style={{ color: "var(--base-10)" }}
              >
                {props.item?.full_name?.slice(0, 1).toUpperCase()}
              </p>
            </div>)
            : <SkeletonElement type="rounded-circle" width={52} height={52} />)}
          <div className="x-member-des">
            <div className="d-flex align-items-center">
              {!isLoading ? <Fragment><p
                className="m-0 ms-2 text-md-bold line-height-sm"
                style={{ color: "var(--neutral-80)" }}
              >
                {props.item.full_name}
              </p>
                {(() => {
                  if (props.item.id_user === id_user) {
                    return <div className="ms-1 x-icon-badge" />;
                  }
                })()}
                {(() => {
                  if (props.item.role === "admin") {
                    return (
                      <div className="d-flex align-items-center">
                        <div className="ms-1 x-dot" />
                        <p
                          className="m-0 ms-1 text-sm-light line-height-sm"
                          style={{ color: "var(--primary-main)" }}
                        >
                          Admin
                        </p>
                      </div>
                    );
                  }
                })()}
              </Fragment>
                : <SkeletonElement type="ms-2 my-1" height={16} width={100}/>}
            </div>
            {!isLoading ? <p
              className="m-0 ms-2 text-sm-light line-height-sm"
              style={{ color: "var(--neutral-50)" }}
            >
              {props.item.job_title}
            </p>
              : <SkeletonElement  type="ms-2" height={12} width={100} />}
          </div>
        </div>
        {!isLoading && props.isAdmin === true && props.item.id_user !== id_user && (
          <div className="x-edit-member ms-auto cursor-pointer">
            <MaterialIcon
              action="more_vert"
              className="ms-auto"
              size={32}
              color="neutral-50"
              onClick={() => expandEditMember()}
            />
            {shownEditMember && dropdownEditMember()}
          </div>
        )}
      </div>
      <div className="x-separator-member" />
      {renderModalConfirmRemove()}
    </div>
  );
};

export default Enhance(MemberList);
