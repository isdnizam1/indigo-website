import { useState, useEffect, Fragment } from "react";
import { Button } from "react-bootstrap";
import { getProfileFollowing, postAddMember } from "/services/actions/api";
import { getAuth, apiDispatch } from "/services/utils/helper";
import { useRouter } from "next/router";
import InputText from "/components/input/InputText";
import { isEmpty } from "lodash-es";
import { setChatData } from "/redux/slices/chatSlice";
import { useDispatch, useSelector } from "react-redux";
import MaterialIcon from "components/utils/MaterialIcon";
import SkeletonElement from "components/skeleton/SkeletonElement";

const AddMember = () => {
  const [listContacts, setListContacts] = useState([]);
  const [showListContacts, setShowListContacts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const { id_user } = getAuth();
  let dynamicShowListContacts = isLoading ? [1, 2, 3] : showListContacts;

  const router = useRouter();
  const chatData = useSelector((state) => state.chatReducer.data);
  const { selectedMembers, existingMembers, allFeatureUpdateCount } = chatData;
  const dispatch = useDispatch();

  useEffect(() => {
    apiDispatch(getProfileFollowing, { followed_by: id_user }, true).then(
      (response) => {
        setIsLoading(false);
        if (response.code === 200) {
          let result = response.result;
          if (!isEmpty(existingMembers)) {
            setListContacts(
              result.filter((item) => !existingMembers.includes(item.id_user))
            );
            setShowListContacts(
              result.filter((item) => !existingMembers.includes(item.id_user))
            );
          } else {
            setListContacts(result);
            setShowListContacts(result);
          }
        }
      }
    );
  }, []);

  const addMember = (e) => {
    const member = JSON.parse(e.target.value);
    const isChecked = e.target.checked;

    const selected = isChecked
      ? [...selectedMembers, member]
      : selectedMembers.filter(
        (person) => person.id_follow !== member.id_follow
      );

    dispatch(setChatData({ selectedMembers: selected }));
  };

  const searchContactByKeyword = (event) => {
    const keyword = event.target.value.toLowerCase();
    const filteredContact = listContacts.filter((item) =>
      item.full_name.toLowerCase().includes(keyword)
    );
    setShowListContacts(filteredContact);
  };

  return (
    <div className="x-addmember-list">
      <div className="x-contact-list-container">
        <div className="x-header-top">
          <div className="x-icon-back" onClick={() => router.back()}></div>
          <h3>Add Member</h3>
        </div>
        <div className="msg-hr mb-2" />
        <div className="contact-list">
          <div className="contact-card-container w-100">
            <div className="contact-wrapper">
              <div className="contact-body">
                <InputText
                  disableError={true}
                  placeholder={"Search Friend"}
                  inlineIcon={"search"}
                  onChangeText={(event) => searchContactByKeyword(event)}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="msg-hr mb-2" />
        <div className="d-flex flex-row ms-3 me-3 justify-content-between">
          <p className="text-sm-normal" style={{ color: "var(--neutral-50" }}>
            Friend List
          </p>
        </div>
        <div className="x-friend-list">
          {dynamicShowListContacts.map(
            (item, i) =>
              item.id_user !== id_user && (
                <div className="form-check cursor-pointer" key={i}>
                  <input
                    className="checkbox-round"
                    type="checkbox"
                    id={"addMember" + i}
                    onChange={addMember}
                    checked={selectedMembers.some((user) =>
                      user.id_follow === item.id_follow ? true : false
                    )}
                    value={JSON.stringify(item)}
                  />
                  <label className="form-label" htmlFor={"addMember" + i}>
                    <span className="checkmark" />
                    <div className="contact-list">
                      <div className="contact-card-container">
                        <div className="contact-wrapper">
                          <div className="img-container">
                            {item.profile_picture ? (
                              <img src={item.profile_picture} alt="profile" />
                            ) : ( !isLoading ? 
                              <div className="x-empty-image">
                                <p
                                  className="text-xl-normal m-0"
                                  style={{ color: "var(--base-10)" }}
                                >
                                  {item.full_name.slice(0, 1).toUpperCase()}
                                </p>
                              </div>:
                              <SkeletonElement width={48} height={48} type="rounded-circle" />
                            )}
                          </div>
                          <div className="contact-body">
                            {!isLoading ?
                              <Fragment><h3 className="contact-name mb-1">
                                {item.full_name}
                              </h3>
                                <p className="contact-text">{item.job_title}</p>
                              </Fragment> :
                              <SkeletonElement width={120} height={14} count={2} className="my-1" />}
                          </div>
                        </div>
                      </div>
                    </div>
                  </label>
                </div>
              )
          )}
          <div className="x-btn-add">
            <Button
              disabled={isEmpty(selectedMembers)}
              className="btn-add"
              onClick={() => {
                if (!isEmpty(existingMembers)) {
                  let selectedMembersById = [];
                  selectedMembers.forEach((item) =>
                    selectedMembersById.push(item.id_user)
                  );

                  apiDispatch(
                    postAddMember,
                    {
                      id_user,
                      id_groupmessage: router.query.id_message,
                      id_user_insert: selectedMembersById,
                    },
                    true
                  ).then((response) => {
                    // if (response.code === 200) {
                    dispatch(
                      setChatData({
                        allFeatureUpdateCount: 1 + allFeatureUpdateCount,
                      })
                    );

                    router.push({
                      query: {
                        id_message: router.query.id_message,
                        message_type: "message",
                        type: "group",
                      },
                    });
                    // }
                  });
                } else {
                  router.push({
                    query: router.query?.id_message
                      ? {
                        id_message: router.query.id_message,
                        message_type: "group_chat",
                      }
                      : { message_type: "group_chat" },
                  });
                }
              }}
            >
              <MaterialIcon
                action="arrow_forward"
                color="base-10"
              />
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddMember;
