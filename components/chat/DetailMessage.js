import React, {
	useEffect,
	useState,
	useRef,
	useCallback,
	Fragment,
} from 'react';
import { Dropdown } from 'react-bootstrap';
import {
	getAuth,
	dateTimeFormatter,
	messageFormatter,
	isDev,
	convertToBase64,
} from '/services/utils/helper';
import classnames from 'classnames';
import InputText from '/components/input/InputText';
import { isEmpty, isNull } from 'lodash-es';
import {
	getDetailMessage,
	getIdGroupMessage,
	getGroupMember,
} from '/services/actions/api';
import DetailGroup from './detail_group/DetailGroup';
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';
import { setChatData } from '/redux/slices/chatSlice';
import { apiDispatch, random, stringHtmlMentionParser } from '/services/utils/helper';
import { string } from 'yup';
import 'react-medium-image-zoom/dist/styles.css';
import ModalImgZoom from '../modal/ModalImgZoom';
import MaterialIcon from 'components/utils/MaterialIcon';
import InputMention from 'components/input/InputMention';
import SkeletonElement from "/components/skeleton/SkeletonElement";
import CardUser from 'components/home/CardUser';
import { MQTT_SUBSCRIPTION } from "/services/constants/Constants";

const DetailMessage = () => {
	const { id_user, token } = getAuth();
	const router = useRouter();
	const [header, setHeader] = useState({});
	const [members, setMembers] = useState({});
	const [messages, setMessages] = useState([]);
	const [reply, setReply] = useState('');
	const [showDetailGroup, setShowDetailGroup] = useState(false);
	const chatRef = useRef(null);
	const [shownMemberList, setShownMemberList] = useState(false);
	const [groupMembers, setGroupMembers] = useState([]);
	// const [groupMemberUpdateCount, setGroupMemberUpdateCount] = useState(0);
	const ref = useRef();
	const dispatch = useDispatch();
	const chatData = useSelector((state) => state.chatReducer.data);
	const {
		client,
		selectedMessage,
		message,
		listMessages,
		allFeatureUpdateCount,
		detailMessageUpdateCount,
		groupMemberUpdateCount,
	} = chatData;
	const [isLoadingMessages, setIsLoadingMessages] = useState(true);
	let dynamicMessages = isLoadingMessages ? [1, 2, 3] : messages;

	const [messageFieldHeight, setMessageFieldHeight] = useState('40px');

	const colors = [
		'var(--semantic-pressed)',
		'var(--secondary-main)',
		'var(--semantic-success-main)',
		'var(--semantic-danger-main)',
		'var(--semantic-warning-main)',
		'var(--semantic-info-main)',
	];

	const users = [
		{
			id: 'walter',
			display: 'Walter White',
		},
		{
			id: 'pipilu',
			display: '皮皮鲁',
		},
		{
			id: 'luxixi',
			display: '鲁西西',
		},
	];

	useEffect(() => {
		setIsLoadingMessages(true)
		setShowDetailGroup(false);
		setReply('');
	}, [router.query?.id_message]);

	useEffect(() => {
		chatRef?.current?.scrollTo({ top: 999 * messages.length });
	}, [messages]);

	useEffect(() => {
		selectedMessage.id_groupmessage === message.id_groupmessage &&
			setMessages((state) => [...state, message]);
	}, [message]);

	useEffect(() => {
		getData();
		getMemberGroup();
	}, [allFeatureUpdateCount, detailMessageUpdateCount]);

	useEffect(() => getMemberGroup(), [groupMemberUpdateCount]);

	useEffect(() => {
		const handleMemberList = (event) => {
			if (ref.current) {
				!ref.current.contains(event.target) &&
					!document.querySelector('.x-group').contains(event.target) &&
					setShownMemberList(false);
			}
		};

		document.removeEventListener('mousedown', handleMemberList);
		document.addEventListener('mousedown', handleMemberList);

		return () => {
			document.removeEventListener('mousedown', handleMemberList);
		};
	}, [ref]);

	const getMemberGroup = () => {
		apiDispatch(
			getGroupMember,
			{
				id_groupmessage: selectedMessage.id_groupmessage,
			},
			true
		).then((response) => {
			if (response.code === 200) {
				const { result } = response;

				let members = [];
				result?.map((member) => {
					members.push({
						...member,
						id: member.id_user,
						display: member.full_name,
					});
				});
				setGroupMembers(members);
			}
		});
	};

	const handleClick = (item) => {
		setReply(reply + item.full_name);
		setShownMemberList(false);
	};

	const dropdownMemberList = () => {
		return (
			<div ref={ref} className="x-memberlist cursor-pointer">
				<p
					className="m-0 ms-4 mt-4 text-lg-bold color-neutral-80"
				>
					Member
				</p>
				{groupMembers?.map((item, idx) => item.id_user !== id_user && (
          <div
            className="x-group-member d-flex align-items-center cursor-pointer"
            key={idx}
            onClick={() => handleClick(item)}
          >
            {item.profile_picture ? (
              <img
                src={item.profile_picture}
                className="x-image-account"
                alt=""
              />
            ) : (
              <div className="x-empty-image">
                <p
                  className="text-xl-normal m-0 color-base-10"
                >
                  {item.full_name?.slice(0, 1).toUpperCase()}
                </p>
              </div>
            )}
            <p
              className="m-0 ms-2 text-lg-bold color-neutral-80"
            >
              {item.full_name}
            </p>
          </div>
				))}
			</div>
		);
	};

	const handleChange = (e) => {
		if (e.target.value.match(/@$/)) {
			setShownMemberList(true);
		} else setShownMemberList(false);
	};

	const getData = () => {
		apiDispatch(
			getDetailMessage,
			{
				// id_groupmessage:
				//   router.query.id_message === "false"
				//     ? null
				//     : router.query.id_message,
				// id_user,
				// type: selectedMessage.type,
				id_groupmessage: !selectedMessage.id_groupmessage
					? null
					: selectedMessage.id_groupmessage,
				id_user,
				type: selectedMessage.type === 'group' ? 1 : 0,
			},
			true
		).then((response) => {
			setIsLoadingMessages(false);
			// console.log("response while get data: ", response);
			if (response.code === 200) {
				if (!isEmpty(response.result.header)) {
					setHeader(response.result.header);
					setMessages(response.result.message);
					!isEmpty(response.result.member)
						? setMembers(response.result.member)
						: setMembers([]);
				} else {
					setHeader({});
					setMessages([]);
					setMembers([]);
				}
			}
		});
	};

	const onFileUploadChange = (event, type) => {
		if (event.target.files && event.target.files[0]) {
			let file = event.target.files[0];
			convertToBase64(file).then((result) => {
				// const typeFile = result.split("base64,")[0];
				// const messageType = typeFile.includes("pdf") ? "pdf" : "photo";
				sendMessage(result, type);
			});
		}
	};

	const sendMessage = async (file = '', messageType) => {
		const { id_user: with_id_user, type, id_groupmessage } = selectedMessage;
		const withIdUser = !header.id_user ? with_id_user : header.id_user;
		const topic =
			isNull(id_groupmessage) && type === 'personal'
				? `${with_id_user}/${MQTT_SUBSCRIPTION}`
				: `${MQTT_SUBSCRIPTION}/${id_groupmessage}`;

		if (!isEmpty(reply) || !isEmpty(file)) {
			const { name_user } = getAuth();
			const data = {
				created_at: dateTimeFormatter(new Date()),
				token,
				full_name: name_user,
				id_user,
				id_groupmessage,
				with_id_user: type === 'personal' ? withIdUser : null,
				message: reply,
				attachment: file,
				message_type: isEmpty(file) ? 'txt' : messageType,
				type,
			};

			// console.log("data before publish: ", data);
			// console.log("data selected message: ", selectedMessage);

			await client.publish(topic, JSON.stringify(data));

			setTimeout(
				() =>
					isNull(id_groupmessage) &&
					apiDispatch(
						getIdGroupMessage,
						{ id_user: id_user, with_id_user: withIdUser },
						true
					).then((response) => {
						if (response.code === 200) {
							const result = response.result;

							// console.log("response api get idgroupmessage: ", result);

							let updatedListMessages = listMessages.map((item) => item);
							updatedListMessages.push(
								`${MQTT_SUBSCRIPTION}/${result.id_groupmessage}`
							);

							client.subscribe(updatedListMessages);

							dispatch(
								setChatData({
									selectedMessage: {
										id_user: selectedMessage.id_user,
										full_name: selectedMessage.full_name,
										image: selectedMessage.avatar,
										id_groupmessage: result.id_groupmessage,
										type: 'personal',
									},
									listMessages: updatedListMessages,
									detailMessageUpdateCount: random(),
									groupMemberUpdateCount: random(),
								})
							);

							router.push({
								query: {
									id_message: result.id_groupmessage,
									message_type: 'message',
									type: 'personal',
								},
							});
						}
					}),
				500
			);

			setReply('');
		}
	};

	const [openModal, setOpenModal] = useState(false);
	const [tempData, setTempData] = useState([]);

	const getDataZoom = (img) => {
		let tempData = [img];
		setTempData((item) => [1, ...tempData]);
		return setOpenModal(true);
	};

	return (
    <div className="x-chat">
      <div className="x-chat">
        <div className="x-chat-detail">
          <div className="d-flex flex-wrap h-100">
            <div
              className="x-chat-header"
              onClick={() => {
                selectedMessage.type === "group"
                  ? setShowDetailGroup(true)
                  : router.push({
                      pathname: "/profile",
                      query: { id: selectedMessage.id_user },
                    });
              }}
            >
              <div className="d-flex align-items-center cursor-pointer">
                <CardUser
                  image={
                    selectedMessage.type !== "personal"
                      ? selectedMessage.image
                      : selectedMessage.avatar
                  }
                  name={
                    selectedMessage.type !== "group"
                      ? selectedMessage.full_name
                      : selectedMessage.title
                  }
                  TextCentered
                />
              </div>
            </div>

            <div
              className="x-chat-wrapper"
              style={{ height: `calc(100% - (108px + ${messageFieldHeight}))` }}
            >
              <div className="x-main-chat" ref={chatRef}>
                {dynamicMessages?.map((item, idx) =>
                  !isLoadingMessages ? (
                    <div className="x-list-message" key={idx}>
                      {messages[-1 + idx]?.created_at?.slice(0, 10) === null ||
                        (messages[-1 + idx]?.created_at?.slice(0, 10) !==
                          messages[idx]?.created_at?.slice(0, 10) && (
                          <div className="x-chat-notification x-date mb-3">
                            <p className="m-0 text-md-normal">
                              {messageFormatter(messages[idx].created_at)}
                            </p>
                          </div>
                        ))}
                      {item.type === "notification" ? (
                        <div className="x-chat-notification x-activity mb-3">
                          <p className="m-0 text-md-normal">
                            {!item.message.includes("You")
                              ? item.message
                              : item.id_user !== id_user
                              ? item.full_name + item.message.slice(3)
                              : item.message}
                          </p>
                        </div>
                      ) : item.id_user === id_user ? (
                        <div className="d-flex">
                          <div className="ms-auto d-flex me-4 mb-3">
                            <p className="m-0 mt-auto text-sm-light color-neutral-600">
                              {item.created_at?.slice(11, 16)}
                            </p>
                            <div className="x-chat-sender ms-2 me-5">
                              {item.type === "photo" ||
                              item.message_type === "photo" ? (
                                <img
                                  onClick={() =>
                                    getDataZoom(
                                      !isEmpty(item.message)
                                        ? item.message
                                        : item.attachment
                                    )
                                  }
                                  src={
                                    !isEmpty(item.attachment)
                                      ? item.attachment
                                      : item.message
                                  }
                                  alt=""
                                />
                              ) : (
                                <p>
                                  {item.type === "pdf" ||
                                  item.message_type === "pdf" ? (
                                    <div
                                      className="cursor-pointer user-select-none d-flex align-items-center"
                                      onClick={() => window.open(item.message)}
                                    >
                                      <MaterialIcon
                                        action="file_download"
                                        className="pe-1"
                                      />
                                      {
                                        item.message.split(
                                          item.id_user + "_"
                                        )[1]
                                      }
                                    </div>
                                  ) : (
                                    stringHtmlMentionParser(
                                      item.message,
                                      "semantic-focus",
                                      "/profile",
                                      router.push
                                    )
                                  )}
                                </p>
                              )}
                            </div>
                          </div>
                        </div>
                      ) : (
                        <div>
                          {!isEmpty(selectedMessage.title) ? (
                            <div className="d-flex">
                              <div className="d-flex mb-3">
                                <div className="x-chat-receiver me-2 ms-5">
                                  <p
                                    className="m-0 text-md-bold line-height-sm"
                                    style={{ color: colors[item.id_user % 6] }}
                                  >
                                    {item.full_name}
                                  </p>
                                  {item.type === "photo" ||
                                  item.message_type === "photo" ? (
                                    <img
                                      onClick={() =>
                                        getDataZoom(
                                          !isEmpty(item.message)
                                            ? item.message
                                            : item.attachment
                                        )
                                      }
                                      src={
                                        !isEmpty(item.attachment)
                                          ? item.attachment
                                          : item.message
                                      }
                                      alt=""
                                    />
                                  ) : (
                                    <p className="m-0 text-lg-light color-neutral-70">
                                      {item.type === "pdf" ||
                                      item.message_type === "pdf" ? (
                                        <div
                                          className="cursor-pointer user-select-none d-flex align-items-center"
                                          onClick={() =>
                                            window.open(item.message)
                                          }
                                        >
                                          <MaterialIcon
                                            action="file_download"
                                            className="pe-1"
                                          />
                                          {
                                            item.message.split(
                                              item.id_user + "_"
                                            )[1]
                                          }
                                        </div>
                                      ) : (
                                        item.message
                                      )}
                                    </p>
                                  )}
                                </div>
                                <p className="m-0 mt-auto ms-2 text-sm-light color-neutral-600">
                                  {item.created_at?.slice(11, 16)}
                                </p>
                              </div>
                            </div>
                          ) : (
                            <div className="d-flex">
                              <div className="d-flex mb-3">
                                <div className="x-chat-receiver me-2 ms-5">
                                  <p
                                    className="m-0 text-md-bold line-height-sm"
                                    style={{ color: colors[item.id_user % 6] }}
                                  >
                                    {item.full_name}
                                  </p>
                                  {item.type === "photo" ||
                                  item.message_type === "photo" ? (
                                    <img
                                      src={
                                        !isEmpty(item.attachment)
                                          ? item.attachment
                                          : item.message
                                      }
                                      alt=""
                                    />
                                  ) : (
                                    <p className="m-0 text-lg-light color-neutral-70">
                                      {item.type === "pdf" ||
                                      item.message_type === "pdf" ? (
                                        <div
                                          className="cursor-pointer user-select-none d-flex align-items-center"
                                          onClick={() =>
                                            window.open(item.message)
                                          }
                                        >
                                          <MaterialIcon
                                            action="file_download"
                                            className="pe-1"
                                          />
                                          {
                                            item.message.split(
                                              item.id_user + "_"
                                            )[1]
                                          }
                                        </div>
                                      ) : (
                                        stringHtmlMentionParser(
                                          item.message,
                                          "semantic-main",
                                          "/profile",
                                          router.push
                                        )
                                      )}
                                    </p>
                                  )}
                                </div>
                                <p className="m-0 mt-auto text-sm-light color-neutral-600">
                                  {item.created_at?.slice(11, 16)}
                                </p>
                              </div>
                            </div>
                          )}
                        </div>
                      )}
                    </div>
                  ) : (
                    <Fragment key={idx}>
                      <SkeletonElement
                        width={76}
                        height={24}
                        className="x-chat-notification"
                      />
                      <SkeletonElement
                        width={405}
                        height={263}
                        className="d-flex ms-5 my-3"
                        type="me-auto"
                      />
                      <SkeletonElement
                        width={250}
                        height={40}
                        className="d-flex ms-5 my-3"
                        type="me-auto"
                      />
                      <SkeletonElement
                        width={320}
                        height={40}
                        className="d-flex me-5 my-3"
                        type="ms-auto"
                      />
                    </Fragment>
                  )
                )}
                {openModal && (
                  <ModalImgZoom
                    img={tempData[1]}
                    hide={() => setOpenModal(false)}
                  />
                )}
              </div>
            </div>

            <div className="x-chat-footer">
              <div className="w-100 m-0 d-flex align-items-center">
                <div className="x-attachment">
                  <Dropdown>
                    <Dropdown.Toggle>
                      <MaterialIcon
                        action="add"
                        size={32}
                        color="semantic-main"
                        className="p-0"
                      />
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      <Dropdown.Item>
                        <div className="x-attachment-menu">
                          <div className="x-icon-wrapper">
                            <MaterialIcon action="image" />
                          </div>
                          <div className="x-detail-wrapper">
                            <p>Add image from your library</p>
                            <p>max. 10mb</p>
                          </div>
                          <input
                            type="file"
                            accept="image/png, image/jpg, image/jpeg"
                            name="image"
                            onChange={(event) => {
                              onFileUploadChange(event, "photo");
                              event.target.value = null;
                            }}
                            onClick={(event) => event.stopPropagation()}
                          />
                        </div>
                      </Dropdown.Item>
                      {/* <Dropdown.Item>
                        <div className="x-attachment-menu">
                          <div className="x-icon-wrapper">
														<MaterialIcon action="photo_camera" />
                          </div>
                          <div className="x-detail-wrapper">
                            <p>Take photo</p>
                          </div>
                        </div>
                      </Dropdown.Item> */}
                      <Dropdown.Item>
                        <div className="x-attachment-menu">
                          <div className="x-icon-wrapper">
                            <MaterialIcon action="content_paste" />
                          </div>
                          <div className="x-detail-wrapper">
                            <p>Add .pdf file</p>
                            <p>max. 30mb</p>
                          </div>
                          <input
                            type="file"
                            accept="application/pdf"
                            name="pdf"
                            onChange={(event) => {
                              onFileUploadChange(event, "pdf");
                              event.target.value = null;
                            }}
                            onClick={(event) => event.stopPropagation()}
                          />
                        </div>
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
                <div className="input-reply">
                  <InputMention
                    value={reply}
                    onChange={(e) => setReply(e.target.value)}
                    titleMention={"Member"}
                    onKeyDown={(e) => {
                      if (e.code === "Enter" && !e.shiftKey) {
                        sendMessage("", "txt");
                        setTimeout(() => setReply(""), 10);
                      }
                    }}
                    placeholder={"Enter a message"}
                    className="x-input-message"
                    dataMention={groupMembers}
                    renderSuggestion={(
                      entry,
                      search,
                      highlightedDisplay,
                      index,
                      focused
                    ) => {
                      return (
                        <>
                          {entry.id_user !== id_user && (
                            <div className="x-group-member">
                              {entry.profile_picture ? (
                                <img
                                  src={entry.profile_picture}
                                  className="x-image-account"
                                  alt=""
                                />
                              ) : (
                                <div className="x-empty-image">
                                  <p className="text-xl-normal m-0 color-base-10">
                                    {entry.full_name?.slice(0, 1).toUpperCase()}
                                  </p>
                                </div>
                              )}
                              <p className="m-0 ms-2 text-lg-bold color-neutral-80">
                                {entry.full_name}
                              </p>
                            </div>
                          )}
                        </>
                      );
                    }}
                  />
                </div>
                <div
                  className={classnames(
                    "x-send",
                    !reply.length && "x-send-disable"
                  )}
                  onClick={() => reply.length && sendMessage("", "txt")}
                >
                  <div className="x-icon-send-message" />
                </div>
              </div>
            </div>
            {/* membertag */}
            <div className="x-group">
              {shownMemberList && dropdownMemberList()}
            </div>
          </div>
        </div>

        {showDetailGroup && (
          <DetailGroup
            setShowDetailGroup={setShowDetailGroup}
            header={header}
            members={members}
          />
        )}
      </div>
    </div>
  );
};

export default DetailMessage;
