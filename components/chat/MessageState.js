import React from "react";

const MessageState = () => {
  return (
    <div className="x-message">
      <div className="x-empty-message">
        <div className="d-flex flex-column align-items-center">
          <div className="x-ic-eventeer" />
          <p
            className="m-0 mt-1 heading-sm-light"
            style={{ color: "var(--neutral-50)" }}
          >
            Start a new conversation!
          </p>
        </div>
      </div>
    </div>
  );
}

export default MessageState;
