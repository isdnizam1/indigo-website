import { getListMessages } from "/services/actions/api";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { apiDispatch, getAuth, isDev, stringHtmlMentionParser } from "/services/utils/helper";
import { useDispatch, useSelector } from "react-redux";
import { setChatData } from "/redux/slices/chatSlice";
import MessageCard from "/components/chat/list_message/MessageCard";
import EmptyStateLv1 from "/components/empty_state/EmptyStateLv1";
import classnames from "classnames";
import { isEmpty } from "lodash-es";
import { MQTT_SUBSCRIPTION } from "/services/constants/Constants";

const ListMessage = () => {
  const { id_user } = getAuth();
  const router = useRouter();
  const [listMessage, setListMessage] = useState([]);
  const chatData = useSelector((state) => state.chatReducer.data);
  const { message, allFeatureUpdateCount, listMessageUpdateCount, detailMessageUpdateCount } = chatData;
  const [isLoading, setIsLoading] = useState(true);

  const dispatch = useDispatch();

  useEffect(() => dispatch(setChatData({ selectedMembers: [] })), []);

  useEffect(() => getData(), [allFeatureUpdateCount, listMessageUpdateCount]);

  const getData = () => {
    apiDispatch(getListMessages, { id_user }, true).then((response) => {
      setIsLoading(false);
      if (response.code === 200) {
        const result = response.result;
        setListMessage(result);

        let listMessages = [`${id_user}/${MQTT_SUBSCRIPTION}`];
        response.result.map((item) =>
          listMessages.push(`${MQTT_SUBSCRIPTION}/${item.id_groupmessage}`)
        );
        dispatch(setChatData({ listMessages }));
      }
    });
  };

  return (
    <div className="x-test-message">
      <div className="x-message-list-container">
        <div className="message-top">
          <h3>Message</h3>
          <div
            className="x-icon-add-msg"
            onClick={() => {
              router.push({
                query: router.query?.id_message
                  ? {
                      id_message: router.query.id_message,
                      message_type: "contact",
                    }
                  : { message_type: "contact" },
              });
            }}
          ></div>
        </div>
        {!isLoading ? <div className={classnames("message-list", listMessage.length > 1 && "flex-column")}>
          {isEmpty(listMessage) ? (
            <div className="d-flex justify-content-center align-items-center h-100 w-100">
              <EmptyStateLv1
                materialIcons="chat"
                title="No conversation yet"
                subTitle="Let's start a conversation with your friends now"
              />
            </div>
          ) : (
            listMessage.map((item, idx) => {
              let messageText = item.message?.split("\n").join(" ");

              if (item.message_type === "photo") messageText = "Image";
              else if(item.message_type === "pdf") messageText = "PDF";
              else if (item.message?.includes("You")) {
                if (item.id_user_sender !== id_user) {
                  if (item.type === "group")
                    messageText = item.full_name_sender + item.message.slice(3);
                  else messageText = item.full_name + item.message.slice(3);
                }
              }

              return (
                <div key={idx} className="w-100">
                  <MessageCard
                    idGroupMessage={item.id_groupmessage}
                    isAdminTenant={item.is_admin_tenant}
                    fullName={
                      item.type === "group" ? item.title : item.full_name
                    }
                    onOpenMessage={() => {
                      dispatch(
                        setChatData({
                          selectedMessage: item,
                          detailMessageUpdateCount:
                            1 + detailMessageUpdateCount,
                        })
                      );
                      router.push({
                        query: {
                          id_message: item.id_groupmessage,
                          message_type: "message",
                          type: item.type !== "group" ? "personal" : "group",
                        },
                      });
                      setTimeout(() => getData(), 200);
                    }}
                    onSuccessDeleteMessage={() => getData()}
                    text={stringHtmlMentionParser(messageText, "neutral-old-40", "/profile", router.push)}
                    updatedAt={item.updated_at}
                    unreadCount={
                      item.unread_count < 99 ? item.unread_count : 99
                    }
                    isRead={item.status === "unread"}
                    type={item.type}
                    profilePicture={
                      item.type === "group" ? item.image : item.avatar
                    }
                  />
                </div>
              );
            })
          )}
        </div> :
        <div className="message-list flex-column" ><MessageCard isLoading={true} count={6}/></div>}
      </div>
    </div>
  );
};

export default ListMessage;
