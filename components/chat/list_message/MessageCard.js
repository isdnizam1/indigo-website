import React, { useState, Fragment } from "react";
import { Button, Dropdown } from "react-bootstrap";
import ModalConfirmation from "/components/modal/ModalConfirmation";
import { postDeleteChat } from "/services/actions/api";
import { getAuth, messageFormatter, apiDispatch } from "/services/utils/helper";
import { useRouter } from "next/router";
import SkeletonElement from "/components/skeleton/SkeletonElement";
import CardUser from "components/home/CardUser";

const MessageCard = ({
  idGroupMessage,
  isAdminTenant,
  profilePicture,
  fullName,
  idUser,
  text,
  updatedAt,
  type,
  unreadCount,
  isRead,
  onOpenMessage,
  onSuccessDeleteMessage,
  isLoading,
  count,
}) => {
  const [modal, setModal] = useState(false);
  const { id_user } = getAuth();
  const router = useRouter();

  const parserHTML = (textHtml) => {
    let html = textHtml;
    if (Array.isArray(html)) {
      html = "";
      textHtml.forEach((item) => {
        if (item?.props?.children !== undefined)
          html = html + item.props.children;
        else html = html + item;
      });
    }
    return html?.length > 26 ? html.slice(0, 26) + "..." : html;
  };

  return Array(count)
    .fill(0)
    .map((item, idx) => (
      <Fragment key={idx}>
        <div className="message-card-container">
          <div className="d-flex w-100" onClick={onOpenMessage}>
            <CardUser
              image={profilePicture}
              isAdminTenant={type === "personal" && isAdminTenant}
              name={fullName}
              customInformation={
                <p className="text-md-light color-neutral-550 m-0">
                  {parserHTML(text)}
                </p>
              }
            />
          </div>
          <div className="message-additional">
            {!isLoading ? (
              <span
                className="message-time mb-1"
                style={{
                  color: !unreadCount ? "var(--neutral-50)" : "var(--semantic-main)",
                }}
              >
                {messageFormatter(updatedAt) === "Today"
                  ? updatedAt.split(" ")[1].slice(0, 5)
                  : messageFormatter(updatedAt)}
              </span>
            ) : (
              <SkeletonElement width={65} height={16} />
            )}
            {!isLoading ? (
              <Dropdown>
                <Dropdown.Toggle>
                  {!unreadCount && (
                    <span className="material-icons-outlined">expand_more</span>
                  )}
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item onClick={() => setModal(true)}>
                    Delete chat
                  </Dropdown.Item>
                  {/* <Dropdown.Item
                onClick={() => {
                  console.log("pin chat");
                }}
              >
                Pin chat
              </Dropdown.Item> */}
                </Dropdown.Menu>
              </Dropdown>
            ) : (
              <SkeletonElement width={24} height={24} />
            )}
            {unreadCount && (
              <span
                className="message-number"
                style={{ color: "var(--base-10)" }}
              >
                {unreadCount}
              </span>
            )}
          </div>
        </div>
        <div className="msg-hr my-3" />

        <ModalConfirmation
          backdrop={"static"}
          show={modal}
          customExitIcon={
            <span
              className="material-icons-round"
              style={{ fontWeight: "bolder", color: "var(--neutral-70)" }}
            >
              clear
            </span>
          }
          onHide={() => setModal(false)}
          title={<h3 className="heading-md-bold m-0">Delete Chat</h3>}
          subTitle={
            <>
              <p className="text-lg-normal m-0">
                You selected this chat to be deleted.
              </p>
              <p className="text-lg-normal m-0">Continue deleting chat?</p>
            </>
          }
          customBody={
            <>
              <div className="d-flex align-items-center flex-column mt-3 mb-2">
                <Button
                  className="btn-primary w-100 my-2"
                  style={{ width: "fit-content" }}
                  onClick={() => {
                    apiDispatch(
                      postDeleteChat,
                      { id_groupmessage: idGroupMessage, id_user },
                      true
                    ).then((response) => {
                      if (response.code === 200) {
                        onSuccessDeleteMessage();
                        setModal(false);
                      }
                    });

                    router.push({ query: { message_type: "message" } });
                  }}
                >
                  <p className="m-0 py-1">Delete Chat</p>
                </Button>

                <Button
                  className="btn-topic w-100 my-2"
                  style={{ width: "fit-content" }}
                  onClick={() => setModal(false)}
                >
                  <p className="m-0 py-1">Cancel</p>
                </Button>
              </div>
            </>
          }
        />
      </Fragment>
    ));
};

export default MessageCard;
