import React, { Fragment } from "react";
import SideNavigationBar from "/components/sidebar/SideNavigationBar";
import HeaderLv1 from "/components/header/HeaderLv1";
import classnames from "classnames";
import BreadcrumbLv1 from "/components/header/BreadcrumbLv1";
import { Button, Container } from "react-bootstrap";
import SkeletonElement from "/components/skeleton/SkeletonElement";
import { isEmpty } from "lodash-es";
import { getAuth, apiDispatch } from "/services/utils/helper";
import { getSuggestedContacts, postMessage } from "/services/actions/api";
import { BsFillChatDotsFill } from "react-icons/bs";
import MaterialIcon from "components/utils/MaterialIcon";
import { useState } from "react";
import ModalConfirmation from "components/modal/ModalConfirmation";
import InputSelectV2 from "components/input/InputSelectV2";
import InputText from "components/input/InputText";
import { useFormik } from "formik";
import { adminAskSchema } from "/services/constants/Schemas";
import { setChatData } from "/redux/slices/chatSlice";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";
import { HiChatAlt2 } from "react-icons/hi";

const MainLayout = ({
  children,
  HideSideNavigationBar,
  HasMarginSideNavigationBar,
  banner,
  bannerLv3,
  size,
  Breadcrumb,
  rightBarContent,
  rightBarClassName,
  CenteredRightBar,
  customBanner,
  isLoading,
}) => {
  const [modal, setModal] = useState(false);
  const [isSubmit, setIsSubmit] = useState(false);
  const [showAskAdmin, setShowAskAdmin] = useState(false);
  const [suggestedAdmin, setSuggestedAdmin] = useState([]);
  const [initialData] = useState({
    admin: undefined,
    message: "",
  });
  const boxSize = {
    sm: "x-sm",
    md: "x-md",
    lg: "x-lg",
    xl: "x-xl",
  };

  const { id_user } = getAuth();
  const dispatch = useDispatch();
  const { push } = useRouter();

  const formik = useFormik({
    initialValues: initialData,
    enableReinitialize: true,
    validateOnMount: true,
    validationSchema: adminAskSchema,
    onSubmit: (values) => {
      setIsSubmit(true);
      const admin = JSON.parse(values.admin);

      apiDispatch(
        postMessage,
        {
          id_user,
          message: values.message,
          with_id_user: admin.id_user,
          id_groupmessage: admin.id_groupmessage,
        },
        true
      ).then((response) => {
        if (response.code === 200) {
          const { id_groupmessage } = response.result;
          dispatch(
            setChatData({
              selectedMessage: {
                id_user: admin.id_user,
                full_name: admin.full_name,
                image: admin.profile_picture,
                id_groupmessage,
                type: "personal",
              },
            })
          );
          setTimeout(() => {
            push({
              pathname: "/chat",
              query: {
                id_message: id_groupmessage,
                message_type: "message",
                type: "personal",
              },
            });
          }, 500);
        }
      });
    },
  });

  const getSuggestedAdmin = () => {
    formik.validateForm();
    setModal(true);

    apiDispatch(getSuggestedContacts, { id_user }, true).then((response) => {
      response.code === 200 && setSuggestedAdmin(response.result);
    });
  };

  const renderModalAdmin = () => {
    function header() {
      return (
        <div className="d-flex justify-content-between align-items-center p-1 w-100">
          <div className="d-flex align-items-center gap-2">
            <div className="rounded-circle bg-primary-40 p-1 d-flex justify-content-center align-items-center">
              <HiChatAlt2 size={20} color="var(--primary-100)" />
            </div>
            <p className="text-lg-bold color-primary-pr-120 m-0">
              Select admin to ask
            </p>
          </div>
          <MaterialIcon
            action="clear"
            size={28}
            weight={500}
            className="cursor-pointer"
            color="semantic-main"
            onClick={() => setModal(false)}
          />
        </div>
      );
    }

    return (
      <ModalConfirmation
        size="md"
        show={modal}
        modalClass="x-admin-message"
        onHide={() => {
          setModal(false);
          formik.resetForm();
        }}
        customTitle={header()}
        customBody={
          <Fragment>
            <form onSubmit={formik.handleSubmit}>
              <h3 className="heading-md-normal color-neutral-old-80 text-center">
                Select admin you want to ask
              </h3>

              <p className="text-lg-light color-neutral-old-40 text-center">
                Pilih admin yang ingin kamu tanya, dan masukkan keluhan atau
                saran yang ingin kamu sampaikan.
              </p>

              <div className="mb-3">
                <InputSelectV2
                  value={formik.values.admin}
                  name="admin"
                  disabled={isSubmit}
                  title="Admin"
                  placeholder="Select admin"
                  onChange={formik.handleChange}
                  customOptions={suggestedAdmin?.map((item, idx) => (
                    <option
                      key={idx}
                      value={JSON.stringify(item)}
                      selected={formik.values.admin}
                    >
                      {item.full_name}
                    </option>
                  ))}
                />
              </div>

              <InputText
                className="x-input-message"
                as="textarea"
                disabled={isSubmit}
                name="message"
                title="Message"
                value={formik.values.message}
                onChangeText={formik.handleChange}
                onBlur={formik.handleBlur}
                placeholder="Enter your message"
                disableError
              />

              <Button
                type="submit"
                className="mt-3 mb-1"
                disabled={!isEmpty(formik.errors) || isSubmit}
              >
                {!isSubmit ? "Kirim" : "Sending..."}
              </Button>
            </form>
          </Fragment>
        }
      />
    );
  };

  return (
    <div className="x-mainlayout-component">
      <HeaderLv1 />

      <div className="x-content-wrapper">
        {!HideSideNavigationBar && <SideNavigationBar />}
        {HasMarginSideNavigationBar && <div className="x-marginleft-sidebar" />}
        <div className={classnames("x-content", size && boxSize[size])}>
          <Container>
            {Breadcrumb && (
              <div className="x-breadscrumb-wrapper">
                <BreadcrumbLv1 />
              </div>
            )}

            {banner && <img src={banner} className="x-banner" alt="" />}
            {(isLoading || bannerLv3) && (
              <Fragment>
                <div className="x-card">
                  <img src={bannerLv3.banner} className="x-card-cover" alt="" />
                  <div className="x-cover">
                    {!isLoading ? (
                      <img
                        src={bannerLv3.image}
                        className="x-image mb-4"
                        alt=""
                      />
                    ) : (
                      <SkeletonElement
                        width={200}
                        height={190}
                        className="mb-4 x-image"
                      />
                    )}
                  </div>
                </div>

                <div className="card d-flex pt-1 mt-5 border-0">
                  <div className="d-flex justify-content-center align-items-center flex-column m-0">
                    {(isLoading || bannerLv3.title) &&
                      (!isLoading ? (
                        <p className="m-0 heading-sm-bold text-center">
                          {bannerLv3.title}
                        </p>
                      ) : (
                        <SkeletonElement
                          width={350}
                          height={24}
                          className="mb-2"
                        />
                      ))}
                    {bannerLv3.customTitle && bannerLv3.customTitle}
                    {bannerLv3.subTitle && (
                      <h3 className="heading-sm-light text-center color-neutral-old-40 m-0 pt-1">
                        {bannerLv3.subTitle}
                      </h3>
                    )}
                    {bannerLv3.customSubTitle && bannerLv3.customSubTitle}
                  </div>
                </div>

                <hr className="mt-4 mb-2" />
              </Fragment>
            )}
            {customBanner && customBanner}
            {children}
          </Container>
        </div>
        {rightBarContent && (
          <div
            className={classnames(
              `x-content-rightbar ${boxSize[size]}`,
              rightBarClassName && rightBarClassName
            )}
          >
            <Container
              className={classnames(
                "d-flex",
                CenteredRightBar && "justify-content-center"
              )}
            >
              {rightBarContent}
            </Container>
          </div>
        )}
        <div
          className={classnames(
            "x-bubble-absolute",
            !showAskAdmin ? "d-none" : "d-block"
          )}
        >
          Ask Admin
        </div>
        <div className="x-chat-absolute">
          <Button
            onClick={() => getSuggestedAdmin()}
            onMouseEnter={() => setShowAskAdmin(true)}
            onMouseLeave={() => setShowAskAdmin(false)}
          >
            <BsFillChatDotsFill size="1.2rem" />
          </Button>
        </div>
      </div>
      {renderModalAdmin()}
    </div>
  );
};

export default MainLayout;
