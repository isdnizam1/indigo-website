import React, { Fragment } from "react";
import BreadcrumbLv0 from "/components/header/Breadcrumb";
import HeaderLv0 from "/components/header/HeaderLv0";
import { useRouter } from "next/router";

const AuthLayout = ({
  children,
  Breadcrumb,
  breadcrumbOnBackhandler,
  headerDisableRoute,
}) => {
  const router = useRouter();

  const routePath = {
    "/login": "login",
    "/register": "register",
  };

  return (
    <Fragment>
      <HeaderLv0
        route={routePath[router.pathname]}
        disableRoute={headerDisableRoute}
      />
      {Breadcrumb && <BreadcrumbLv0 onBackHandler={breadcrumbOnBackhandler} />}
      {children}
    </Fragment>
  );
};

export default AuthLayout;
