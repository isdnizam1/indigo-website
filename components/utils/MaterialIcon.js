import React from "react";
import classnames from "classnames";

const MaterialIcon = ({
  className,
  type,
  action,
  size,
  weight,
  color,
  onClick,
}) => {
  const TYPE = {
    outlined: "material-icons-outlined",
    filled: "material-icons",
    rounded: "material-icons-round",
    sharp: "material-icons-sharp",
    "two-tone": "material-icons-two-tone",
  };
  return (
    <span
      onClick={onClick}
      className={classnames(
        !type ? TYPE.rounded : TYPE[type],
        className && className,
        color && `color-${color}`
      )}
      style={{
        fontSize: !size ? 24 : size,
        fontWeight: !weight ? 400 : weight,
      }}
    >
      {action}
    </span>
  );
};

export default MaterialIcon;
