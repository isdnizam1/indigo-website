import React, { useEffect, useState } from "react";
import { Button, Col, Container, Row } from "react-bootstrap";
import { convertToPrice } from "/services/utils/helper";
import InputToggleGroup from "/components/input/InputToggleGroup";

const Pricing = ({ data }) => {
  const [plan, setPlan] = useState("annually");
  const [pricing, setPricing] = useState({});
  const [url] = useState({
    monthly: "https://web.eventeer.id/registration?package=premium&duration=1",
    annually: "https://web.eventeer.id/registration?package=premium&duration=12",
  });

  useEffect(() => {
    setPricing(data);
  }, [data]);

  return (
    <div className="x-landing-pricing" id="featurePricing">
      <div className="x-dot-background" />

      <Container>
        <div className="d-flex justify-content-center-align-items-center flex-column w-100">
          <h1 className="x-pricing-title">Pricing</h1>
          <h3 className="x-pricing-subtitle">
            Pick a plan that's right for you
          </h3>
        </div>

        <div className="d-flex justify-content-center align-items-center w-100 mt-4">
          <div className="x-pricing-plan">
            <InputToggleGroup
              data={[
                { name: "Annually", value: "annually" },
                { name: "Monthly", value: "monthly" },
              ]}
              onClick={(e) => setPlan(e.target.value)}
              toggleValue={plan}
            />
          </div>
        </div>

        <Row className="x-card-wrapper">
          {pricing[plan]?.map((pricingPlan, idx) => (
            <Col md={4} className="d-flex justify-content-center" key={idx}>
              <div className="x-rate-card">
                <div className="x-card-title">
                  <h2 className="x-title">{pricingPlan.package_name}</h2>
                  <h2 className="x-price">
                    Rp {convertToPrice(pricingPlan.price)}
                  </h2>
                </div>
                <div className="x-card-detail">
                  <div className="x-card-feature">
                    {pricingPlan.benefit?.map((benefit, idx) => (
                      <div className="x-detail" key={idx}>
                        <div className="x-rounded x-rounded-premium">
                          <span className="material-icons-round">check</span>
                        </div>
                        <p>{benefit}</p>
                      </div>
                    ))}
                  </div>

                  <div className="d-flex justify-content-center align-items-center w-100">
                    <a href={url[plan]} style={{textDecoration: "none"}}>
                      <Button className="btn-primary">
                        <p>Get Started</p>
                      </Button>
                    </a>
                  </div>
                </div>
              </div>
            </Col>
          ))}
        </Row>
      </Container>
    </div>
  );
};

export default Pricing;
