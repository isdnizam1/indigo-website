import React, { useState } from 'react';
import { Col, Container, Row } from "react-bootstrap";
import classnames from "classnames";


const Value = () => {
  const [state, setState] = useState({
    activeMenu: "Event",
  });

  const [sectionValue] = useState({
    Event: {
      description:
        "The all-in-one event management platform, make your event program more interesting effectively and efficiently through Eventeer",
      image:
        "https://i.ibb.co/cNPqn1w/alexandre-pellaes-6v-Ajp0psc-X0-unsplash-1-1.png",
    },
    Community: {
      description:
        "A better way to build, collaborate and connect community just in one platform",
      image: "https://i.ibb.co/qYqwQsL/unsplash-Zyx1b-K9mqm-A.png",
    },
    Learning: {
      description:
        "Create the most interesting learning ecosystem and try to feel the excitement of learning with Eventeers.",
      image: "https://i.ibb.co/ss1wv3p/unsplash-Pe-UJyoylfe4.png",
    },
  });

  return (
    <div className="x-landing-value">
      <Container className="w-100">
        <div className="x-title-wrapper">
          <h1>Our Value</h1>
        </div>

        <div className="x-menu-wrapper">
          {Object.keys(sectionValue).map((value, idx) => (
            <div
              className="cursor-pointer user-select-none"
              onClick={() => setState({ activeMenu: value })}
              key={idx}
            >
              <h3
                className={classnames(state.activeMenu === value && "active")}
                style={{ color: "var(--neutral-40)" }}
              >
                {value}
              </h3>
              <div
                className={classnames(
                  "x-divider",
                  state.activeMenu === value && "active"
                )}
              />
            </div>
          ))}
        </div>

        <Row className="h-100">
          <Col lg={7}>
            <h3
              // className="heading-md-light mt-4 pe-xl-5"
              // style={{ color: "var(--neutral-70)", lineHeight: "40px" }}
            >
              {sectionValue[state.activeMenu].description}
            </h3>
          </Col>

          <Col
            lg={5}
            className="d-flex justify-content-center align-items-center"
          >
            <img src={sectionValue[state.activeMenu].image} alt="" />
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default Value;
