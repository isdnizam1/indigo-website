import React, { Fragment } from 'react';
import { Col, Container, Row } from 'react-bootstrap';

const FeatureDetail = () => {
  return (
    <div className="x-landing-featuredetail-wrapper">
      <div className="x-padding-top" />

      <div className="x-rectangle-background" />

      <div className="x-landing-featuredetail" id="featureEvent">
        <Container className="h-100">
          <Row className="w-100 h-100">
            <Col
              md={5}
              className="d-flex py-3 justify-content-center flex-column"
            >
              <div className="x-title-wrapper">
                <h1>Event</h1>
              </div>
              <h3>
                Providing event management platform for creating the event by
                prioritizing the best mentor and knowledge. Creating the best
                experience for your attendees at every set of your event.
              </h3>
            </Col>

            <Col md={7} className="d-flex align-items-center">
              <div className="x-background x-background-event" />
            </Col>
          </Row>
        </Container>
      </div>

      <div className="x-landing-featuredetail x-reverse" id="featureCommunity">
        <Container className="h-100">
          <Row className="w-100 h-100">
            <Col
              md={5}
              className="d-flex py-3 justify-content-center flex-column"
            >
              <div className="x-title-wrapper">
                <h1>Community</h1>
              </div>
              <h3>
                Strengthen participant loyalty through an attractive event
                management platform. Creating relationships and getting new
                customers with our features.
              </h3>
            </Col>

            <Col md={7} className="d-flex align-items-center">
              <div className="x-background x-background-community" />
            </Col>
          </Row>
        </Container>
      </div>

      <div className="x-landing-featuredetail" id="featureLearning">
        <Container className="h-100">
          <Row className="w-100 h-100">
            <Col
              md={5}
              className="d-flex py-3 justify-content-center flex-column"
            >
              <div className="x-title-wrapper">
                <h1>Learning</h1>
              </div>
              <h3>
                Create a fun learning ecosystem with Eventeers. Learning will
                not be boring because we prepared audio and visual features.
              </h3>
            </Col>

            <Col md={7} className="d-flex align-items-center">
              <div className="x-background x-background-learning" />
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
}

export default FeatureDetail;
