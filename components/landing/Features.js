import React, { useState } from "react";
import { Container, Row } from "react-bootstrap";

const Features = () => {
  const [data] = useState({
    title: "All the features you need to get started in minutes",
    features: {
      Event: [
        "Organized your event",
        "Webinar ticketing",
        "Created an event",
        "Easy-to-use features",
      ],
      Community: [
        "Collaborated with others",
        "Organized discussion",
        "Published a content",
        "Boost audience engagement",
      ],
      Learning: [
        "Educated audience with visual feature",
        "Spread audio information with the podcast"
      ]
    },
  });

  return (
    <div className="x-landing-features">
      <h1 className="x-feature-title">
        All the features you need to get started in minutes
      </h1>

      <Container>
        <Row>
          {Object.keys(data.features).map((value, idx) => (
            <div className="x-feature-wrapper" key={idx}>
              <div className="d-flex align-items-center mb-xl-4 mb-lg-2 mb-md-2 mb-sm-2">
                <div className="x-circle x-circle-lg" />
                <h2 className="x-feature-detail-title">{value}</h2>
              </div>

              {data.features[value].map((feature, idx) => (
                <div className="d-flex mb-lg-1" key={idx}>
                  <div className="x-circle x-circle-sm">
                    <span className="material-icons-round">done</span>
                  </div>
                  <h3 className="x-feature-detail">{feature}</h3>
                </div>
              ))}
            </div>
          ))}
        </Row>
      </Container>
    </div>
  );
};

export default Features;
