import React from "react";
import { Col, Container, Row } from "react-bootstrap";

const Services = () => {
  return (
    <div className="x-landing-services">
      <div className="x-landing-mobile">
        <Container fluid className="h-100">
          <Row className="mx-5 h-100 flex-wrap">
            <div className="x-margin-top">
              <h1>Are you ready to Boost event with Eventeer?</h1>
              <h2>Download Free Now</h2>

              <div className="x-download-wrapper">
                <div className="x-img-download x-img-playstore" onClick={() => window.open("https://play.google.com/store/apps/details?id=com.amoeba.eventeer", "_blank")}/>
                <div className="x-img-download x-img-appstore" onClick={() => window.open("https://apps.apple.com/us/app/eventeer-event-management/id1599930175", "_blank")} />
              </div>
            </div>

            <div className="x-img-mobile" />
          </Row>
        </Container>
      </div>

      <div className="x-landing-web">
        <Container fluid className="h-100">
          <Row className="mx-2 h-100">
            <Col md={5}>
              <div className="x-img-device" />
            </Col>
            <Col md={7} className="x-title-wrapper">
              <h1>Something exciting is coming!</h1>
              <h2>We'll be available on your website now</h2>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
};

export default Services;
