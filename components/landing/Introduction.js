import React from 'react';
import { Button, Col, Container, Row } from "react-bootstrap";

const Introduction = () => {
  return (
    <div className="x-landing-introduction">
      <div className="x-background">
        <div className="x-layer-first" />
        <div className="x-layer-second" />
        <div className="x-layer-third" />
      </div>

      <Container className="w-100 h-100">
        <Row className="h-100">
          <Col md={6} className="d-flex flex-column justify-content-center">
            <div className="x-description">
              <h1 className="x-description-title">
                The All-In-One Event Platform <br /> for Event Organizer
              </h1>
              <h4 className="x-description-subtitle">
                Manage events, embrace the community and discover the potential
                to learn together with just one platform
              </h4>
            </div>
            <a href="https://web.eventeer.id/registration?package=free&duration=14" style={{textDecoration: "none"}}>
              <Button className="x-button-trial">
                Start your 14-day free trial
                <span className="material-icons-round">east</span>
              </Button>
            </a>
          </Col>
          <Col
            md={6}
            className="d-flex justify-content-center align-items-center position-relative"
          >
            <div className="x-introduction-image" />
          </Col>

          <div className="x-introduction-image-desktop" />
        </Row>
      </Container>
    </div>
  );
}

export default Introduction;
