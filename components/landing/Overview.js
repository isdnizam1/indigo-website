import React, { useEffect, useState } from 'react';
import { Col, Container, Row } from "react-bootstrap";
import BannerCarousel from "/components/home/BannerCarousel";
import { ScrollMenu, VisibilityContext } from "react-horizontal-scrolling-menu";

const Overview = ({data}) => {
  const [banner, setBanner] = useState([]);
  const [feedback, setFeedback] = useState([]);
  const [about, setAbout] = useState({});

  useEffect(() => {
    setBanner([]);
    setFeedback([]);
    setAbout({});

    if(data) {
      data.slideshow.forEach(image => {
        setBanner((banner) => [...banner, { image }]);
      });
      setAbout(data.about);
      setFeedback(data.feedback);
    }
  }, [data]);

  const [eventExisting] = useState([
    "https://i.ibb.co/CBnjJ3h/Telkom-Indonesia-2013-1.png",
    "https://i.ibb.co/4tsRtg4/Leap-1.png",
    "https://i.ibb.co/Qft7NgC/download-1.png",
    "https://i.ibb.co/kKFRsph/uxidlogo-c-1.png",
    "https://i.ibb.co/sqh6XVP/logo-telkomsel-terbaru-2021-1.png",
    "https://i.ibb.co/M9Rvm3H/805d861f71c172ce260a247028cb0718-1.png",
  ]);

  return (
    <div className="x-landing-overview">
      <div className="x-background" />
      <div className="x-dot-background" />
      <div className="x-rectangle-background" />

      <Container>
        <Row className="pb-lg-5 pb-md-0 px-3 m-0">
          <Col md={3} className="d-flex justify-content-center">
            <div className="x-traction-wrapper">
              <div className="x-icon x-icon-user" />
              <h1>{about.total_users}+</h1>
              <h3>Total User</h3>
            </div>
          </Col>
          <Col md={6} className="d-flex justify-content-center">
            <div className="x-traction-wrapper">
              <div className="x-icon x-icon-connection" />
              <h1>{about.total_connection}+</h1>
              <h3>Total Connections</h3>
            </div>
          </Col>
          <Col md={3} className="d-flex justify-content-center">
            <div className="x-traction-wrapper">
              <div className="x-icon x-icon-organizer" />
              <h1>{about.total_organizer}+</h1>
              <h3>Total Organizer</h3>
            </div>
          </Col>
        </Row>

        <Row className="pt-5 m-0">
          <Col md={12} className="x-eventjoined-wrapper">
            <h2>Event Organizer who joined eventeer</h2>
          </Col>
        </Row>

        <Row className="pt-4 pt-lg-4 pt-md-0 pt-sm-4 m-0">
          {eventExisting.map((image, idx) => (
            <Col
              md={2}
              xs={6}
              key={idx}
              className="d-flex justify-content-center py-4 py-md-0 py-lg-0"
            >
              <img className="x-image-eventjoined" src={image} alt="" />
            </Col>
          ))}
        </Row>

        <Row className="x-landing-carousel">
          <BannerCarousel data={banner} arrowAlwaysDisplay={true} />
        </Row>
      </Container>

      <Container fluid>
        <div className="mx-4">
          <h2 className="x-feedback-title">
            What Our Even Organizer Says About Us?
          </h2>
          <Row>
            {feedback?.map((value, idx) => (
              <Col md={4} className="my-3" key={idx}>
                <div className="x-card-feedback">
                  <div className="d-flex flex-column justify-content-between h-100">
                    <div>
                      <img src={value.logo} alt="" />
                      <p className="x-desc">{value.desc}</p>
                    </div>

                    <div className="x-user-detail">
                      <img src={value.photo} alt="" />
                      <div className="d-flex justify-content-center flex-column">
                        <p>{value.name}</p>
                        <p>{value.position}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </Col>
            ))}
          </Row>
        </div>
      </Container>
    </div>
  );
}

export default Overview;
