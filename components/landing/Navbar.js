import React, { useState } from "react";
import { Button, Container, Dropdown, Offcanvas } from "react-bootstrap";
import EventeerLogo from "/public/assets/images/img-logo-eventeer-blue.svg";
import ModalConfirmation from "/components/modal/ModalConfirmation";
import Router from "next/router";

import { gsap } from "gsap";

const NavBar = ({ eventRef }) => {
  const [modal, setModal] = useState(false);
  const [showOffcanvas, setShowOffcanvas] = useState(false);

  return (
    <div className="x-landing-navbar">
      <Container className="d-flex align-items-center justify-content-between">
        <div className="x-menu">
          <img
            src={EventeerLogo.src}
            alt=""
            onClick={() => {
              window.scroll({
                top: 0,
                behavior: "smooth",
                inline: "nearest",
              });
            }}
          />
          <Dropdown>
            <Dropdown.Toggle className="btn-link x-dropdown">
              <span className="text-lg-bold">Features</span>
              <span className="material-icons-round">expand_more</span>
            </Dropdown.Toggle>

            <Dropdown.Menu>
              <Dropdown.Item
                onClick={() => {
                  const element = document.querySelector("#featureEvent");
                  window.scroll({
                    top:
                      element.getBoundingClientRect().top +
                      (window.scrollY - 170),
                    behavior: "smooth",
                    inline: "nearest",
                  });
                }}
              >
                Event
              </Dropdown.Item>
              <Dropdown.Item
                onClick={() => {
                  const element = document.querySelector("#featureCommunity");
                  window.scroll({
                    top:
                      element.getBoundingClientRect().top +
                      (window.scrollY - 170),
                    behavior: "smooth",
                    inline: "nearest",
                  });
                }}
              >
                Community
              </Dropdown.Item>
              <Dropdown.Item
                onClick={() => {
                  const element = document.querySelector("#featureLearning");
                  window.scroll({
                    top:
                      element.getBoundingClientRect().top +
                      (window.scrollY - 170),
                    behavior: "smooth",
                    inline: "nearest",
                  });
                }}
              >
                Learning
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
          <Button
            className="btn-link"
            style={{ color: "var(--neutral-50)" }}
            onClick={() => {
              const element = document.querySelector("#featurePricing");
              window.scroll({
                top:
                  element.getBoundingClientRect().top + (window.scrollY - 65),
                behavior: "smooth",
                inline: "nearest",
              });
            }}
          >
            <span className="text-lg-bold">Pricing</span>
          </Button>
        </div>

        <div className="x-action">
          <Button
            className="btn-link"
            style={{ color: "var(--neutral-50)" }}
            onClick={() => setModal(true)}
          >
            Log In
          </Button>
          <Button
            className="btn-primary"
            onClick={() => {
              const element = document.querySelector("#featurePricing");
              window.scroll({
                top:
                  element.getBoundingClientRect().top + (window.scrollY - 65),
                behavior: "smooth",
                inline: "nearest",
              });
            }}
          >
            Get Started
          </Button>

          <span
            className="material-icons-round cursor-pointer"
            onClick={() => setShowOffcanvas(true)}
          >
            menu
          </span>
        </div>
      </Container>

      <Offcanvas
        className="x-offcanvas"
        placement="end"
        backdrop={true}
        onHide={() => setShowOffcanvas(false)}
        show={showOffcanvas}
      >
        <div className="x-header">
          <div
            className="x-logo"
            onClick={() => {
              window.scroll({
                top: 0,
                behavior: "smooth",
                inline: "nearest",
              });
              setShowOffcanvas(false);
            }}
          />
          <span
            className="material-icons-round cursor-pointer"
            onClick={() => setShowOffcanvas(false)}
          >
            clear
          </span>
        </div>
        <div className="x-menu">
          <div
            className="d-flex align-items-center cursor-pointer user-select-none py-2"
            onClick={() => {
              const element = document.querySelector("#featureEvent");
              window.scroll({
                top:
                  element.getBoundingClientRect().top + (window.scrollY - 170),
                behavior: "smooth",
                inline: "nearest",
              });
              setShowOffcanvas(false);
            }}
          >
            <p
              className="text-lg-bold m-0 pe-1"
              style={{ color: "var(--primary-pressed)" }}
            >
              Event
            </p>
            <span
              className="material-icons-round"
              style={{ color: "var(--neutral-50)", fontSize: "18px" }}
            >
              chevron_right
            </span>
          </div>
          <div
            className="d-flex align-items-center cursor-pointer user-select-none py-2"
            onClick={() => {
              const element = document.querySelector("#featureCommunity");
              window.scroll({
                top:
                  element.getBoundingClientRect().top + (window.scrollY - 170),
                behavior: "smooth",
                inline: "nearest",
              });
              setShowOffcanvas(false);
            }}
          >
            <p
              className="text-lg-bold m-0 pe-1"
              style={{ color: "var(--primary-pressed)" }}
            >
              Community
            </p>
            <span
              className="material-icons-round"
              style={{ color: "var(--neutral-50)", fontSize: "18px" }}
            >
              chevron_right
            </span>
          </div>
          <div
            className="d-flex align-items-center cursor-pointer user-select-none py-2"
            onClick={() => {
              const element = document.querySelector("#featureLearning");
              window.scroll({
                top:
                  element.getBoundingClientRect().top + (window.scrollY - 170),
                behavior: "smooth",
                inline: "nearest",
              });
              setShowOffcanvas(false);
            }}
          >
            <p
              className="text-lg-bold m-0 pe-1"
              style={{ color: "var(--primary-pressed)" }}
            >
              Learning
            </p>
            <span
              className="material-icons-round"
              style={{ color: "var(--neutral-50)", fontSize: "18px" }}
            >
              chevron_right
            </span>
          </div>
          <div
            className="d-flex align-items-center cursor-pointer user-select-none py-2"
            onClick={() => {
              const element = document.querySelector("#featurePricing");
              window.scroll({
                top:
                  element.getBoundingClientRect().top + (window.scrollY - 65),
                behavior: "smooth",
                inline: "nearest",
              });
              setShowOffcanvas(false);
            }}
          >
            <p
              className="text-lg-bold m-0 pe-1"
              style={{ color: "var(--primary-pressed)" }}
            >
              Pricing
            </p>
            <span
              className="material-icons-round"
              style={{ color: "var(--neutral-50)", fontSize: "18px" }}
            >
              chevron_right
            </span>
          </div>
        </div>
        <div className="x-action">
          <div className="x-btn-tablet">
            <a href="https://admin.eventeer.id" style={{ width: "100%" }}>
              <Button className="btn-primary">
                <div className="x-button-text-wrapper">
                  <p className="text-md-normal m-0">Log In as</p>
                  <p className="text-lg-bolder m-0">Event Organizer</p>
                </div>
              </Button>
            </a>

            <Button className="btn-cancel">
              <div
                className="x-button-text-wrapper"
                onClick={() => Router.push({pathname: "/login"})}
              >
                <p className="text-md-normal m-0">Log In as</p>
                <p className="text-lg-bolder m-0">Participant</p>
              </div>
            </Button>
          </div>

          <div className="x-btn-mobile">
            <a href="https://admin.eventeer.id" style={{ width: "100%" }}>
              <Button className="btn-primary">
                <p className="text-lg-normal m-0">Log In as Event Organizer</p>
              </Button>
            </a>

            <Button
              className="btn-cancel"
              onClick={() => Router.push({pathname: "/login"})}
            >
              <p className="text-lg-normal m-0">Log In as Participant</p>
            </Button>
          </div>
        </div>
      </Offcanvas>

      <ModalConfirmation
        modalClass="x-modal-login"
        size="lg"
        show={modal}
        customExitIcon={
          <span
            className="material-icons-round"
            style={{
              fontWeight: "bolder",
              fontSize: "28px",
              color: "var(--neutral-70)",
            }}
          >
            clear
          </span>
        }
        onHide={() => setModal(false)}
        title={<h2 className="heading-lg-bold mb-0 mt-2">Log In</h2>}
        subTitle={
          <>
            <p
              className="text-lg-normal m-0"
              style={{ color: "var(--neutral-60)" }}
            >
              You can login according to your role.
            </p>
            <p
              className="text-lg-normal m-0"
              style={{ color: "var(--neutral-60)" }}
            >
              Please select the activity you need
            </p>
          </>
        }
        customBody={
          <div className="d-flex align-items-center flex-column mt-3 mb-2">
            <a href="https://admin.eventeer.id" style={{ width: "100%" }}>
              <Button
                className="btn-primary w-75 my-2"
                style={{ width: "fit-content" }}
              >
                <p className="m-0 py-2">Login as Event Organizer</p>
              </Button>
            </a>

            <Button
              className="btn-cancel w-75 my-2"
              style={{ width: "fit-content" }}
              onClick={() => Router.push({pathname: "/login"})}
            >
              <p className="m-0 py-2">Login as participant</p>
            </Button>
          </div>
        }
      />
    </div>
  );
};

export default NavBar;
