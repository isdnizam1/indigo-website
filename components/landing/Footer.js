import React, { useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";

const Footer = ({ data }) => {
  const [about, setAbout] = useState({});

  useEffect(() => setAbout(data), [data]);

  return (
    <div className="x-landing-footer">
      <Container>
        <Row>
          <Col md={5}>
            <div className="x-logo" />
            <p className="text-lg-normal" style={{ color: "var(--base-30)" }}>
              {about.address}
            </p>
          </Col>
          <Col
            md={4}
            className="ps-md-5 d-flex justify-content-between flex-md-column"
          >
            <div className="pb-5">
              <p
                className="text-xl-bold mb-3"
                style={{ color: "var(--primary-main)" }}
              >
                Company
              </p>
              <p
                className="text-lg-normal m-0 cursor-pointer user-select-none"
                style={{ color: "var(--base-30)" }}
              >
                Our Value
              </p>
              <p
                className="text-lg-normal m-0 cursor-pointer user-select-none"
                style={{ color: "var(--base-30)" }}
              >
                Pricing
              </p>
            </div>
            <div className="pb-5">
              <p
                className="text-xl-bold mb-3"
                style={{ color: "var(--primary-main)" }}
              >
                Get in touch
              </p>
              <p
                className="text-lg-normal m-0 cursor-pointer user-select-none"
                style={{ color: "var(--base-30)" }}
              >
                Help Center
              </p>
              <p
                className="text-lg-normal m-0 cursor-pointer user-select-none"
                style={{ color: "var(--base-30)" }}
              >
                Our Location
              </p>
            </div>
          </Col>
          <Col md={3} className="d-flex justify-content-between flex-column">
            <div className="pb-5">
              <p
                className="text-xl-bold mb-3"
                style={{ color: "var(--primary-main)" }}
              >
                Partnership Opportunity
              </p>
              <p
                className="text-lg-normal m-0 pb-4"
                style={{ color: "var(--base-30)" }}
              >
                {about.partnership_opportunity}
              </p>
            </div>
            <div className="pb-5">
              <p
                className="text-xl-bold mb-0"
                style={{ color: "var(--primary-main)" }}
              >
                Now Available at:
              </p>
              <div className="x-download-wrapper">
                <div className="x-img-download x-img-playstore" onClick={() => window.open("https://play.google.com/store/apps/details?id=com.amoeba.eventeer", "_blank")}/>
                <div className="x-img-download x-img-appstore" onClick={() => window.open("https://apps.apple.com/us/app/eventeer-event-management/id1599930175", "_blank")} />
              </div>
            </div>
          </Col>
        </Row>
        <div className="x-divider" />
        <div className="d-flex justify-content-between pt-3">
          <p className="text-lg-normal m-0 cursor-pointer user-select-none" style={{ color: "var(--base-30)" }} onClick={() => window.open("https://digitalamoeba.id", "_blank")}>
            Powered by Digital Amoeba
          </p>
          <p className="text-lg-normal m-0" style={{ color: "var(--base-30)" }}>
            All Rights Reserved, 2022
          </p>
        </div>
      </Container>
    </div>
  );
};

export default Footer;
