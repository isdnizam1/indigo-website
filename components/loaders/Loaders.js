import React, { Fragment } from "react";
import { isEmpty } from "lodash-es";
import classnames from "classnames";
import BannerCarousel from "/components/home/BannerCarousel";
import PostCard from "/components/cards/PostCard";
import Card from "/components/home/Card";
import CardUser from "/components/home/CardUser";
import EventSelectionList from "/components/event_selection/EventSelectionList";

export const Loaders = {
  // universal used
  Elements: ({
    className,
    type,
    Column,
    count = 1,
    md,
    width,
    height,
    radius,
    color,
    children = null,
    isLoading,
    emptyStateContent,
  }) => {
    return isLoading ? (
      <div className={classnames("d-flex", Column && "flex-column")}>
        {Array(count)
          .fill(0)
          .map((item, idx) => (
            <div
              className={classnames(
                className && className,
                String(width + height).includes("%") && "w-100",
                md && `col-md-${md}`
              )}
              key={idx}
            >
              <div
                style={{
                  width,
                  height,
                  borderRadius: radius,
                  backgroundColor: color,
                }}
                className={classnames("x-skeleton loading", type && type)}
              />
            </div>
          ))}
      </div>
    ) : !isEmpty(children) ? (
      children
    ) : (
      emptyStateContent
    );
  },

  // specific component
  PostCard: ({
    children,
    isLoading,
    count = 1,
    emptyStateContent = null,
  }) => {
    return isLoading
      ? Array(count)
          .fill(0)
          .map((item, idx) => (
            <Fragment key={idx}>
              <PostCard isLoading={true} />
            </Fragment>
          ))
      : !isEmpty(children)
      ? children
      : emptyStateContent;
  },
  Card: ({
    children,
    isLoading,
    col = 4,
    count = 3,
    emptyStateContent = null,
  }) => {
    return isLoading
      ? Array(count)
          .fill(0)
          .map((item, idx) => (
            <Fragment key={idx}>
              <Card isLoading={true} col={col} />
            </Fragment>
          ))
      : !isEmpty(children)
      ? children
      : emptyStateContent;
  },
  BannerCarousel: ({
    children,
    isLoading,
    count = 1,
    emptyStateContent = null,
  }) => {
    return isLoading
      ? Array(count)
          .fill(0)
          .map((item, idx) => (
            <Fragment key={idx}>
              <BannerCarousel isLoading={true} />
            </Fragment>
          ))
      : !isEmpty(children)
      ? children
      : emptyStateContent;
  },
  CardUser: ({ children, isLoading, count = 1, emptyStateContent = null }) => {
    return isLoading
      ? Array(count)
          .fill(0)
          .map((item, idx) => (
            <Fragment key={idx}>
              <CardUser isLoading={true} />
            </Fragment>
          ))
      : !isEmpty(children)
      ? children
      : emptyStateContent;
  },
  EventSelectionList: ({
    children,
    isLoading,
    count = 1,
    emptyStateContent = null,
  }) => {
    return isLoading
      ? Array(count)
          .fill(0)
          .map((item, idx) => (
            <Fragment key={idx}>
              <EventSelectionList isLoading={true} />
            </Fragment>
          ))
      : !isEmpty(children)
      ? children
      : emptyStateContent;
  },
  Components: ({
    component,
    children,
    props,
    isLoading,
    count = 1,
    emptyStateContent = null,
  }) =>
    isLoading
      ? Array(count)
          .fill(0)
          .map((item, idx) => (
            <Fragment key={idx}>
              {React.createElement(component, { ...props, isLoading: true })}
            </Fragment>
          ))
      : !isEmpty(children)
      ? children
      : emptyStateContent,
};

export const LoaderContent = ({
  className,
  type,
  Column,
  count = 1,
  md,
  width,
  height,
  radius,
  color,
  children = null,
  isLoading,
  emptyStateContent = null,
}) => {
  return isLoading
  ? (
    <div className={classnames("d-flex", Column && "flex-column")}>
      {Array(count).fill(0).map((item, idx) => (
        <div
          className={classnames(
            className && className,
            String(width + height).includes("%") && "w-100",
            md && `col-md-${md}`
          )}
          key={idx}
        >
          <div
            style={{
              width,
              height,
              borderRadius: radius,
              backgroundColor: color,
            }}
            className={classnames("x-skeleton loading", type && type)}
          />
        </div>
      ))}
    </div>
  )
  : !isEmpty(children)
  ? children
  : emptyStateContent
}

export const LoaderComponent = ({
  component,
  children = null,
  props,
  isLoading,
  count = 1,
  emptyStateContent = null,
}) => {
  return isLoading
    ? Array(count).fill(0).map((item, idx) => (
      <Fragment key={idx}>
        {React.createElement(component, { ...props, isLoading: true })}
      </Fragment>
    ))
    : !isEmpty(children)
    ? children
    : emptyStateContent
};
