import React, { useEffect, useState, useRef, Fragment } from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import { getAuth, isLogin, apiDispatch } from "/services/utils/helper";
import classnames from "classnames";
import {
  getProfileDetail,
  getTotalUnreadMessages,
} from "/services/actions/api";
import { isEmpty } from "lodash-es";
import DefaultPicture from "/public/assets/icons/icon-default-picture.svg";
import { useRouter } from "next/router";
import ProfileCard from "../profile/ProfileCard";
import ModalLogin from "/components/modal/ModalLogin";
import { HiChatAlt2, HiBell } from "react-icons/hi";
import InputText from "components/input/InputText";

const HeaderLv1 = ({ HideMenuTopBar, customCta }) => {
  const { push, pathname } = useRouter();
  const { id_user } = getAuth();
  const [shownProfile, setShownProfile] = useState(false);
  const [shownNotification, setShownNotification] = useState(false);
  const [toggleNotification, setToggleNotification] = useState("updates");
  const [modalLogin, setModalLogin] = useState(false);
  const [unreadCount, setUnreadCount] = useState(0);
  const [user, setUser] = useState({});
  const ref = useRef();

  useEffect(() => !isEmpty(getAuth()) && getData(), []);

  const getData = () => {
    apiDispatch(getProfileDetail, { id_user }, true).then((response) => {
      response.code === 200 && setUser(response.result);
    });
    apiDispatch(getTotalUnreadMessages, { id_user }, true).then(
      (response) =>
        response.code === 200 && setUnreadCount(response.result.total_unread)
    );
  };

  useEffect(() => {
    const handleShowProfile = (event) => {
      if (ref.current) {
        if (
          !ref.current.contains(event.target) &&
          !document.querySelector(".x-headerlv1-wrapper").contains(event.target)
        ) {
          setShownProfile(false);
          setShownNotification(false);
        }
      }
    };

    document.removeEventListener("mousedown", handleShowProfile);
    document.addEventListener("mousedown", handleShowProfile);

    return () => document.removeEventListener("mousedown", handleShowProfile);
  }, [ref]);

  const dropdownNotification = () => {
    return (
      <div ref={ref} className="x-notification-expand">
        <Row className="x-header-notification">
          <Col md={3} />
          <Col
            md={6}
            className="d-flex justify-content-center align-items-center m-0"
          >
            <p className="text-lg-bold m-0 color-primary-pressed">
              Notification
            </p>
          </Col>
          <Col md={3} className="d-flex justify-content-end align-items-center">
            <span className="material-icons-round cursor-pointer color-primary-pressed">
              more_horiz
            </span>
          </Col>
        </Row>

        <Row className="x-menu-notification">
          <Col
            md={6}
            className="d-flex flex-column justify-content-center align-items-center p-0 cursor-pointer"
            onClick={() => setToggleNotification("updates")}
          >
            <p
              className={classnames(
                "text-sm-bold m-0 pb-1 h-100 pt-2 color-neutral-old-40",
                toggleNotification === "updates" && "active"
              )}
            >
              {"UPDATES (2)"}
            </p>
            <div
              className={classnames(
                "x-divider",
                toggleNotification === "updates" && "active"
              )}
            />
          </Col>

          <Col
            md={6}
            className="d-flex flex-column justify-content-center align-items-center p-0 cursor-pointer"
            onClick={() => setToggleNotification("activities")}
          >
            <p
              className={classnames(
                "text-sm-bold m-0 pb-1 h-100 pt-2 color-neutral-old-40",
                toggleNotification === "activities" && "active"
              )}
            >
              {"ACTIVITIES (0)"}
            </p>
            <div
              className={classnames(
                "x-divider",
                toggleNotification === "activities" && "active"
              )}
            />
          </Col>
        </Row>

        <div className="x-activity-list">
          <Row className="x-activity">
            <Col md={2} className="pe-0">
              <div className="x-rounded-icon">
                <span className="material-icons-round">sms</span>
              </div>
            </Col>
            <Col md={10}>
              <div className="d-flex">
                <div>
                  <p className="text-sm-light line-height-sm m-0 color-neutral-80">
                    <span className="text-sm-bold line-height-sm">
                      {"Andika Leonardo Surya" + " "}
                    </span>
                    memberikan komentar: Soo excited with your post!
                  </p>
                  <p className="text-sm-normal m-0 color-grey-4">1 Hari lalu</p>
                </div>
              </div>
            </Col>
          </Row>

          <Row className="x-activity">
            <Col md={2} className="pe-0">
              <div className="x-rounded-icon">
                <span className="material-icons-round">favorite</span>
              </div>
            </Col>
            <Col md={10}>
              <div className="d-flex">
                <div>
                  <p className="text-sm-light line-height-sm m-0 color-neutral-80">
                    <span className="text-sm-bold line-height-sm">
                      {"Andika Leonardo Surya" + " "}
                    </span>
                    menyukai post kamu.
                  </p>
                  <p className="text-sm-normal m-0 color-grey-4">1 Hari lalu</p>
                </div>
              </div>
            </Col>
          </Row>

          <Row className="x-activity">
            <Col md={2} className="pe-0">
              <div className="x-rounded-icon">
                <span className="material-icons-round">person_add_alt_1</span>
              </div>
            </Col>
            <Col md={10}>
              <div className="d-flex">
                <div>
                  <p className="text-sm-light line-height-sm m-0 color-neutral-80">
                    <span className="text-sm-bold line-height-sm">
                      {"Andika Leonardo Surya" + " "}
                    </span>
                    mengikuti kamu.
                  </p>
                  <p className="text-sm-normal m-0 color-grey-4">1 Hari lalu</p>
                </div>
                <div className="d-flex align-items-center">
                  <Button className="btn-primary m-0">Ikuti</Button>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    );
  };

  const {
    about_me,
    full_name,
    interest,
    job_title,
    location,
    profile_picture,
    total_followers,
    total_following,
  } = user;

  return (
    <header className="x-headerlv1-wrapper">
      <Container>
        <Row className="w-100">
          <Col
            md={12}
            className="pe-0 d-flex align-items-center justify-content-between"
          >
            <div className="x-logo-wrapper">
              <div className="x-logo-indigo" onClick={() => push("/home")} />
            </div>
            {!HideMenuTopBar && (
              <Fragment>
                {isLogin() ? (
                  <Fragment>
                    {/* <div className="d-flex w-100 align-items-center">
                      <InputText
                        disableError={true}
                        placeholder={"Coba cari designer"}
                        inlineIcon={"search"}
                      />
                      <Button className="x-button-search">Search</Button>
                    </div> */}

                    <div className="d-flex align-items-center gap-3">
                      <div
                        className={classnames(
                          "x-icon-menu",
                          pathname.includes("/chat") && "active"
                        )}
                        onClick={() =>
                          push({
                            pathname: "/chat",
                            query: {
                              message_type: "message",
                            },
                          })
                        }
                      >
                        <HiChatAlt2
                          size={24}
                          color={`var(--${
                            pathname.includes("/chat")
                              ? "semantic-main"
                              : "neutral-650"
                          })`}
                        />
                        {unreadCount > 0 && (
                          <div className="x-unread-count">
                            <span>{unreadCount}</span>
                          </div>
                        )}
                      </div>

                      <div
                        className={classnames(
                          "x-icon-menu",
                          pathname.includes("/notification") && "active"
                        )}
                        onClick={() => {
                          push("/home/notification");
                          // setShownNotification(shownNotification ? false : true);
                          // setShownProfile(false);
                        }}
                      >
                        <HiBell
                          size={24}
                          color={`var(--${
                            pathname.includes("/notification")
                              ? "semantic-main"
                              : "neutral-650"
                          })`}
                        />
                      </div>

                      <div
                        className="x-profile-wrapper"
                        onClick={() => {
                          setShownProfile(shownProfile ? false : true);
                          setShownNotification(false);
                        }}
                      >
                        {!isEmpty(profile_picture) ? (
                          <img src={profile_picture} alt="" />
                        ) : (
                          <span
                            className="material-icons color-base-30"
                            style={{ fontSize: 40 }}
                          >
                            account_circle
                          </span>
                        )}

                        <span className="material-icons-round">
                          expand_more
                        </span>
                      </div>
                    </div>
                  </Fragment>
                ) : (
                  <Button
                    className="w-auto"
                    onClick={() => setModalLogin(true)}
                  >
                    Login
                  </Button>
                )}
              </Fragment>
            )}
            {customCta && customCta}
          </Col>
        </Row>
      </Container>

      {shownProfile && (
        <div ref={ref}>
          <ProfileCard
            className="x-profile-expand"
            onClick={() => push("/profile")}
            image={
              !isEmpty(profile_picture) ? profile_picture : DefaultPicture.src
            }
            name={full_name}
            jobTitle={job_title}
            cityName={location.city_name}
            countryName={location.city_name}
            about={about_me}
            followersCount={total_followers}
            followingCount={total_following}
            interests={interest}
          />
        </div>
      )}
      {shownNotification && dropdownNotification()}

      <ModalLogin show={modalLogin} onHide={() => setModalLogin(false)} />
    </header>
  );
};

export default HeaderLv1;
