// import { Link } from "react-router-dom";
import Link from "next/link";

export default function BreadcrumbLv1({ children }) {
  return <ul className="x-header-breadcrumblv1">{children}</ul>;
}

function Item({ name, target }) {
  return (
    <>
      <li>{target ? <Link href={target}>{name}</Link> : <span>{name}</span>}</li>
      <div className="material-icons-round x-icon-right">chevron_right</div>
    </>
  );
}

BreadcrumbLv1.Item = Item;
