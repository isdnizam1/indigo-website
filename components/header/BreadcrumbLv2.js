import { Link } from "react-router-dom";

export default function BreadcrumbLv2({ name }) {
  return (
    <div className="x-header-breadcrumblv2">
      <Link to="/feeds">Feeds</Link>
      <span className="material-icons-round">chevron_right</span>
      <Link className="active">{name}'s Post</Link>
    </div>
  );
}
