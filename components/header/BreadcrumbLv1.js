import React, { Fragment, useEffect, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import classnames from "classnames";
import { isEmpty } from "lodash-es";
import { getAuth } from "/services/utils/helper";

const BreadcrumbLv1 = (props) => {
  const router = useRouter();

  const [basePath, setBasePath] = useState({
    name: "Home",
    pathname: "/home",
  });

  const { query } = router;

  const [path] = useState({
    "/home/banner-detail": [
      { name: "Banner Details", path: "/home/banner-detail" },
    ],
    // Collaboration
    "/home/collaboration": [
      { name: "Collaboration", path: "/home/collaboration" },
    ],
    "/home/collaboration/create-collaboration": [
      { name: "Collaboration", path: "/home/collaboration" },
      { name: "Create Collaboration", path: "/home/collaboration/create-collaboration" },
    ],
    "/home/collaboration/edit-collaboration": [
      { name: "Collaboration", path: "/home/collaboration" },
      { name: "Edit Collaboration", path: "/home/collaboration/edit-collaboration" },
    ],
    "/home/collaboration/my-projects": [
      { name: "Collaboration", path: "/home/collaboration" },
      { name: "My Collaboration Projects", path: "/home/collaboration/my-projects" },
    ],
    "/home/collaboration/explore": [
      { name: "Collaboration", path: "/home/collaboration" },
      {
        name: "Explore Collaboration",
        path: "/home/collaboration/explore",
      },
    ],
    "/home/collaboration/explore/detail": [
      { name: "Collaboration", path: "/home/collaboration" },
      {
        name: "Explore Collaboration",
        path: "/home/collaboration/explore",
      },
      {
        name: "Detail Project",
        path: "/home/collaboration/explore/detail",
        query
      },
    ],

    "/home/collaboration/detail": [
      { name: "Collaboration", path: "/home/collaboration" },

      {
        name: "Detail Project",
        path: "/home/collaboration/detail",
      },
    ],
    // my project
    "/home/collaboration/my-projects/collaboration-detail": [
      { name: "Collaboration", path: "/home/collaboration" },
      { name: "My Collaboration Projects", path: "/home/collaboration/my-projects", query },
      {
        name: "Collaboration Project",
        path: "/home/collaboration/my-projects/collaboration-detail",
        query
      },
    ],
    "/home/collaboration/my-projects/group-chat": [
      { name: "Collaboration", path: "/home/collaboration" },
      { name: "My Collaboration Projects", path: "/home/collaboration/my-projects", query },
      {
        name: "Group Chat Confirmation",
        path: "/home/collaboration/my-projects/group-chat",
      },
    ],

    "/home/collaboration/detail": [
      { name: "Collaboration", path: "/home/collaboration" },

      {
        name: "Project",
        path: "/home/collaboration/detail",
      },
    ],
    "/home/collaboration/my-projects/profile": [
      { name: "Collaboration", path: "/home/collaboration" },
      { name: "My Collaboration Projects", path: "/home/collaboration/my-projects", query },
      { name: "Profile User", path: "/home/collaboration/my-projects/profile" },
    ],
    "/home/collaboration/my-network": [
      { name: "Collaboration", path: "/home/collaboration" },
      { name: "My Network", path: "/home/collaboration/my-network" },
    ],
    "/home/collaboration/my-network/profile": [
      { name: "Collaboration", path: "/home/collaboration" },
      { name: "My Network", path: "/home/collaboration/my-network" },
      {
        name: "Profile User",
        path: "/home/collaboration/my-network/profile",
      },
    ],
    "/home/collaboration/recent": [
      { name: "Collaboration", path: "/home/collaboration" },
      {
        name: "Recently Updates",
        path: "/home/collaboration/recent",
      },
    ],
    "/home/collaboration/popular": [
      { name: "Collaboration", path: "/home/collaboration" },
      {
        name: "Popular Collaborations",
        path: "/home/collaboration/popular",
      },
    ],

    // agenda
    "/home/agenda": [{ name: "Agenda", path: "/home/agenda" }],
    "/home/agenda/detail": [
      { name: "Agenda", path: "/home/agenda" },
      { name: "Session Detail", path: "/home/agenda/detail", query },
    ],
    "/home/agenda/detail/registration": [
      { name: "Agenda", path: "/home/agenda" },
      { name: "Session Detail", path: "/home/agenda/detail", query },
      {
        name: "Registration",
        path: "/home/agenda/detail/registration",
        query,
      },
    ],


    // agenda direct link
    "/agenda-register/session-detail": [{ name: "Session Detail", path: "/agenda-register/session-detail", query }],
    "/agenda-register/session-detail/registration": [
      { name: "Session Detail", path: "/agenda-register/session-detail", query },
      { name: "Registration", path: "/agenda-register/session-detail/registration", query },
    ],

    // media learning
    "/home/media-learning": [
      { name: "Media Learning", path: "/home/media-learning" },
    ],
    "/home/media-learning/podcast": [
      { name: "Media Learning", path: "/home/media-learning" },
      { name: "Podcast", path: "/home/media-learning/podcast", query },
    ],
    "/home/media-learning/video": [
      { name: "Media Learning", path: "/home/media-learning" },
      { name: "Video", path: "/home/media-learning/video", query },
    ],

    // submission
    "/home/submission": [{ name: "Submission", path: "/home/submission" }],
    "/home/submission/detail": [
      { name: "Submission", path: "/home/submission" },
      { name: "Submission Detail", path: "/home/submission/detail", query },
    ],
    "/home/submission/detail/register": [
      { name: "Submission", path: "/home/submission" },
      { name: "Submission Detail", path: "/home/submission/detail", query },
      {
        name: "Submit Submission",
        path: "/home/submission/detail/register",
        query,
      },
    ],

    // feeds
    "/feeds/detail": [{ name: "Feed Detail", path: "/feeds/detail" }],
  });

  useEffect(() => {
    const path = [
      { name: "Home", pathname: "/home" },
      { name: "Feeds", pathname: "/feeds" },
    ];

    path.map(
      (item) =>
        router.pathname.includes(item.pathname) &&
        setBasePath({
          name: item.name,
          pathname: item.pathname,
        })
    );
  }, [router.isReady]);

  return (
    <div className="x-header-breadcrumblv1">
      {!isEmpty(getAuth()) && (
        <Fragment>
          <p
            className="text-lg-light m-0 cursor-pointer neutral-old-40"
            onClick={() => router.push(basePath.pathname)}
          >
            {basePath.name}
          </p>
          <span className="material-icons-round">chevron_right</span>
        </Fragment>
      )}
      {path[router.pathname] &&
        path[router.pathname].map((route, idx) => (
          <Fragment key={idx}>
          {idx >= 1 && <span className="material-icons-round">chevron_right</span>}
            <p
              className={classnames(
                "text-lg-light m-0 cursor-pointer",
                route.path === router.pathname && "active"
              )}
              onClick={() =>
                router.pathname !== route.path &&
                router.push({
                  pathname: route.path,
                  query: route.query ? router.query : {},
                })
              }
            >
              {route.name}
            </p>
          </Fragment>
        ))}
    </div>
  );
};

export default BreadcrumbLv1;
