import { Fragment, useState } from "react";
import { Container, Row, Col, Button } from "react-bootstrap";
import { removeAuth } from "/services/utils/helper";
import ModalConfirmation from "../modal/ModalConfirmation";
import { useRouter } from "next/router";
import classnames from "classnames";

const Breadcrumb = ({ onBackHandler }) => {
  const { reload, pathname, push } = useRouter();

  const logout = () => {
    removeAuth();
    reload();
  }

  const [modal, setModal] = useState(false);

  const breadcrumbPath = {
    "/register/profile": [
      { name: "Register", path: "/register" },
      { name: "Atur profile", path: "/register/profile" },
    ],
    "/register/profession": [
      { name: "Register", path: "/register" },
      { name: "Atur profile", path: "/register/profile" },
      { name: "Atur profesi", path: "/register/profession" },
    ],
    "/register/interest": [
      { name: "Register", path: "/register" },
      { name: "Atur profile", path: "/register/profile" },
      { name: "Atur profesi", path: "/register/profession" },
      { name: "Pilih interest", path: "/register/interest" },
    ],
    "/forgot-password": [
      { name: "Login", path: "/login" },
      { name: "Lupa password", path: "/forgot-password" },
    ],
    "/forgot-password/recovery-code": [
      { name: "Login", path: "/login" },
      { name: "Lupa password", path: "/forgot-password" },
      { name: "Recovery code", path: "/forgot-password/recovery-code" },
    ],
    "/forgot-password/password-recovery": [
      { name: "Login", path: "/login" },
      { name: "Lupa password", path: "/forgot-password" },
      { name: "Buat password baru", path: "/forgot-password/password-recovery" },
    ],
  };

  return (
    <div className="x-header-breadcrumb">
      <Container>
        <Row className="w-100 pt-2 pb-2 m-0 flex-nowrap">
          <Col sm={1}>
            <Button className="btn-cancel" onClick={onBackHandler !== "register" ? onBackHandler : () => setModal(true)}>
              <span className="material-icons-round">chevron_left</span>
            </Button>
          </Col>

          <Col
            sm={10}
            className="d-flex justify-content-center align-items-center"
          >
            {breadcrumbPath[pathname] &&
              breadcrumbPath[pathname].map((route, idx) => (
                <Fragment key={idx}>
                  {idx !== 0 && (
                    <span className="material-icons-round">chevron_right</span>
                  )}
                  <p
                    className={classnames("text-lg-light m-0 cursor-pointer color-neutral-old-40",
                      route.path === pathname && "active"
                    )}
                    onClick={
                      route.path !== "/register"
                        ? () =>
                            pathname !== route.path &&
                            push(route.path)
                        : () => setModal(true)
                    }
                  >
                    {route.name}
                  </p>
                </Fragment>
              ))}
          </Col>
          <Col sm={1} />
        </Row>
      </Container>

      <ModalConfirmation
        show={modal}
        onHide={() => setModal(false)}
        title={"Apakah anda yakin ingin keluar?"}
        subTitle={"Anda dapat login kembali untuk melanjutkan registrasi"}
        onSubmit={() => setModal(false)}
        onSubmitText={"Tidak, lanjutkan registrasi"}
        onCancel={() => logout()}
        onCancelText={"Keluar"}
      />
    </div>
  );
};

export default Breadcrumb;
