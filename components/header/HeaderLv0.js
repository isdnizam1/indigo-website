import React, { Fragment } from "react";
import { Container, Row, Col, Button, Nav, Navbar } from "react-bootstrap";
import classnames from "classnames";
import { useRouter } from "next/router";
import { HiMenu } from "react-icons/hi";
import { logout, isLogin } from "/services/utils/helper";

const HeaderLv0 = (props) => {
  const { route, disableRoute, isWelcomeScreen } = props;
  const router = useRouter();

  return (
    <Navbar
      expand="lg"
      className="x-headerlv0-wrapper"
      sticky="top"
      style={{
        backgroundColor: !isWelcomeScreen ? "var(--base-50)" : null,
        padding: !disableRoute ? "16px 0" : "24px 0",
      }}
    >
      <Container>
        {!isWelcomeScreen ? (
          <div className="x-logo" onClick={() => router.push("/home")} />
        ) : (
          <div className="x-eventeer-logo" />
        )}
        {!isWelcomeScreen ? (
          !disableRoute && (
            <Fragment>
              <Navbar.Toggle aria-controls="basic-navbar-nav">
                <HiMenu color="var(--semantic-main)" size={24} />
              </Navbar.Toggle>
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className={classnames("x-menu-wrapper", isLogin() && "ms-auto")}>
                  {isLogin() ? (
                    <Nav.Item as="div" className="x-menu">
                      <Nav.Link
                        as={"div"}
                        onClick={() => logout()}
                        className={classnames(
                          "x-route",
                          route === "login" && "active"
                        )}
                      >
                        <p>Logout</p>
                        <div
                          className={classnames(
                            route === "login" && "x-active-divider"
                          )}
                        />
                      </Nav.Link>
                    </Nav.Item>
                  ) : (
                    <Fragment>
                      <Nav.Item as="div" className="x-menu">
                        <Nav.Link
                          as={"div"}
                          onClick={() => router.push("/login")}
                          className={classnames(
                            "x-route",
                            route === "login" && "active"
                          )}
                        >
                          <p>Login</p>
                          <div
                            className={classnames(
                              route === "login" && "x-active-divider"
                            )}
                          />
                        </Nav.Link>
                      </Nav.Item>
                      <Nav.Item as="div" className="x-menu">
                        <Nav.Link
                          as={"div"}
                          onClick={() => router.push("/register")}
                          className={classnames(
                            "x-route",
                            route === "register" && "active"
                          )}
                        >
                          <p>Register</p>
                          <div
                            className={classnames(
                              route === "register" && "x-active-divider"
                            )}
                          />
                        </Nav.Link>
                      </Nav.Item>
                    </Fragment>
                  )}
                </Nav>
              </Navbar.Collapse>
            </Fragment>
          )
        ) : (
          <Button className="btn-secondary" style={{ width: "207px" }}>
            Kembali ke Home
          </Button>
        )}
      </Container>
    </Navbar>
  );
};

export default HeaderLv0;

// ) : (
//   <div className="x-header-welcome">
<Button className="btn-secondary" style={{ width: "207px" }}>
  Kembali ke Home
</Button>;
//   </div>
// )}
