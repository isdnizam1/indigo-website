import { isEmpty } from "lodash-es";
import { Col, Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import classnames from "classnames";

const InputSelect = (props) => {
  const {
    title,
    extraTitle,
    type,
    placeholder,
    className,
    value,
    errorSign,
    icon,
    hasIcon = true,
    extraErrorSignLink,
    linkTo,
    isError,
    data,
    disableInput,
    onChangeText,
    suggestionOnClick,
    suggestionText,
    onRemoveSelected,
    onKeyPress,
    selectedValue,
    disableError,
    onExpand,
    onFocus,
    onBlur,
    style
  } = props;

  return (
    <div className={classnames("x-input-select", className && className)}>
      {(title || extraTitle) && (
        <Form.Label column sm={12} className="text-lg-normal p-0">
          {title} {extraTitle && <span>{" " + extraTitle}</span>}
        </Form.Label>
      )}
      <Col
        sm={12}
        className="p-0 mt-1 d-flex align-items-center flex-column position-relative"
      >
        <Form.Control
          type={type}
          value={value}
          onChange={onChangeText}
          onFocus={onFocus}
          disabled={disableInput}
          onKeyPress={onKeyPress}
          onBlur={onBlur}
          className={isError ? "x-danger" : null}
          placeholder={placeholder}
          style={style}
        />
        {hasIcon && (
          <div className="x-icon">
            {!isEmpty(selectedValue) && (
              <span
                className="material-icons-round x-icon-close"
                onClick={onRemoveSelected}
              >
                close
              </span>
            )}
            <span className="material-icons-round" onClick={onExpand}>
              {!icon ? "expand_more" : icon}
            </span>
          </div>
        )}
        {!isEmpty(data) && (
          <div className="x-suggestion-wrapper">
            {data.map((value, idx) => {
              return (
                <div
                  className="x-suggestion-data"
                  key={idx}
                  onClick={() => suggestionOnClick(value)}
                >
                  <p>
                    {!value[suggestionText] ? value : value[suggestionText]}
                  </p>
                </div>
              );
            })}
          </div>
        )}
        {isError && (
          <div className="x-icon">
            <span className="material-icons-outlined x-danger-icon">error</span>
          </div>
        )}
      </Col>
      {!disableError && (
        <Form.Label column sm={12} className="p-0 x-danger-text">
          {isError && errorSign}
          {isError && extraErrorSignLink && (
            <Link to={linkTo} className="x-danger-text x-underline">
              {" "}
              {extraErrorSignLink}
            </Link>
          )}
        </Form.Label>
      )}
    </div>
  );
};

export default InputSelect;
