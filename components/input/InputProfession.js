import classNames from "classnames";
import { useRouter } from "next/router";
import { Col, Form } from "react-bootstrap";

export default function InputProfession(props) {
  const {
    title,
    type,
    placeholder,
    value,
    onChangeText,
    onKeyPress,
    onKeyDown,
    onBlur,
    as,
    rows,
    maxLength,
    ref,
    style,
    id,
    min,
    max,
  } = props;

  

  return (
    <div className="x-input-profession">
      <div className="input-title mb-1 gap-2">
        <Form.Label className={"p-0 text-lg-normal m-0 text-nowrap"}>
          {title ?? `Search Profession :`}
        </Form.Label>
        <div className="w-100">
          <Form.Control
            id={id}
            as={as}
            rows={rows}
            type={type}
            value={value}
            onChange={onChangeText}
            onKeyPress={onKeyPress}
            onKeyDown={onKeyDown}
            style={style}
            onBlur={onBlur}
            placeholder={
              placeholder ?? `(Example : front end, back end, UI/UX designer)`
            }
            maxLength={maxLength}
            ref={ref}
            min={min}
            max={max}
          />
        </div>
      </div>
    </div>
  );
}
