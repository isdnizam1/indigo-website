import { useEffect, useState } from "react";
import classnames from "classnames";
import { Enhance } from "/services/Enhancer";

const InputToggleGroup = (props) => {
  const {
    title,
    data,
    onClick,
    toggleValue
  } = props;

  const [eventValue, setToggleValue] = useState("");

  const { isMobile } = props.dimension;

  useEffect(() => {
    setToggleValue(toggleValue)
  }, [toggleValue]);

  return (
    <div className="x-toggle-input">
      <p className={classnames("m-0 pb-1", isMobile ? "text-md-normal" : "text-lg-normal")}>{title}</p>

      <div className="btn-group btn-group-toggle" data-toggle="buttons">
        {data.map((data, idx) => (
          <label
            key={idx}
            className={
              eventValue === data.value
                ? "btn btn-secondary active"
                : "btn btn-secondary"
            }
          >
            <input
              type="radio"
              name="options"
              id="option1"
              value={data.value}
              onClick={onClick}
              onChange={(e) => setToggleValue(e.target.value)}
            />
            {data.name}
          </label>
        ))}
      </div>
    </div>
  );
}

export default Enhance(InputToggleGroup);
