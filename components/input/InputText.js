import { Col, Form } from "react-bootstrap";
import classnames from "classnames";
import { useRouter } from "next/router";

const InputText = (props) => {
  const {
    title,
    extraTitle,
    type,
    placeholder,
    className,
    disabled,
    value,
    errorSign,
    extraErrorSignLink,
    linkTo,
    isError,
    onChangeText,
    onKeyPress,
    onKeyDown,
    onBlur,
    disableError,
    inlineIcon,
    inlineIconRight,
    as,
    rows,
    maxLength,
    ref,
    style,
    id,
    min,
    max,
    name,
    customFooter,
    autoComplete,
  } = props;

  const router = useRouter();

  return (
    <>
      <div className={classnames("x-input-text mb-1", className && className)}>
        {title && (
          <Form.Label column sm={12} className="p-0 text-lg-normal">
            {title} {extraTitle && <span>{" " + extraTitle}</span>}
          </Form.Label>
        )}

        <Col
          sm={12}
          className="p-0 mt-1 d-flex align-items-center position-relative"
        >
          <Form.Control
            id={id}
            name={name}
            as={as}
            disabled={!disabled ? false : true}
            autoComplete={autoComplete}
            rows={rows}
            type={type}
            value={value}
            onChange={onChangeText}
            onKeyPress={onKeyPress}
            onKeyDown={onKeyDown}
            style={style}
            onBlur={onBlur}
            className={classnames(
              isError && "x-danger",
              inlineIcon && "x-padding-inlineicon"
            )}
            placeholder={placeholder}
            maxLength={maxLength}
            ref={ref}
            min={min}
            max={max}
          />
          {inlineIcon && (
            <div className="x-inline-icon">
              <span className="material-icons-round">{inlineIcon}</span>
            </div>
          )}

          {inlineIconRight && (
            <div className="x-inline-icon-right">
              <span className="material-icons-round">{inlineIconRight}</span>
            </div>
          )}

          {isError && (
            <div className="x-icon">
              <span className="material-icons-outlined x-danger-icon">
                error
              </span>
            </div>
          )}
        </Col>
        <div className="d-flex justify-content-between">
          <div>
            {!disableError && (
              <Form.Label column sm={12} className="p-0 x-danger-text">
                {isError && errorSign}{" "}
                {isError && extraErrorSignLink && (
                  <span
                    className="x-danger-text x-underline cursor-pointer"
                    onClick={() => router.push(linkTo)}
                  >
                    {extraErrorSignLink}
                  </span>
                )}
              </Form.Label>
            )}
          </div>
          {isError && <span className="text-danger">{isError}</span>}
          {customFooter && customFooter}
        </div>
      </div>
    </>
  );
};

export default InputText;
