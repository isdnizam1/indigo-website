import { Form } from "react-bootstrap";

export default function InputDate(props) {
  const {
    placeholder,
    value,
    disableInput,
    onChange,
    onKeyPress,
    onFocus,
    onBlur,
    children,
    defaultValue,
  } = props;

  return (
    <div>
      <Form.Control
        type="date"
        value={value}
        onChange={onChange}
        defaultValue={defaultValue}
        onFocus={onFocus}
        disabled={disableInput}
        onKeyPress={onKeyPress}
        onBlur={onBlur}
        placeholder={placeholder}
      >
        {children}
      </Form.Control>
      <span className="material-icons-round x-icon-close">close</span>
    </div>
  );
}
