import SkeletonElement from "components/skeleton/SkeletonElement";
const AddPhoto = ({ image, onClick, isEmptyImage, initialName, isUploadDisable, isLoading }) => {
  return (
    <>
      <div className="x-image-upload">
        <div className="x-upload-wrapper">
          {!isEmptyImage ? (
            <img src={image} alt="" />
          ) : !isLoading ?
            <div className="x-empty-image">
              <p>{initialName}</p>
            </div>
           : <SkeletonElement width={150} height={150} type="rounded-circle"/>}
          {!isUploadDisable && (
            <label htmlFor="image-upload" className="x-icon-add-images">
              <span className="material-icons">add_a_photo</span>
              <input
                id="image-upload"
                accept="image/png, image/jpg, image/jpeg"
                type="file"
                name="myImage"
                onChange={onClick}
              />
            </label>
          )}
        </div>
      </div>
    </>
  );
};

export default AddPhoto;
