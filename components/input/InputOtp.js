import { isEmpty } from 'lodash-es';
import React, { Fragment, useEffect, useState } from 'react';
import { FormControl } from 'react-bootstrap';
import classnames from "classnames";
import { Enhance } from '/services/Enhancer';

const InputOtp = (props) => {
  const { numInputs, onChangeValue, errorSign, isError } = props;
  const { isMobile } = props.dimension;
  const [numElements, setNumElements] = useState([]);

  useEffect(() => {
    setNumElements([]);

    for (let i = 0; i < numInputs; i++) {
      setNumElements((numElements) => [...numElements, i]);
    }
  }, [numInputs]);

  return (
    <Fragment>
      <div className="x-input-otp" style={{ gap: isMobile ? "12px" : "24px" }}>
        <div className="x-otp">
          {numElements.map((element, idx) => (
            <FormControl
              key={idx}
              type="number"
              id={`inputOtp${idx}`}
              className={classnames(
                !isMobile ? "x-input-normal" : "x-input-mobile"
              )}
              onFocus={(e) => e.target.select()}
              onInput={(e) => {
                if (e.target.value.length > 1)
                  e.target.value = e.target.value.slice(0, 1);
                if (
                  !isEmpty(e.target.value) &&
                  !isEmpty(e.target.nextElementSibling)
                )
                  e.target.nextElementSibling.focus();
                else if (
                  isEmpty(e.target.value) &&
                  !isEmpty(e.target.previousElementSibling)
                )
                  e.target.previousElementSibling.focus();

                let valueElement = "";
                for (let i = 0; i < numInputs; i++) {
                  let el = document.querySelector(`#inputOtp${i}`);
                  valueElement = valueElement += el.value;
                }

                e.target.otpValue = valueElement;
              }}
              onChange={onChangeValue}
            />
          ))}
        </div>
        <div className="x-error-sign">
          {isError && (
            <p
              className="text-md-light m-0 pt-2"
              style={{ color: "var(--semantic-danger-main)" }}
            >
              {errorSign}
            </p>
          )}
        </div>
      </div>
    </Fragment>
  );
}

export default Enhance(InputOtp);
