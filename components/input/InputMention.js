import React, { Fragment } from "react";
import { Mention, MentionsInput } from "react-mentions";

const InputMention = ({
  value,
  onChange,
  onKeyDown,
  placeholder,
  className,
  dataMention,
  renderSuggestion,
  titleMention,
}) => (
  <MentionsInput
    value={value}
    onChange={onChange}
    onKeyDown={onKeyDown}
    placeholder={placeholder}
    className={className}
    customSuggestionsContainer={(children) => (
      <Fragment>
        <p className="m-0 mx-4 mt-3 mb-1 text-lg-bold color-neutral-80">
          {titleMention}
        </p>
        {children}
      </Fragment>
    )}
  >
    <Mention
      trigger="@"
      data={dataMention}
      displayTransform={(id, display) => `@${display}`}
      appendSpaceOnAdd={true}
      markup={`<mention id="__id__">__display__</mention>`}
      renderSuggestion={renderSuggestion}
    />
  </MentionsInput>
);

export default InputMention;
