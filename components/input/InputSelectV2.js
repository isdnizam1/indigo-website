import React, { Fragment } from "react";
import { Form } from "react-bootstrap";
import classnames from "classnames";

const InputSelectV2 = ({
  className,
  options,
  disabled,
  name,
  value = "",
  customOptions,
  onChange,
  title,
  placeholder,
}) => {
  return (
    <div className={classnames("x-input-selectv1", className && className)}>
      {title && (
        <Form.Label column sm={12} className="text-lg-normal p-0">
          {title}
        </Form.Label>
      )}
      <Form.Select
        className="mt-1"
        disabled={!disabled ? false : true}
        onChange={onChange}
        value={value}
        name={name}
      >
        {placeholder && (
          <option key="blankChoice" hidden value>
            {placeholder}
          </option>
        )}
        {customOptions}
        {options &&
          options?.map((item, idx) => (
            <option key={idx} value={item} selected={value}>
              {item}
            </option>
          ))}
      </Form.Select>
    </div>
  );
};

export default InputSelectV2;
