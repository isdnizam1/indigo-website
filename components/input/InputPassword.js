import { useState } from "react";
import { Col, Form } from "react-bootstrap";

const InputPassword = (props) => {
  const {
    title,
    extraTitle,
    placeholder,
    value,
    errorSign,
    isError,
    onChangeText,
  } = props;

  const [show, setShow] = useState(true);

  return (
    <div className="x-input-password mb-1">
      <Form.Label column sm={12} className="text-lg-normal p-0">
        {title} {extraTitle && (<span>{" " + extraTitle}</span>)}
      </Form.Label>

      <Col sm={12} className="p-0 mt-1 d-flex align-items-center position-relative">
        <Form.Control
          type={show ? "password" : "text"}
          value={value}
          placeholder={placeholder}
          onChange={onChangeText}
          className={isError ? "x-danger" : null}
        />
        <div
          className="x-icon"
          onClick={() => show ? setShow(false) : setShow(true)}
        >
          <span className={!isError ? "material-icons-outlined" : "material-icons-outlined x-danger-icon"}>
            {!show ? "visibility_off" : "visibility"}
          </span>
        </div>
      </Col>
      <Form.Label column sm={12} className="p-0 x-danger-text">{isError && errorSign}</Form.Label>
    </div>
  );
};

export default InputPassword;
