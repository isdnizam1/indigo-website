import React, { Fragment, useState } from "react";
import { Button, Col, Row } from "react-bootstrap";
import { isEmpty } from "lodash-es";
import CardUser from "/components/home/CardUser";
import classnames from "classnames";
import { postSendLike } from "/services/actions/api";
import { apiDispatch, getAuth } from "/services/utils/helper";
import { LoaderContent, LoaderComponent } from "components/loaders/Loaders";

const PostCard = ({
  image,
  banner,
  className,
  idTimeline,
  idUser,
  isAdminTenant,
  name,
  jobTitle,
  customInformation,
  customContent,
  city,
  postTimeAgo,
  title,
  totalView,
  totalLike,
  isLiked,
  totalComment,
  profession,
  HasHr,
  HideFooter,
  HideReshare,
  onCta,
  onCtaCustom,
  onCtaCard,
  onCtaReshare,
  onCtaComment,
  onCtaFooter,
  onCtaLike,
  isLoading,
}) => {
  const [isLikedPost, setIsLikedPost] = useState(isLiked);
  const [totalLikedPost, setTotalLikedPost] = useState(totalLike);

  const { id_user } = getAuth();

  const onLikeAction = () => {
    apiDispatch(postSendLike, {
      related_to: "id_timeline",
      id_related_to: idTimeline,
      id_user,
    }).then((response) => {
      const { react_status, total_like } = response;

      setTotalLikedPost(total_like);
      setIsLikedPost(react_status === "unlike" ? false : true);
    });
  };

  // const onLikeAction = () => {
  //   let total = totalLikedPost;
  //   total > 1 ? !isLikedPost ?
  //   setTotalLikedPost(!isLikedPost ? totalLikedPost++ : totalLikedPost--)
  //   setIsLikedPost(isLikedPost ? false : true);
  //   console.log({isLikedPost})
  //   return onCtaLike();
  // }

  return (
    <Col
      xl={12}
      className={classnames("x-postcard-section", className && className)}
    >
      <div className="x-postcard">
        <Row
          className={classnames(
            "w-100 m-0 pt-3 pb-3 flex-nowrap",
            onCtaCard && "cursor-pointer"
          )}
        >
          <Col
            sm={11}
            onClick={onCtaCard}
          >
            <LoaderComponent component={CardUser} isLoading={isLoading}>
              <CardUser
                image={image}
                id={idUser}
                isAdminTenant={isAdminTenant}
                name={name}
                jobTitle={jobTitle}
                customInformation={
                  customInformation ? (
                    customInformation
                  ) : (
                    <div className="d-flex align-items-center">
                      <p className="text-sm-light m-0 color-neutral-old-50 line-height-md">
                        {city}
                      </p>
                      <span
                        className="material-icons-round color-neutral-old-50 px-1 line-height-md"
                        style={{ fontSize: "8px" }}
                      >
                        fiber_manual_record
                      </span>
                      <p className="text-md-light m-0 color-neutral-old-40 line-height-md">
                        {postTimeAgo}
                      </p>
                    </div>
                  )
                }
              />
            </LoaderComponent>
          </Col>
          <Col sm={1} className="d-flex justify-content-end">
            {onCta && (
              <Button onClick={onCta} className="btn-primary">
                Join
              </Button>
            )}

            {onCtaCustom}
          </Col>
        </Row>

        <Row className="w-100 m-0">
          <Col md={12}>
            <LoaderContent
              width="80%"
              height={18}
              className="my-2"
              isLoading={isLoading}
            >
              <p className="text-lg-light text-start color-neutral-old-80 m-0">
                {title}
              </p>
            </LoaderContent>
            {banner && <img src={banner} className="x-banner" alt="" />}
            {customContent && customContent}
          </Col>
        </Row>

        {HasHr && <hr className="mb-0" />}

        {!isEmpty(profession) && (
          <LoaderContent
            width={100}
            height={24}
            count={3}
            className="my-3 ms-3"
            isLoading={isLoading}
          >
            <div className="x-tag-wrapper">
              {profession.map((item, idx) => (
                <div className="x-tag-label" key={idx}>
                  <p className="text-md-normal m-0 color-neutral-old-40">
                    {item.name ? item.name : item}
                  </p>
                </div>
              ))}
            </div>
          </LoaderContent>
        )}

        {!HideFooter && (
          <div
            className={classnames(
              "x-footer-wrapper",
              onCtaFooter && "cursor-pointer"
            )}
            onClick={onCtaFooter}
          >
            <LoaderContent
              width={90}
              height={20}
              count={2}
              className="ms-3"
              isLoading={isLoading}
            >
              <div className="w-100 d-flex justify-content-between m-0 ps-3 pe-3">
                <div className="d-flex align-items-center">
                  <div className="d-flex align-items-center cursor-pointer">
                    {totalView && (
                      <Fragment>
                        <span className="material-icons color-neutral-old-50">
                          visibility
                        </span>
                        <p className="text-md-light m-0 color-neutral-old-40">
                          {totalView}
                        </p>
                      </Fragment>
                    )}

                    {totalLike && (
                      <LoaderContent
                        width={40}
                        height={16}
                        count={1}
                        isLoading={isLoading}
                      >
                        <div
                          className="d-flex align-items-center cursor-pointer"
                          onClick={() => onLikeAction()}
                        >
                          <span
                            className={classnames(
                              "material-icons",
                              isLikedPost
                                ? "color-neutral-old-80"
                                : "color-neutral-old-50"
                            )}
                          >
                            thumb_up_alt
                          </span>
                          <p
                            className={classnames(
                              "text-md-light m-0",
                              isLikedPost
                                ? "color-neutral-old-80"
                                : "color-neutral-old-50"
                            )}
                          >
                            {totalLikedPost}
                          </p>
                        </div>
                      </LoaderContent>
                    )}
                  </div>

                  <LoaderContent
                    width={40}
                    height={16}
                    count={1}
                    isLoading={isLoading}
                  >
                    <div
                      className="d-flex align-items-center cursor-pointer ms-4"
                      onClick={onCtaComment}
                    >
                      <span className="material-icons color-neutral-old-50">
                        textsms
                      </span>
                      <p className="text-md-light m-0 color-neutral-old-40">{`${totalComment} Comments`}</p>
                    </div>
                  </LoaderContent>
                </div>

                {!HideReshare && (
                  <div
                    className="d-flex align-items-center cursor-pointer"
                    onClick={onCtaReshare}
                  >
                    <span className="x-icon-reshare" />
                    <p className="text-md-light m-0 color-neutral-old-40">
                      Reshare
                    </p>
                  </div>
                )}
              </div>
            </LoaderContent>
          </div>
        )}
      </div>
    </Col>
  );
};

export default PostCard;
