import React from 'react';
import { TransformWrapper, TransformComponent } from 'react-zoom-pan-pinch';

const ModalImgZoom = ({ img, hide }) => {
	return (
		<div className="x-wrapper-modal">
			<TransformWrapper
				initialScale={1}
				initialPositionX={0}
				initialPositionY={0}
			>
				{({ zoomIn, zoomOut, resetTransform, ...rest }) => (
					<>
						<div className="x-tools">
							<div className="x-button-tool" onClick={() => zoomIn()}>
								<div className="x-zoom-in"></div>
							</div>
							<div className="x-button-tool" onClick={() => zoomOut()}>
								<div className="x-zoom-out"></div>
							</div>
							<div className="x-button-tool" onClick={() => resetTransform()}>
								<div className="x-zoom-reset"></div>
							</div>
							<div className="x-button-tool" onClick={() => hide()}>
								<div className="x-close-modal-zoom"></div>
							</div>
						</div>
						<TransformComponent>
							<img src={img} alt="image-zoom" />
						</TransformComponent>
					</>
				)}
			</TransformWrapper>
		</div>
	);
};

export default ModalImgZoom;
