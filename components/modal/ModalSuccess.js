
import { Fragment } from 'react';
import { Container, Row, Col, Navbar, Nav, Form, Button, Modal } from 'react-bootstrap';

const ModalConfirmation = (props) => {
  const {
    show,
    onHide,
    backdrop,
    title,
    customTitle,
    subTitle,
    onSubmit,
    onSubmitText,
    onCancel,
    onCancelText,
    customFooter
  } = props
  return (
    <Modal show={show} onHide={onHide} backdrop={backdrop} centered>
      <Modal.Header>
        <Modal.Title>{title}</Modal.Title>
        {customTitle && (
          <Modal.Title>{(customTitle)}</Modal.Title>
        )}
      </Modal.Header>
      {subTitle && (<Modal.Body>{subTitle}</Modal.Body>)}
      {(onSubmit || onCancel || customFooter) && (
        <Modal.Footer>
        {onSubmit && (
          <Button className="btn-primary" onClick={onSubmit}>
            {onSubmitText}
          </Button>
        )}
        {onCancel && (
          <Button className="btn-cancel" onClick={onCancel}>
            {onCancelText}
          </Button>
        )}
        {customFooter && (customFooter)}
        </Modal.Footer>
      )}
    </Modal>
  );
}

export default ModalConfirmation;
