
import { Fragment } from 'react';
import { Container, Row, Col, Navbar, Nav, Form, Button, Modal } from 'react-bootstrap';

const ModalConfirmation = (props) => {
  const {
    show,
    onHide,
    backdrop,
    title,
    customTitle,
    subTitle,
  } = props
  return (
    <Modal show={show} onHide={onHide} backdrop={backdrop} centered>
      <Modal.Header>
        <Modal.Title>{title}</Modal.Title>
        {customTitle && (
          <Modal.Title>{(customTitle)}</Modal.Title>
        )}
      </Modal.Header>
      {subTitle && (<Modal.Body>{subTitle}</Modal.Body>)}
    </Modal>
  );
}

export default ModalConfirmation;
