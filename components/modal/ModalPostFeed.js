import React, { useState, Fragment, useEffect } from "react";
import { Row, Col, Button, Form } from "react-bootstrap";
import { postJourney, postShareTimeline } from "/services/actions/api";
import ModalConfirmation from "/components/modal/ModalConfirmation";
import classnames from "classnames";
import {
  getAuth,
  apiDispatch,
  convertToBase64,
  capitalizeFirstLetter,
} from "/services/utils/helper";
import { isEmpty } from "lodash-es";
import { MAPPER_FEEDS_EVENT_DATA } from "/services/constants/Constants";
import InputMention from "/components/input/InputMention";
import CardUser from "/components/home/CardUser";
import PostCard from "/components/cards/PostCard";
import ModalOnSubmit from "./ModalOnSubmit";

const ModalPostFeed = ({
  show,
  onHide,
  onShow,
  isAdminTenant,
  onEscapeKeyDown,
  feedReshareData = {},
  feedEditData = {},
  topics,
  role = "community",
  onSuccess,
}) => {
  const [modalPostSubmit, setModalPostSubmit] = useState(false);
  const [modalPostCancel, setModalPostCancel] = useState(false);

  const [isLoading, setIsLoading] = useState(false);

  const [participants, setParticipants] = useState([]);

  const { id_user, name_user, picture_user } = getAuth();

  const [selectedTopics, setSelectedTopics] = useState([]);
  const [selectedRole, setSelectedRole] = useState(role);
  const [description, setDescription] = useState("");
  const [selectedImages, setSelectedImages] = useState([]);

  const isEdit = !isEmpty(feedEditData);

  useEffect(() => {
    if(isEdit) {
      setDescription(feedEditData.description);
      !isEmpty(feedEditData.attachment) && feedEditData.attachment.map(item => {
        setSelectedImages(prev => [...prev, item.attachment_file])
      })
      setSelectedTopics([]);
      topics.map((item) => {
        feedEditData.topic_name.map(topic => {
          topic === item.topic_name && setSelectedTopics(prev => [...prev, item])
        })
      })
    }
  }, [feedEditData, feedReshareData]);

  const renderModalPostOnSubmit = () => {
    return (
      <ModalOnSubmit
        backdrop={isLoading ? "static" : true}
        size="md"
        show={modalPostSubmit}
        onHide={() => isLoading && setModalPostSubmit(false)}
        subTitle={
          !isEdit
            ? "Your post published"
            : `${capitalizeFirstLetter(feedEditData.full_name)}${feedEditData.full_name.slice(-1) !== "s" ? `'s` : ""} post edited`
        }
        isLoading={isLoading}
        ctaText="Back to feed"
        action="success"
        cta={() => setModalPostSubmit(false)}
      />
    );
  };

  const renderModalPostOnCancel = () => {
    return (
      <ModalConfirmation
        backdrop="static"
        size="md"
        show={modalPostCancel}
        onHide={() => {
          setModalPostCancel(false);
          onShow();
        }}
        modalClass="x-postfeeds-modalconfirmation"
        title={!isEdit ? "Buat Post" : "Edit Post"}
        subTitle={`Post yang sedang kamu ${
          !isEdit ? "buat" : "edit"
        } belum selesai. Lanjutkan ${!isEdit ? "membuat" : "edit"} post?`}
        customBody={
          <div className="d-flex justify-content-center gap-3 pt-2">
            <Button
              className="btn-delete w-auto"
              onClick={() => {
                setModalPostCancel(false);

                // initialize
                setDescription("");
                setSelectedImages([]);
                setSelectedRole(role);
                setSelectedTopics([]);
              }}
            >
              Batalkan
            </Button>
            <Button
              className="w-auto"
              onClick={() => {
                setModalPostCancel(false);
                onShow();
              }}
            >
              Lanjutkan
            </Button>
          </div>
        }
      />
    );
  };

  const onSelectImage = (e) => {
    const selectedFiles = e.target.files;
    const selectedFilesArray = Array.from(selectedFiles);

    selectedFilesArray.map((file) =>
      convertToBase64(file).then((result) =>
        setSelectedImages((images) => [...images, result])
      )
    );
  };

  const onSubmit = () => {
    onHide();
    setIsLoading(true);
    setModalPostSubmit(true);

    const idTopics = selectedTopics.map((item) => item.id_topic);

    const paramData = {
      feedPost: {
        id_user: isEdit ? feedEditData.id_user : id_user,
        id_topic: idTopics,
        role: selectedRole,
        description: description,
        image: selectedImages,
        id_journey: isEdit ? feedEditData.id_journey : "",
        action: isEdit ? "edit" : ""
      },
      feedReshare: {
        id_user,
        related_to: "id_journey", // string id timeline
        id: feedReshareData.id_journey, // value id timeline
        role: selectedRole, // circle/community
        description: description, // title
        id_topic: idTopics, // array, e.g ['21', '35']
      },
    };

    const api = !isEmpty(feedReshareData) ? postShareTimeline : postJourney;
    const params = !isEmpty(feedReshareData)
      ? paramData.feedReshare
      : paramData.feedPost;

    apiDispatch(api, params, true).then((response) => {
      if (response.code === 200) {
        setIsLoading(false);

        setSelectedTopics([]);
        setDescription("");
        setSelectedImages([]);

        onSuccess && onSuccess();
      }
    });
  };

  return (
    <Fragment>
      <ModalConfirmation
        backdrop={"static"}
        size="lg"
        show={show}
        onHide={() => {
          const isDirty =
            !isEmpty(selectedImages) ||
            !isEmpty(description) ||
            !isEmpty(selectedTopics);

          onHide();
          isDirty && setModalPostCancel(true);
        }}
        HasExitIcon
        modalClass="x-postfeeds-modal"
        onEscapeKeyDown={onEscapeKeyDown}
        customBody={
          <Fragment>
            <CardUser
              image={
                !isEdit
                  ? picture_user
                  : feedEditData.profile_picture
              }
              name={!isEdit ? name_user : feedEditData.full_name}
              isAdminTenant={
                !isEdit
                  ? isAdminTenant
                  : feedEditData.is_admin_tenant
              }
              customInformation={
                <Fragment>
                  <Form.Select
                    className="x-select-form"
                    value={selectedRole}
                    onChange={(e) => setSelectedRole(e.target.value)}
                  >
                    <option value="community">Community</option>
                    <option value="circle">Circle</option>
                  </Form.Select>
                </Fragment>
              }
            />
            <div className={classnames(isEmpty(feedReshareData) && "my-2")}>
              <InputMention
                className={classnames(
                  "x-input-description",
                  isEmpty(feedReshareData) ? "x-input-post" : "x-input-reshare"
                )}
                placeholder={
                  !isEmpty(feedReshareData)
                    ? "Give your comments..."
                    : "I am so excited right now !"
                }
                titleMention={"Peserta"}
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                dataMention={participants}
                renderSuggestion={(
                  entry,
                  search,
                  highlightedDisplay,
                  index,
                  focused
                ) =>
                  entry.id_user !== id_user && (
                    <div className="x-group-member">
                      {entry.profile_picture ? (
                        <img
                          src={entry.profile_picture}
                          className="x-image-account"
                          alt=""
                        />
                      ) : (
                        <div className="x-empty-image">
                          <p className="text-xl-normal m-0 color-base-10">
                            {entry.full_name?.slice(0, 1).toUpperCase()}
                          </p>
                        </div>
                      )}
                      <p className="m-0 ms-2 text-lg-bold color-neutral-80">
                        {entry.full_name}
                      </p>
                    </div>
                  )
                }
              />
            </div>

            {!isEmpty(feedReshareData) && (
              <Fragment>
                <PostCard
                  image={feedReshareData.profile_picture}
                  className="p-0 pb-2"
                  name={feedReshareData.full_name}
                  jobTitle={feedReshareData.job_title}
                  isAdminTenant={feedReshareData.is_admin_tenant}
                  customInformation={
                    <p className="text-sm-normal m-0 line-height-sm color-neutral-60 p-0">
                      {feedReshareData.created_at}
                    </p>
                  }
                  title={feedReshareData.description}
                  customContent={
                    <Fragment>
                      {!isEmpty(feedReshareData.attachment) && (
                        <div className="x-contentfeeds-wrapper">
                          <div className="mt-2">
                            <Row className="x-imageupload-row">
                              {feedReshareData.attachment.map((item, idx) => {
                                const length =
                                  feedReshareData.attachment.length;
                                const modulus = length % 3;
                                const maxLoop = !modulus || length > 5 ? 3 : 2;
                                const isEvenRow =
                                  length === 1 ||
                                  length === 2 ||
                                  length === 4 ||
                                  length === 5;
                                return (
                                  <Fragment key={idx}>
                                    {idx < maxLoop && (
                                      <Col className="p-1 d-flex">
                                        <img
                                          className={classnames(
                                            isEvenRow && "x-height-lg",
                                            length === 1 && "x-height-xl"
                                          )}
                                          style={{ borderRadius: 0 }}
                                          src={item.attachment_file}
                                          alt=""
                                        />
                                      </Col>
                                    )}
                                  </Fragment>
                                );
                              })}
                            </Row>
                            <Row className="x-imageupload-row">
                              {feedReshareData.attachment.length > 2 &&
                                feedReshareData.attachment.map((item, idx) => {
                                  const length =
                                    feedReshareData.attachment.length;
                                  const modulus = length % 3;
                                  const maxLoop =
                                    !modulus || length > 5 ? 3 : 2;
                                  return (
                                    <Fragment key={idx}>
                                      {idx >= maxLoop && idx < 6 && (
                                        <Col className="p-1 d-flex">
                                          <img
                                            className={classnames(
                                              length === 4 && "x-height-lg"
                                            )}
                                            style={{ borderRadius: 0 }}
                                            src={item.attachment_file}
                                            alt=""
                                          />
                                        </Col>
                                      )}
                                    </Fragment>
                                  );
                                })}
                            </Row>
                          </div>
                        </div>
                      )}
                      {!isEmpty(feedReshareData.data_ads) && (
                        <Fragment>
                          <div className="mx-2 mt-2">
                            <Row className="x-cardevent-component">
                              <Col lg={3} className="p-0">
                                <img
                                  style={{ height: "108px" }}
                                  src={feedReshareData.data_ads.image}
                                  alt=""
                                />
                              </Col>
                              <Col
                                lg={9}
                                className="d-flex flex-column justify-content-center align-items-start"
                              >
                                <p className="text-lg-normal color-semantic-main mb-1">
                                  Join{" "}
                                  {
                                    MAPPER_FEEDS_EVENT_DATA[
                                      feedReshareData.data_ads.category
                                    ]
                                  }
                                </p>
                                <p className="text-lg-bold color-neutral-100 text-start mb-1">
                                  {feedReshareData.data_ads.title}
                                </p>
                                <p className="text-md-normal color-neutral-70 m-0">
                                  {feedReshareData.data_ads.location.city_name}
                                </p>
                              </Col>
                            </Row>
                          </div>
                        </Fragment>
                      )}
                    </Fragment>
                  }
                  // profession={feedReshareData.topic_name}
                  HideFooter
                />
              </Fragment>
            )}

            {!isEmpty(selectedImages) && (
              <div className="mb-2">
                <Row className="x-imageupload-row">
                  {selectedImages.map((item, idx) => {
                    const length = selectedImages.length;
                    const modulus = length % 3;
                    const maxLoop = !modulus || length > 5 ? 3 : 2;
                    const isEvenRow =
                      length === 1 ||
                      length === 2 ||
                      length === 4 ||
                      length === 5;
                    return (
                      <Fragment key={idx}>
                        {idx < maxLoop && (
                          <Col className="p-1 d-flex">
                            <img
                              className={classnames(
                                isEvenRow && "x-height-lg",
                                length === 1 && "w-50"
                              )}
                              src={item}
                              onClick={() =>
                                setSelectedImages(
                                  selectedImages.filter(
                                    (base64image) => base64image !== item
                                  )
                                )
                              }
                              alt=""
                            />
                          </Col>
                        )}
                      </Fragment>
                    );
                  })}
                </Row>
                <Row className="x-imageupload-row">
                  {selectedImages.length > 2 &&
                    selectedImages.map((item, idx) => {
                      const length = selectedImages.length;
                      const modulus = length % 3;
                      const maxLoop = !modulus || length > 5 ? 3 : 2;
                      return (
                        <Fragment key={idx}>
                          {idx >= maxLoop && idx < 6 && (
                            <Col className="p-1 d-flex">
                              <img
                                className={classnames(
                                  length === 4 && "x-height-lg"
                                )}
                                src={item}
                                onClick={() =>
                                  setSelectedImages(
                                    selectedImages.filter((tag) => tag !== item)
                                  )
                                }
                                alt=""
                              />
                              {idx === 5 && length > 6 && (
                                <div
                                  className="x-countlimit-wrapper"
                                  onClick={() =>
                                    setSelectedImages(
                                      selectedImages.slice(0, -1)
                                    )
                                  }
                                >
                                  <div className="x-countlimit" />
                                  <h1 className="heading-xl-normal m-0 color-base-10 position-absolute">
                                    {`+${selectedImages.length - 6}`}
                                  </h1>
                                </div>
                              )}
                            </Col>
                          )}
                        </Fragment>
                      );
                    })}
                </Row>
              </div>
            )}

            {!isEmpty(selectedTopics) && (
              <div className="x-tag-wrapper mb-2">
                {selectedTopics.map((item, idx) => (
                  <div
                    className="x-tag-label"
                    key={idx}
                    onClick={() => {
                      setSelectedTopics(
                        selectedTopics.filter(
                          (topic) => topic.id_topic !== item.id_topic
                        )
                      );
                    }}
                  >
                    <p className="text-md-normal m-0 color-neutral-60">
                      {item.topic_name || item}
                    </p>
                  </div>
                ))}
              </div>
            )}
          </Fragment>
        }
        customFooter={
          <div className="d-flex justify-content-between align-items-center w-100 my-0">
            <div className="d-flex gap-3">
              <div className="d-flex justify align-items-center position-relative">
                <span
                  className={classnames(
                    "material-icons-round",
                    !isEmpty(feedReshareData)
                      ? "color-neutral-30"
                      : "color-semantic-main"
                  )}
                >
                  image
                </span>
                <p
                  className={classnames(
                    "m-0 ms-2 text-lg-normal cursor-pointer",
                    !isEmpty(feedReshareData)
                      ? "color-neutral-30"
                      : "color-semantic-main"
                  )}
                >
                  Image
                </p>
                <input
                  type="file"
                  name="Image"
                  onChange={onSelectImage}
                  onClick={(e) => e.stopPropagation()}
                  multiple
                  accept="image/png , image/jpeg , image/jpg"
                  disabled={
                    selectedImages.length > 6 || !isEmpty(feedReshareData)
                  }
                />
              </div>
              <div className="d-flex align-items-center color-semantic-main">
                <span className="material-icons-round">expand_more</span>
                <Form.Select
                  className="x-select-topic"
                  value={"Add topics"}
                  disabled={selectedTopics.length > 1}
                  onChange={(e) => {
                    const selected = JSON.parse(e.target.value);
                    setSelectedTopics((prev) => [...prev, selected]);
                  }}
                >
                  <option key="blankChoice" hidden value>
                    Add topics
                  </option>
                  {topics?.map((item, idx) => {
                    const existed = selectedTopics.filter(
                      (topic) => topic.topic_name === item.topic_name
                    );
                    if (!isEmpty(item.topic_name) && isEmpty(existed))
                      return (
                        <option value={JSON.stringify(item)} key={idx}>
                          {item.topic_name}
                        </option>
                      );
                  })}
                </Form.Select>
              </div>
            </div>

            <Button
              disabled={isEmpty(description) || isEmpty(selectedTopics)}
              onClick={() => onSubmit()}
              className="px-3 py-2 w-auto"
            >
              {!isEdit ? "Post" : "Edit"}
            </Button>
          </div>
        }
      />
      {renderModalPostOnSubmit()}
      {renderModalPostOnCancel()}
    </Fragment>
  );
};

export default ModalPostFeed;
