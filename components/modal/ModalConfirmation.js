import React from "react";
import { Button, Modal } from "react-bootstrap";

const ModalConfirmation = ({
  show,
  onHide,
  backdrop,
  modalClass,
  onEscapeKeyDown,
  title,
  HasExitIcon,
  customTitle,
  subTitle,
  onSubmit,
  onSubmitText,
  onCancel,
  onCancelText,
  customFooter,
  customBody,
  customExitIcon,
  size
}) => {
  return (
    <Modal
      className={modalClass}
      size={size}
      show={show}
      onHide={onHide}
      backdrop={backdrop}
      onEscapeKeyDown={onEscapeKeyDown}
      centered
    >
      {(customExitIcon || HasExitIcon) && (
        <div className="x-modalexit-icon" onClick={onHide}>
          {customExitIcon && customExitIcon}
          {HasExitIcon && (
            <span
              className="material-icons-round font-weight-bolder color-neutral-old-80"
              style={{ fontSize: "28px" }}
              onClick={onHide}
            >
              clear
            </span>
          )}
        </div>
      )}

      {(customTitle || title) && (
        <Modal.Header>
          {customTitle && customTitle}
          {title && <Modal.Title>{title}</Modal.Title>}
        </Modal.Header>
      )}
      {subTitle && <Modal.Body>{subTitle}</Modal.Body>}
      {customBody && <Modal.Body>{customBody}</Modal.Body>}
      {(onSubmit || onCancel || customFooter) && (
        <Modal.Footer>
          {onSubmit && (
            <Button className="btn-primary" onClick={onSubmit}>
              {onSubmitText}
            </Button>
          )}
          {onCancel && (
            <Button className="btn-cancel" onClick={onCancel}>
              {onCancelText}
            </Button>
          )}
          {customFooter && customFooter}
        </Modal.Footer>
      )}
    </Modal>
  );
};

export default ModalConfirmation;
