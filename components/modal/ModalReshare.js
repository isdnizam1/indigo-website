// import { Fragment } from "react";
import {
  Button,
  Modal,
  Form,
  Col,
  Row,
  Container,
  Spinner,
} from "react-bootstrap";
import ImageAccount from "/public/assets/images/img-account.png";
import InputText from "/components/input/InputText";
import classnames from "classnames";
import { getProfileDetail, postShareTimeline } from "/services/actions/api";
import { useEffect, useState } from "react";
import { Enhance } from "/services/Enhancer";
import { getAuth } from "/services/utils/helper";
import ModalSuccess from "/components/modal/ModalSuccess";
import ModalLoading from "/components/modal/ModalLoading";
import { useRouter } from "next/router";

const ModalReshare = (props) => {
  const { apiDispatch } = props;
  const { show, onHide, backdrop, dataReshare } = props;
  const { id_user } = getAuth();
  const [user, setUser] = useState({});
  const [role, setRole] = useState("community");
  const [description, setDescription] = useState("");
  const [relateTo, setRelateTo] = useState([]);
  const [idrelateto, setIdRelateTo] = useState([]);
  const [modalSuccessIsOpened, setmodalSuccessIsOpened] = useState(false);
  const [modalLoadingIsOpened, setmodalLoadingIsOpened] = useState(false);

  const reshareFeed = {
    id_user,
    related_to: relateTo,
    id: idrelateto,
    role: role,
    description: description,
  };

  const router = useRouter();

  const submitPostReshare = () => {
    apiDispatch(postShareTimeline, reshareFeed, true).then((response) => {
      if (response.code === 200) {
        reshareSuccess();
      }
    });
  };

  useEffect(() => {
    apiDispatch(getProfileDetail, { id_user }, true).then((response) => {
      if (response.code === 200) {
        const result = response.result;
        setUser(result);
      }
    });
  }, []);

  const isiDescrip = (e) => {
    setDescription(e.target.value);
  };

  const disablePost = description.length == 0;

  useEffect(() => {
    setRelateTo(dataReshare?.related_to);
    setIdRelateTo(dataReshare?.id_related_to);
  }, [dataReshare]);

  const expandSuccess = () => {
    setmodalSuccessIsOpened(modalSuccessIsOpened ? false : true);
  };
  const expandLoading = () => {
    setmodalLoadingIsOpened(modalLoadingIsOpened ? false : true);
  };

  const reshareSuccess = () => {
    setmodalLoadingIsOpened(false);
    expandSuccess();
  };
  return (
    <div className="modal">
      <Modal show={show} onHide={onHide} backdrop={backdrop} centered>
        <Modal.Header className="modal-header-post">
          <div className="d-flex align-items-center justify-content-start">
            <img
              src={
                !user.profile_picture ? ImageAccount.src : user.profile_picture
              }
              className="x-image-account"
              alt=""
            />
            <div className="x-name-account align-items-center">
              <div className="align-items-end">
                <h2 className="line-height-sm heading-sm-normal">
                  {user.full_name}
                </h2>
              </div>
              <div>
                <Form.Select
                  className="text-sm-normal x-role"
                  style={{
                    color: "var(--neutral-60)",
                    background: "var(--neutral-10)",
                  }}
                  value={role}
                  onChange={(e) => setRole(e.target.value)}
                >
                  <option value={"community"}>Community</option>
                  <option value={"circle"}>Circle</option>
                </Form.Select>
              </div>
            </div>

            <Button
              className="x-btn-close x-icon-close"
              onClick={onHide}
            ></Button>
          </div>
        </Modal.Header>
        <Modal.Body>
          <div className="x-input-post">
            <div className="x-input-caption-nonborder">
              <InputText
                as={"textarea"}
                rows={1}
                disableError={true}
                placeholder={"Give your comments.."}
                value={description}
                onChangeText={isiDescrip}
                style={{ border: "none" }}
              />
            </div>
            <Container className="p-0 x-reshare">
              <Row className="m-0 p-0 w-100  align-items-center">
                <Col md={2}>
                  {!dataReshare?.profile_picture ? (
                    <div className="x-empty-image">
                      <p
                        className="text-xl-normal m-0"
                        style={{ color: "var(--base-10)" }}
                      >
                        {dataReshare?.full_name.slice(0, 1).toUpperCase()}
                      </p>
                    </div>
                  ) : (
                    <img
                      src={dataReshare?.profile_picture}
                      className="x-image-reshare"
                      alt=""
                    />
                  )}
                </Col>
                <Col md={10} className="x-reshare-info">
                  <div className="d-flex">
                    <h2
                      className="line-height-md heading-sm-normal m-0"
                      style={{ color: "var(--neutral-100)" }}
                    >
                      {dataReshare?.full_name}
                    </h2>
                  </div>
                  <div className="d-flex">
                    <h3
                      className="text-sm-normal line-height-md m-0"
                      style={{ color: "var(--neutral-60)" }}
                    >
                      {dataReshare?.job_title}
                    </h3>
                  </div>
                  <div className="d-flex">
                    <h4
                      className="text-sm-normal line-height-md m-0"
                      style={{ color: "var(--neutral-60)" }}
                    >
                      {dataReshare?.created_at}
                    </h4>
                  </div>
                </Col>
              </Row>
              <Row className="m-0 p-0 w-100">
                <p
                  className="x-reshare-decsrip text-justify heading-sm-normal"
                  style={{ color: "var(--neutral-80)" }}
                >
                  {dataReshare?.description}
                </p>
              </Row>
            </Container>
            <div className="d-flex x-topic">
              {dataReshare?.topic_name?.map((item, index) => (
                <div
                  className="x-tag-topic me-2 cursor-pointer"
                  key={index}
                  //   onClick={() => handleDelete(item)}
                >
                  {item}
                </div>
              ))}
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer className="modal-footer-post">
          <div className="modal-footer-post-input">
            <div className="d-flex justify-content-start">
              <div className={classnames("d-flex align-items-center")}>
                <label className="d-flex align-items-center">
                  <span
                    className="material-icons-round"
                    style={{ color: "var(--primary-main)" }}
                  >
                    image
                  </span>
                  <p
                    className="m-0 ms-2 text-lg-bold cursor-pointer"
                    style={{ color: "var(--primary-main)" }}
                  >
                    Image
                  </p>
                </label>
              </div>
              <select
                disabled={true}
                // onChange={handleTopic}
                className="m-0 ms-3 x-select-topic cursor-pointer heading-sm-normal"
                // defaultValue={""}
                // style={{ color: "var(--primary-main)" }}
                // placeholder={"add topic"}
              >
                <option>Add Topics</option>
              </select>
              <div className="ms-auto">
                <Button
                  disabled={disablePost}
                  onClick={() => {
                    submitPostReshare();
                    expandLoading();
                  }}
                  className="x-button-post"
                >
                  Post
                </Button>
              </div>
            </div>
          </div>
        </Modal.Footer>
      </Modal>
      <ModalLoading
        backdrop={"static"}
        show={modalLoadingIsOpened}
        onHide={() => setmodalLoadingIsOpened(false)}
        customTitle={
          <>
            <Spinner
              animation="border"
              variant="primary"
              style={{ width: "100px", height: "100px" }}
            />

            <p
              className="pt-1 mt-2 text-lg-normal"
              style={{ color: "var(--neutral-80)" }}
            >
              Please wait a minute...
            </p>
          </>
        }
      />
      <ModalSuccess
        backdrop={"static"}
        show={modalSuccessIsOpened}
        onHide={() => setmodalSuccessIsOpened(false)}
        customTitle={
          <>
            <div className="x-icon-post" />

            <p
              className="pt-1 mt-2 text-lg-normal"
              style={{ color: "var(--neutral-80)" }}
            >
              Your Post Published !
            </p>

            <Button
              className="btn-primary m-2"
              onClick={() => {
                setmodalSuccessIsOpened(false);
                router.pathname === "/feeds/detail"
                  ? router.push("/feeds")
                  : window.location.reload(false);
              }}
            >
              Back to feed
            </Button>
          </>
        }
      />
    </div>
  );
};

export default Enhance(ModalReshare);
