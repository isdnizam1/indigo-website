// import { Fragment } from "react";
import {
  Button,
  Modal,
  Form,
  Col,
  Row,
  Container,
  Spinner,
} from "react-bootstrap";
import ImageAccount from "/public/assets/images/img-account.png";
import InputText from "/components/input/InputText";
import classnames from "classnames";
import {
  getProfileDetail,
  postShareTimeline,
  getListTopics,
  getGenreInterest,
} from "/services/actions/api";
import { useEffect, useState } from "react";
import { Enhance } from "/services/Enhancer";
import { getAuth } from "/services/utils/helper";
import ModalSuccess from "/components/modal/ModalSuccess";
import ModalLoading from "/components/modal/ModalLoading";
import { useRouter } from "next/router";
import InputSelect from "/components/input/InputSelect";
import { isEmpty } from "lodash-es";

const ModalShare = (props) => {
  const { apiDispatch } = props;
  const { show, onHide, backdrop, dataShare } = props;
  const { id_user } = getAuth();
  const [user, setUser] = useState({});
  const [role, setRole] = useState("community");
  const [description, setDescription] = useState("");
  const [relateTo, setRelateTo] = useState([]);
  const [idrelateto, setIdRelateTo] = useState([]);
  const [modalSuccessIsOpened, setmodalSuccessIsOpened] = useState(false);
  const [modalLoadingIsOpened, setmodalLoadingIsOpened] = useState(false);
  const [allInterests, setAllInterests] = useState([]);
  const [suggestionInterests, setSuggestionInterests] = useState([]);
  const [interestKeyword, setInterestKeyword] = useState("");
  const [selectedInterest, setSelectedInterest] = useState([]);

  const reshareFeed = {
    id: idrelateto,
    related_to: relateTo,
    id_user,
    description: description,
    id_topic: selectedInterest.map((item) => item.id_topic),
  };

  const router = useRouter();
  useEffect(() => getData(), []);

  const submitPostReshare = () => {
    apiDispatch(postShareTimeline, reshareFeed, true).then((response) => {
      if (response.code === 200) {
        onHide(true);
        reshareSuccess();
      }
    });
  };

  const getData = () => {
    apiDispatch(getListTopics, { show: true }, true).then((response) => {
      if (response.code === 200) {
        const data = response.result;
        setAllInterests(data);
      }
    });
  };

  const handleInterestChange = (keyword) => {
    let datas = allInterests.filter((data) => {
      return data.topic_name?.toLowerCase().includes(keyword.toLowerCase());
    });

    setInterestKeyword(keyword);
    setSuggestionInterests(!isEmpty(keyword) ? datas.slice(0, 10) : []);
  };

  useEffect(() => {
    apiDispatch(getProfileDetail, { id_user }, true).then((response) => {
      if (response.code === 200) {
        const result = response.result;
        setUser(result);
      }
    });
  }, []);

  const isiDescrip = (e) => {
    setDescription(e.target.value);
  };

  const disablePost = description.length == 0;

  useEffect(() => {
    setRelateTo("id_ads");
    setIdRelateTo(dataShare?.id_ads);
  }, [dataShare]);

  const expandSuccess = () => {
    setmodalSuccessIsOpened(modalSuccessIsOpened ? false : true);
  };
  const expandLoading = () => {
    setmodalLoadingIsOpened(modalLoadingIsOpened ? false : true);
  };

  const reshareSuccess = () => {
    setmodalLoadingIsOpened(false);
    expandSuccess();
  };

  return (
    <div className="modal m-0">
      <Modal show={show} onHide={onHide} backdrop={backdrop} centered>
        <Modal.Header className="modal-header-post">
          <div className="d-flex align-items-center justify-content-start">
            <img
              src={
                !user.profile_picture ? ImageAccount.src : user.profile_picture
              }
              className="x-image-account"
              alt=""
            />
            <div className="x-name-account align-items-center">
              <div className="align-items-end">
                <h2 className="line-height-sm heading-sm-normal">
                  {user.full_name}
                </h2>
              </div>
              <div>
                <Form.Select
                  className="text-sm-normal x-role"
                  style={{
                    color: "var(--neutral-60)",
                    background: "var(--neutral-10)",
                  }}
                  value={role}
                  onChange={(e) => setRole(e.target.value)}
                >
                  <option value={"community"}>Community</option>
                  <option value={"circle"}>Circle</option>
                </Form.Select>
              </div>
            </div>

            <Button
              className="x-btn-close x-icon-close"
              onClick={onHide}
            ></Button>
          </div>
        </Modal.Header>
        <Modal.Body>
          <div className="x-input-share">
            <div className="x-input-caption-nonborder">
              <InputText
                as={"textarea"}
                rows={1}
                disableError={true}
                placeholder={"Give your comments.."}
                value={description}
                onChangeText={isiDescrip}
                style={{ border: "none" }}
              />
            </div>
            <Container className="p-0 x-share">
              <Row className="m-0  w-100 x-share-decsrip">
                <p
                  className=" text-justify text-sm-normal m-0"
                  style={{ color: "var(--primary-main)" }}
                >
                  Join Collaboration
                </p>
                <p
                  className=" text-justify heading-sm-normal m-0"
                  style={{ color: "var(--body-60)" }}
                >
                  {dataShare?.title}
                </p>
                <p
                  className=" text-justify text-sm-normal heading-sm-normal m-0"
                  style={{ color: "var(--grey-4)" }}
                >
                  {dataShare?.city_name}
                </p>
              </Row>
            </Container>

            <div className="d-flex align-items-center flex-wrap w-100 pt-1">
              {selectedInterest.map((item, idx) => (
                <div className="x-tag-interest" key={idx}>
                  <p
                    className="text-sm-normal m-0 line-height-sm p-0"
                    style={{ color: "var(--primary-main)" }}
                  >
                    {item.topic_name}
                  </p>
                  <span
                    className="material-icons-round"
                    onClick={() => {
                      setSelectedInterest(
                        selectedInterest.filter((data) => item !== data)
                      );
                    }}
                  >
                    close
                  </span>
                </div>
              ))}
            </div>
            <InputSelect
              type="text"
              value={interestKeyword}
              placeholder="Tambahkan interest"
              data={suggestionInterests}
              disableInput={selectedInterest.length > 1}
              selectedValue={""}
              disableError={true}
              icon={"add"}
              onFocus={() => setSuggestionInterests(allInterests.slice(0, 6))}
              onKeyPress={(event) => {
                if (event.key === "Enter") {
                  setInterestKeyword("");
                  setSuggestionInterests([]);
                  setSelectedInterest([
                    ...selectedInterest,
                    event.target.value,
                  ]);
                }
              }}
              onBlur={() => {
                setTimeout(() => {
                  setInterestKeyword("");
                  setSuggestionInterests([]);
                }, 300);
              }}
              onChangeText={(event) => handleInterestChange(event.target.value)}
              suggestionText={"topic_name"}
              suggestionOnClick={(interest) => {
                setInterestKeyword("");
                setSuggestionInterests([]);
                setSelectedInterest([...selectedInterest, interest]);
              }}
            />
          </div>
        </Modal.Body>
        <Modal.Footer className="modal-footer-post">
          <div className="modal-footer-post-input">
            <div className="d-flex justify-content-start">
              {/* <div className="d-flex align-items-center ms-3">
                <span
                  className="material-icons-round pb-1"
                  style={{ color: "var(--primary-main)" }}
                >
                  expand_more
                </span>
                <select
                  disabled={handleDisable}
                  onChange={handleTopic}
                  className="x-select-topic"
                  value={topic.topic_name}
                  style={{ color: "var(--primary-main)" }}
                  placeholder={"add topic"}
                >
                  {topic.length > 0 &&
                    topic.map((item, idx) => (
                      <option value={JSON.stringify(item)} key={idx}>
                        {item.topic_name}
                      </option>
                    ))}
                </select>
              </div> */}
              <div className="ms-auto">
                <Button
                  disabled={disablePost}
                  onClick={() => {
                    submitPostReshare();

                    expandLoading();
                  }}
                  className="x-button-post"
                >
                  Post
                </Button>
              </div>
            </div>
          </div>
        </Modal.Footer>
      </Modal>
      <ModalLoading
        backdrop={"static"}
        show={modalLoadingIsOpened}
        onHide={() => setmodalLoadingIsOpened(false)}
        customTitle={
          <>
            <Spinner
              animation="border"
              variant="primary"
              style={{ width: "100px", height: "100px" }}
            />

            <p
              className="pt-1 mt-2 text-lg-normal"
              style={{ color: "var(--neutral-80)" }}
            >
              Please wait a minute...
            </p>
          </>
        }
      />
      <ModalSuccess
        backdrop={"static"}
        show={modalSuccessIsOpened}
        onHide={() => setmodalSuccessIsOpened(false)}
        customTitle={
          <>
            <div className="x-icon-post" />

            <p
              className="pt-1 mt-2 text-lg-normal"
              style={{ color: "var(--neutral-80)" }}
            >
              Your Post Published !
            </p>

            <Button
              className="btn-primary m-2"
              onClick={() => {
                setmodalSuccessIsOpened(false);
                router.pathname === "/feeds/detail"
                  ? router.push("/feeds")
                  : window.location.reload(false);
              }}
            >
              Back to feed
            </Button>
          </>
        }
      />
    </div>
  );
};

export default Enhance(ModalShare);
