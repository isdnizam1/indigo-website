/* eslint-disable no-useless-escape */
import React from "react";
import {
  postCheckEmail,
  postLogin,
} from "/services/actions/api";
import { useEffect, useState } from "react";
import { Modal } from "react-bootstrap";
import { useRouter } from "next/router";
import {
  isEmailValid,
  setAuth,
} from "/services/utils/helper";
import { isNil } from "lodash-es";
import LoginForm from "/components/authentication/LoginForm";
import { useDispatch } from "react-redux";
import {
  setRegisterBehaviour,
  setRegisterData,
} from "/redux/slices/registerSlice";

const ModalLogin = ({
  show,
  onHide
}) => {
  const { query, push, reload, pathname } = useRouter();
  const { id: id_ads } = query;

  const [state, setState] = useState({
    email: "",
    password: "",
    emailIsError: false,
    passwordIsError: false,
  });

  const { email, password, emailIsError, passwordIsError } = state;

  const dispatch = useDispatch();

  const [submitDisabled, setSubmitDisabled] = useState(true);

  useEffect(() => {
    if (isEmailValid(email) && password) {
      password.length > 6 && setSubmitDisabled(false);
    } else setSubmitDisabled(true);
  }, [email, password]);

  const emailOnChangeValue = (event) => {
    const email = event.target.value;
    setState({ ...state, email, emailIsError: false, passwordIsError: false });
  };

  const passwordOnChangeValue = (event) => {
    const password = event.target.value;
    setState({
      ...state,
      password,
      passwordIsError: false,
      emailIsError: false,
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    const { email, password } = state;

    if (!isEmailValid(email)) setState({ ...state, emailIsError: true });
    else {
      postLogin({ email, password }).then(async (response) => {
        const { status, result } = response.data;
        if (status === "failed") {
          postCheckEmail({ email }).then(({ data: { status } }) => {
            if (status === "success") {
              setState({ ...state, passwordIsError: true });
            } else {
              setState({ ...state, emailIsError: true });
            }
          });
        } else {
          const {
            email,
            company,
            full_name,
            gender,
            id_city,
            id_user,
            job_title,
            profile_picture,
            interest,
            location,
            event_title,
            profile_type,
            referral_code,
            registered_via,
            registration_step,
            token,
          } = result;

          setAuth({
            id_user,
            email,
            registration_step,
            event_title,
            token,
            picture_user: profile_picture,
            name_user: full_name,
          }).then((res) => {
            if (registration_step !== "finish") {
              dispatch(
                setRegisterData({
                  company,
                  email: !isNil(email) ? email : "",
                  gender,
                  id_city,
                  id_user,
                  city_name: !isNil(location) ? location.city_name : "",
                  city_keyword: !isNil(location) ? location.city_name : "",
                  interest: !isNil(interest) ? interest : [],
                  job_title,
                  job_title_keyword: job_title,
                  full_name: !isNil(full_name) ? full_name : "",
                  password,
                  profile_picture,
                  profile_type,
                  referral_code: !isNil(referral_code) ? referral_code : "",
                  registered_via,
                })
              );

              dispatch(setRegisterBehaviour({ step: registration_step }));
            }
            reload();

            push({ pathname: `/home/agenda/detail${pathname.includes("registration") ? "/registration" : ""}`, query: { id: id_ads } });
          });
        }
      });
    }
  };

  return (
    <Modal
      className={"x-modallogin-component"}
      size="md"
      show={show}
      onHide={onHide}
      centered
    >
      <LoginForm
        onSubmit={onSubmit}
        onEmailInput={emailOnChangeValue.bind(this)}
        onPasswordInput={passwordOnChangeValue.bind(this)}
        isButtonDisable={submitDisabled}
        emailIsError={emailIsError}
        passwordIsError={passwordIsError}
      />
    </Modal>
  );
};

export default ModalLogin;
