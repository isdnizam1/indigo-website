// import { Fragment } from "react";
import { Button, Modal, Col, Row, Container } from "react-bootstrap";
import BGImage from "/public/assets/images/img-bg-detail-speaker.svg";
import classnames from "classnames";
import { getProfileDetail, getAdsDetail } from "/services/actions/api";
import { useEffect, useState } from "react";
import { Enhance } from "/services/Enhancer";
import { getAuth } from "/services/utils/helper";

const ModalReshare = (props) => {
  const { apiDispatch } = props;
  const { show, onHide, backdrop, onClick, isMobile, dataDetail } = props;
  const { id_user } = getAuth();
  const [user, setUser] = useState({});
  const [dataSpeaker, setDataSpeaker] = useState([]);

  //   const submitPostReshare = () => {
  //     apiDispatch(postShareTimeline, reshareFeed, true).then((response) => {
  //       if (response.code === 200) {
  //         reshareSuccess();
  //         console.log(response);
  //       }
  //     });
  //     console.log("hasil ambil data", reshareFeed);
  //   };

  useEffect(() => {
    apiDispatch(getProfileDetail, { id_user }, true).then((response) => {
      if (response.code === 200) {
        const result = response.result;
        setUser(result);
      }
    });
  }, []);

  useEffect(() => {
    apiDispatch(getAdsDetail, { id_user, id_ads: dataDetail }, true).then(
      (response) => {
        if (response.code === 200) {
          const result = response.result;
          setDataSpeaker(result);
        }
      }
    );
  }, [dataDetail]);

  return (
    <div className="modal">
      <Modal
        show={show}
        onHide={onHide}
        backdrop={backdrop}
        centered
        dialogClassName="my-modal"
      >
        <Modal.Header className="modal-header-speaker p-0 ">
          <div className="d-flex align-items-center">
            <Col md={11} className="p-0 m-0">
              <div className="d-flex justify-content-center align-items-center heading-sm-light">
                <p>Speaker Profile</p>
              </div>
            </Col>
            <Col md={1} className="p-0 m-0">
              <Button className="x-icon-closedetail" onClick={onHide}></Button>
            </Col>
          </div>
        </Modal.Header>
        <Modal.Body>
          <div className="x-detail">
            <Col className="p-0 m-0">
              <div className="x-bg-image">
                <img src={BGImage} />
              </div>
            </Col>
            {/* <Container className="p-0"> */}
            <Row className="m-0 p-0 w-100">
              <Container className="d-flex justify-content-center w-100">
                <div>
                  <Col md={12} className="x-image-speaker">
                    <img src={dataSpeaker.image}></img>
                  </Col>
                  <Row className="m-0 pt-4 w-100">
                    <Col md={12}>
                      <div className="pb-1">
                        <h3
                          className={classnames(
                            "line-height-sm",
                            isMobile ? "text-sm-normal" : "heading-md-bold"
                          )}
                          style={{ color: "var(--neutral-100)" }}
                        >
                          {dataSpeaker.title}
                        </h3>
                      </div>
                      <div className="pb-4">
                        <h3
                          className="text-sm-normal line-height-sm"
                          style={{ color: "var(--neutral-50)" }}
                        >
                          {dataSpeaker?.additional_data_arr?.profession}
                        </h3>
                      </div>
                    </Col>
                  </Row>
                </div>
              </Container>
            </Row>
            <Row className="p-0 m-0 w-100 ">
              <div>
                <p className="heading-sm-bold" style={{ color: "#215C7A" }}>
                  Sesi Bersama {dataSpeaker.title} :{" "}
                </p>
              </div>
            </Row>
            <Row className="m-0 w-100 d-flex justify-content-center">
              <Col md={12} className="p-0 m-0">
                {dataSpeaker?.data_session?.map((items) => {
                  return (
                    <div className="x-session-card ms-2 me-2 mb-3">
                      <Row className="w-100 m-0 align-items-start">
                        <Col md={3} className="p-0 m-0">
                          <div className="x-session-img">
                            <img
                              src={items.image}
                              // className="x-image cursor-pointer"
                              // style={{width: "100px", height: "100px", border: "10px"}}
                              // alt=""
                            />
                          </div>
                        </Col>
                        <Col md={9} className="p-0 m-0">
                          <div className="">
                            <p
                              className={classnames(
                                "x-session-title m-0 d-flex",
                                isMobile ? "text-sm-normal" : "heading-sm-bold"
                              )}
                              style={{ color: "var(--neutral-100" }}
                            >
                              {items.title}
                            </p>
                            <div
                              className={classnames(
                                "d-flex align-items-center cursor-pointer mt-1 mb-4"
                              )}
                            >
                              {items.speaker?.map((items, i) => {
                                return (
                                  <p
                                    className={classnames(
                                      "m-0 ms-2",
                                      isMobile
                                        ? "text-sm-normal"
                                        : "heading-sm-normal"
                                    )}
                                    style={{ color: "var(--neutral-50)" }}
                                    key={i}
                                  >
                                    {items.title}
                                    {/* {i !== items.speaker.length - 1 ? "," : ""} */}
                                  </p>
                                );
                              })}
                            </div>
                            <div className="d-flex ms-2 mt-3">
                              <div>
                                <Button className="x-button-join">
                                  <p className="heading-sm-normal p-0 m-0">
                                    {" "}
                                    Join{" "}
                                  </p>
                                </Button>
                              </div>
                            </div>
                          </div>
                        </Col>
                      </Row>
                    </div>
                  );
                })}
              </Col>
            </Row>
            {/* </Container> */}
          </div>
        </Modal.Body>
        <Modal.Footer className="modal-footer-detail">
          <div className="x-about-speaker">
            <p className="heading-sm-bold" style={{ color: "#215C7A" }}>
              Tentang {dataSpeaker.title} :
            </p>
            <Row className="p-0 m-0 w-100">
              <Col md={12} className="p-0 m-0">
                <div
                  className="text-md-light"
                  style={{ color: "var(--neutral-50)" }}
                >
                  {" "}
                  {dataSpeaker.description}
                </div>
              </Col>
            </Row>
          </div>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default Enhance(ModalReshare);
