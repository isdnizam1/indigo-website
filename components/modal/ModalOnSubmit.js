import React, { Fragment } from 'react';
import { Button, Spinner } from 'react-bootstrap';
import ModalConfirmation from './ModalConfirmation';
import classnames from "classnames";

const ModalOnSubmit = ({
  backdrop,
  size,
  className,
  show,
  onHide,
  title,
  subTitle,
  loadingTitle,
  loadingSubTitle,
  isLoading,
  action = "success", // ("success", "delete")
  ctaText = "Continue",
  cta,
  customBody
}) => {
  return (
    <ModalConfirmation
      backdrop={backdrop}
      size={size ?? "md"}
      show={show}
      modalClass={classnames("x-modal-onsubmit", className && className)}
      onEscapeKeyDown={onHide}
      onHide={onHide}
      customTitle={
        <Fragment>
          {isLoading ? (
            <Spinner animation="border" className="x-spinner-loading" />
          ) : (
            <div
              className={classnames(
                "x-success",
                action === "success"
                  ? "x-success-image"
                  : "x-delete-image"
              )}
            />
          )}
        </Fragment>
      }
      title={
        isLoading
          ? loadingTitle
          : title
      }
      subTitle={
        isLoading
          ? loadingSubTitle ?? "Please wait a minute..."
          : subTitle
      }
      customBody={
        <Fragment>
          {customBody ? customBody : cta && !isLoading && (
            <div className="d-flex justify-content-center">
              <Button
                className="w-auto py-2 px-3 mt-2"
                onClick={!cta ? onHide : cta}
              >
                {ctaText}
              </Button>
            </div>
          )}
        </Fragment>
      }
    />
  );
}

export default ModalOnSubmit;
