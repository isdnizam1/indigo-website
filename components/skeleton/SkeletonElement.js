import React from "react";
import classnames from "classnames";

const SkeletonElement = ({
  className,
  type,
  count,
  md,
  width,
  height,
  radius,
  color,
}) =>
  Array(count)
    .fill(0)
    .map((item, idx) => (
      <div
        className={classnames(className && className, md && `col-md-${md}`)}
        key={idx}
      >
        <div
          style={{
            width,
            height,
            borderRadius: radius,
            backgroundColor: color,
          }}
          className={classnames("x-skeleton loading", type && type)}
        />
      </div>
    ));

export default SkeletonElement;
