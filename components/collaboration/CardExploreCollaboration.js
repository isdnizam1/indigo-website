import React from "react";
import { Badge, Card } from "react-bootstrap";
import CardUser from "/components/home/CardUser";
import classnames from "classnames";
import { LoaderContent } from "/components/loaders/Loaders";

const CardExploreCollaboration = ({
  status,
  image,
  title,
  professions,
  profilePicture,
  name,
  jobTitle,
  city,
  totalView,
  timeAgo,
  onClick,
  onComment,
  onShare,
  isLoading,
}) => {
  const BADGE_CLASSNAME_STATUS = {
    "In progress": "x-badge-progress",
    active: "x-badge-active",
  };
  return (
    <Card className="x-cardexplorecollaboration-component">
      <LoaderContent isLoading={isLoading}>
        <Badge className={`x-badge ${BADGE_CLASSNAME_STATUS[status]}`}>
          {status}
        </Badge>
      </LoaderContent>
      <LoaderContent width="100%" height={185} isLoading={isLoading}>
        <Card.Img
          variant="top"
          src={image}
          className={classnames("x-card-image", onClick && "cursor-pointer")}
          onClick={onClick}
        />
      </LoaderContent>
      <Card.Body className="x-card-body">
        <div
          onClick={onClick}
          className={classnames(onClick && "cursor-pointer")}
        >
          <LoaderContent width="80%" height={12} isLoading={isLoading}>
            <p className="text-sm-bold color-neutral-80 m-0">{title}</p>
          </LoaderContent>

          <div className="d-flex gap-2 flex-wrap my-2">
            <LoaderContent width={80} height={20} isLoading={isLoading}>
              {professions?.map((interest, index) => (
                <p className="x-interest-tag" key={index}>
                  {interest.name}
                </p>
              ))}
            </LoaderContent>
          </div>

          <CardUser
            image={profilePicture}
            name={name}
            jobTitle={jobTitle}
            city={city}
            isLoading={isLoading}
          />
        </div>

        <LoaderContent width="30%" height={12} isLoading={isLoading}>
          <div className="d-flex align-items-center justify-content-between mt-1">
            <p className="text-sm-normal color-grey-4 m-0">{timeAgo}</p>

            <div className="d-flex align-items-center gap-3">
              <div className="d-flex align-items-center">
                <span
                  className="material-icons color-grey-4"
                  style={{
                    fontSize: "16px",
                  }}
                >
                  visibility
                </span>
                <p className="text-sm-light color-neutral-60 m-0 ps-1">
                  {totalView}
                </p>
              </div>

              <span
                className="material-icons color-grey-4 cursor-pointer"
                style={{
                  fontSize: "16px",
                }}
                onClick={onComment}
              >
                comment
              </span>

              <span
                className="material-icons color-grey-4 cursor-pointer"
                style={{
                  fontSize: "16px",
                }}
                onClick={onShare}
              >
                share
              </span>
            </div>
          </div>
        </LoaderContent>
      </Card.Body>
    </Card>
  );
};

export default CardExploreCollaboration;
