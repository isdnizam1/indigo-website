import React, { Fragment } from "react";
import { Button, Col, Form, Row } from "react-bootstrap";
import InputText from "/components/input/InputText";
import ModalConfirmation from "/components/modal/ModalConfirmation";

const ModalFilterDetail = ({
  show,
  title,
  data,
  dataTitleReferred,
  dataIdReferred,
  popularData,
  popularTitle,
  filteredData,
  onFilterSearch,
  onHide,
  onAddData,
  onReset,
  onSubmit,
}) => {
  return (
    <ModalConfirmation
      modalClass="x-filterdetail-collaboration"
      backdrop={"static"}
      show={show}
      size={"lg"}
      onHide={onHide}
      customTitle={
        <div className="x-modal modal-lg">
          <div className="d-flex align-items-center pb-2">
            <h1
              className="heading-md-bold line-height-lg me-auto m-0"
              style={{ color: "var(--neutral-80)" }}
            >
              {title}
            </h1>
            <InputText
              disableError={true}
              placeholder={"Find " + title}
              inlineIconSmall={"search"}
              style={{ width: "100%", height: "40px", margin: "0 12px" }}
              onChangeText={onFilterSearch}
            />
            <span
              className="material-icons-round cursor-pointer"
              style={{ fontWeight: "bolder", color: "var(--primary-main)" }}
              onClick={onHide}
            >
              clear
            </span>
          </div>
          <div className="x-separator mt-1 mb-2" />
          <div className="x-list-city ps-4 pe-4 text-sm-light align-items-center">
            <div className="x-city-word d-flex mt-1">
              <p className="m-0 text-md-normal">{popularTitle}</p>
            </div>
            <Row>
              {popularData?.map((item, idx) => (
                <Col md={4} className="align-items-center" key={idx}>
                  <Form.Check
                    onChange={onAddData}
                    name={"group" + idx}
                    type={"checkbox"}
                    id={"city" + idx}
                    label={item?.[dataTitleReferred]}
                    value={JSON.stringify(item)}
                    checked={filteredData.some((user) =>
                      user?.[dataIdReferred] === item?.[dataIdReferred]
                        ? true
                        : false
                    )}
                  />
                </Col>
              ))}

              {data?.map((item, idx) => (
                <Fragment key={idx}>
                  {data[-1 + idx]?.[dataTitleReferred]?.slice(0, 1) !==
                    data[idx]?.[dataTitleReferred]?.slice(0, 1) && (
                    <div className="x-city-word d-flex mt-1">
                      <p className="m-0 text-md-normal">
                        {data[idx]?.[dataTitleReferred]?.slice(0, 1)}
                      </p>
                    </div>
                  )}

                  <Col md={4} className="align-items-center">
                    <Form.Check
                      name="group1"
                      type={"checkbox"}
                      onChange={onAddData}
                      id={"UI/UX"}
                      checked={filteredData.some((user) =>
                        user?.[dataIdReferred] === item?.[dataIdReferred]
                          ? true
                          : false
                      )}
                      label={item?.[dataTitleReferred]}
                      value={JSON.stringify(item)}
                    />
                  </Col>
                </Fragment>
              ))}
            </Row>
          </div>
        </div>
      }
      customFooter={
        <div className="d-flex x-footer2 align-items-center">
          <div className="ms-auto">
            <div className="d-flex align-items-center">
              <Button className="btn-cancel m-2 mb-0" onClick={onReset}>
                Reset
              </Button>
              <Button className="btn-primary m-2 mb-0" onClick={onSubmit}>
                Apply
              </Button>
            </div>
          </div>
        </div>
      }
    />
  );
};

export default ModalFilterDetail;
