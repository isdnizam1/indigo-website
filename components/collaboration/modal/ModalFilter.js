import React from "react";
import { Button } from "react-bootstrap";
import ModalConfirmation from "/components/modal/ModalConfirmation";

const ModalFilter = ({
  show,
  cities,
  professions,
  onHide,
  onSeeAllCities,
  onSeeAllProfessions,
  onReset,
  onSubmit,
}) => {
  return (
    <ModalConfirmation
      modalClass="x-modalfilter-collaboration"
      show={show}
      size={"lg"}
      onHide={onHide}
      customTitle={
        <div className="x-modal w-100">
          <div className="d-flex align-items-center" style={{ width: "700px" }}>
            <h1
              className="heading-md-bold line-height-lg me-auto m-0 ps-0 pe-0 me-5 color-neutral-80"
            >
              Filter
            </h1>
            <span
              className="material-icons-round cursor-pointer color-neutral-old-80 font-weight-bolder"
              onClick={onHide}
            >
              clear
            </span>
          </div>
          <div className="x-separator mt-2 mb-2 ps-0 pe-0 ms-0 me-0" />
          <div className="d-flex align-items-center">
            <h1 className="text-md-normal m-0">City</h1>
            <h1
              className="text-md-normal m-0 ms-auto cursor-pointer color-neutral-old-80"
              onClick={onSeeAllCities}
            >
              See All
            </h1>
          </div>
          <div className="d-flex align-items-center flex-wrap mt-2">
            <div className="x-prof d-flex align-items-center">
              {cities?.map((item, idx) => {
                return (
                  <p
                    className="x-top-prof text-sm-light m-0 me-2 color-base-10"
                    key={idx}
                    style={{
                      backgroundColor: "var(--primary-main)",
                    }}
                  >
                    {item.city_name}
                  </p>
                );
              })}
            </div>
          </div>
          <div className="x-separator mt-2 mb-2 ps-0 pe-0 ms-0 me-0" />
          <div className="d-flex align-items-center">
            <h1 className="text-md-normal m-0">Profession</h1>
            <h1
              className="text-md-normal m-0 ms-auto cursor-pointer color-neutral-old-80"
              onClick={onSeeAllProfessions}
            >
              See All
            </h1>
          </div>
          <div className="d-flex align-items-center flex-wrap mt-2">
            <div className="x-prof d-flex align-items-center">
              {professions?.map((item, idx) => {
                return (
                  <p
                    className="x-top-prof text-sm-light m-0 me-2 color-base-10"
                    key={idx}
                    style={{
                      backgroundColor: "var(--primary-main)",
                    }}
                  >
                    {item.job_title}
                  </p>
                );
              })}
            </div>
          </div>
        </div>
      }
      customFooter={
        <div className="d-flex x-footer2 align-items-center">
          <div className="ms-auto">
            <div className="d-flex align-items-center">
              <Button
                className="btn-cancel m-2 mb-0"
                onClick={onReset}
              >
                Reset
              </Button>
              <Button
                className="btn-primary m-2 mb-0"
                onClick={onSubmit}
              >
                Apply
              </Button>
            </div>
          </div>
        </div>
      }
    />
  );
};

export default ModalFilter;
