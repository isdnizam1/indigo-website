import { Enhance } from "/services/Enhancer";
import { formatDate } from "/services/utils/helper";
import { Button } from "react-bootstrap";
import { useState } from "react";
import ModalShare from "/components/modal/ModalShare";
export default function CardPostCollab({ data }) {
  const {
    id_ads,
    id_user,
    profile_picture,
    full_name,
    job_title,
    updated_at,
    city_name,
    topic_name,
    total_view,
    total_comment,
    title,
    image,
  } = data;

  const [dataShare, setDataShare] = useState(null);
  const [modalShareIsOpened, setModalShareIsOpened] = useState(false);

  const expandShare = () => {
    setModalShareIsOpened(modalShareIsOpened ? false : true);
  };

  const getDataShare = (item) => {
    setDataShare(item);
  };
  return (
    <div className="x-content-collab">
      {data && (
        <>
          <div className="detail-head">
            <div className="head-left">
              <div className="left-pic">
                {profile_picture ? (
                  <img src={profile_picture} alt="profile" />
                ) : (
                  <div className="x-empty-image">
                    <p
                      className="text-xl-normal m-0"
                      style={{ color: "var(--base-10)" }}
                    >
                      {full_name?.slice(0, 1).toUpperCase()}
                    </p>
                  </div>
                )}
              </div>
              <div className="left-profile">
                <h2 className="m-0 text-lg-normal line-height-sm">
                  {full_name}
                </h2>
                <h3
                  className="text-sm-normal m-0 line-height-sm"
                  style={{ color: "var(--neutral-60)" }}
                >
                  {job_title}
                </h3>
                <div className="d-flex align-items-center">
                  <p
                    className="text-sm-light m-0 line-height-sm"
                    style={{ color: "var(--neutral-60)" }}
                  >
                    {city_name}
                  </p>
                  <div className="x-dot" />
                  <p
                    className="text-sm-light m-0 line-height-sm"
                    style={{ color: "var(--neutral-60)" }}
                  >
                    {formatDate(updated_at)}
                  </p>
                </div>
              </div>
            </div>
            <div className="head-right">
              <Button onClick={() => {}}>Join</Button>
            </div>
          </div>
          <div className="detail-content">
            {title && (
              <p
                className="text-lg-normal mt-3"
                style={{ color: "var(--neutral-80)" }}
              >
                {title}
              </p>
            )}
            {image && (
              <div className="content-img">
                <img src={image} alt="image-content" />
              </div>
            )}
            <div className="content-tag">
              {topic_name?.length > 0 &&
                topic_name?.split(",").map((item, idx) => (
                  <Button
                    className="btn-topic me-3"
                    style={{ width: "max-content" }}
                    key={idx}
                  >
                    {item}
                  </Button>
                ))}
            </div>
          </div>
          <div className="x-footer-feeds">
            <div className="x-footer-feeds-input w-100  mx-3 my-1">
              <div className="d-flex justify-content-between w-100">
                <div className="d-flex">
                  <div className="d-flex align-items-center me-4">
                    <span
                      className="material-icons-round cursor-pointer user-select-none"
                      style={
                        false
                          ? { color: "var(--primary-main)" }
                          : { color: "var(--neutral-60)" }
                      }
                      onClick={() => {}}
                    >
                      visibility
                    </span>

                    <p
                      className="m-0 ms-1 text-lg-light"
                      style={
                        false
                          ? { color: "var(--primary-main)" }
                          : { color: "var(--neutral-60)" }
                      }
                    >
                      {total_view}
                    </p>
                  </div>
                  <div className="x-footer-comment" onClick={() => {}}>
                    <div className="d-flex align-items-center cursor-pointer">
                      <span
                        className="material-icons-round ms-0"
                        style={{ color: "var(--neutral-60)" }}
                      >
                        sms
                      </span>
                      <span
                        className="m-0 ms-1 heading-sm-light"
                        style={{ color: "var(--neutral-60)" }}
                      >
                        {total_comment}
                      </span>
                      <span
                        className="m-0 ms-1 heading-sm-light"
                        style={{ color: "var(--neutral-60)" }}
                      >
                        Comments
                      </span>
                    </div>
                  </div>
                </div>

                <div className="x-footer-reshare ms-auto">
                  <div className="d-flex align-items-center cursor-pointer">
                    <span
                      className="material-icons-round"
                      style={{ color: "var(--neutral-60)" }}
                      onClick={() => {
                        expandShare();
                        getDataShare(data);
                      }}
                    >
                      share
                    </span>
                    <p
                      className="m-0 ms-2 heading-sm-light"
                      style={{ color: "var(--neutral-60)" }}
                      onClick={() => {}}
                    >
                      Share
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <ModalShare
            show={modalShareIsOpened}
            onHide={() => setModalShareIsOpened(false)}
            dataShare={dataShare}
          />
        </>
      )}
    </div>
  );
}
