import { getEventList, postEventParticipant } from "/services/actions/api";
import { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { Enhance } from "/services/Enhancer";
import { getAuth, setAuth } from "/services/utils/helper";
import EventeerLogo from "/public/assets/icons/icon-eventeer.svg";
import classnames from "classnames";
import { useRouter } from "next/router";

const SideBar = (props) => {
  const router = useRouter();
  const [events, setEvent] = useState([]);
  const { apiDispatch } = props;
  const { id_user, email, registration_step, event_title } = getAuth();

  useEffect(() => {
    apiDispatch(getEventList, { id_user }).then((data) => {
      setEvent(data.filter((item) => item.user_joined === true));
    });
  }, [id_user, apiDispatch]);

  const onSubmit = (event) => {
    const { event_title, id_event } = event;

    apiDispatch(postEventParticipant, { id_event, id_user }, true).then(
      (response) => {
        if (response.code === 200) {
          setAuth({
            ...getAuth(),
            id_user,
            email,
            registration_step,
            event_title,
          }).then((auth) => window.location.reload());
        }
      }
    );
  }

  return (
    <div className="x-sidebar-wrapper">
      <div className="x-sidebar">
        <Button
          className="x-backhandler"
          onClick={() => router.push("/home/event-selection")}
        >
          <span className="material-icons-round">chevron_left</span>
        </Button>

        {events.map((event, idx) => {
          if(event.event_title === event_title) return (
            <div className="x-event active" key={idx}>
              {/* <img src={EventeerLogo} alt="" /> */}
              <p className="text-xl-normal m-0 pt-1" style={{color: "var(--primary-pressed)"}}>{event.event_title.slice(0,1)}</p>
            </div>
          );
        })}

        {events.map((event, idx) => {
          if(event.event_title !== event_title) return (
            <div className="x-event" key={idx} onClick={() => onSubmit(event)}>
              {/* <img src={EventeerLogo} alt="" /> */}
              <p className="text-xl-normal m-0 pt-1" style={{color: "var(--primary-pressed)"}}>{event.event_title.slice(0,1)}</p>
            </div>
          );
        })}

        {/* {events.map((event, idx) => (
          <div className={classnames("x-event", event.event_title === event_title && "active")} key={idx} onClick={() => onSubmit(event)}>
            <img src={EventeerLogo} alt="" />
          </div>
        ))} */}
      </div>
    </div>
  );
}

export default Enhance(SideBar);
