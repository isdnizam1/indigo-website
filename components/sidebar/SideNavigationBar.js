import { Fragment, useEffect } from "react";
import classnames from "classnames";
import {
  capitalizeFirstLetter,
  dateFormatter,
  getAuth,
  setAuth,
  apiDispatch,
} from "/services/utils/helper";
import { useState } from "react";
import ModalConfirmation from "../modal/ModalConfirmation";
import DefaultPicture from "/public/assets/icons/icon-default-picture.svg";
import {
  getEventList,
  getListTopics,
  postEventParticipant,
} from "/services/actions/api";
import { useRouter } from "next/router";
import { Loaders } from "/components/loaders/Loaders";
import { isEmpty } from "lodash-es";

const SideNavigationBar = () => {
  const { push, reload, pathname, query } = useRouter();
  const [modal, setModal] = useState(false);
  const [events, setEvent] = useState([]);
  const [topic, setTopic] = useState([]);
  const { id_user, email, registration_step, event_title, token } = getAuth();

  const [isLoading, setLoading] = useState(true);

  const menus = [
    { path: "/home", name: "Home", iconName: "home" },
    { path: "/participants", name: "Participants", iconName: "people" },
    { path: "/feeds", name: "Feeds", iconName: "feed" },
    { path: "/faq", name: "FAQ", iconName: "help" },
    { path: "/settings", name: "Settings", iconName: "settings" },
  ];

  useEffect(() => getTopics(), []);

  const getTopics = () => {
    apiDispatch(
      getListTopics,
      { show: true, id_user, view_form: "timeline" },
      true
    ).then((response) => {
      setLoading(false);
      response.code === 200 && setTopic(response.result);
    });
  };

  const getEvents = () => {
    apiDispatch(getEventList, { id_user }).then((data) => {
      setEvent(data.filter((item) => item.user_joined === true));
    });
  };

  const onSubmit = (event) => {
    const { event_title, id_event } = event;
    const auth = getAuth();

    event_title !== auth.event_title &&
      apiDispatch(postEventParticipant, { id_event, id_user }, true).then(
        (response) => {
          if (response.code === 200) {
            setAuth({
              ...getAuth(),
              id_user,
              email,
              registration_step,
              event_title,
              token,
            }).then((auth) => {
              reload();
              push("/home");
            });
          }
        }
      );
  };

  return (
    <div className="x-sidenavbar-wrapper">
      <div className="x-sidebar-content">
        <div className="x-event-action">
          <div
            className="x-selected-event"
            onClick={() => {
              isEmpty(events) && getEvents();
              setModal(true);
            }}
          >
            <p className="text-md-normal m-0 color-base-10">Selected Event</p>
            <p className="text-lg-bold m-0 line-height-md color-base-10">
              {event_title}
            </p>
          </div>

          <div className="x-add-event" onClick={() => push("/event-selection")}>
            <span className="material-icons-round">add_circle</span>
          </div>
        </div>

        <div className="x-navbar">
          {menus.map((menu, idx) => (
            <div className="x-navbar-menu" key={idx}>
              <div
                className={classnames(
                  "d-flex align-items-center cursor-pointer w-100",
                  pathname.includes(menu.path) && "active"
                )}
                onClick={() => {
                  const disabledPath = ["/faq", "/settings"];
                  const { path } = menu;
                  !disabledPath.includes(path) && push(path);
                }}
              >
                <span className="material-icons-round">{menu.iconName}</span>
                <p className="m-0 ps-2 text-lg-bold">{menu.name}</p>
              </div>
              {menu.path === "/feeds" && (
                <Loaders.Elements
                  width="70%"
                  height={20}
                  Column
                  count={3}
                  className="my-2 ms-4"
                  isLoading={isLoading}
                >
                  <Fragment>
                    <ul className="x-navbar-submenu">
                      <li
                        className={classnames(
                          "list-unstyled pt-1 text-lg-light cursor-pointer color-neutral-650",
                          !query?.id && pathname.includes("/feeds") && "active"
                        )}
                        onClick={() => push("/feeds")}
                      >
                        All Posts
                      </li>
                    </ul>
                    {topic.map((item, idx) => {
                      if (item.topic_name)
                        return (
                          <ul className="x-navbar-submenu" key={idx}>
                            <li
                              className={classnames(
                                "list-unstyled pt-1 text-lg-light cursor-pointer color-neutral-650",
                                item.id_topic === query?.id &&
                                  pathname.includes("/feeds") &&
                                  "active"
                              )}
                              onClick={() =>
                                push({
                                  pathname: "/feeds",
                                  query: { id: item.id_topic },
                                })
                              }
                            >
                              {item.topic_name}
                            </li>
                          </ul>
                        );
                    })}
                  </Fragment>
                </Loaders.Elements>
              )}
            </div>
          ))}
        </div>
      </div>

      <ModalConfirmation
        show={modal}
        onHide={() => setModal(false)}
        title="Select event?"
        subTitle="Choose an event that you have participated in"
        customExitIcon={
          <span className="material-icons-round font-weight-bolder color-neutral-70">
            clear
          </span>
        }
        customBody={
          <Fragment>
            {events.map((event, idx) => (
              <div
                className="x-modalevent-card"
                key={idx}
                onClick={() => onSubmit(event)}
              >
                <div className="d-flex">
                  <img className="x-event-image" src={event.image} alt="" />

                  <div className="ps-2 d-flex flex-column justify-content-center">
                    <p className="text-lg-bold m-0 text-start line-height-sm color-neutral-70">
                      {capitalizeFirstLetter(event.event_title)}
                    </p>

                    <p className="text-sm-normal m-0 text-start color-neutral-old-40">
                      {capitalizeFirstLetter(event.event_location)}
                      <span
                        className="material-icons-round ps-1 pe-1 color-neutral-old-40"
                        style={{ fontSize: "6px" }}
                      >
                        fiber_manual_record
                      </span>
                      {dateFormatter(event.start_date)}
                      {" - "}
                      {dateFormatter(event.end_date)}
                    </p>
                    <div className="d-flex align-items-center pt-1">
                      <img
                        className="x-vendor-image"
                        src={
                          !event.vendor_image
                            ? event.vendor_image
                            : DefaultPicture
                        }
                        alt=""
                      />
                      <p className="text-sm-normal m-0 color-neutral-old-40">
                        {event.vendor_name}
                      </p>
                    </div>
                  </div>
                </div>

                <div
                  className={classnames(
                    "x-radio-icon",
                    event.event_title === event_title && "active"
                  )}
                />
              </div>
            ))}
          </Fragment>
        }
      />
    </div>
  );
};

export default SideNavigationBar;
