import React from "react";
import { Container, Button } from "react-bootstrap";
import InputPassword from "/components/input/InputPassword";
import InputText from "/components/input/InputText";
import { useRouter } from "next/router";

const LoginForm = ({
  onSubmit,
  onEmailInput,
  onPasswordInput,
  isButtonDisable,
  emailIsError,
  passwordIsError,
}) => {
  const { push } = useRouter();

  return (
    <div className="x-loginform-component">
      <Container className="d-flex justify-content-center w-100 h-100">
        <form onSubmit={onSubmit} className="x-card-form">
          <h1>Welcome back!</h1>
          <p>Log in to Connect, Interact & Collaborate</p>

          <InputText
            title="Email"
            type="email"
            onChangeText={onEmailInput}
            placeholder="Masukkan email"
            errorSign="Email belum terdaftar, silahkan melakukan"
            extraErrorSignLink="registrasi"
            linkTo="/register"
            isError={emailIsError}
          />

          <InputPassword
            title="Password"
            onChangeText={onPasswordInput}
            placeholder="Masukkan password"
            errorSign="Password tidak sesuai, silahkan coba kembali"
            isError={passwordIsError}
          />

          <div className="x-password-recovery">
            <p
              onClick={() => push("/forgot-password")}
              className="lg m-0 text-md-bold cursor-pointer user-select-none"
            >
              Lupa Password?
            </p>
          </div>

          <Button
            className="btn-primary mt-2"
            type="submit"
            disabled={isButtonDisable}
          >
            Masuk
          </Button>
        </form>
      </Container>
    </div>
  );
};

export default LoginForm;
